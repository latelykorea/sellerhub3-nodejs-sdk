/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.SellerhubApi);
  }
}(this, function(expect, SellerhubApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new SellerhubApi.DeliveryResource();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DeliveryResource', function() {
    it('should create an instance of DeliveryResource', function() {
      // uncomment below and update the code to test DeliveryResource
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be.a(SellerhubApi.DeliveryResource);
    });

    it('should have the property deliveryNumber (base name: "deliveryNumber")', function() {
      // uncomment below and update the code to test the property deliveryNumber
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

    it('should have the property deliveryCompany (base name: "deliveryCompany")', function() {
      // uncomment below and update the code to test the property deliveryCompany
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

    it('should have the property deliveryUrl (base name: "deliveryUrl")', function() {
      // uncomment below and update the code to test the property deliveryUrl
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

    it('should have the property useYn (base name: "useYn")', function() {
      // uncomment below and update the code to test the property useYn
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

    it('should have the property courier (base name: "courier")', function() {
      // uncomment below and update the code to test the property courier
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

    it('should have the property trackerId (base name: "trackerId")', function() {
      // uncomment below and update the code to test the property trackerId
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

    it('should have the property validLength (base name: "validLength")', function() {
      // uncomment below and update the code to test the property validLength
      //var instance = new SellerhubApi.DeliveryResource();
      //expect(instance).to.be();
    });

  });

}));
