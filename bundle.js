(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.sellerhub3 = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (Buffer){(function (){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _superagent = _interopRequireDefault(require("superagent"));

var _querystring = _interopRequireDefault(require("querystring"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* @module ApiClient
* @version 0.0.1
*/

/**
* Manages low level client-server communications, parameter marshalling, etc. There should not be any need for an
* application to use this class directly - the *Api and model classes provide the public API for the service. The
* contents of this file should be regarded as internal but are documented for completeness.
* @alias module:ApiClient
* @class
*/
var ApiClient = /*#__PURE__*/function () {
  /**
   * The base URL against which to resolve every API call's (relative) path.
   * Overrides the default value set in spec file if present
   * @param {String} basePath
   */
  function ApiClient() {
    var basePath = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'http://localhost';

    _classCallCheck(this, ApiClient);

    /**
     * The base URL against which to resolve every API call's (relative) path.
     * @type {String}
     * @default http://localhost
     */
    this.basePath = basePath.replace(/\/+$/, '');
    /**
     * The authentication methods to be included for all API calls.
     * @type {Array.<String>}
     */

    this.authentications = {
      'oauth2': {
        type: 'bearer'
      },
      // JWT
      'sanctum': {
        type: 'apiKey',
        'in': 'header',
        name: 'Authorization'
      }
    };
    /**
     * The default HTTP headers to be included for all API calls.
     * @type {Array.<String>}
     * @default {}
     */

    this.defaultHeaders = {
      'User-Agent': 'OpenAPI-Generator/0.0.1/Javascript'
    };
    /**
     * The default HTTP timeout for all API calls.
     * @type {Number}
     * @default 60000
     */

    this.timeout = 60000;
    /**
     * If set to false an additional timestamp parameter is added to all API GET calls to
     * prevent browser caching
     * @type {Boolean}
     * @default true
     */

    this.cache = true;
    /**
     * If set to true, the client will save the cookies from each server
     * response, and return them in the next request.
     * @default false
     */

    this.enableCookies = false;
    /*
     * Used to save and return cookies in a node.js (non-browser) setting,
     * if this.enableCookies is set to true.
     */

    if (typeof window === 'undefined') {
      this.agent = new _superagent["default"].agent();
    }
    /*
     * Allow user to override superagent agent
     */


    this.requestAgent = null;
    /*
     * Allow user to add superagent plugins
     */

    this.plugins = null;
  }
  /**
  * Returns a string representation for an actual parameter.
  * @param param The actual parameter.
  * @returns {String} The string representation of <code>param</code>.
  */


  _createClass(ApiClient, [{
    key: "paramToString",
    value: function paramToString(param) {
      if (param == undefined || param == null) {
        return '';
      }

      if (param instanceof Date) {
        return param.toJSON();
      }

      if (ApiClient.canBeJsonified(param)) {
        return JSON.stringify(param);
      }

      return param.toString();
    }
    /**
    * Returns a boolean indicating if the parameter could be JSON.stringified
    * @param param The actual parameter
    * @returns {Boolean} Flag indicating if <code>param</code> can be JSON.stringified
    */

  }, {
    key: "buildUrl",
    value:
    /**
     * Builds full URL by appending the given path to the base URL and replacing path parameter place-holders with parameter values.
     * NOTE: query parameters are not handled here.
     * @param {String} path The path to append to the base URL.
     * @param {Object} pathParams The parameter values to append.
     * @param {String} apiBasePath Base path defined in the path, operation level to override the default one
     * @returns {String} The encoded path with parameter values substituted.
     */
    function buildUrl(path, pathParams, apiBasePath) {
      var _this = this;

      if (!path.match(/^\//)) {
        path = '/' + path;
      }

      var url = this.basePath + path; // use API (operation, path) base path if defined

      if (apiBasePath !== null && apiBasePath !== undefined) {
        url = apiBasePath + path;
      }

      url = url.replace(/\{([\w-\.]+)\}/g, function (fullMatch, key) {
        var value;

        if (pathParams.hasOwnProperty(key)) {
          value = _this.paramToString(pathParams[key]);
        } else {
          value = fullMatch;
        }

        return encodeURIComponent(value);
      });
      return url;
    }
    /**
    * Checks whether the given content type represents JSON.<br>
    * JSON content type examples:<br>
    * <ul>
    * <li>application/json</li>
    * <li>application/json; charset=UTF8</li>
    * <li>APPLICATION/JSON</li>
    * </ul>
    * @param {String} contentType The MIME content type to check.
    * @returns {Boolean} <code>true</code> if <code>contentType</code> represents JSON, otherwise <code>false</code>.
    */

  }, {
    key: "isJsonMime",
    value: function isJsonMime(contentType) {
      return Boolean(contentType != null && contentType.match(/^application\/json(;.*)?$/i));
    }
    /**
    * Chooses a content type from the given array, with JSON preferred; i.e. return JSON if included, otherwise return the first.
    * @param {Array.<String>} contentTypes
    * @returns {String} The chosen content type, preferring JSON.
    */

  }, {
    key: "jsonPreferredMime",
    value: function jsonPreferredMime(contentTypes) {
      for (var i = 0; i < contentTypes.length; i++) {
        if (this.isJsonMime(contentTypes[i])) {
          return contentTypes[i];
        }
      }

      return contentTypes[0];
    }
    /**
    * Checks whether the given parameter value represents file-like content.
    * @param param The parameter to check.
    * @returns {Boolean} <code>true</code> if <code>param</code> represents a file.
    */

  }, {
    key: "isFileParam",
    value: function isFileParam(param) {
      // fs.ReadStream in Node.js and Electron (but not in runtime like browserify)
      if (typeof require === 'function') {
        var fs;

        try {
          fs = require('fs');
        } catch (err) {}

        if (fs && fs.ReadStream && param instanceof fs.ReadStream) {
          return true;
        }
      } // Buffer in Node.js


      if (typeof Buffer === 'function' && param instanceof Buffer) {
        return true;
      } // Blob in browser


      if (typeof Blob === 'function' && param instanceof Blob) {
        return true;
      } // File in browser (it seems File object is also instance of Blob, but keep this for safe)


      if (typeof File === 'function' && param instanceof File) {
        return true;
      }

      return false;
    }
    /**
    * Normalizes parameter values:
    * <ul>
    * <li>remove nils</li>
    * <li>keep files and arrays</li>
    * <li>format to string with `paramToString` for other cases</li>
    * </ul>
    * @param {Object.<String, Object>} params The parameters as object properties.
    * @returns {Object.<String, Object>} normalized parameters.
    */

  }, {
    key: "normalizeParams",
    value: function normalizeParams(params) {
      var newParams = {};

      for (var key in params) {
        if (params.hasOwnProperty(key) && params[key] != undefined && params[key] != null) {
          var value = params[key];

          if (this.isFileParam(value) || Array.isArray(value)) {
            newParams[key] = value;
          } else {
            newParams[key] = this.paramToString(value);
          }
        }
      }

      return newParams;
    }
    /**
    * Builds a string representation of an array-type actual parameter, according to the given collection format.
    * @param {Array} param An array parameter.
    * @param {module:ApiClient.CollectionFormatEnum} collectionFormat The array element separator strategy.
    * @returns {String|Array} A string representation of the supplied collection, using the specified delimiter. Returns
    * <code>param</code> as is if <code>collectionFormat</code> is <code>multi</code>.
    */

  }, {
    key: "buildCollectionParam",
    value: function buildCollectionParam(param, collectionFormat) {
      if (param == null) {
        return null;
      }

      switch (collectionFormat) {
        case 'csv':
          return param.map(this.paramToString, this).join(',');

        case 'ssv':
          return param.map(this.paramToString, this).join(' ');

        case 'tsv':
          return param.map(this.paramToString, this).join('\t');

        case 'pipes':
          return param.map(this.paramToString, this).join('|');

        case 'multi':
          //return the array directly as SuperAgent will handle it as expected
          return param.map(this.paramToString, this);

        case 'passthrough':
          return param;

        default:
          throw new Error('Unknown collection format: ' + collectionFormat);
      }
    }
    /**
    * Applies authentication headers to the request.
    * @param {Object} request The request object created by a <code>superagent()</code> call.
    * @param {Array.<String>} authNames An array of authentication method names.
    */

  }, {
    key: "applyAuthToRequest",
    value: function applyAuthToRequest(request, authNames) {
      var _this2 = this;

      authNames.forEach(function (authName) {
        var auth = _this2.authentications[authName];

        switch (auth.type) {
          case 'basic':
            if (auth.username || auth.password) {
              request.auth(auth.username || '', auth.password || '');
            }

            break;

          case 'bearer':
            if (auth.accessToken) {
              var localVarBearerToken = typeof auth.accessToken === 'function' ? auth.accessToken() : auth.accessToken;
              request.set({
                'Authorization': 'Bearer ' + localVarBearerToken
              });
            }

            break;

          case 'apiKey':
            if (auth.apiKey) {
              var data = {};

              if (auth.apiKeyPrefix) {
                data[auth.name] = auth.apiKeyPrefix + ' ' + auth.apiKey;
              } else {
                data[auth.name] = auth.apiKey;
              }

              if (auth['in'] === 'header') {
                request.set(data);
              } else {
                request.query(data);
              }
            }

            break;

          case 'oauth2':
            if (auth.accessToken) {
              request.set({
                'Authorization': 'Bearer ' + auth.accessToken
              });
            }

            break;

          default:
            throw new Error('Unknown authentication type: ' + auth.type);
        }
      });
    }
    /**
     * Deserializes an HTTP response body into a value of the specified type.
     * @param {Object} response A SuperAgent response object.
     * @param {(String|Array.<String>|Object.<String, Object>|Function)} returnType The type to return. Pass a string for simple types
     * or the constructor function for a complex type. Pass an array containing the type name to return an array of that type. To
     * return an object, pass an object with one property whose name is the key type and whose value is the corresponding value type:
     * all properties on <code>data<code> will be converted to this type.
     * @returns A value of the specified type.
     */

  }, {
    key: "deserialize",
    value: function deserialize(response, returnType) {
      if (response == null || returnType == null || response.status == 204) {
        return null;
      } // Rely on SuperAgent for parsing response body.
      // See http://visionmedia.github.io/superagent/#parsing-response-bodies


      var data = response.body;

      if (data == null || _typeof(data) === 'object' && typeof data.length === 'undefined' && !Object.keys(data).length) {
        // SuperAgent does not always produce a body; use the unparsed response as a fallback
        data = response.text;
      }

      return ApiClient.convertToType(data, returnType);
    }
    /**
     * Callback function to receive the result of the operation.
     * @callback module:ApiClient~callApiCallback
     * @param {String} error Error message, if any.
     * @param data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Invokes the REST service using the supplied settings and parameters.
     * @param {String} path The base URL to invoke.
     * @param {String} httpMethod The HTTP method to use.
     * @param {Object.<String, String>} pathParams A map of path parameters and their values.
     * @param {Object.<String, Object>} queryParams A map of query parameters and their values.
     * @param {Object.<String, Object>} headerParams A map of header parameters and their values.
     * @param {Object.<String, Object>} formParams A map of form parameters and their values.
     * @param {Object} bodyParam The value to pass as the request body.
     * @param {Array.<String>} authNames An array of authentication type names.
     * @param {Array.<String>} contentTypes An array of request MIME types.
     * @param {Array.<String>} accepts An array of acceptable response MIME types.
     * @param {(String|Array|ObjectFunction)} returnType The required type to return; can be a string for simple types or the
     * constructor for a complex type.
     * @param {String} apiBasePath base path defined in the operation/path level to override the default one
     * @param {module:ApiClient~callApiCallback} callback The callback function.
     * @returns {Object} The SuperAgent request object.
     */

  }, {
    key: "callApi",
    value: function callApi(path, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, accepts, returnType, apiBasePath, callback) {
      var _this3 = this;

      var url = this.buildUrl(path, pathParams, apiBasePath);
      var request = (0, _superagent["default"])(httpMethod, url);

      if (this.plugins !== null) {
        for (var index in this.plugins) {
          if (this.plugins.hasOwnProperty(index)) {
            request.use(this.plugins[index]);
          }
        }
      } // apply authentications


      this.applyAuthToRequest(request, authNames); // set query parameters

      if (httpMethod.toUpperCase() === 'GET' && this.cache === false) {
        queryParams['_'] = new Date().getTime();
      }

      request.query(this.normalizeParams(queryParams)); // set header parameters

      request.set(this.defaultHeaders).set(this.normalizeParams(headerParams)); // set requestAgent if it is set by user

      if (this.requestAgent) {
        request.agent(this.requestAgent);
      } // set request timeout


      request.timeout(this.timeout);
      var contentType = this.jsonPreferredMime(contentTypes);

      if (contentType) {
        // Issue with superagent and multipart/form-data (https://github.com/visionmedia/superagent/issues/746)
        if (contentType != 'multipart/form-data') {
          request.type(contentType);
        }
      }

      if (contentType === 'application/x-www-form-urlencoded') {
        request.send(_querystring["default"].stringify(this.normalizeParams(formParams)));
      } else if (contentType == 'multipart/form-data') {
        var _formParams = this.normalizeParams(formParams);

        for (var key in _formParams) {
          if (_formParams.hasOwnProperty(key)) {
            var _formParamsValue = _formParams[key];

            if (this.isFileParam(_formParamsValue)) {
              // file field
              request.attach(key, _formParamsValue);
            } else if (Array.isArray(_formParamsValue) && _formParamsValue.length && this.isFileParam(_formParamsValue[0])) {
              // multiple files
              _formParamsValue.forEach(function (file) {
                return request.attach(key, file);
              });
            } else {
              request.field(key, _formParamsValue);
            }
          }
        }
      } else if (bodyParam !== null && bodyParam !== undefined) {
        if (!request.header['Content-Type']) {
          request.type('application/json');
        }

        request.send(bodyParam);
      }

      var accept = this.jsonPreferredMime(accepts);

      if (accept) {
        request.accept(accept);
      }

      if (returnType === 'Blob') {
        request.responseType('blob');
      } else if (returnType === 'String') {
        request.responseType('text');
      } // Attach previously saved cookies, if enabled


      if (this.enableCookies) {
        if (typeof window === 'undefined') {
          this.agent._attachCookies(request);
        } else {
          request.withCredentials();
        }
      }

      request.end(function (error, response) {
        if (callback) {
          var data = null;

          if (!error) {
            try {
              data = _this3.deserialize(response, returnType);

              if (_this3.enableCookies && typeof window === 'undefined') {
                _this3.agent._saveCookies(response);
              }
            } catch (err) {
              error = err;
            }
          }

          callback(error, data, response);
        }
      });
      return request;
    }
    /**
    * Parses an ISO-8601 string representation or epoch representation of a date value.
    * @param {String} str The date value as a string.
    * @returns {Date} The parsed date object.
    */

  }, {
    key: "hostSettings",
    value:
    /**
      * Gets an array of host settings
      * @returns An array of host settings
      */
    function hostSettings() {
      return [{
        'url': "",
        'description': "No description provided"
      }];
    }
  }, {
    key: "getBasePathFromSettings",
    value: function getBasePathFromSettings(index) {
      var variables = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var servers = this.hostSettings(); // check array index out of bound

      if (index < 0 || index >= servers.length) {
        throw new Error("Invalid index " + index + " when selecting the host settings. Must be less than " + servers.length);
      }

      var server = servers[index];
      var url = server['url']; // go through variable and assign a value

      for (var variable_name in server['variables']) {
        if (variable_name in variables) {
          var variable = server['variables'][variable_name];

          if (!('enum_values' in variable) || variable['enum_values'].includes(variables[variable_name])) {
            url = url.replace("{" + variable_name + "}", variables[variable_name]);
          } else {
            throw new Error("The variable `" + variable_name + "` in the host URL has invalid value " + variables[variable_name] + ". Must be " + server['variables'][variable_name]['enum_values'] + ".");
          }
        } else {
          // use default value
          url = url.replace("{" + variable_name + "}", server['variables'][variable_name]['default_value']);
        }
      }

      return url;
    }
    /**
    * Constructs a new map or array model from REST data.
    * @param data {Object|Array} The REST data.
    * @param obj {Object|Array} The target object or array.
    */

  }], [{
    key: "canBeJsonified",
    value: function canBeJsonified(str) {
      if (typeof str !== 'string' && _typeof(str) !== 'object') return false;

      try {
        var type = str.toString();
        return type === '[object Object]' || type === '[object Array]';
      } catch (err) {
        return false;
      }
    }
  }, {
    key: "parseDate",
    value: function parseDate(str) {
      if (isNaN(str)) {
        return new Date(str.replace(/(\d)(T)(\d)/i, '$1 $3'));
      }

      return new Date(+str);
    }
    /**
    * Converts a value to the specified type.
    * @param {(String|Object)} data The data to convert, as a string or object.
    * @param {(String|Array.<String>|Object.<String, Object>|Function)} type The type to return. Pass a string for simple types
    * or the constructor function for a complex type. Pass an array containing the type name to return an array of that type. To
    * return an object, pass an object with one property whose name is the key type and whose value is the corresponding value type:
    * all properties on <code>data<code> will be converted to this type.
    * @returns An instance of the specified type or null or undefined if data is null or undefined.
    */

  }, {
    key: "convertToType",
    value: function convertToType(data, type) {
      if (data === null || data === undefined) return data;

      switch (type) {
        case 'Boolean':
          return Boolean(data);

        case 'Integer':
          return parseInt(data, 10);

        case 'Number':
          return parseFloat(data);

        case 'String':
          return String(data);

        case 'Date':
          return ApiClient.parseDate(String(data));

        case 'Blob':
          return data;

        default:
          if (type === Object) {
            // generic object, return directly
            return data;
          } else if (typeof type.constructFromObject === 'function') {
            // for model type like User and enum class
            return type.constructFromObject(data);
          } else if (Array.isArray(type)) {
            // for array type like: ['String']
            var itemType = type[0];
            return data.map(function (item) {
              return ApiClient.convertToType(item, itemType);
            });
          } else if (_typeof(type) === 'object') {
            // for plain object type like: {'String': 'Integer'}
            var keyType, valueType;

            for (var k in type) {
              if (type.hasOwnProperty(k)) {
                keyType = k;
                valueType = type[k];
                break;
              }
            }

            var result = {};

            for (var k in data) {
              if (data.hasOwnProperty(k)) {
                var key = ApiClient.convertToType(k, keyType);
                var value = ApiClient.convertToType(data[k], valueType);
                result[key] = value;
              }
            }

            return result;
          } else {
            // for unknown type, return the data directly
            return data;
          }

      }
    }
  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj, itemType) {
      if (Array.isArray(data)) {
        for (var i = 0; i < data.length; i++) {
          if (data.hasOwnProperty(i)) obj[i] = ApiClient.convertToType(data[i], itemType);
        }
      } else {
        for (var k in data) {
          if (data.hasOwnProperty(k)) obj[k] = ApiClient.convertToType(data[k], itemType);
        }
      }
    }
  }]);

  return ApiClient;
}();
/**
 * Enumeration of collection format separator strategies.
 * @enum {String}
 * @readonly
 */


ApiClient.CollectionFormatEnum = {
  /**
   * Comma-separated values. Value: <code>csv</code>
   * @const
   */
  CSV: ',',

  /**
   * Space-separated values. Value: <code>ssv</code>
   * @const
   */
  SSV: ' ',

  /**
   * Tab-separated values. Value: <code>tsv</code>
   * @const
   */
  TSV: '\t',

  /**
   * Pipe(|)-separated values. Value: <code>pipes</code>
   * @const
   */
  PIPES: '|',

  /**
   * Native array. Value: <code>multi</code>
   * @const
   */
  MULTI: 'multi'
};
/**
* The default API client implementation.
* @type {module:ApiClient}
*/

ApiClient.instance = new ApiClient();
var _default = ApiClient;
exports["default"] = _default;
}).call(this)}).call(this,require("buffer").Buffer)
},{"buffer":76,"fs":75,"querystring":80,"superagent":69}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Address service.
* @module api/AddressApi
* @version 0.0.1
*/
var AddressApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AddressApi. 
  * @alias module:api/AddressApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AddressApi(apiClient) {
    _classCallCheck(this, AddressApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getAddress operation.
   * @callback module:api/AddressApi~getAddressCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 주소조회
   * 주소조회
   * @param {String} keyword 검색어
   * @param {Object} opts Optional parameters
   * @param {Number} opts.currentPage 현재 페이지
   * @param {Number} opts.countPerPage 페이지당 검색
   * @param {module:model/String} opts.hstryYn 변동된 주소정보 포함 여부 (default to 'N')
   * @param {module:model/String} opts.firstSort 정확도순 정렬 (default to 'none')
   * @param {module:model/String} opts.addInfoYn 출력결과에 추가된 항목 (default to 'N')
   * @param {module:api/AddressApi~getAddressCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(AddressApi, [{
    key: "getAddress",
    value: function getAddress(keyword, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'keyword' is set

      if (keyword === undefined || keyword === null) {
        throw new Error("Missing the required parameter 'keyword' when calling getAddress");
      }

      var pathParams = {};
      var queryParams = {
        'currentPage': opts['currentPage'],
        'countPerPage': opts['countPerPage'],
        'keyword': keyword,
        'hstryYn': opts['hstryYn'],
        'firstSort': opts['firstSort'],
        'addInfoYn': opts['addInfoYn']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/address/search', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AddressApi;
}();

exports["default"] = AddressApi;
},{"../ApiClient":1}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Alarm service.
* @module api/AlarmApi
* @version 0.0.1
*/
var AlarmApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AlarmApi. 
  * @alias module:api/AlarmApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AlarmApi(apiClient) {
    _classCallCheck(this, AlarmApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getAlarm operation.
   * @callback module:api/AlarmApi~getAlarmCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 알람 목록 조회
   * 알람 목록 조회
   * @param {Object} opts Optional parameters
   * @param {Date} opts.regDateTime 보낸시간
   * @param {String} opts.alarmType1 알람 메인타입
   * @param {String} opts.alarmType2 알람 타입
   * @param {Date} opts.startDate 시작 시간
   * @param {Date} opts.endDate 종료 시간
   * @param {String} opts.keyword 검색어
   * @param {module:model/String} opts.isRead 읽음 여부 (default to '')
   * @param {module:api/AlarmApi~getAlarmCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(AlarmApi, [{
    key: "getAlarm",
    value: function getAlarm(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'regDateTime': opts['regDateTime'],
        'alarmType1': opts['alarmType1'],
        'alarmType2': opts['alarmType2'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'keyword': opts['keyword'],
        'isRead': opts['isRead']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/alarm', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getAlarmByNo operation.
     * @callback module:api/AlarmApi~getAlarmByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 알람읽기
     * @param {Number} alarmNumber 알람 번호
     * @param {module:api/AlarmApi~getAlarmByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getAlarmByNo",
    value: function getAlarmByNo(alarmNumber, callback) {
      var postBody = null; // verify the required parameter 'alarmNumber' is set

      if (alarmNumber === undefined || alarmNumber === null) {
        throw new Error("Missing the required parameter 'alarmNumber' when calling getAlarmByNo");
      }

      var pathParams = {
        'alarmNumber': alarmNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/alarm/read/{alarmNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AlarmApi;
}();

exports["default"] = AlarmApi;
},{"../ApiClient":1}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _AttemptMobileAuthRequest = _interopRequireDefault(require("../model/AttemptMobileAuthRequest"));

var _ConfirmMobileAuthRequest = _interopRequireDefault(require("../model/ConfirmMobileAuthRequest"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Authentication service.
* @module api/AuthenticationApi
* @version 0.0.1
*/
var AuthenticationApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AuthenticationApi. 
  * @alias module:api/AuthenticationApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AuthenticationApi(apiClient) {
    _classCallCheck(this, AuthenticationApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the attemptMobileAuth operation.
   * @callback module:api/AuthenticationApi~attemptMobileAuthCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 휴대폰 인증 번호 요청
   * 휴대폰 인증 번호 요청
   * @param {module:model/AttemptMobileAuthRequest} AttemptMobileAuthRequest 
   * @param {module:api/AuthenticationApi~attemptMobileAuthCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(AuthenticationApi, [{
    key: "attemptMobileAuth",
    value: function attemptMobileAuth(AttemptMobileAuthRequest, callback) {
      var postBody = AttemptMobileAuthRequest; // verify the required parameter 'AttemptMobileAuthRequest' is set

      if (AttemptMobileAuthRequest === undefined || AttemptMobileAuthRequest === null) {
        throw new Error("Missing the required parameter 'AttemptMobileAuthRequest' when calling attemptMobileAuth");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/mobileAuth/attempt', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the confirmMobileAuth operation.
     * @callback module:api/AuthenticationApi~confirmMobileAuthCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴대폰 인증 번호 검증
     * 휴대폰 인증 번호 검증
     * @param {module:model/ConfirmMobileAuthRequest} ConfirmMobileAuthRequest 
     * @param {module:api/AuthenticationApi~confirmMobileAuthCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "confirmMobileAuth",
    value: function confirmMobileAuth(ConfirmMobileAuthRequest, callback) {
      var postBody = ConfirmMobileAuthRequest; // verify the required parameter 'ConfirmMobileAuthRequest' is set

      if (ConfirmMobileAuthRequest === undefined || ConfirmMobileAuthRequest === null) {
        throw new Error("Missing the required parameter 'ConfirmMobileAuthRequest' when calling confirmMobileAuth");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/mobileAuth/confirm', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AuthenticationApi;
}();

exports["default"] = AuthenticationApi;
},{"../ApiClient":1,"../model/AttemptMobileAuthRequest":40,"../model/ConfirmMobileAuthRequest":44}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _BoardManual = _interopRequireDefault(require("../model/BoardManual"));

var _BoardNotice = _interopRequireDefault(require("../model/BoardNotice"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Board service.
* @module api/BoardApi
* @version 0.0.1
*/
var BoardApi = /*#__PURE__*/function () {
  /**
  * Constructs a new BoardApi. 
  * @alias module:api/BoardApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function BoardApi(apiClient) {
    _classCallCheck(this, BoardApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createBoard operation.
   * @callback module:api/BoardApi~createBoardCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 판매자지원 게시물 등록
   * @param {Object} subject 제목
   * @param {Object} contents 내용
   * @param {Object} opts Optional parameters
   * @param {module:model/String} opts.category 구분값 카테고리
   * @param {module:model/String} opts.secret 비밀글 여부
   * @param {module:api/BoardApi~createBoardCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(BoardApi, [{
    key: "createBoard",
    value: function createBoard(subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'subject' is set

      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling createBoard");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createBoard");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createBoardReply operation.
     * @callback module:api/BoardApi~createBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 등록
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/BoardApi~createBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createBoardReply",
    value: function createBoardReply(boardNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling createBoardReply");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling createBoardReply");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createBoardReply");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}/reply', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteBoardByNo operation.
     * @callback module:api/BoardApi~deleteBoardByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 삭제
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 판매자지원 게시물 번호
     * @param {module:api/BoardApi~deleteBoardByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteBoardByNo",
    value: function deleteBoardByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteBoardByNo");
      }

      var pathParams = {
        'boardNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteBoardReplyByNo operation.
     * @callback module:api/BoardApi~deleteBoardReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 삭제
     * @param {Number} replyNumber 판매자지원 답변 번호
     * @param {module:api/BoardApi~deleteBoardReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteBoardReplyByNo",
    value: function deleteBoardReplyByNo(replyNumber, callback) {
      var postBody = null; // verify the required parameter 'replyNumber' is set

      if (replyNumber === undefined || replyNumber === null) {
        throw new Error("Missing the required parameter 'replyNumber' when calling deleteBoardReplyByNo");
      }

      var pathParams = {
        'replyNumber': replyNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/reply/{replyNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getBoard operation.
     * @callback module:api/BoardApi~getBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 판매자지원 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Date} opts.startDate 시작일
     * @param {Date} opts.endDate 종료일
     * @param {module:model/String} opts.sortBy 정렬 (default to 'none')
     * @param {module:model/String} opts.sortDirection 정렬 방향 (default to 'desc')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/BoardApi~getBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getBoard",
    value: function getBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getBoardReply operation.
     * @callback module:api/BoardApi~getBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 조회
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/BoardApi~getBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getBoardReply",
    value: function getBoardReply(boardNumber, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling getBoardReply");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}/reply', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getFaqBoard operation.
     * @callback module:api/BoardApi~getFaqBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * FAQ 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Date} opts.startDate 시작일
     * @param {Date} opts.endDate 종료일
     * @param {module:model/String} opts.sortBy 정렬 (default to 'none')
     * @param {module:model/String} opts.sortDirection 정렬 방향 (default to 'desc')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/BoardApi~getFaqBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getFaqBoard",
    value: function getFaqBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/faq', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getManualBoard operation.
     * @callback module:api/BoardApi~getManualBoardCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/BoardManual>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 매뉴얼 목록
     * @param {Object} opts Optional parameters
     * @param {Date} opts.startDate 등록 시작일
     * @param {Date} opts.endDate 등록 종료일
     * @param {module:model/String} opts.searchType 검색 타입
     * @param {String} opts.searchContents 검색 내용
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/BoardApi~getManualBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/BoardManual>}
     */

  }, {
    key: "getManualBoard",
    value: function getManualBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchType': opts['searchType'],
        'searchContents': opts['searchContents'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_BoardManual["default"]];
      return this.apiClient.callApi('/shapi/v1/board/manuals', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getManualBoardByNo operation.
     * @callback module:api/BoardApi~getManualBoardByNoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/BoardManual} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 매뉴얼
     * @param {Number} manualNumber 공급사 매뉴얼 번호
     * @param {module:api/BoardApi~getManualBoardByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/BoardManual}
     */

  }, {
    key: "getManualBoardByNo",
    value: function getManualBoardByNo(manualNumber, callback) {
      var postBody = null; // verify the required parameter 'manualNumber' is set

      if (manualNumber === undefined || manualNumber === null) {
        throw new Error("Missing the required parameter 'manualNumber' when calling getManualBoardByNo");
      }

      var pathParams = {
        'manualNumber': manualNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _BoardManual["default"];
      return this.apiClient.callApi('/shapi/v1/board/manuals/{manualNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getNoticeBoard operation.
     * @callback module:api/BoardApi~getNoticeBoardCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/BoardNotice>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 공지사항 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.searchKey 검색키
     * @param {String} opts.searchWord 검색어
     * @param {Date} opts.startDate 시작일
     * @param {Date} opts.endDate 종료일
     * @param {module:model/String} opts.isPopup 팝업 여부
     * @param {module:model/String} opts.isNotice 공지 여부
     * @param {Array.<module:model/String>} opts.popupPositions 팝업 노출 위치
     * @param {module:model/String} opts.boardStatus 공지 상태
     * @param {module:model/String} opts.isAgree 동의 여부
     * @param {module:model/String} opts.isCurrentVisible 현재 노출 여부
     * @param {module:model/String} opts.sortBy 정렬
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/BoardApi~getNoticeBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/BoardNotice>}
     */

  }, {
    key: "getNoticeBoard",
    value: function getNoticeBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'isPopup': opts['isPopup'],
        'isNotice': opts['isNotice'],
        'popupPositions': this.apiClient.buildCollectionParam(opts['popupPositions'], 'multi'),
        'boardStatus': opts['boardStatus'],
        'isAgree': opts['isAgree'],
        'isCurrentVisible': opts['isCurrentVisible'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_BoardNotice["default"]];
      return this.apiClient.callApi('/shapi/v1/board/notices', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getNoticeBoardByNo operation.
     * @callback module:api/BoardApi~getNoticeBoardByNoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/BoardNotice} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 공지사항
     * @param {Number} noticeNumber 공급사 공지사항 번호
     * @param {module:api/BoardApi~getNoticeBoardByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/BoardNotice}
     */

  }, {
    key: "getNoticeBoardByNo",
    value: function getNoticeBoardByNo(noticeNumber, callback) {
      var postBody = null; // verify the required parameter 'noticeNumber' is set

      if (noticeNumber === undefined || noticeNumber === null) {
        throw new Error("Missing the required parameter 'noticeNumber' when calling getNoticeBoardByNo");
      }

      var pathParams = {
        'noticeNumber': noticeNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _BoardNotice["default"];
      return this.apiClient.callApi('/shapi/v1/board/notices/{noticeNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateBoard operation.
     * @callback module:api/BoardApi~updateBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 수정
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/BoardApi~updateBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateBoard",
    value: function updateBoard(boardNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling updateBoard");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling updateBoard");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateBoard");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateBoardReply operation.
     * @callback module:api/BoardApi~updateBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 수정
     * @param {Number} replyNumber 판매자지원 답변 번호
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/BoardApi~updateBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateBoardReply",
    value: function updateBoardReply(replyNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'replyNumber' is set

      if (replyNumber === undefined || replyNumber === null) {
        throw new Error("Missing the required parameter 'replyNumber' when calling updateBoardReply");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling updateBoardReply");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateBoardReply");
      }

      var pathParams = {
        'replyNumber': replyNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/reply/{replyNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return BoardApi;
}();

exports["default"] = BoardApi;
},{"../ApiClient":1,"../model/BoardManual":41,"../model/BoardNotice":42}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _UpdateProviderBrandBrandDatasParameterInner = _interopRequireDefault(require("../model/UpdateProviderBrandBrandDatasParameterInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Brand service.
* @module api/BrandApi
* @version 0.0.1
*/
var BrandApi = /*#__PURE__*/function () {
  /**
  * Constructs a new BrandApi. 
  * @alias module:api/BrandApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function BrandApi(apiClient) {
    _classCallCheck(this, BrandApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createProviderBrand operation.
   * @callback module:api/BrandApi~createProviderBrandCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 공급사 브랜드 추가하기
   * 공급사 브랜드 추가하기
   * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드명
   * @param {module:model/null} UNKNOWN_PARAMETER_NAME2 브랜드 번호
   * @param {module:api/BrandApi~createProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(BrandApi, [{
    key: "createProviderBrand",
    value: function createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createProviderBrand");
      } // verify the required parameter 'UNKNOWN_PARAMETER_NAME2' is set


      if (UNKNOWN_PARAMETER_NAME2 === undefined || UNKNOWN_PARAMETER_NAME2 === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME2' when calling createProviderBrand");
      }

      var pathParams = {
        'brandName': UNKNOWN_PARAMETER_NAME,
        'brandNumber': UNKNOWN_PARAMETER_NAME2
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteProviderBrandByNo operation.
     * @callback module:api/BrandApi~deleteProviderBrandByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 삭제하기
     * 공급사 브랜드 삭제하기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:api/BrandApi~deleteProviderBrandByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteProviderBrandByNo",
    value: function deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteProviderBrandByNo");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider/{brandNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderBrand operation.
     * @callback module:api/BrandApi~getProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 목록 가져오기
     * 공급사 브랜드 목록 가져오기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME2 브랜드명
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/BrandApi~getProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProviderBrand",
    value: function getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getProviderBrand");
      } // verify the required parameter 'UNKNOWN_PARAMETER_NAME2' is set


      if (UNKNOWN_PARAMETER_NAME2 === undefined || UNKNOWN_PARAMETER_NAME2 === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME2' when calling getProviderBrand");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME,
        'brandName': UNKNOWN_PARAMETER_NAME2
      };
      var queryParams = {
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderBrandByNo operation.
     * @callback module:api/BrandApi~getProviderBrandByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 특정 브랜드 가져오기
     * 공급사 특정 브랜드 가져오기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:api/BrandApi~getProviderBrandByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProviderBrandByNo",
    value: function getProviderBrandByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getProviderBrandByNo");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider/{$brandNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateProviderBrand operation.
     * @callback module:api/BrandApi~updateProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 변경하기
     * 공급사 브랜드 변경하기
     * @param {Array.<module:model/UpdateProviderBrandBrandDatasParameterInner>} brandDatas 브랜드 데이터
     * @param {module:api/BrandApi~updateProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateProviderBrand",
    value: function updateProviderBrand(brandDatas, callback) {
      var postBody = null; // verify the required parameter 'brandDatas' is set

      if (brandDatas === undefined || brandDatas === null) {
        throw new Error("Missing the required parameter 'brandDatas' when calling updateProviderBrand");
      }

      var pathParams = {
        'brandDatas': brandDatas
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return BrandApi;
}();

exports["default"] = BrandApi;
},{"../ApiClient":1,"../model/UpdateProviderBrandBrandDatasParameterInner":63}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Calculate service.
* @module api/CalculateApi
* @version 0.0.1
*/
var CalculateApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CalculateApi. 
  * @alias module:api/CalculateApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CalculateApi(apiClient) {
    _classCallCheck(this, CalculateApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getApplicablePreCalculate operation.
   * @callback module:api/CalculateApi~getApplicablePreCalculateCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 신청가능금액
   * @param {module:api/CalculateApi~getApplicablePreCalculateCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(CalculateApi, [{
    key: "getApplicablePreCalculate",
    value: function getApplicablePreCalculate(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/preCalculate/applicable', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateRequest operation.
     * @callback module:api/CalculateApi~getCalculateRequestCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 목록
     * @param {module:api/CalculateApi~getCalculateRequestCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateRequest",
    value: function getCalculateRequest(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/request', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateRequestDeduction operation.
     * @callback module:api/CalculateApi~getCalculateRequestDeductionCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 공제 목록
     * @param {Number} calculateRequestNumber 정산요청번호
     * @param {module:api/CalculateApi~getCalculateRequestDeductionCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateRequestDeduction",
    value: function getCalculateRequestDeduction(calculateRequestNumber, callback) {
      var postBody = null; // verify the required parameter 'calculateRequestNumber' is set

      if (calculateRequestNumber === undefined || calculateRequestNumber === null) {
        throw new Error("Missing the required parameter 'calculateRequestNumber' when calling getCalculateRequestDeduction");
      }

      var pathParams = {
        'calculateRequestNumber': calculateRequestNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/request/{calculateRequestNumber}/deductions', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateRequestDetail operation.
     * @callback module:api/CalculateApi~getCalculateRequestDetailCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 상세 목록
     * @param {Number} calculateRequestNumber 정산요청번호
     * @param {module:api/CalculateApi~getCalculateRequestDetailCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateRequestDetail",
    value: function getCalculateRequestDetail(calculateRequestNumber, callback) {
      var postBody = null; // verify the required parameter 'calculateRequestNumber' is set

      if (calculateRequestNumber === undefined || calculateRequestNumber === null) {
        throw new Error("Missing the required parameter 'calculateRequestNumber' when calling getCalculateRequestDetail");
      }

      var pathParams = {
        'calculateRequestNumber': calculateRequestNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/request/{calculateRequestNumber}/items', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateScheduled operation.
     * @callback module:api/CalculateApi~getCalculateScheduledCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 예정
     * @param {module:api/CalculateApi~getCalculateScheduledCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateScheduled",
    value: function getCalculateScheduled(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/scheduled', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateScheduledDeduction operation.
     * @callback module:api/CalculateApi~getCalculateScheduledDeductionCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 공제 예정 내역
     * @param {module:api/CalculateApi~getCalculateScheduledDeductionCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateScheduledDeduction",
    value: function getCalculateScheduledDeduction(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/scheduled/deductions', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateScheduledDelivery operation.
     * @callback module:api/CalculateApi~getCalculateScheduledDeliveryCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 예정 패널티
     * @param {module:api/CalculateApi~getCalculateScheduledDeliveryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateScheduledDelivery",
    value: function getCalculateScheduledDelivery(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/scheduled/deliveries', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateScheduledDetail operation.
     * @callback module:api/CalculateApi~getCalculateScheduledDetailCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 정산 예정 상품 상세
     * @param {module:api/CalculateApi~getCalculateScheduledDetailCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateScheduledDetail",
    value: function getCalculateScheduledDetail(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/scheduled/items', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCalculateSurtax operation.
     * @callback module:api/CalculateApi~getCalculateSurtaxCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 부가세 신고 참고자료
     * @param {module:api/CalculateApi~getCalculateSurtaxCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCalculateSurtax",
    value: function getCalculateSurtax(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/calculate/surtax', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPreCalculate operation.
     * @callback module:api/CalculateApi~getPreCalculateCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 빠른정산 목록
     * @param {module:api/CalculateApi~getPreCalculateCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPreCalculate",
    value: function getPreCalculate(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/preCalculate', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPreCalculateRequest operation.
     * @callback module:api/CalculateApi~getPreCalculateRequestCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 신청 목록
     * @param {module:api/CalculateApi~getPreCalculateRequestCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPreCalculateRequest",
    value: function getPreCalculateRequest(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/preCalculate/request', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CalculateApi;
}();

exports["default"] = CalculateApi;
},{"../ApiClient":1}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Category service.
* @module api/CategoryApi
* @version 0.0.1
*/
var CategoryApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CategoryApi. 
  * @alias module:api/CategoryApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CategoryApi(apiClient) {
    _classCallCheck(this, CategoryApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getCategory operation.
   * @callback module:api/CategoryApi~getCategoryCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 카테고리 목록
   * 카테고리 목록 조회
   * @param {Object} opts Optional parameters
   * @param {Number} opts.categoryNumber 카테고리 번호
   * @param {String} opts.categoryName 카테고리 이름
   * @param {String} opts.categoryCode 카테고리 코드
   * @param {module:model/String} opts.isDesigner 디자이너 카테고리 여부
   * @param {Boolean} opts.withSubCategory 하위 카테고리 포함
   * @param {Number} opts.maxDepth 최대 계층
   * @param {module:model/String} opts.sortBy 정렬 타입
   * @param {module:model/String} opts.sortDirection 정렬 방향
   * @param {module:api/CategoryApi~getCategoryCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(CategoryApi, [{
    key: "getCategory",
    value: function getCategory(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'categoryNumber': opts['categoryNumber'],
        'categoryName': opts['categoryName'],
        'categoryCode': opts['categoryCode'],
        'isDesigner': opts['isDesigner'],
        'withSubCategory': opts['withSubCategory'],
        'maxDepth': opts['maxDepth'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/categorys', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCategoryByNo operation.
     * @callback module:api/CategoryApi~getCategoryByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 카테고리 조회
     * 카테고리 조회
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.withSubCategory 하위 카테고리 포함
     * @param {Number} opts.maxDepth 최대 계층
     * @param {module:api/CategoryApi~getCategoryByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCategoryByNo",
    value: function getCategoryByNo(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'withSubCategory': opts['withSubCategory'],
        'maxDepth': opts['maxDepth']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/categorys/{categoryNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getViolation operation.
     * @callback module:api/CategoryApi~getViolationCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품정보위반 목록
     * 상품정보위반 목록 조회
     * @param {Object} opts Optional parameters
     * @param {Number} opts.violationId 상품정보위반 ID
     * @param {Number} opts.shoppingMallProductNumber 제휴몰 등록 상품 번호
     * @param {String} opts.shoppingMall 제휴몰
     * @param {String} opts.violationType 위반 타입
     * @param {Date} opts.violationStartDate 위반일시 시작일자
     * @param {Date} opts.violationEndDate 위반일시 종료일자
     * @param {Date} opts.registedViolationStartDate 위반등록일시 시작일자
     * @param {Date} opts.registedViolationEndDate 위반등록일시 종료일자
     * @param {Date} opts.updatedViolationStartDate 위반수정일시 시작일자
     * @param {Date} opts.updatedViolationEndDate 위반수정일시 종료일자
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/CategoryApi~getViolationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getViolation",
    value: function getViolation(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'violationId': opts['violationId'],
        'shoppingMallProductNumber': opts['shoppingMallProductNumber'],
        'shoppingMall': opts['shoppingMall'],
        'violationType': opts['violationType'],
        'violationStartDate': opts['violationStartDate'],
        'violationEndDate': opts['violationEndDate'],
        'registedViolationStartDate': opts['registedViolationStartDate'],
        'registedViolationEndDate': opts['registedViolationEndDate'],
        'updatedViolationStartDate': opts['updatedViolationStartDate'],
        'updatedViolationEndDate': opts['updatedViolationEndDate'],
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi'),
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/violations', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getViolationById operation.
     * @callback module:api/CategoryApi~getViolationByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품정보위반 조회
     * 상품정보위반 목록 조회
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {module:api/CategoryApi~getViolationByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getViolationById",
    value: function getViolationById(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi')
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/violations/{violationId}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CategoryApi;
}();

exports["default"] = CategoryApi;
},{"../ApiClient":1}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CodeResource = _interopRequireDefault(require("../model/CodeResource"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Code service.
* @module api/CodeApi
* @version 0.0.1
*/
var CodeApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CodeApi. 
  * @alias module:api/CodeApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CodeApi(apiClient) {
    _classCallCheck(this, CodeApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getCode operation.
   * @callback module:api/CodeApi~getCodeCallback
   * @param {String} error Error message, if any.
   * @param {Array.<module:model/CodeResource>} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 그룹코드 조회
   * 상위 그룹코드 조회
   * @param {module:api/CodeApi~getCodeCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Array.<module:model/CodeResource>}
   */


  _createClass(CodeApi, [{
    key: "getCode",
    value: function getCode(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_CodeResource["default"]];
      return this.apiClient.callApi('/shapi/v1/codes', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCodeByGroupCd operation.
     * @callback module:api/CodeApi~getCodeByGroupCdCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/CodeResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 그룹코드 조회
     * 그룹코드로 코드 조회
     * @param {String} groupCd 그룹코드
     * @param {module:api/CodeApi~getCodeByGroupCdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/CodeResource>}
     */

  }, {
    key: "getCodeByGroupCd",
    value: function getCodeByGroupCd(groupCd, callback) {
      var postBody = null; // verify the required parameter 'groupCd' is set

      if (groupCd === undefined || groupCd === null) {
        throw new Error("Missing the required parameter 'groupCd' when calling getCodeByGroupCd");
      }

      var pathParams = {
        'groupCd': groupCd
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_CodeResource["default"]];
      return this.apiClient.callApi('/shapi/v1/codes/{groupCd}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CodeApi;
}();

exports["default"] = CodeApi;
},{"../ApiClient":1,"../model/CodeResource":43}],10:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CodeResource = _interopRequireDefault(require("../model/CodeResource"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Common service.
* @module api/CommonApi
* @version 0.0.1
*/
var CommonApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CommonApi. 
  * @alias module:api/CommonApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CommonApi(apiClient) {
    _classCallCheck(this, CommonApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createToken operation.
   * @callback module:api/CommonApi~createTokenCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 토큰 생성
   * 토큰 생성
   * @param {String} name 토큰명
   * @param {Object} opts Optional parameters
   * @param {Array.<module:model/String>} opts.permissions 권한
   * @param {module:api/CommonApi~createTokenCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(CommonApi, [{
    key: "createToken",
    value: function createToken(name, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'name' is set

      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createToken");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'permissions': this.apiClient.buildCollectionParam(opts['permissions'], 'csv')
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/token', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getAddress operation.
     * @callback module:api/CommonApi~getAddressCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주소조회
     * 주소조회
     * @param {String} keyword 검색어
     * @param {Object} opts Optional parameters
     * @param {Number} opts.currentPage 현재 페이지
     * @param {Number} opts.countPerPage 페이지당 검색
     * @param {module:model/String} opts.hstryYn 변동된 주소정보 포함 여부 (default to 'N')
     * @param {module:model/String} opts.firstSort 정확도순 정렬 (default to 'none')
     * @param {module:model/String} opts.addInfoYn 출력결과에 추가된 항목 (default to 'N')
     * @param {module:api/CommonApi~getAddressCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getAddress",
    value: function getAddress(keyword, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'keyword' is set

      if (keyword === undefined || keyword === null) {
        throw new Error("Missing the required parameter 'keyword' when calling getAddress");
      }

      var pathParams = {};
      var queryParams = {
        'currentPage': opts['currentPage'],
        'countPerPage': opts['countPerPage'],
        'keyword': keyword,
        'hstryYn': opts['hstryYn'],
        'firstSort': opts['firstSort'],
        'addInfoYn': opts['addInfoYn']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/address/search', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getAlarm operation.
     * @callback module:api/CommonApi~getAlarmCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 알람 목록 조회
     * 알람 목록 조회
     * @param {Object} opts Optional parameters
     * @param {Date} opts.regDateTime 보낸시간
     * @param {String} opts.alarmType1 알람 메인타입
     * @param {String} opts.alarmType2 알람 타입
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {String} opts.keyword 검색어
     * @param {module:model/String} opts.isRead 읽음 여부 (default to '')
     * @param {module:api/CommonApi~getAlarmCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getAlarm",
    value: function getAlarm(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'regDateTime': opts['regDateTime'],
        'alarmType1': opts['alarmType1'],
        'alarmType2': opts['alarmType2'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'keyword': opts['keyword'],
        'isRead': opts['isRead']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/alarm', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getAlarmByNo operation.
     * @callback module:api/CommonApi~getAlarmByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 알람읽기
     * @param {Number} alarmNumber 알람 번호
     * @param {module:api/CommonApi~getAlarmByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getAlarmByNo",
    value: function getAlarmByNo(alarmNumber, callback) {
      var postBody = null; // verify the required parameter 'alarmNumber' is set

      if (alarmNumber === undefined || alarmNumber === null) {
        throw new Error("Missing the required parameter 'alarmNumber' when calling getAlarmByNo");
      }

      var pathParams = {
        'alarmNumber': alarmNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/alarm/read/{alarmNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCode operation.
     * @callback module:api/CommonApi~getCodeCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/CodeResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 그룹코드 조회
     * 상위 그룹코드 조회
     * @param {module:api/CommonApi~getCodeCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/CodeResource>}
     */

  }, {
    key: "getCode",
    value: function getCode(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_CodeResource["default"]];
      return this.apiClient.callApi('/shapi/v1/codes', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCodeByGroupCd operation.
     * @callback module:api/CommonApi~getCodeByGroupCdCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/CodeResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 그룹코드 조회
     * 그룹코드로 코드 조회
     * @param {String} groupCd 그룹코드
     * @param {module:api/CommonApi~getCodeByGroupCdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/CodeResource>}
     */

  }, {
    key: "getCodeByGroupCd",
    value: function getCodeByGroupCd(groupCd, callback) {
      var postBody = null; // verify the required parameter 'groupCd' is set

      if (groupCd === undefined || groupCd === null) {
        throw new Error("Missing the required parameter 'groupCd' when calling getCodeByGroupCd");
      }

      var pathParams = {
        'groupCd': groupCd
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_CodeResource["default"]];
      return this.apiClient.callApi('/shapi/v1/codes/{groupCd}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCsrfToken operation.
     * @callback module:api/CommonApi~getCsrfTokenCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * CSRF 토큰가져오기
     * @param {module:api/CommonApi~getCsrfTokenCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCsrfToken",
    value: function getCsrfToken(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csrfToken', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getScmUploadFileOfProvider operation.
     * @callback module:api/CommonApi~getScmUploadFileOfProviderCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사의 업로드 파일 (사업자등록증 등) 보기
     * 공급사의 파일관리
     * @param {Object} opts Optional parameters
     * @param {String} opts.model SCM 모델
     * @param {Number} opts.key SCM 모델의 키
     * @param {String} opts.column SCM 모델의 컬럼명
     * @param {module:api/CommonApi~getScmUploadFileOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "getScmUploadFileOfProvider",
    value: function getScmUploadFileOfProvider(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {
        'model': opts['model'],
        'key': opts['key'],
        'column': opts['column']
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/shapi/v1/scm/{model}/{key}/{column}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the login operation.
     * @callback module:api/CommonApi~loginCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그인
     * 사용자 로그인
     * @param {Object} opts Optional parameters
     * @param {String} opts.m_id 사용자아이디
     * @param {String} opts.password 사용자비밀번호
     * @param {module:api/CommonApi~loginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "login",
    value: function login(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'm_id': opts['m_id'],
        'password': opts['password']
      };
      var authNames = ['sanctum'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/login', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the logout operation.
     * @callback module:api/CommonApi~logoutCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그아웃
     * Destroy an authenticated session.
     * @param {module:api/CommonApi~logoutCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "logout",
    value: function logout(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/logout', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the subLogin operation.
     * @callback module:api/CommonApi~subLoginCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그인
     * @param {Object} opts Optional parameters
     * @param {String} opts.sadmid 사용자아이디
     * @param {String} opts.password 사용자비밀번호
     * @param {module:api/CommonApi~subLoginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "subLogin",
    value: function subLogin(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'sadmid': opts['sadmid'],
        'password': opts['password']
      };
      var authNames = ['sanctum'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/subLogin', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CommonApi;
}();

exports["default"] = CommonApi;
},{"../ApiClient":1,"../model/CodeResource":43}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* CsQna service.
* @module api/CsQnaApi
* @version 0.0.1
*/
var CsQnaApi = /*#__PURE__*/function () {
  /**
  * Constructs a new CsQnaApi. 
  * @alias module:api/CsQnaApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function CsQnaApi(apiClient) {
    _classCallCheck(this, CsQnaApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getCsQna operation.
   * @callback module:api/CsQnaApi~getCsQnaCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * cs 리스트
   * cs목록
   * @param {Object} opts Optional parameters
   * @param {module:model/String} opts.dateType 기간검색 옵션
   * @param {Date} opts.startDate 시작 일자
   * @param {Date} opts.endDate 종료 일자
   * @param {Number} opts.status 처리상태
   * @param {String} opts.inquireType 문의유형
   * @param {module:model/String} opts.searchType 상세검색 옵션
   * @param {String} opts.searchWord 검색어
   * @param {module:model/String} opts.sortBy 정렬 옵션
   * @param {module:model/String} opts.sortDirection 정렬 방향
   * @param {Number} opts.page 페이지 번호
   * @param {Number} opts.pageSize 페이지 개수
   * @param {module:api/CsQnaApi~getCsQnaCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(CsQnaApi, [{
    key: "getCsQna",
    value: function getCsQna(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'dateType': opts['dateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'status': opts['status'],
        'inquireType': opts['inquireType'],
        'searchType': opts['searchType'],
        'searchWord': opts['searchWord'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csQna', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCsQnaByNo operation.
     * @callback module:api/CsQnaApi~getCsQnaByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * CS문의 상세내역 조회
     * CS문의 상세내역
     * @param {Number} csNumber cs문의 번호
     * @param {module:api/CsQnaApi~getCsQnaByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCsQnaByNo",
    value: function getCsQnaByNo(csNumber, callback) {
      var postBody = null; // verify the required parameter 'csNumber' is set

      if (csNumber === undefined || csNumber === null) {
        throw new Error("Missing the required parameter 'csNumber' when calling getCsQnaByNo");
      }

      var pathParams = {};
      var queryParams = {
        'csNumber': csNumber
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csQna/{csNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateCsQna operation.
     * @callback module:api/CsQnaApi~updateCsQnaCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * CS 답변 등록
     * CS 답변 등록
     * @param {Number} csNumber cs문의 번호
     * @param {module:api/CsQnaApi~updateCsQnaCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateCsQna",
    value: function updateCsQna(csNumber, callback) {
      var postBody = null; // verify the required parameter 'csNumber' is set

      if (csNumber === undefined || csNumber === null) {
        throw new Error("Missing the required parameter 'csNumber' when calling updateCsQna");
      }

      var pathParams = {};
      var queryParams = {
        'csNumber': csNumber
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csQna/{csNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return CsQnaApi;
}();

exports["default"] = CsQnaApi;
},{"../ApiClient":1}],12:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _PopularProduct = _interopRequireDefault(require("../model/PopularProduct"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Dashboard service.
* @module api/DashboardApi
* @version 0.0.1
*/
var DashboardApi = /*#__PURE__*/function () {
  /**
  * Constructs a new DashboardApi. 
  * @alias module:api/DashboardApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function DashboardApi(apiClient) {
    _classCallCheck(this, DashboardApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the clearDashboardCache operation.
   * @callback module:api/DashboardApi~clearDashboardCacheCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 대시보드 캐시삭제
   * @param {module:api/DashboardApi~clearDashboardCacheCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(DashboardApi, [{
    key: "clearDashboardCache",
    value: function clearDashboardCache(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/cacheClear', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardBanner operation.
     * @callback module:api/DashboardApi~getDashboardBannerCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 하단 배너
     * @param {module:api/DashboardApi~getDashboardBannerCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardBanner",
    value: function getDashboardBanner(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/banner', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardExchangeStatus operation.
     * @callback module:api/DashboardApi~getDashboardExchangeStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 교환 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardExchangeStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardExchangeStatus",
    value: function getDashboardExchangeStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/exchangeStatus', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardLinkageStatus operation.
     * @callback module:api/DashboardApi~getDashboardLinkageStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 연동현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardLinkageStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardLinkageStatus",
    value: function getDashboardLinkageStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/linkageStatus', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardOrderStat operation.
     * @callback module:api/DashboardApi~getDashboardOrderStatCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 매출 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardOrderStatCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardOrderStat",
    value: function getDashboardOrderStat(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/orderStat', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardPenaltyStatus operation.
     * @callback module:api/DashboardApi~getDashboardPenaltyStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 패널티 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardPenaltyStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardPenaltyStatus",
    value: function getDashboardPenaltyStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/penaltyStatus', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardPopularProducts operation.
     * @callback module:api/DashboardApi~getDashboardPopularProductsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/PopularProduct>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 인기상품 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardPopularProductsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/PopularProduct>}
     */

  }, {
    key: "getDashboardPopularProducts",
    value: function getDashboardPopularProducts(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_PopularProduct["default"]];
      return this.apiClient.callApi('/shapi/v1/dashboard/popularProducts', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardProductState operation.
     * @callback module:api/DashboardApi~getDashboardProductStateCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardProductStateCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardProductState",
    value: function getDashboardProductState(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/productState', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardProductViolation operation.
     * @callback module:api/DashboardApi~getDashboardProductViolationCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품정보고시 위반 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardProductViolationCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardProductViolation",
    value: function getDashboardProductViolation(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/productViolation', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDashboardSalesStatus operation.
     * @callback module:api/DashboardApi~getDashboardSalesStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매 현황
     * @param {Object} opts Optional parameters
     * @param {Boolean} opts.forceReflush 캐시 삭제
     * @param {module:api/DashboardApi~getDashboardSalesStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getDashboardSalesStatus",
    value: function getDashboardSalesStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'forceReflush': opts['forceReflush']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/dashboard/salesStatus', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return DashboardApi;
}();

exports["default"] = DashboardApi;
},{"../ApiClient":1,"../model/PopularProduct":60}],13:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _DeliveryResource = _interopRequireDefault(require("../model/DeliveryResource"));

var _ProviderDeliveryResource = _interopRequireDefault(require("../model/ProviderDeliveryResource"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Delivery service.
* @module api/DeliveryApi
* @version 0.0.1
*/
var DeliveryApi = /*#__PURE__*/function () {
  /**
  * Constructs a new DeliveryApi. 
  * @alias module:api/DeliveryApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function DeliveryApi(apiClient) {
    _classCallCheck(this, DeliveryApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createProviderDelivery operation.
   * @callback module:api/DeliveryApi~createProviderDeliveryCallback
   * @param {String} error Error message, if any.
   * @param {module:model/ProviderDeliveryResource} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 공급사의 택배사 정보 추가
   * @param {Number} brandNumber 브랜드번호
   * @param {Number} deliveryCompanyNumber 택배사 번호
   * @param {module:model/String} isUsed 사용여부
   * @param {Number} sort 순서
   * @param {Object} opts Optional parameters
   * @param {String} opts.deliveryCompanyUrl 택배사 주소
   * @param {String} opts.deliveryContractCode 택배사 계약번호
   * @param {module:model/String} opts.isAvaliableInvoiceLinking 송장연동 출력기능 사용여부
   * @param {module:api/DeliveryApi~createProviderDeliveryCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/ProviderDeliveryResource}
   */


  _createClass(DeliveryApi, [{
    key: "createProviderDelivery",
    value: function createProviderDelivery(brandNumber, deliveryCompanyNumber, isUsed, sort, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'brandNumber' is set

      if (brandNumber === undefined || brandNumber === null) {
        throw new Error("Missing the required parameter 'brandNumber' when calling createProviderDelivery");
      } // verify the required parameter 'deliveryCompanyNumber' is set


      if (deliveryCompanyNumber === undefined || deliveryCompanyNumber === null) {
        throw new Error("Missing the required parameter 'deliveryCompanyNumber' when calling createProviderDelivery");
      } // verify the required parameter 'isUsed' is set


      if (isUsed === undefined || isUsed === null) {
        throw new Error("Missing the required parameter 'isUsed' when calling createProviderDelivery");
      } // verify the required parameter 'sort' is set


      if (sort === undefined || sort === null) {
        throw new Error("Missing the required parameter 'sort' when calling createProviderDelivery");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'brandNumber': brandNumber,
        'deliveryCompanyNumber': deliveryCompanyNumber,
        'deliveryCompanyUrl': opts['deliveryCompanyUrl'],
        'deliveryContractCode': opts['deliveryContractCode'],
        'isUsed': isUsed,
        'sort': sort,
        'isAvaliableInvoiceLinking': opts['isAvaliableInvoiceLinking']
      };
      var authNames = ['sanctum'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = _ProviderDeliveryResource["default"];
      return this.apiClient.callApi('/shapi/v1/delivery/provider', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteProviderDeliveryByNo operation.
     * @callback module:api/DeliveryApi~deleteProviderDeliveryByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사가 사용중인 택배사 정보 삭제
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 공급사의 택배사번호
     * @param {module:api/DeliveryApi~deleteProviderDeliveryByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteProviderDeliveryByNo",
    value: function deleteProviderDeliveryByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteProviderDeliveryByNo");
      }

      var pathParams = {
        'deliveryCompanyNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/delivery/provider/{deliveryNumer}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getDelivery operation.
     * @callback module:api/DeliveryApi~getDeliveryCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/DeliveryResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 사용 택배사 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.isUsed 사용여부 (default to '')
     * @param {module:api/DeliveryApi~getDeliveryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/DeliveryResource>}
     */

  }, {
    key: "getDelivery",
    value: function getDelivery(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'isUsed': opts['isUsed']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_DeliveryResource["default"]];
      return this.apiClient.callApi('/shapi/v1/delivery', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderDelivery operation.
     * @callback module:api/DeliveryApi~getProviderDeliveryCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/ProviderDeliveryResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사가 사용중인 택배사 가져오기
     * @param {Object} opts Optional parameters
     * @param {Number} opts.bramdId 브랜드 번호
     * @param {Number} opts.deliveryCompanyNumber 택배사 번호
     * @param {String} opts.deliveryCompanyName 택배사명
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/DeliveryApi~getProviderDeliveryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/ProviderDeliveryResource>}
     */

  }, {
    key: "getProviderDelivery",
    value: function getProviderDelivery(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'bramdId': opts['bramdId'],
        'deliveryCompanyNumber': opts['deliveryCompanyNumber'],
        'deliveryCompanyName': opts['deliveryCompanyName'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_ProviderDeliveryResource["default"]];
      return this.apiClient.callApi('/shapi/v1/delivery/provider', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateProviderDelivery operation.
     * @callback module:api/DeliveryApi~updateProviderDeliveryCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사가 사용중인 택배사 정보 수정
     * @param {Number} deliveryCompanyNumber 공급사의 택배사번호
     * @param {Number} brandNumber 브랜드번호
     * @param {Number} deliveryCompanyNumber2 택배사 번호
     * @param {module:model/String} isUsed 사용여부
     * @param {Number} sort 순서
     * @param {Object} opts Optional parameters
     * @param {String} opts.deliveryCompanyUrl 택배사 주소
     * @param {String} opts.deliveryContractCode 택배사 계약번호
     * @param {module:model/String} opts.isAvaliableInvoiceLinking 송장연동 출력기능 사용여부
     * @param {module:api/DeliveryApi~updateProviderDeliveryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateProviderDelivery",
    value: function updateProviderDelivery(deliveryCompanyNumber, brandNumber, deliveryCompanyNumber2, isUsed, sort, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'deliveryCompanyNumber' is set

      if (deliveryCompanyNumber === undefined || deliveryCompanyNumber === null) {
        throw new Error("Missing the required parameter 'deliveryCompanyNumber' when calling updateProviderDelivery");
      } // verify the required parameter 'brandNumber' is set


      if (brandNumber === undefined || brandNumber === null) {
        throw new Error("Missing the required parameter 'brandNumber' when calling updateProviderDelivery");
      } // verify the required parameter 'deliveryCompanyNumber2' is set


      if (deliveryCompanyNumber2 === undefined || deliveryCompanyNumber2 === null) {
        throw new Error("Missing the required parameter 'deliveryCompanyNumber2' when calling updateProviderDelivery");
      } // verify the required parameter 'isUsed' is set


      if (isUsed === undefined || isUsed === null) {
        throw new Error("Missing the required parameter 'isUsed' when calling updateProviderDelivery");
      } // verify the required parameter 'sort' is set


      if (sort === undefined || sort === null) {
        throw new Error("Missing the required parameter 'sort' when calling updateProviderDelivery");
      }

      var pathParams = {
        'deliveryCompanyNumber': deliveryCompanyNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'brandNumber': brandNumber,
        'deliveryCompanyNumber': deliveryCompanyNumber2,
        'deliveryCompanyUrl': opts['deliveryCompanyUrl'],
        'deliveryContractCode': opts['deliveryContractCode'],
        'isUsed': isUsed,
        'sort': sort,
        'isAvaliableInvoiceLinking': opts['isAvaliableInvoiceLinking']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/delivery/provider', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return DeliveryApi;
}();

exports["default"] = DeliveryApi;
},{"../ApiClient":1,"../model/DeliveryResource":56,"../model/ProviderDeliveryResource":61}],14:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Event service.
* @module api/EventApi
* @version 0.0.1
*/
var EventApi = /*#__PURE__*/function () {
  /**
  * Constructs a new EventApi. 
  * @alias module:api/EventApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function EventApi(apiClient) {
    _classCallCheck(this, EventApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createMarketingEvent operation.
   * @callback module:api/EventApi~createMarketingEventCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 행사신청 게시물 등록
   * @param {Object} name 신청자이름
   * @param {Object} email 신청자이메일
   * @param {Object} phoneNumber 신청자연락처
   * @param {Object} title 행사제목
   * @param {Object} contents 행사내용
   * @param {module:model/String} isCoupon 쿠폰사용여부
   * @param {Number} returnDeliveryCharge 반품배송비(편도)
   * @param {Number} exchangeDeliveryCharge 교환배송비(왕복)
   * @param {Object} opts Optional parameters
   * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
   * @param {module:api/EventApi~createMarketingEventCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(EventApi, [{
    key: "createMarketingEvent",
    value: function createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'name' is set

      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createMarketingEvent");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling createMarketingEvent");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling createMarketingEvent");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createMarketingEvent");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEvent");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling createMarketingEvent");
      } // verify the required parameter 'returnDeliveryCharge' is set


      if (returnDeliveryCharge === undefined || returnDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'returnDeliveryCharge' when calling createMarketingEvent");
      } // verify the required parameter 'exchangeDeliveryCharge' is set


      if (exchangeDeliveryCharge === undefined || exchangeDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'exchangeDeliveryCharge' when calling createMarketingEvent");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate'],
        'returnDeliveryCharge': returnDeliveryCharge,
        'exchangeDeliveryCharge': exchangeDeliveryCharge
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingEventReplyByNo operation.
     * @callback module:api/EventApi~createMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 등록
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} contents 댓글내용
     * @param {module:api/EventApi~createMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingEventReplyByNo",
    value: function createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createMarketingEventReplyByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'contents': contents
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteMarketingEventByNo operation.
     * @callback module:api/EventApi~deleteMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 삭제
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/EventApi~deleteMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteMarketingEventByNo",
    value: function deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteMarketingEventByNo");
      }

      var pathParams = {
        'boardNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEvent operation.
     * @callback module:api/EventApi~getMarketingEventCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 목록 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.isApprove 승인 여부 (default to 'all')
     * @param {module:model/String} opts.isAnswer 답변 여부 (default to 'all')
     * @param {module:model/String} opts.searchDateType 검색 일자 설정 (default to 'registed')
     * @param {Number} opts.startDate 시작일
     * @param {Number} opts.endDate 종료일
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/EventApi~getMarketingEventCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEvent",
    value: function getMarketingEvent(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'isApprove': opts['isApprove'],
        'isAnswer': opts['isAnswer'],
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventApprovalByNo operation.
     * @callback module:api/EventApi~getMarketingEventApprovalByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 재신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/EventApi~getMarketingEventApprovalByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventApprovalByNo",
    value: function getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventApprovalByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/approval/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventByNo operation.
     * @callback module:api/EventApi~getMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/EventApi~getMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventByNo",
    value: function getMarketingEventByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventReplyByNo operation.
     * @callback module:api/EventApi~getMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 목록 조회
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/EventApi~getMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventReplyByNo",
    value: function getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateMarketingEventByNo operation.
     * @callback module:api/EventApi~updateMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 등록
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Number} returnDeliveryCharge 반품배송비(편도)
     * @param {Number} exchangeDeliveryCharge 교환배송비(왕복)
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/EventApi~updateMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateMarketingEventByNo",
    value: function updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling updateMarketingEventByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateMarketingEventByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling updateMarketingEventByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling updateMarketingEventByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling updateMarketingEventByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateMarketingEventByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling updateMarketingEventByNo");
      } // verify the required parameter 'returnDeliveryCharge' is set


      if (returnDeliveryCharge === undefined || returnDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'returnDeliveryCharge' when calling updateMarketingEventByNo");
      } // verify the required parameter 'exchangeDeliveryCharge' is set


      if (exchangeDeliveryCharge === undefined || exchangeDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'exchangeDeliveryCharge' when calling updateMarketingEventByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate'],
        'returnDeliveryCharge': returnDeliveryCharge,
        'exchangeDeliveryCharge': exchangeDeliveryCharge
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return EventApi;
}();

exports["default"] = EventApi;
},{"../ApiClient":1}],15:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* EventProposal service.
* @module api/EventProposalApi
* @version 0.0.1
*/
var EventProposalApi = /*#__PURE__*/function () {
  /**
  * Constructs a new EventProposalApi. 
  * @alias module:api/EventProposalApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function EventProposalApi(apiClient) {
    _classCallCheck(this, EventProposalApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createMarketingEventProposalRequestByNo operation.
   * @callback module:api/EventProposalApi~createMarketingEventProposalRequestByNoCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 행사제안 신청
   * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
   * @param {Object} name 신청자이름
   * @param {Object} email 신청자이메일
   * @param {Object} phoneNumber 신청자연락처
   * @param {Object} title 행사제목
   * @param {Object} contents 행사내용
   * @param {module:model/String} isCoupon 쿠폰사용여부
   * @param {Object} opts Optional parameters
   * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
   * @param {module:api/EventProposalApi~createMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(EventProposalApi, [{
    key: "createMarketingEventProposalRequestByNo",
    value: function createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling createMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventProposalNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventProposalNumber}', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposal operation.
     * @callback module:api/EventProposalApi~getMarketingEventProposalCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 목록 조회
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts.channel 행사 채널
     * @param {module:model/String} opts.searchDateType 검색 일자 설정 (default to 'startDate')
     * @param {Number} opts.startDate 시작일
     * @param {Number} opts.endDate 종료일
     * @param {Array.<String>} opts.category 카테고리
     * @param {module:model/String} opts.eventStatus 행사진행여부 (default to 'all')
     * @param {module:model/String} opts.requestStatus 행사신청여부 (default to 'all')
     * @param {module:model/String} opts.isApprove 답변여부 (default to 'all')
     * @param {module:model/String} opts.searchKey 검색키 (default to 'title')
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/EventProposalApi~getMarketingEventProposalCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposal",
    value: function getMarketingEventProposal(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'channel': this.apiClient.buildCollectionParam(opts['channel'], 'multi'),
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'category': this.apiClient.buildCollectionParam(opts['category'], 'multi'),
        'eventStatus': opts['eventStatus'],
        'requestStatus': opts['requestStatus'],
        'isApprove': opts['isApprove'],
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposalByNo operation.
     * @callback module:api/EventProposalApi~getMarketingEventProposalByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 내역
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {module:api/EventProposalApi~getMarketingEventProposalByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposalByNo",
    value: function getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventProposalByNo");
      }

      var pathParams = {
        'eventProposalNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/{eventProposalNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposalRequestByNo operation.
     * @callback module:api/EventProposalApi~getMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청내역
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {module:api/EventProposalApi~getMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposalRequestByNo",
    value: function getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateMarketingEventProposalRequestByNo operation.
     * @callback module:api/EventProposalApi~updateMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청내용 재신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 신청 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/EventProposalApi~updateMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateMarketingEventProposalRequestByNo",
    value: function updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling updateMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return EventProposalApi;
}();

exports["default"] = EventProposalApi;
},{"../ApiClient":1}],16:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Exit service.
* @module api/ExitApi
* @version 0.0.1
*/
var ExitApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ExitApi. 
  * @alias module:api/ExitApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ExitApi(apiClient) {
    _classCallCheck(this, ExitApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the checkExitProvider operation.
   * @callback module:api/ExitApi~checkExitProviderCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 퇴점 가능여부 확인
   * 퇴점 가능여부 확인
   * @param {module:api/ExitApi~checkExitProviderCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(ExitApi, [{
    key: "checkExitProvider",
    value: function checkExitProvider(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/exit/check', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the doExitProvider operation.
     * @callback module:api/ExitApi~doExitProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 퇴점신청
     * 퇴점신청
     * @param {Object.<String, Object>} reasons 퇴점 사유
     * @param {String} cancelPhoneNumber 퇴점 안내 받을 연락처
     * @param {module:api/ExitApi~doExitProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "doExitProvider",
    value: function doExitProvider(reasons, cancelPhoneNumber, callback) {
      var postBody = null; // verify the required parameter 'reasons' is set

      if (reasons === undefined || reasons === null) {
        throw new Error("Missing the required parameter 'reasons' when calling doExitProvider");
      } // verify the required parameter 'cancelPhoneNumber' is set


      if (cancelPhoneNumber === undefined || cancelPhoneNumber === null) {
        throw new Error("Missing the required parameter 'cancelPhoneNumber' when calling doExitProvider");
      }

      var pathParams = {
        'reasons': reasons,
        'cancelPhoneNumber': cancelPhoneNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/exit/check', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ExitApi;
}();

exports["default"] = ExitApi;
},{"../ApiClient":1}],17:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Faq service.
* @module api/FaqApi
* @version 0.0.1
*/
var FaqApi = /*#__PURE__*/function () {
  /**
  * Constructs a new FaqApi. 
  * @alias module:api/FaqApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function FaqApi(apiClient) {
    _classCallCheck(this, FaqApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getFaqBoard operation.
   * @callback module:api/FaqApi~getFaqBoardCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * FAQ 조회
   * @param {Object} opts Optional parameters
   * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
   * @param {String} opts.searchWord 검색어
   * @param {Date} opts.startDate 시작일
   * @param {Date} opts.endDate 종료일
   * @param {module:model/String} opts.sortBy 정렬 (default to 'none')
   * @param {module:model/String} opts.sortDirection 정렬 방향 (default to 'desc')
   * @param {Number} opts.page 페이지 번호
   * @param {Number} opts.pageSize 페이지당 노출 개수
   * @param {module:api/FaqApi~getFaqBoardCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(FaqApi, [{
    key: "getFaqBoard",
    value: function getFaqBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/faq', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return FaqApi;
}();

exports["default"] = FaqApi;
},{"../ApiClient":1}],18:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Forbidden service.
* @module api/ForbiddenApi
* @version 0.0.1
*/
var ForbiddenApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ForbiddenApi. 
  * @alias module:api/ForbiddenApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ForbiddenApi(apiClient) {
    _classCallCheck(this, ForbiddenApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getForbiddenWords operation.
   * @callback module:api/ForbiddenApi~getForbiddenWordsCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 금칙어 목록
   * @param {Object} opts Optional parameters
   * @param {String} opts.categoryCode 셀러허브 카테고리 코드
   * @param {String} opts.word 검색 금칙어
   * @param {module:model/String} opts.isAppliedSubCategory 하위카테고리 적용여부
   * @param {Array.<module:model/String>} opts._with WITH Model
   * @param {Number} opts.page 페이지 번호
   * @param {Number} opts.pageSize 페이지당 노출 개수
   * @param {module:api/ForbiddenApi~getForbiddenWordsCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(ForbiddenApi, [{
    key: "getForbiddenWords",
    value: function getForbiddenWords(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'categoryCode': opts['categoryCode'],
        'word': opts['word'],
        'isAppliedSubCategory': opts['isAppliedSubCategory'],
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi'),
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/forbiddenWords', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ForbiddenApi;
}();

exports["default"] = ForbiddenApi;
},{"../ApiClient":1}],19:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* HolidayNotice service.
* @module api/HolidayNoticeApi
* @version 0.0.1
*/
var HolidayNoticeApi = /*#__PURE__*/function () {
  /**
  * Constructs a new HolidayNoticeApi. 
  * @alias module:api/HolidayNoticeApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function HolidayNoticeApi(apiClient) {
    _classCallCheck(this, HolidayNoticeApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createHolidayNotice operation.
   * @callback module:api/HolidayNoticeApi~createHolidayNoticeCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 휴무 공지 등록
   * 휴무 공지 등록
   * @param {String} title 제목
   * @param {Date} startDate 휴무 시작일
   * @param {Date} endDate 휴무 종료일
   * @param {Date} orderDeadLineDate 주문 마감 일자
   * @param {Date} deliveryStartDate 배송 시작일
   * @param {Object} opts Optional parameters
   * @param {String} opts.contents 내용
   * @param {String} opts.image 휴무 공지 이미지 base64
   * @param {module:model/String} opts.isShow 노출 여부
   * @param {module:api/HolidayNoticeApi~createHolidayNoticeCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(HolidayNoticeApi, [{
    key: "createHolidayNotice",
    value: function createHolidayNotice(title, startDate, endDate, orderDeadLineDate, deliveryStartDate, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'title' is set

      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createHolidayNotice");
      } // verify the required parameter 'startDate' is set


      if (startDate === undefined || startDate === null) {
        throw new Error("Missing the required parameter 'startDate' when calling createHolidayNotice");
      } // verify the required parameter 'endDate' is set


      if (endDate === undefined || endDate === null) {
        throw new Error("Missing the required parameter 'endDate' when calling createHolidayNotice");
      } // verify the required parameter 'orderDeadLineDate' is set


      if (orderDeadLineDate === undefined || orderDeadLineDate === null) {
        throw new Error("Missing the required parameter 'orderDeadLineDate' when calling createHolidayNotice");
      } // verify the required parameter 'deliveryStartDate' is set


      if (deliveryStartDate === undefined || deliveryStartDate === null) {
        throw new Error("Missing the required parameter 'deliveryStartDate' when calling createHolidayNotice");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'title': title,
        'startDate': startDate,
        'endDate': endDate,
        'orderDeadLineDate': orderDeadLineDate,
        'deliveryStartDate': deliveryStartDate,
        'contents': opts['contents'],
        'image': opts['image'],
        'isShow': opts['isShow']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/holidayNotice', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteHolidayNoticeById operation.
     * @callback module:api/HolidayNoticeApi~deleteHolidayNoticeByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴무 공지 삭제
     * 휴무 공지 삭제
     * @param {module:api/HolidayNoticeApi~deleteHolidayNoticeByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteHolidayNoticeById",
    value: function deleteHolidayNoticeById(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/holidayNotice/{id}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getHolidayNotice operation.
     * @callback module:api/HolidayNoticeApi~getHolidayNoticeCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴무 공지 목록
     * 휴무 공지 목록
     * @param {module:api/HolidayNoticeApi~getHolidayNoticeCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getHolidayNotice",
    value: function getHolidayNotice(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/holidayNotice', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getHolidayNoticeById operation.
     * @callback module:api/HolidayNoticeApi~getHolidayNoticeByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴무 공지
     * 휴무 공지
     * @param {module:api/HolidayNoticeApi~getHolidayNoticeByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getHolidayNoticeById",
    value: function getHolidayNoticeById(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/holidayNotice/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateHolidayNoticeById operation.
     * @callback module:api/HolidayNoticeApi~updateHolidayNoticeByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴무 공지 수정
     * 휴무 공지 수정
     * @param {Object} opts Optional parameters
     * @param {String} opts.title 제목
     * @param {String} opts.contents 내용
     * @param {String} opts.image 휴무 공지 이미지 base64
     * @param {module:model/String} opts.isShow 노출 여부
     * @param {module:api/HolidayNoticeApi~updateHolidayNoticeByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateHolidayNoticeById",
    value: function updateHolidayNoticeById(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'title': opts['title'],
        'contents': opts['contents'],
        'image': opts['image'],
        'isShow': opts['isShow']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/holidayNotice/{id}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return HolidayNoticeApi;
}();

exports["default"] = HolidayNoticeApi;
},{"../ApiClient":1}],20:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* KakaoPay service.
* @module api/KakaoPayApi
* @version 0.0.1
*/
var KakaoPayApi = /*#__PURE__*/function () {
  /**
  * Constructs a new KakaoPayApi. 
  * @alias module:api/KakaoPayApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function KakaoPayApi(apiClient) {
    _classCallCheck(this, KakaoPayApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the call283fca4e62590665f3db9e7ba1abf39c operation.
   * @callback module:api/KakaoPayApi~call283fca4e62590665f3db9e7ba1abf39cCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 구독 결제 등록 준비 취소
   * 구독 결제 등록 준비 취소
   * @param {String} shToken 결제 요청 관리 키값
   * @param {module:api/KakaoPayApi~call283fca4e62590665f3db9e7ba1abf39cCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(KakaoPayApi, [{
    key: "call283fca4e62590665f3db9e7ba1abf39c",
    value: function call283fca4e62590665f3db9e7ba1abf39c(shToken, callback) {
      var postBody = null; // verify the required parameter 'shToken' is set

      if (shToken === undefined || shToken === null) {
        throw new Error("Missing the required parameter 'shToken' when calling call283fca4e62590665f3db9e7ba1abf39c");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'shToken': shToken
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/kakaoPay/subscribe/cancel', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the call5e85b8719a105c8bd65b5eac7d84fdc4 operation.
     * @callback module:api/KakaoPayApi~call5e85b8719a105c8bd65b5eac7d84fdc4Callback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 구독 결제 등록 준비
     * 구독 결제 등록 준비
     * @param {Object.<String, Object>} body 
     * @param {module:api/KakaoPayApi~call5e85b8719a105c8bd65b5eac7d84fdc4Callback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "call5e85b8719a105c8bd65b5eac7d84fdc4",
    value: function call5e85b8719a105c8bd65b5eac7d84fdc4(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling call5e85b8719a105c8bd65b5eac7d84fdc4");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/kakaoPay/subscribe/ready', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the eee5ed068b07412eecc9079210555b9a operation.
     * @callback module:api/KakaoPayApi~eee5ed068b07412eecc9079210555b9aCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 구독 결제 등록 준비 실패
     * 구독 결제 등록 준비 실패
     * @param {String} shToken 결제 요청 관리 키값
     * @param {module:api/KakaoPayApi~eee5ed068b07412eecc9079210555b9aCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "eee5ed068b07412eecc9079210555b9a",
    value: function eee5ed068b07412eecc9079210555b9a(shToken, callback) {
      var postBody = null; // verify the required parameter 'shToken' is set

      if (shToken === undefined || shToken === null) {
        throw new Error("Missing the required parameter 'shToken' when calling eee5ed068b07412eecc9079210555b9a");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'shToken': shToken
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/kakaoPay/subscribe/fail', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the ef0c8611ac5670707a9f5f8b649e31c0 operation.
     * @callback module:api/KakaoPayApi~ef0c8611ac5670707a9f5f8b649e31c0Callback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 구독 결제 승인 요청
     * 구독 결제 등록 요청
     * @param {module:api/KakaoPayApi~ef0c8611ac5670707a9f5f8b649e31c0Callback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "ef0c8611ac5670707a9f5f8b649e31c0",
    value: function ef0c8611ac5670707a9f5f8b649e31c0(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/kakaoPay/subscribe/approve', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return KakaoPayApi;
}();

exports["default"] = KakaoPayApi;
},{"../ApiClient":1}],21:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Login service.
* @module api/LoginApi
* @version 0.0.1
*/
var LoginApi = /*#__PURE__*/function () {
  /**
  * Constructs a new LoginApi. 
  * @alias module:api/LoginApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function LoginApi(apiClient) {
    _classCallCheck(this, LoginApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the login operation.
   * @callback module:api/LoginApi~loginCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 로그인
   * 사용자 로그인
   * @param {Object} opts Optional parameters
   * @param {String} opts.m_id 사용자아이디
   * @param {String} opts.password 사용자비밀번호
   * @param {module:api/LoginApi~loginCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(LoginApi, [{
    key: "login",
    value: function login(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'm_id': opts['m_id'],
        'password': opts['password']
      };
      var authNames = ['sanctum'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/login', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the logout operation.
     * @callback module:api/LoginApi~logoutCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그아웃
     * Destroy an authenticated session.
     * @param {module:api/LoginApi~logoutCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "logout",
    value: function logout(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/logout', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the subLogin operation.
     * @callback module:api/LoginApi~subLoginCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그인
     * @param {Object} opts Optional parameters
     * @param {String} opts.sadmid 사용자아이디
     * @param {String} opts.password 사용자비밀번호
     * @param {module:api/LoginApi~subLoginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "subLogin",
    value: function subLogin(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'sadmid': opts['sadmid'],
        'password': opts['password']
      };
      var authNames = ['sanctum'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/subLogin', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return LoginApi;
}();

exports["default"] = LoginApi;
},{"../ApiClient":1}],22:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateDetailInfoOfProviderRequest = _interopRequireDefault(require("../model/CreateDetailInfoOfProviderRequest"));

var _CreateStaffInfoOfProviderRequest = _interopRequireDefault(require("../model/CreateStaffInfoOfProviderRequest"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Manage service.
* @module api/ManageApi
* @version 0.0.1
*/
var ManageApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ManageApi. 
  * @alias module:api/ManageApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ManageApi(apiClient) {
    _classCallCheck(this, ManageApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createDeliveryNotice operation.
   * @callback module:api/ManageApi~createDeliveryNoticeCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 기본관리 - 배송공지 등록
   * 기본관리 - 배송공지 등록
   * @param {String} image 배송 공지 이미지 base64
   * @param {Date} expiredAt 만기일자
   * @param {module:api/ManageApi~createDeliveryNoticeCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(ManageApi, [{
    key: "createDeliveryNotice",
    value: function createDeliveryNotice(image, expiredAt, callback) {
      var postBody = null; // verify the required parameter 'image' is set

      if (image === undefined || image === null) {
        throw new Error("Missing the required parameter 'image' when calling createDeliveryNotice");
      } // verify the required parameter 'expiredAt' is set


      if (expiredAt === undefined || expiredAt === null) {
        throw new Error("Missing the required parameter 'expiredAt' when calling createDeliveryNotice");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'image': image,
        'expiredAt': expiredAt
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/deliveryNotice', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createDetailInfoOfProvider operation.
     * @callback module:api/ManageApi~createDetailInfoOfProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 입점사 상세정보 관리
     * 기본관리 - 입점사 상세정보 관리
     * @param {module:model/CreateDetailInfoOfProviderRequest} CreateDetailInfoOfProviderRequest 
     * @param {module:api/ManageApi~createDetailInfoOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createDetailInfoOfProvider",
    value: function createDetailInfoOfProvider(CreateDetailInfoOfProviderRequest, callback) {
      var postBody = CreateDetailInfoOfProviderRequest; // verify the required parameter 'CreateDetailInfoOfProviderRequest' is set

      if (CreateDetailInfoOfProviderRequest === undefined || CreateDetailInfoOfProviderRequest === null) {
        throw new Error("Missing the required parameter 'CreateDetailInfoOfProviderRequest' when calling createDetailInfoOfProvider");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/detailInfo', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createSellerPlanOfProvider operation.
     * @callback module:api/ManageApi~createSellerPlanOfProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청
     * 기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청
     * @param {Object} opts Optional parameters
     * @param {String} opts.planCode 셀러허브 카테고리 코드
     * @param {Boolean} opts.doPlanUpgrade 플랜 업그레이드 진행여부
     * @param {module:api/ManageApi~createSellerPlanOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createSellerPlanOfProvider",
    value: function createSellerPlanOfProvider(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'planCode': opts['planCode'],
        'doPlanUpgrade': opts['doPlanUpgrade']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/sellerPlan', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createStaffInfoOfProvider operation.
     * @callback module:api/ManageApi~createStaffInfoOfProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 담당자 정보 설정
     * 기본관리 - 담당자 정보 설정
     * @param {module:model/CreateStaffInfoOfProviderRequest} CreateStaffInfoOfProviderRequest 
     * @param {module:api/ManageApi~createStaffInfoOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createStaffInfoOfProvider",
    value: function createStaffInfoOfProvider(CreateStaffInfoOfProviderRequest, callback) {
      var postBody = CreateStaffInfoOfProviderRequest; // verify the required parameter 'CreateStaffInfoOfProviderRequest' is set

      if (CreateStaffInfoOfProviderRequest === undefined || CreateStaffInfoOfProviderRequest === null) {
        throw new Error("Missing the required parameter 'CreateStaffInfoOfProviderRequest' when calling createStaffInfoOfProvider");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/staff', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteDeliveryNotice operation.
     * @callback module:api/ManageApi~deleteDeliveryNoticeCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 배송공지 삭제
     * 기본관리 - 배송공지 삭제
     * @param {module:api/ManageApi~deleteDeliveryNoticeCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteDeliveryNotice",
    value: function deleteDeliveryNotice(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/deliveryNotice', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ManageApi;
}();

exports["default"] = ManageApi;
},{"../ApiClient":1,"../model/CreateDetailInfoOfProviderRequest":45,"../model/CreateStaffInfoOfProviderRequest":52}],23:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _BoardManual = _interopRequireDefault(require("../model/BoardManual"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Manual service.
* @module api/ManualApi
* @version 0.0.1
*/
var ManualApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ManualApi. 
  * @alias module:api/ManualApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ManualApi(apiClient) {
    _classCallCheck(this, ManualApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getManualBoard operation.
   * @callback module:api/ManualApi~getManualBoardCallback
   * @param {String} error Error message, if any.
   * @param {Array.<module:model/BoardManual>} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 공급사 매뉴얼 목록
   * @param {Object} opts Optional parameters
   * @param {Date} opts.startDate 등록 시작일
   * @param {Date} opts.endDate 등록 종료일
   * @param {module:model/String} opts.searchType 검색 타입
   * @param {String} opts.searchContents 검색 내용
   * @param {Number} opts.page 페이지 번호
   * @param {Number} opts.pageSize 페이지당 노출 개수
   * @param {module:api/ManualApi~getManualBoardCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Array.<module:model/BoardManual>}
   */


  _createClass(ManualApi, [{
    key: "getManualBoard",
    value: function getManualBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchType': opts['searchType'],
        'searchContents': opts['searchContents'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_BoardManual["default"]];
      return this.apiClient.callApi('/shapi/v1/board/manuals', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getManualBoardByNo operation.
     * @callback module:api/ManualApi~getManualBoardByNoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/BoardManual} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 매뉴얼
     * @param {Number} manualNumber 공급사 매뉴얼 번호
     * @param {module:api/ManualApi~getManualBoardByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/BoardManual}
     */

  }, {
    key: "getManualBoardByNo",
    value: function getManualBoardByNo(manualNumber, callback) {
      var postBody = null; // verify the required parameter 'manualNumber' is set

      if (manualNumber === undefined || manualNumber === null) {
        throw new Error("Missing the required parameter 'manualNumber' when calling getManualBoardByNo");
      }

      var pathParams = {
        'manualNumber': manualNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _BoardManual["default"];
      return this.apiClient.callApi('/shapi/v1/board/manuals/{manualNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ManualApi;
}();

exports["default"] = ManualApi;
},{"../ApiClient":1,"../model/BoardManual":41}],24:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Marketing service.
* @module api/MarketingApi
* @version 0.0.1
*/
var MarketingApi = /*#__PURE__*/function () {
  /**
  * Constructs a new MarketingApi. 
  * @alias module:api/MarketingApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function MarketingApi(apiClient) {
    _classCallCheck(this, MarketingApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createMarketingEvent operation.
   * @callback module:api/MarketingApi~createMarketingEventCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 행사신청 게시물 등록
   * @param {Object} name 신청자이름
   * @param {Object} email 신청자이메일
   * @param {Object} phoneNumber 신청자연락처
   * @param {Object} title 행사제목
   * @param {Object} contents 행사내용
   * @param {module:model/String} isCoupon 쿠폰사용여부
   * @param {Number} returnDeliveryCharge 반품배송비(편도)
   * @param {Number} exchangeDeliveryCharge 교환배송비(왕복)
   * @param {Object} opts Optional parameters
   * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
   * @param {module:api/MarketingApi~createMarketingEventCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(MarketingApi, [{
    key: "createMarketingEvent",
    value: function createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'name' is set

      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createMarketingEvent");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling createMarketingEvent");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling createMarketingEvent");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createMarketingEvent");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEvent");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling createMarketingEvent");
      } // verify the required parameter 'returnDeliveryCharge' is set


      if (returnDeliveryCharge === undefined || returnDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'returnDeliveryCharge' when calling createMarketingEvent");
      } // verify the required parameter 'exchangeDeliveryCharge' is set


      if (exchangeDeliveryCharge === undefined || exchangeDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'exchangeDeliveryCharge' when calling createMarketingEvent");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate'],
        'returnDeliveryCharge': returnDeliveryCharge,
        'exchangeDeliveryCharge': exchangeDeliveryCharge
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingEventProposalRequestByNo operation.
     * @callback module:api/MarketingApi~createMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/MarketingApi~createMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingEventProposalRequestByNo",
    value: function createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling createMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventProposalNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventProposalNumber}', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingEventReplyByNo operation.
     * @callback module:api/MarketingApi~createMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 등록
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} contents 댓글내용
     * @param {module:api/MarketingApi~createMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingEventReplyByNo",
    value: function createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createMarketingEventReplyByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'contents': contents
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingPromotioon operation.
     * @callback module:api/MarketingApi~createMarketingPromotioonCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 마케팅 프로모션 동의
     * 프로모션 동의
     * @param {Object} opts Optional parameters
     * @param {Number} opts.agreeNumber 프로모션 번호
     * @param {module:model/String} opts.agreeCheck 프로모션 동의 여부
     * @param {module:api/MarketingApi~createMarketingPromotioonCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingPromotioon",
    value: function createMarketingPromotioon(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'agreeNumber': opts['agreeNumber'],
        'agreeCheck': opts['agreeCheck']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/promotion', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteMarketingEventByNo operation.
     * @callback module:api/MarketingApi~deleteMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 삭제
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/MarketingApi~deleteMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteMarketingEventByNo",
    value: function deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteMarketingEventByNo");
      }

      var pathParams = {
        'boardNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEvent operation.
     * @callback module:api/MarketingApi~getMarketingEventCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 목록 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.isApprove 승인 여부 (default to 'all')
     * @param {module:model/String} opts.isAnswer 답변 여부 (default to 'all')
     * @param {module:model/String} opts.searchDateType 검색 일자 설정 (default to 'registed')
     * @param {Number} opts.startDate 시작일
     * @param {Number} opts.endDate 종료일
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/MarketingApi~getMarketingEventCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEvent",
    value: function getMarketingEvent(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'isApprove': opts['isApprove'],
        'isAnswer': opts['isAnswer'],
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventApprovalByNo operation.
     * @callback module:api/MarketingApi~getMarketingEventApprovalByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 재신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/MarketingApi~getMarketingEventApprovalByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventApprovalByNo",
    value: function getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventApprovalByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/approval/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventByNo operation.
     * @callback module:api/MarketingApi~getMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/MarketingApi~getMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventByNo",
    value: function getMarketingEventByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposal operation.
     * @callback module:api/MarketingApi~getMarketingEventProposalCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 목록 조회
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts.channel 행사 채널
     * @param {module:model/String} opts.searchDateType 검색 일자 설정 (default to 'startDate')
     * @param {Number} opts.startDate 시작일
     * @param {Number} opts.endDate 종료일
     * @param {Array.<String>} opts.category 카테고리
     * @param {module:model/String} opts.eventStatus 행사진행여부 (default to 'all')
     * @param {module:model/String} opts.requestStatus 행사신청여부 (default to 'all')
     * @param {module:model/String} opts.isApprove 답변여부 (default to 'all')
     * @param {module:model/String} opts.searchKey 검색키 (default to 'title')
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/MarketingApi~getMarketingEventProposalCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposal",
    value: function getMarketingEventProposal(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'channel': this.apiClient.buildCollectionParam(opts['channel'], 'multi'),
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'category': this.apiClient.buildCollectionParam(opts['category'], 'multi'),
        'eventStatus': opts['eventStatus'],
        'requestStatus': opts['requestStatus'],
        'isApprove': opts['isApprove'],
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposalByNo operation.
     * @callback module:api/MarketingApi~getMarketingEventProposalByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 내역
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {module:api/MarketingApi~getMarketingEventProposalByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposalByNo",
    value: function getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventProposalByNo");
      }

      var pathParams = {
        'eventProposalNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/{eventProposalNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposalRequestByNo operation.
     * @callback module:api/MarketingApi~getMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청내역
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {module:api/MarketingApi~getMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposalRequestByNo",
    value: function getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventReplyByNo operation.
     * @callback module:api/MarketingApi~getMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 목록 조회
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/MarketingApi~getMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventReplyByNo",
    value: function getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingPromotioon operation.
     * @callback module:api/MarketingApi~getMarketingPromotioonCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 마케팅 프로모션 리스트
     * 프로모션목록
     * @param {Object} opts Optional parameters
     * @param {Date} opts.startDate 시작 일자
     * @param {Date} opts.endDate 종료 일자
     * @param {module:model/String} opts.searchType 검색 옵션
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/MarketingApi~getMarketingPromotioonCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingPromotioon",
    value: function getMarketingPromotioon(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchType': opts['searchType'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/promotion', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateMarketingEventByNo operation.
     * @callback module:api/MarketingApi~updateMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 등록
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Number} returnDeliveryCharge 반품배송비(편도)
     * @param {Number} exchangeDeliveryCharge 교환배송비(왕복)
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/MarketingApi~updateMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateMarketingEventByNo",
    value: function updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling updateMarketingEventByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateMarketingEventByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling updateMarketingEventByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling updateMarketingEventByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling updateMarketingEventByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateMarketingEventByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling updateMarketingEventByNo");
      } // verify the required parameter 'returnDeliveryCharge' is set


      if (returnDeliveryCharge === undefined || returnDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'returnDeliveryCharge' when calling updateMarketingEventByNo");
      } // verify the required parameter 'exchangeDeliveryCharge' is set


      if (exchangeDeliveryCharge === undefined || exchangeDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'exchangeDeliveryCharge' when calling updateMarketingEventByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate'],
        'returnDeliveryCharge': returnDeliveryCharge,
        'exchangeDeliveryCharge': exchangeDeliveryCharge
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateMarketingEventProposalRequestByNo operation.
     * @callback module:api/MarketingApi~updateMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청내용 재신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 신청 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/MarketingApi~updateMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateMarketingEventProposalRequestByNo",
    value: function updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling updateMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return MarketingApi;
}();

exports["default"] = MarketingApi;
},{"../ApiClient":1}],25:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _BoardNotice = _interopRequireDefault(require("../model/BoardNotice"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Notice service.
* @module api/NoticeApi
* @version 0.0.1
*/
var NoticeApi = /*#__PURE__*/function () {
  /**
  * Constructs a new NoticeApi. 
  * @alias module:api/NoticeApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function NoticeApi(apiClient) {
    _classCallCheck(this, NoticeApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getNoticeBoard operation.
   * @callback module:api/NoticeApi~getNoticeBoardCallback
   * @param {String} error Error message, if any.
   * @param {Array.<module:model/BoardNotice>} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 공급사 공지사항 조회
   * @param {Object} opts Optional parameters
   * @param {module:model/String} opts.searchKey 검색키
   * @param {String} opts.searchWord 검색어
   * @param {Date} opts.startDate 시작일
   * @param {Date} opts.endDate 종료일
   * @param {module:model/String} opts.isPopup 팝업 여부
   * @param {module:model/String} opts.isNotice 공지 여부
   * @param {Array.<module:model/String>} opts.popupPositions 팝업 노출 위치
   * @param {module:model/String} opts.boardStatus 공지 상태
   * @param {module:model/String} opts.isAgree 동의 여부
   * @param {module:model/String} opts.isCurrentVisible 현재 노출 여부
   * @param {module:model/String} opts.sortBy 정렬
   * @param {module:model/String} opts.sortDirection 정렬 방향
   * @param {Number} opts.page 페이지 번호
   * @param {Number} opts.pageSize 페이지당 노출 개수
   * @param {module:api/NoticeApi~getNoticeBoardCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Array.<module:model/BoardNotice>}
   */


  _createClass(NoticeApi, [{
    key: "getNoticeBoard",
    value: function getNoticeBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'isPopup': opts['isPopup'],
        'isNotice': opts['isNotice'],
        'popupPositions': this.apiClient.buildCollectionParam(opts['popupPositions'], 'multi'),
        'boardStatus': opts['boardStatus'],
        'isAgree': opts['isAgree'],
        'isCurrentVisible': opts['isCurrentVisible'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_BoardNotice["default"]];
      return this.apiClient.callApi('/shapi/v1/board/notices', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getNoticeBoardByNo operation.
     * @callback module:api/NoticeApi~getNoticeBoardByNoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/BoardNotice} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 공지사항
     * @param {Number} noticeNumber 공급사 공지사항 번호
     * @param {module:api/NoticeApi~getNoticeBoardByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/BoardNotice}
     */

  }, {
    key: "getNoticeBoardByNo",
    value: function getNoticeBoardByNo(noticeNumber, callback) {
      var postBody = null; // verify the required parameter 'noticeNumber' is set

      if (noticeNumber === undefined || noticeNumber === null) {
        throw new Error("Missing the required parameter 'noticeNumber' when calling getNoticeBoardByNo");
      }

      var pathParams = {
        'noticeNumber': noticeNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _BoardNotice["default"];
      return this.apiClient.callApi('/shapi/v1/board/notices/{noticeNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return NoticeApi;
}();

exports["default"] = NoticeApi;
},{"../ApiClient":1,"../model/BoardNotice":42}],26:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Order service.
* @module api/OrderApi
* @version 0.0.1
*/
var OrderApi = /*#__PURE__*/function () {
  /**
  * Constructs a new OrderApi. 
  * @alias module:api/OrderApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function OrderApi(apiClient) {
    _classCallCheck(this, OrderApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the call384c0bc85845dc729988acf253b757f2 operation.
   * @callback module:api/OrderApi~call384c0bc85845dc729988acf253b757f2Callback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * @param {module:model/null} UNKNOWN_PARAMETER_NAME 주문번호
   * @param {module:api/OrderApi~call384c0bc85845dc729988acf253b757f2Callback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(OrderApi, [{
    key: "call384c0bc85845dc729988acf253b757f2",
    value: function call384c0bc85845dc729988acf253b757f2(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling call384c0bc85845dc729988acf253b757f2");
      }

      var pathParams = {
        'order': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/{ordno}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCancelOrder operation.
     * @callback module:api/OrderApi~getCancelOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문취소 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/Array} opts.cancelType 주문상태
     * @param {module:model/String} opts.settlekind 결제방법 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/OrderApi~getCancelOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCancelOrder",
    value: function getCancelOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'cancelType': opts['cancelType'],
        'settlekind': opts['settlekind'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/cancel', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getExchangeOrder operation.
     * @callback module:api/OrderApi~getExchangeOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 교환접수/완료 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.exchangeType 교환상태 (default to '')
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/String} opts.cancelkey 상태 (default to 'all')
     * @param {module:model/String} opts.exchange_flag 교환상태 (default to 'all')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/OrderApi~getExchangeOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getExchangeOrder",
    value: function getExchangeOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'exchangeType': opts['exchangeType'],
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'cancelkey': opts['cancelkey'],
        'exchange_flag': opts['exchange_flag'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/exchnage', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getOrder operation.
     * @callback module:api/OrderApi~getOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to '')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/String} opts.sgkey 상품검색 옵션 (default to '')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 상품검색 키워드
     * @param {module:model/String} opts.dtkind 처리일자 (옵션) (default to 'orddt')
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/String} opts.step2false 정상주문만 조회 (default to '')
     * @param {module:model/String} opts.step 주문상태 (default to '')
     * @param {module:model/String} opts.step2 주문상태 (default to '')
     * @param {module:model/String} opts.exchange_flag 주문상태 (default to '')
     * @param {module:model/String} opts.bundle_deli_cnt 주문상태 (default to '')
     * @param {module:model/String} opts.saleType 판매처 (default to '')
     * @param {module:model/String} opts.order_delayed 배송지연 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/OrderApi~getOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getOrder",
    value: function getOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'sgkey': opts['sgkey'],
        'sgword': opts['UNKNOWN_PARAMETER_NAME2'],
        'dtkind': opts['dtkind'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'step2false': opts['step2false'],
        'step': opts['step'],
        'step2': opts['step2'],
        'exchange_flag': opts['exchange_flag'],
        'bundle_deli_cnt': opts['bundle_deli_cnt'],
        'saleType': opts['saleType'],
        'order_delayed': opts['order_delayed'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getOrderCsMemoByNo operation.
     * @callback module:api/OrderApi~getOrderCsMemoByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * cs 메모
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 주문번호
     * @param {module:api/OrderApi~getOrderCsMemoByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getOrderCsMemoByNo",
    value: function getOrderCsMemoByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getOrderCsMemoByNo");
      }

      var pathParams = {
        'ordno': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/csMemo/{ordno}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProcessingOrder operation.
     * @callback module:api/OrderApi~getProcessingOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 배송준비 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/String} opts.dtkind 처리일자 (옵션) (default to 'orddt')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 처리일자 (키워드)
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 배송준비상태
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/OrderApi~getProcessingOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProcessingOrder",
    value: function getProcessingOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'dtkind': opts['dtkind'],
        'regdt': opts['UNKNOWN_PARAMETER_NAME2'],
        'srstatus': opts['UNKNOWN_PARAMETER_NAME3'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/processing', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getRefundOrder operation.
     * @callback module:api/OrderApi~getRefundOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 반품접수/완료 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/String} opts.dtkind 반품요청일 (옵션) (default to 'regdt')
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/Array} opts.cancelstatus 상태
     * @param {module:model/String} opts.cls 반품완료 여부 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/OrderApi~getRefundOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getRefundOrder",
    value: function getRefundOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'dtkind': opts['dtkind'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'cancelstatus': opts['cancelstatus'],
        'cls': opts['cls'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/refund', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateOrderDeliveryStatus operation.
     * @callback module:api/OrderApi~updateOrderDeliveryStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 배송상태 변경
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 아이템 sno
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 현재 istep
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 현재 istep2
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME4 발송예정일
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME5 발송지연 예정일
     * @param {module:model/String} opts.rstatus 배송상태 변경값 (default to '')
     * @param {module:api/OrderApi~updateOrderDeliveryStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateOrderDeliveryStatus",
    value: function updateOrderDeliveryStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'itemsSno': opts['UNKNOWN_PARAMETER_NAME'],
        'viewIstep': opts['UNKNOWN_PARAMETER_NAME2'],
        'viewIstep2': opts['UNKNOWN_PARAMETER_NAME3'],
        'rdelivery': opts['UNKNOWN_PARAMETER_NAME4'],
        'rdelivery2': opts['UNKNOWN_PARAMETER_NAME5'],
        'rstatus': opts['rstatus']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/deliveryStatus', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateOrderStatus operation.
     * @callback module:api/OrderApi~updateOrderStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문상태 변경
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문상태값
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 아이템 sno
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 현재 istep
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME4 현재 istep2
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME5 발송예정일
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME6 발송지연 예정일
     * @param {module:model/String} opts.rstatus 배송상태 변경값 (default to '')
     * @param {module:api/OrderApi~updateOrderStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateOrderStatus",
    value: function updateOrderStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'case': opts['UNKNOWN_PARAMETER_NAME'],
        'itemsSno': opts['UNKNOWN_PARAMETER_NAME2'],
        'viewIstep': opts['UNKNOWN_PARAMETER_NAME3'],
        'viewIstep2': opts['UNKNOWN_PARAMETER_NAME4'],
        'rdelivery': opts['UNKNOWN_PARAMETER_NAME5'],
        'rdelivery2': opts['UNKNOWN_PARAMETER_NAME6'],
        'rstatus': opts['rstatus']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/orderStatus', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateOrderToRefund operation.
     * @callback module:api/OrderApi~updateOrderToRefundCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 반품처리
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문취소sno
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 반품택배사
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 반품택배 코드
     * @param {module:api/OrderApi~updateOrderToRefundCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateOrderToRefund",
    value: function updateOrderToRefund(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'cancelSno': opts['UNKNOWN_PARAMETER_NAME'],
        'delivery': opts['UNKNOWN_PARAMETER_NAME2'],
        'delivery_code': opts['UNKNOWN_PARAMETER_NAME3']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/refund', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return OrderApi;
}();

exports["default"] = OrderApi;
},{"../ApiClient":1}],27:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Pause service.
* @module api/PauseApi
* @version 0.0.1
*/
var PauseApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PauseApi. 
  * @alias module:api/PauseApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PauseApi(apiClient) {
    _classCallCheck(this, PauseApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the pauseProvider operation.
   * @callback module:api/PauseApi~pauseProviderCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 계약 일시정지
   * 계약 일시정지
   * @param {module:api/PauseApi~pauseProviderCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(PauseApi, [{
    key: "pauseProvider",
    value: function pauseProvider(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/pause', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PauseApi;
}();

exports["default"] = PauseApi;
},{"../ApiClient":1}],28:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Payment service.
* @module api/PaymentApi
* @version 0.0.1
*/
var PaymentApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PaymentApi. 
  * @alias module:api/PaymentApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PaymentApi(apiClient) {
    _classCallCheck(this, PaymentApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getAvailablePaymentMethods operation.
   * @callback module:api/PaymentApi~getAvailablePaymentMethodsCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 결제 수단 등록 목록
   * 결제 수단 등록 목록
   * @param {module:api/PaymentApi~getAvailablePaymentMethodsCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(PaymentApi, [{
    key: "getAvailablePaymentMethods",
    value: function getAvailablePaymentMethods(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/paymentMethods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPaymentHistoryById operation.
     * @callback module:api/PaymentApi~getPaymentHistoryByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 내역
     * 결제 내역
     * @param {Object} opts Optional parameters
     * @param {Number} opts.id 결제 내역 고유 ID
     * @param {module:api/PaymentApi~getPaymentHistoryByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPaymentHistoryById",
    value: function getPaymentHistoryById(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {
        'id': opts['id']
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/payments/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPaymentMethodById operation.
     * @callback module:api/PaymentApi~getPaymentMethodByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 수단 등록 내용
     * 결제 수단 등록 내용
     * @param {module:api/PaymentApi~getPaymentMethodByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPaymentMethodById",
    value: function getPaymentMethodById(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/paymentMethods/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the geyPaymentHistory operation.
     * @callback module:api/PaymentApi~geyPaymentHistoryCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 내역 목록
     * 결제 내역 목록
     * @param {Object} opts Optional parameters
     * @param {Object.<String, Object>} opts.body 
     * @param {module:api/PaymentApi~geyPaymentHistoryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "geyPaymentHistory",
    value: function geyPaymentHistory(opts, callback) {
      opts = opts || {};
      var postBody = opts['body'];
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/payments', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the registPaymentMethod operation.
     * @callback module:api/PaymentApi~registPaymentMethodCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 수단 등록
     * 결제 수단 등록
     * @param {Object.<String, Object>} body 
     * @param {module:api/PaymentApi~registPaymentMethodCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "registPaymentMethod",
    value: function registPaymentMethod(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling registPaymentMethod");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/paymentMethods', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PaymentApi;
}();

exports["default"] = PaymentApi;
},{"../ApiClient":1}],29:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Penalty service.
* @module api/PenaltyApi
* @version 0.0.1
*/
var PenaltyApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PenaltyApi. 
  * @alias module:api/PenaltyApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PenaltyApi(apiClient) {
    _classCallCheck(this, PenaltyApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createPenaltyObjectionByNo operation.
   * @callback module:api/PenaltyApi~createPenaltyObjectionByNoCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 패널티 이이제기
   * @param {Number} penaltySeq 패널티 번호
   * @param {Object} opts Optional parameters
   * @param {String} opts.contents 이이제기 사유
   * @param {module:api/PenaltyApi~createPenaltyObjectionByNoCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(PenaltyApi, [{
    key: "createPenaltyObjectionByNo",
    value: function createPenaltyObjectionByNo(penaltySeq, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'penaltySeq' is set

      if (penaltySeq === undefined || penaltySeq === null) {
        throw new Error("Missing the required parameter 'penaltySeq' when calling createPenaltyObjectionByNo");
      }

      var pathParams = {
        'penaltySeq': penaltySeq
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'contents': opts['contents']
      };
      var authNames = ['sanctum'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/penalty/{penaltySeq}/objections', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPenalty operation.
     * @callback module:api/PenaltyApi~getPenaltyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 패널티 목록
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.status 상태 (default to '')
     * @param {module:model/String} opts.penaltyType 패널티 타입 (default to '')
     * @param {module:api/PenaltyApi~getPenaltyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPenalty",
    value: function getPenalty(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'status': opts['status'],
        'penaltyType': opts['penaltyType']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/penalty', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PenaltyApi;
}();

exports["default"] = PenaltyApi;
},{"../ApiClient":1}],30:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* PreCalculate service.
* @module api/PreCalculateApi
* @version 0.0.1
*/
var PreCalculateApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PreCalculateApi. 
  * @alias module:api/PreCalculateApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PreCalculateApi(apiClient) {
    _classCallCheck(this, PreCalculateApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getApplicablePreCalculate operation.
   * @callback module:api/PreCalculateApi~getApplicablePreCalculateCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 신청가능금액
   * @param {module:api/PreCalculateApi~getApplicablePreCalculateCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(PreCalculateApi, [{
    key: "getApplicablePreCalculate",
    value: function getApplicablePreCalculate(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/preCalculate/applicable', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPreCalculate operation.
     * @callback module:api/PreCalculateApi~getPreCalculateCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 빠른정산 목록
     * @param {module:api/PreCalculateApi~getPreCalculateCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPreCalculate",
    value: function getPreCalculate(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/preCalculate', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPreCalculateRequest operation.
     * @callback module:api/PreCalculateApi~getPreCalculateRequestCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 신청 목록
     * @param {module:api/PreCalculateApi~getPreCalculateRequestCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPreCalculateRequest",
    value: function getPreCalculateRequest(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/preCalculate/request', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PreCalculateApi;
}();

exports["default"] = PreCalculateApi;
},{"../ApiClient":1}],31:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _A8a15b67230f3e43ed1bb29ee377c809Request = _interopRequireDefault(require("../model/A8a15b67230f3e43ed1bb29ee377c809Request"));

var _CreateProductRequest = _interopRequireDefault(require("../model/CreateProductRequest"));

var _Model219f3041d050108b1a2e51838473f6d8Request = _interopRequireDefault(require("../model/Model219f3041d050108b1a2e51838473f6d8Request"));

var _Model69ec8f3e26fa2b1b02ce4897936e91e4Request = _interopRequireDefault(require("../model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request"));

var _UpdateProductByNoRequest = _interopRequireDefault(require("../model/UpdateProductByNoRequest"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Product service.
* @module api/ProductApi
* @version 0.0.1
*/
var ProductApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ProductApi. 
  * @alias module:api/ProductApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ProductApi(apiClient) {
    _classCallCheck(this, ProductApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the a8a15b67230f3e43ed1bb29ee377c809 operation.
   * @callback module:api/ProductApi~a8a15b67230f3e43ed1bb29ee377c809Callback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 상품 가격 수정
   * @param {Number} productNumber 셀러허브 상품 일련번호
   * @param {module:model/A8a15b67230f3e43ed1bb29ee377c809Request} A8a15b67230f3e43ed1bb29ee377c809Request 
   * @param {module:api/ProductApi~a8a15b67230f3e43ed1bb29ee377c809Callback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(ProductApi, [{
    key: "a8a15b67230f3e43ed1bb29ee377c809",
    value: function a8a15b67230f3e43ed1bb29ee377c809(productNumber, A8a15b67230f3e43ed1bb29ee377c809Request, callback) {
      var postBody = A8a15b67230f3e43ed1bb29ee377c809Request; // verify the required parameter 'productNumber' is set

      if (productNumber === undefined || productNumber === null) {
        throw new Error("Missing the required parameter 'productNumber' when calling a8a15b67230f3e43ed1bb29ee377c809");
      } // verify the required parameter 'A8a15b67230f3e43ed1bb29ee377c809Request' is set


      if (A8a15b67230f3e43ed1bb29ee377c809Request === undefined || A8a15b67230f3e43ed1bb29ee377c809Request === null) {
        throw new Error("Missing the required parameter 'A8a15b67230f3e43ed1bb29ee377c809Request' when calling a8a15b67230f3e43ed1bb29ee377c809");
      }

      var pathParams = {
        'productNumber': productNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products/{productNumber}/price', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the call219f3041d050108b1a2e51838473f6d8 operation.
     * @callback module:api/ProductApi~call219f3041d050108b1a2e51838473f6d8Callback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 품절 처리
     * @param {Number} productNumber 셀러허브 상품 일련번호
     * @param {module:model/Model219f3041d050108b1a2e51838473f6d8Request} Model219f3041d050108b1a2e51838473f6d8Request 
     * @param {module:api/ProductApi~call219f3041d050108b1a2e51838473f6d8Callback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "call219f3041d050108b1a2e51838473f6d8",
    value: function call219f3041d050108b1a2e51838473f6d8(productNumber, Model219f3041d050108b1a2e51838473f6d8Request, callback) {
      var postBody = Model219f3041d050108b1a2e51838473f6d8Request; // verify the required parameter 'productNumber' is set

      if (productNumber === undefined || productNumber === null) {
        throw new Error("Missing the required parameter 'productNumber' when calling call219f3041d050108b1a2e51838473f6d8");
      } // verify the required parameter 'Model219f3041d050108b1a2e51838473f6d8Request' is set


      if (Model219f3041d050108b1a2e51838473f6d8Request === undefined || Model219f3041d050108b1a2e51838473f6d8Request === null) {
        throw new Error("Missing the required parameter 'Model219f3041d050108b1a2e51838473f6d8Request' when calling call219f3041d050108b1a2e51838473f6d8");
      }

      var pathParams = {
        'productNumber': productNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products/{productNumber}/runout', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the call69ec8f3e26fa2b1b02ce4897936e91e4 operation.
     * @callback module:api/ProductApi~call69ec8f3e26fa2b1b02ce4897936e91e4Callback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 재고 수정
     * @param {Number} productNumber 셀러허브 상품 일련번호
     * @param {module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request} Model69ec8f3e26fa2b1b02ce4897936e91e4Request 
     * @param {module:api/ProductApi~call69ec8f3e26fa2b1b02ce4897936e91e4Callback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "call69ec8f3e26fa2b1b02ce4897936e91e4",
    value: function call69ec8f3e26fa2b1b02ce4897936e91e4(productNumber, Model69ec8f3e26fa2b1b02ce4897936e91e4Request, callback) {
      var postBody = Model69ec8f3e26fa2b1b02ce4897936e91e4Request; // verify the required parameter 'productNumber' is set

      if (productNumber === undefined || productNumber === null) {
        throw new Error("Missing the required parameter 'productNumber' when calling call69ec8f3e26fa2b1b02ce4897936e91e4");
      } // verify the required parameter 'Model69ec8f3e26fa2b1b02ce4897936e91e4Request' is set


      if (Model69ec8f3e26fa2b1b02ce4897936e91e4Request === undefined || Model69ec8f3e26fa2b1b02ce4897936e91e4Request === null) {
        throw new Error("Missing the required parameter 'Model69ec8f3e26fa2b1b02ce4897936e91e4Request' when calling call69ec8f3e26fa2b1b02ce4897936e91e4");
      }

      var pathParams = {
        'productNumber': productNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products/{productNumber}/stock', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createProduct operation.
     * @callback module:api/ProductApi~createProductCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 등록
     * 상품 등록
     * @param {module:model/CreateProductRequest} CreateProductRequest 
     * @param {module:api/ProductApi~createProductCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createProduct",
    value: function createProduct(CreateProductRequest, callback) {
      var postBody = CreateProductRequest; // verify the required parameter 'CreateProductRequest' is set

      if (CreateProductRequest === undefined || CreateProductRequest === null) {
        throw new Error("Missing the required parameter 'CreateProductRequest' when calling createProduct");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getForbiddenWords operation.
     * @callback module:api/ProductApi~getForbiddenWordsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 금칙어 목록
     * @param {Object} opts Optional parameters
     * @param {String} opts.categoryCode 셀러허브 카테고리 코드
     * @param {String} opts.word 검색 금칙어
     * @param {module:model/String} opts.isAppliedSubCategory 하위카테고리 적용여부
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/ProductApi~getForbiddenWordsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getForbiddenWords",
    value: function getForbiddenWords(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'categoryCode': opts['categoryCode'],
        'word': opts['word'],
        'isAppliedSubCategory': opts['isAppliedSubCategory'],
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi'),
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/forbiddenWords', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProduct operation.
     * @callback module:api/ProductApi~getProductCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 목록
     * 상품 목록
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.target 상품 타입
     * @param {Array.<module:model/String>} opts.approveTypes 상품 승인 타입
     * @param {String} opts.categoryCode 셀러허브 카테고리 코드
     * @param {module:model/String} opts.searchType 검색 타겟
     * @param {String} opts.searchContent 검색 내용
     * @param {module:model/String} opts.isAbsoluteSearch 유사 검색 제외
     * @param {Number} opts.brandNumber 브랜드 고유번호
     * @param {Number} opts.minProductPrice 최소 상품 가격
     * @param {Number} opts.maxProductPrice 최대 상품 가격
     * @param {Number} opts.minProductSupplyPrice 최소 상품 공급가
     * @param {Number} opts.maxProductSupplyPrice 최대 상품 공급가
     * @param {module:model/String} opts.isWholesale 도매상품 여부
     * @param {Number} opts.minProductWolesalePrice 최소 상품 도매가
     * @param {Number} opts.maxProductWolesalePrice 최대 상품 도매가
     * @param {module:model/String} opts.searchDateType 검색 일자 타입
     * @param {Date} opts.startDate 시작일자
     * @param {Date} opts.endDate 종료일자
     * @param {module:model/String} opts.salesStatus 판매 상태
     * @param {module:model/String} opts.isOpened 상품 출력 여부
     * @param {module:model/String} opts.isPreRegistedProduct 선등록 상품 여부
     * @param {module:model/String} opts.isTemporary 임시 상품 여부
     * @param {module:model/String} opts.isDeleted 상품 삭제 여부
     * @param {String} opts.violationType 위반 타입
     * @param {Date} opts.violationStartDate 위반일시 시작일자
     * @param {Date} opts.violationEndDate 위반일시 종료일자
     * @param {Date} opts.registedViolationStartDate 위반등록일시 시작일자
     * @param {Date} opts.registedViolationEndDate 위반등록일시 종료일자
     * @param {Date} opts.updatedViolationStartDate 위반수정일시 시작일자
     * @param {Date} opts.updatedViolationEndDate 위반수정일시 종료일자
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/ProductApi~getProductCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProduct",
    value: function getProduct(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {
        'target': opts['target']
      };
      var queryParams = {
        'approveTypes': this.apiClient.buildCollectionParam(opts['approveTypes'], 'multi'),
        'categoryCode': opts['categoryCode'],
        'searchType': opts['searchType'],
        'searchContent': opts['searchContent'],
        'isAbsoluteSearch': opts['isAbsoluteSearch'],
        'brandNumber': opts['brandNumber'],
        'minProductPrice': opts['minProductPrice'],
        'maxProductPrice': opts['maxProductPrice'],
        'minProductSupplyPrice': opts['minProductSupplyPrice'],
        'maxProductSupplyPrice': opts['maxProductSupplyPrice'],
        'isWholesale': opts['isWholesale'],
        'minProductWolesalePrice': opts['minProductWolesalePrice'],
        'maxProductWolesalePrice': opts['maxProductWolesalePrice'],
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'salesStatus': opts['salesStatus'],
        'isOpened': opts['isOpened'],
        'isPreRegistedProduct': opts['isPreRegistedProduct'],
        'isTemporary': opts['isTemporary'],
        'isDeleted': opts['isDeleted'],
        'violationType': opts['violationType'],
        'violationStartDate': opts['violationStartDate'],
        'violationEndDate': opts['violationEndDate'],
        'registedViolationStartDate': opts['registedViolationStartDate'],
        'registedViolationEndDate': opts['registedViolationEndDate'],
        'updatedViolationStartDate': opts['updatedViolationStartDate'],
        'updatedViolationEndDate': opts['updatedViolationEndDate'],
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi'),
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProductByNo operation.
     * @callback module:api/ProductApi~getProductByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 조회
     * 상품보기
     * @param {Number} productNumber 셀러허브 상품 일련번호
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {module:api/ProductApi~getProductByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProductByNo",
    value: function getProductByNo(productNumber, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'productNumber' is set

      if (productNumber === undefined || productNumber === null) {
        throw new Error("Missing the required parameter 'productNumber' when calling getProductByNo");
      }

      var pathParams = {
        'productNumber': productNumber
      };
      var queryParams = {
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi')
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products/{productNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProductByTarget operation.
     * @callback module:api/ProductApi~getProductByTargetCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 특정 상품 목록
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts.approveTypes 상품 승인 타입
     * @param {String} opts.categoryCode 셀러허브 카테고리 코드
     * @param {module:model/String} opts.searchType 검색 타겟
     * @param {String} opts.searchContent 검색 내용
     * @param {module:model/String} opts.isAbsoluteSearch 유사 검색 제외
     * @param {Number} opts.brandNumber 브랜드 고유번호
     * @param {Number} opts.minProductPrice 최소 상품 가격
     * @param {Number} opts.maxProductPrice 최대 상품 가격
     * @param {Number} opts.minProductSupplyPrice 최소 상품 공급가
     * @param {Number} opts.maxProductSupplyPrice 최대 상품 공급가
     * @param {module:model/String} opts.isWholesale 도매상품 여부
     * @param {Number} opts.minProductWolesalePrice 최소 상품 도매가
     * @param {Number} opts.maxProductWolesalePrice 최대 상품 도매가
     * @param {module:model/String} opts.searchDateType 검색 일자 타입
     * @param {Date} opts.startDate 시작일자
     * @param {Date} opts.endDate 종료일자
     * @param {module:model/String} opts.salesStatus 판매 상태
     * @param {module:model/String} opts.isOpened 상품 출력 여부
     * @param {module:model/String} opts.isPreRegistedProduct 선등록 상품 여부
     * @param {module:model/String} opts.isTemporary 임시 상품 여부
     * @param {module:model/String} opts.isDeleted 상품 삭제 여부
     * @param {String} opts.violationType 위반 타입
     * @param {Date} opts.violationStartDate 위반일시 시작일자
     * @param {Date} opts.violationEndDate 위반일시 종료일자
     * @param {Date} opts.registedViolationStartDate 위반등록일시 시작일자
     * @param {Date} opts.registedViolationEndDate 위반등록일시 종료일자
     * @param {Date} opts.updatedViolationStartDate 위반수정일시 시작일자
     * @param {Date} opts.updatedViolationEndDate 위반수정일시 종료일자
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/ProductApi~getProductByTargetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProductByTarget",
    value: function getProductByTarget(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'approveTypes': this.apiClient.buildCollectionParam(opts['approveTypes'], 'multi'),
        'categoryCode': opts['categoryCode'],
        'searchType': opts['searchType'],
        'searchContent': opts['searchContent'],
        'isAbsoluteSearch': opts['isAbsoluteSearch'],
        'brandNumber': opts['brandNumber'],
        'minProductPrice': opts['minProductPrice'],
        'maxProductPrice': opts['maxProductPrice'],
        'minProductSupplyPrice': opts['minProductSupplyPrice'],
        'maxProductSupplyPrice': opts['maxProductSupplyPrice'],
        'isWholesale': opts['isWholesale'],
        'minProductWolesalePrice': opts['minProductWolesalePrice'],
        'maxProductWolesalePrice': opts['maxProductWolesalePrice'],
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'salesStatus': opts['salesStatus'],
        'isOpened': opts['isOpened'],
        'isPreRegistedProduct': opts['isPreRegistedProduct'],
        'isTemporary': opts['isTemporary'],
        'isDeleted': opts['isDeleted'],
        'violationType': opts['violationType'],
        'violationStartDate': opts['violationStartDate'],
        'violationEndDate': opts['violationEndDate'],
        'registedViolationStartDate': opts['registedViolationStartDate'],
        'registedViolationEndDate': opts['registedViolationEndDate'],
        'updatedViolationStartDate': opts['updatedViolationStartDate'],
        'updatedViolationEndDate': opts['updatedViolationEndDate'],
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi'),
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products/target/{target}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateProductByNo operation.
     * @callback module:api/ProductApi~updateProductByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 상품 수정
     * 상품 수정
     * @param {Number} productNumber 셀러허브 상품 일련번호
     * @param {module:model/UpdateProductByNoRequest} UpdateProductByNoRequest 
     * @param {module:api/ProductApi~updateProductByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateProductByNo",
    value: function updateProductByNo(productNumber, UpdateProductByNoRequest, callback) {
      var postBody = UpdateProductByNoRequest; // verify the required parameter 'productNumber' is set

      if (productNumber === undefined || productNumber === null) {
        throw new Error("Missing the required parameter 'productNumber' when calling updateProductByNo");
      } // verify the required parameter 'UpdateProductByNoRequest' is set


      if (UpdateProductByNoRequest === undefined || UpdateProductByNoRequest === null) {
        throw new Error("Missing the required parameter 'UpdateProductByNoRequest' when calling updateProductByNo");
      }

      var pathParams = {
        'productNumber': productNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/products/{productNumber}', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ProductApi;
}();

exports["default"] = ProductApi;
},{"../ApiClient":1,"../model/A8a15b67230f3e43ed1bb29ee377c809Request":39,"../model/CreateProductRequest":47,"../model/Model219f3041d050108b1a2e51838473f6d8Request":57,"../model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request":58,"../model/UpdateProductByNoRequest":62}],32:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Promotion service.
* @module api/PromotionApi
* @version 0.0.1
*/
var PromotionApi = /*#__PURE__*/function () {
  /**
  * Constructs a new PromotionApi. 
  * @alias module:api/PromotionApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function PromotionApi(apiClient) {
    _classCallCheck(this, PromotionApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createMarketingPromotioon operation.
   * @callback module:api/PromotionApi~createMarketingPromotioonCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 마케팅 프로모션 동의
   * 프로모션 동의
   * @param {Object} opts Optional parameters
   * @param {Number} opts.agreeNumber 프로모션 번호
   * @param {module:model/String} opts.agreeCheck 프로모션 동의 여부
   * @param {module:api/PromotionApi~createMarketingPromotioonCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(PromotionApi, [{
    key: "createMarketingPromotioon",
    value: function createMarketingPromotioon(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'agreeNumber': opts['agreeNumber'],
        'agreeCheck': opts['agreeCheck']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/promotion', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingPromotioon operation.
     * @callback module:api/PromotionApi~getMarketingPromotioonCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 마케팅 프로모션 리스트
     * 프로모션목록
     * @param {Object} opts Optional parameters
     * @param {Date} opts.startDate 시작 일자
     * @param {Date} opts.endDate 종료 일자
     * @param {module:model/String} opts.searchType 검색 옵션
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/PromotionApi~getMarketingPromotioonCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingPromotioon",
    value: function getMarketingPromotioon(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchType': opts['searchType'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/promotion', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return PromotionApi;
}();

exports["default"] = PromotionApi;
},{"../ApiClient":1}],33:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateDetailInfoOfProviderRequest = _interopRequireDefault(require("../model/CreateDetailInfoOfProviderRequest"));

var _CreateStaffInfoOfProviderRequest = _interopRequireDefault(require("../model/CreateStaffInfoOfProviderRequest"));

var _UpdateProviderBrandBrandDatasParameterInner = _interopRequireDefault(require("../model/UpdateProviderBrandBrandDatasParameterInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Provider service.
* @module api/ProviderApi
* @version 0.0.1
*/
var ProviderApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ProviderApi. 
  * @alias module:api/ProviderApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ProviderApi(apiClient) {
    _classCallCheck(this, ProviderApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the checkExitProvider operation.
   * @callback module:api/ProviderApi~checkExitProviderCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 퇴점 가능여부 확인
   * 퇴점 가능여부 확인
   * @param {module:api/ProviderApi~checkExitProviderCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(ProviderApi, [{
    key: "checkExitProvider",
    value: function checkExitProvider(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/exit/check', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createDeliveryNotice operation.
     * @callback module:api/ProviderApi~createDeliveryNoticeCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 배송공지 등록
     * 기본관리 - 배송공지 등록
     * @param {String} image 배송 공지 이미지 base64
     * @param {Date} expiredAt 만기일자
     * @param {module:api/ProviderApi~createDeliveryNoticeCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createDeliveryNotice",
    value: function createDeliveryNotice(image, expiredAt, callback) {
      var postBody = null; // verify the required parameter 'image' is set

      if (image === undefined || image === null) {
        throw new Error("Missing the required parameter 'image' when calling createDeliveryNotice");
      } // verify the required parameter 'expiredAt' is set


      if (expiredAt === undefined || expiredAt === null) {
        throw new Error("Missing the required parameter 'expiredAt' when calling createDeliveryNotice");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'image': image,
        'expiredAt': expiredAt
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/deliveryNotice', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createDetailInfoOfProvider operation.
     * @callback module:api/ProviderApi~createDetailInfoOfProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 입점사 상세정보 관리
     * 기본관리 - 입점사 상세정보 관리
     * @param {module:model/CreateDetailInfoOfProviderRequest} CreateDetailInfoOfProviderRequest 
     * @param {module:api/ProviderApi~createDetailInfoOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createDetailInfoOfProvider",
    value: function createDetailInfoOfProvider(CreateDetailInfoOfProviderRequest, callback) {
      var postBody = CreateDetailInfoOfProviderRequest; // verify the required parameter 'CreateDetailInfoOfProviderRequest' is set

      if (CreateDetailInfoOfProviderRequest === undefined || CreateDetailInfoOfProviderRequest === null) {
        throw new Error("Missing the required parameter 'CreateDetailInfoOfProviderRequest' when calling createDetailInfoOfProvider");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/detailInfo', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createProviderBrand operation.
     * @callback module:api/ProviderApi~createProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 추가하기
     * 공급사 브랜드 추가하기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드명
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME2 브랜드 번호
     * @param {module:api/ProviderApi~createProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createProviderBrand",
    value: function createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createProviderBrand");
      } // verify the required parameter 'UNKNOWN_PARAMETER_NAME2' is set


      if (UNKNOWN_PARAMETER_NAME2 === undefined || UNKNOWN_PARAMETER_NAME2 === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME2' when calling createProviderBrand");
      }

      var pathParams = {
        'brandName': UNKNOWN_PARAMETER_NAME,
        'brandNumber': UNKNOWN_PARAMETER_NAME2
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createSellerPlanOfProvider operation.
     * @callback module:api/ProviderApi~createSellerPlanOfProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청
     * 기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청
     * @param {Object} opts Optional parameters
     * @param {String} opts.planCode 셀러허브 카테고리 코드
     * @param {Boolean} opts.doPlanUpgrade 플랜 업그레이드 진행여부
     * @param {module:api/ProviderApi~createSellerPlanOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createSellerPlanOfProvider",
    value: function createSellerPlanOfProvider(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'planCode': opts['planCode'],
        'doPlanUpgrade': opts['doPlanUpgrade']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/sellerPlan', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createStaffInfoOfProvider operation.
     * @callback module:api/ProviderApi~createStaffInfoOfProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 담당자 정보 설정
     * 기본관리 - 담당자 정보 설정
     * @param {module:model/CreateStaffInfoOfProviderRequest} CreateStaffInfoOfProviderRequest 
     * @param {module:api/ProviderApi~createStaffInfoOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createStaffInfoOfProvider",
    value: function createStaffInfoOfProvider(CreateStaffInfoOfProviderRequest, callback) {
      var postBody = CreateStaffInfoOfProviderRequest; // verify the required parameter 'CreateStaffInfoOfProviderRequest' is set

      if (CreateStaffInfoOfProviderRequest === undefined || CreateStaffInfoOfProviderRequest === null) {
        throw new Error("Missing the required parameter 'CreateStaffInfoOfProviderRequest' when calling createStaffInfoOfProvider");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/staff', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteDeliveryNotice operation.
     * @callback module:api/ProviderApi~deleteDeliveryNoticeCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 기본관리 - 배송공지 삭제
     * 기본관리 - 배송공지 삭제
     * @param {module:api/ProviderApi~deleteDeliveryNoticeCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteDeliveryNotice",
    value: function deleteDeliveryNotice(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/deliveryNotice', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteProviderBrandByNo operation.
     * @callback module:api/ProviderApi~deleteProviderBrandByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 삭제하기
     * 공급사 브랜드 삭제하기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:api/ProviderApi~deleteProviderBrandByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteProviderBrandByNo",
    value: function deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteProviderBrandByNo");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider/{brandNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the doExitProvider operation.
     * @callback module:api/ProviderApi~doExitProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 퇴점신청
     * 퇴점신청
     * @param {Object.<String, Object>} reasons 퇴점 사유
     * @param {String} cancelPhoneNumber 퇴점 안내 받을 연락처
     * @param {module:api/ProviderApi~doExitProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "doExitProvider",
    value: function doExitProvider(reasons, cancelPhoneNumber, callback) {
      var postBody = null; // verify the required parameter 'reasons' is set

      if (reasons === undefined || reasons === null) {
        throw new Error("Missing the required parameter 'reasons' when calling doExitProvider");
      } // verify the required parameter 'cancelPhoneNumber' is set


      if (cancelPhoneNumber === undefined || cancelPhoneNumber === null) {
        throw new Error("Missing the required parameter 'cancelPhoneNumber' when calling doExitProvider");
      }

      var pathParams = {
        'reasons': reasons,
        'cancelPhoneNumber': cancelPhoneNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/exit/check', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getAvailablePaymentMethods operation.
     * @callback module:api/ProviderApi~getAvailablePaymentMethodsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 수단 등록 목록
     * 결제 수단 등록 목록
     * @param {module:api/ProviderApi~getAvailablePaymentMethodsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getAvailablePaymentMethods",
    value: function getAvailablePaymentMethods(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/paymentMethods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPaymentHistoryById operation.
     * @callback module:api/ProviderApi~getPaymentHistoryByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 내역
     * 결제 내역
     * @param {Object} opts Optional parameters
     * @param {Number} opts.id 결제 내역 고유 ID
     * @param {module:api/ProviderApi~getPaymentHistoryByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPaymentHistoryById",
    value: function getPaymentHistoryById(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {
        'id': opts['id']
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/payments/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPaymentMethodById operation.
     * @callback module:api/ProviderApi~getPaymentMethodByIdCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 수단 등록 내용
     * 결제 수단 등록 내용
     * @param {module:api/ProviderApi~getPaymentMethodByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPaymentMethodById",
    value: function getPaymentMethodById(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/paymentMethods/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPaymentMethodHistory operation.
     * @callback module:api/ProviderApi~getPaymentMethodHistoryCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제방법 히스토리
     * 결제방법 변경 이력
     * @param {module:api/ProviderApi~getPaymentMethodHistoryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPaymentMethodHistory",
    value: function getPaymentMethodHistory(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/manages/paymentMethodHistory', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderBrand operation.
     * @callback module:api/ProviderApi~getProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 목록 가져오기
     * 공급사 브랜드 목록 가져오기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME2 브랜드명
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/ProviderApi~getProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProviderBrand",
    value: function getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getProviderBrand");
      } // verify the required parameter 'UNKNOWN_PARAMETER_NAME2' is set


      if (UNKNOWN_PARAMETER_NAME2 === undefined || UNKNOWN_PARAMETER_NAME2 === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME2' when calling getProviderBrand");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME,
        'brandName': UNKNOWN_PARAMETER_NAME2
      };
      var queryParams = {
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderBrandByNo operation.
     * @callback module:api/ProviderApi~getProviderBrandByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 특정 브랜드 가져오기
     * 공급사 특정 브랜드 가져오기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:api/ProviderApi~getProviderBrandByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProviderBrandByNo",
    value: function getProviderBrandByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getProviderBrandByNo");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider/{$brandNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getScmUploadFileOfProvider operation.
     * @callback module:api/ProviderApi~getScmUploadFileOfProviderCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사의 업로드 파일 (사업자등록증 등) 보기
     * 공급사의 파일관리
     * @param {Object} opts Optional parameters
     * @param {String} opts.model SCM 모델
     * @param {Number} opts.key SCM 모델의 키
     * @param {String} opts.column SCM 모델의 컬럼명
     * @param {module:api/ProviderApi~getScmUploadFileOfProviderCallback} callback The callback function, accepting three arguments: error, data, response
     */

  }, {
    key: "getScmUploadFileOfProvider",
    value: function getScmUploadFileOfProvider(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {
        'model': opts['model'],
        'key': opts['key'],
        'column': opts['column']
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = [];
      var returnType = null;
      return this.apiClient.callApi('/shapi/v1/scm/{model}/{key}/{column}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the geyPaymentHistory operation.
     * @callback module:api/ProviderApi~geyPaymentHistoryCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 내역 목록
     * 결제 내역 목록
     * @param {Object} opts Optional parameters
     * @param {Object.<String, Object>} opts.body 
     * @param {module:api/ProviderApi~geyPaymentHistoryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "geyPaymentHistory",
    value: function geyPaymentHistory(opts, callback) {
      opts = opts || {};
      var postBody = opts['body'];
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/payments', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the pauseProvider operation.
     * @callback module:api/ProviderApi~pauseProviderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 계약 일시정지
     * 계약 일시정지
     * @param {module:api/ProviderApi~pauseProviderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "pauseProvider",
    value: function pauseProvider(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/pause', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the registPaymentMethod operation.
     * @callback module:api/ProviderApi~registPaymentMethodCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 결제 수단 등록
     * 결제 수단 등록
     * @param {Object.<String, Object>} body 
     * @param {module:api/ProviderApi~registPaymentMethodCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "registPaymentMethod",
    value: function registPaymentMethod(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new Error("Missing the required parameter 'body' when calling registPaymentMethod");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/paymentMethods', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateProviderBrand operation.
     * @callback module:api/ProviderApi~updateProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 변경하기
     * 공급사 브랜드 변경하기
     * @param {Array.<module:model/UpdateProviderBrandBrandDatasParameterInner>} brandDatas 브랜드 데이터
     * @param {module:api/ProviderApi~updateProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateProviderBrand",
    value: function updateProviderBrand(brandDatas, callback) {
      var postBody = null; // verify the required parameter 'brandDatas' is set

      if (brandDatas === undefined || brandDatas === null) {
        throw new Error("Missing the required parameter 'brandDatas' when calling updateProviderBrand");
      }

      var pathParams = {
        'brandDatas': brandDatas
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ProviderApi;
}();

exports["default"] = ProviderApi;
},{"../ApiClient":1,"../model/CreateDetailInfoOfProviderRequest":45,"../model/CreateStaffInfoOfProviderRequest":52,"../model/UpdateProviderBrandBrandDatasParameterInner":63}],34:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Reply service.
* @module api/ReplyApi
* @version 0.0.1
*/
var ReplyApi = /*#__PURE__*/function () {
  /**
  * Constructs a new ReplyApi. 
  * @alias module:api/ReplyApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function ReplyApi(apiClient) {
    _classCallCheck(this, ReplyApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the createBoardReply operation.
   * @callback module:api/ReplyApi~createBoardReplyCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 판매자지원 게시물 답변 등록
   * @param {Number} boardNumber 판매자지원 게시물 번호
   * @param {Object} subject 제목
   * @param {Object} contents 내용
   * @param {Object} opts Optional parameters
   * @param {module:model/String} opts.category 구분값 카테고리
   * @param {module:model/String} opts.secret 비밀글 여부
   * @param {module:api/ReplyApi~createBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(ReplyApi, [{
    key: "createBoardReply",
    value: function createBoardReply(boardNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling createBoardReply");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling createBoardReply");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createBoardReply");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}/reply', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getBoardReply operation.
     * @callback module:api/ReplyApi~getBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 조회
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/ReplyApi~getBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getBoardReply",
    value: function getBoardReply(boardNumber, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling getBoardReply");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}/reply', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventReplyByNo operation.
     * @callback module:api/ReplyApi~getMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 목록 조회
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/ReplyApi~getMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventReplyByNo",
    value: function getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return ReplyApi;
}();

exports["default"] = ReplyApi;
},{"../ApiClient":1}],35:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CodeResource = _interopRequireDefault(require("../model/CodeResource"));

var _UpdateProviderBrandBrandDatasParameterInner = _interopRequireDefault(require("../model/UpdateProviderBrandBrandDatasParameterInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Sellerhub service.
* @module api/SellerhubApi
* @version 0.0.1
*/
var SellerhubApi = /*#__PURE__*/function () {
  /**
  * Constructs a new SellerhubApi. 
  * @alias module:api/SellerhubApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function SellerhubApi(apiClient) {
    _classCallCheck(this, SellerhubApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the call384c0bc85845dc729988acf253b757f2 operation.
   * @callback module:api/SellerhubApi~call384c0bc85845dc729988acf253b757f2Callback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * @param {module:model/null} UNKNOWN_PARAMETER_NAME 주문번호
   * @param {module:api/SellerhubApi~call384c0bc85845dc729988acf253b757f2Callback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(SellerhubApi, [{
    key: "call384c0bc85845dc729988acf253b757f2",
    value: function call384c0bc85845dc729988acf253b757f2(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling call384c0bc85845dc729988acf253b757f2");
      }

      var pathParams = {
        'order': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['sanctum'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/{ordno}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createBoard operation.
     * @callback module:api/SellerhubApi~createBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 등록
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/SellerhubApi~createBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createBoard",
    value: function createBoard(subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'subject' is set

      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling createBoard");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createBoard");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createBoardReply operation.
     * @callback module:api/SellerhubApi~createBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 등록
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/SellerhubApi~createBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createBoardReply",
    value: function createBoardReply(boardNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling createBoardReply");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling createBoardReply");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createBoardReply");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}/reply', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingEvent operation.
     * @callback module:api/SellerhubApi~createMarketingEventCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 등록
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Number} returnDeliveryCharge 반품배송비(편도)
     * @param {Number} exchangeDeliveryCharge 교환배송비(왕복)
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/SellerhubApi~createMarketingEventCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingEvent",
    value: function createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'name' is set

      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createMarketingEvent");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling createMarketingEvent");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling createMarketingEvent");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createMarketingEvent");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEvent");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling createMarketingEvent");
      } // verify the required parameter 'returnDeliveryCharge' is set


      if (returnDeliveryCharge === undefined || returnDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'returnDeliveryCharge' when calling createMarketingEvent");
      } // verify the required parameter 'exchangeDeliveryCharge' is set


      if (exchangeDeliveryCharge === undefined || exchangeDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'exchangeDeliveryCharge' when calling createMarketingEvent");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate'],
        'returnDeliveryCharge': returnDeliveryCharge,
        'exchangeDeliveryCharge': exchangeDeliveryCharge
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingEventProposalRequestByNo operation.
     * @callback module:api/SellerhubApi~createMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/SellerhubApi~createMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingEventProposalRequestByNo",
    value: function createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEventProposalRequestByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling createMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventProposalNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventProposalNumber}', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingEventReplyByNo operation.
     * @callback module:api/SellerhubApi~createMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 등록
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} contents 댓글내용
     * @param {module:api/SellerhubApi~createMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingEventReplyByNo",
    value: function createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createMarketingEventReplyByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling createMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'contents': contents
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createMarketingPromotioon operation.
     * @callback module:api/SellerhubApi~createMarketingPromotioonCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 마케팅 프로모션 동의
     * 프로모션 동의
     * @param {Object} opts Optional parameters
     * @param {Number} opts.agreeNumber 프로모션 번호
     * @param {module:model/String} opts.agreeCheck 프로모션 동의 여부
     * @param {module:api/SellerhubApi~createMarketingPromotioonCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createMarketingPromotioon",
    value: function createMarketingPromotioon(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'agreeNumber': opts['agreeNumber'],
        'agreeCheck': opts['agreeCheck']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/promotion', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createProviderBrand operation.
     * @callback module:api/SellerhubApi~createProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 추가하기
     * 공급사 브랜드 추가하기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드명
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME2 브랜드 번호
     * @param {module:api/SellerhubApi~createProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createProviderBrand",
    value: function createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling createProviderBrand");
      } // verify the required parameter 'UNKNOWN_PARAMETER_NAME2' is set


      if (UNKNOWN_PARAMETER_NAME2 === undefined || UNKNOWN_PARAMETER_NAME2 === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME2' when calling createProviderBrand");
      }

      var pathParams = {
        'brandName': UNKNOWN_PARAMETER_NAME,
        'brandNumber': UNKNOWN_PARAMETER_NAME2
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the createToken operation.
     * @callback module:api/SellerhubApi~createTokenCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 토큰 생성
     * 토큰 생성
     * @param {String} name 토큰명
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts.permissions 권한
     * @param {module:api/SellerhubApi~createTokenCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "createToken",
    value: function createToken(name, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'name' is set

      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling createToken");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'permissions': this.apiClient.buildCollectionParam(opts['permissions'], 'csv')
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/token', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteBoardByNo operation.
     * @callback module:api/SellerhubApi~deleteBoardByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 삭제
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 판매자지원 게시물 번호
     * @param {module:api/SellerhubApi~deleteBoardByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteBoardByNo",
    value: function deleteBoardByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteBoardByNo");
      }

      var pathParams = {
        'boardNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteBoardReplyByNo operation.
     * @callback module:api/SellerhubApi~deleteBoardReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 삭제
     * @param {Number} replyNumber 판매자지원 답변 번호
     * @param {module:api/SellerhubApi~deleteBoardReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteBoardReplyByNo",
    value: function deleteBoardReplyByNo(replyNumber, callback) {
      var postBody = null; // verify the required parameter 'replyNumber' is set

      if (replyNumber === undefined || replyNumber === null) {
        throw new Error("Missing the required parameter 'replyNumber' when calling deleteBoardReplyByNo");
      }

      var pathParams = {
        'replyNumber': replyNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/reply/{replyNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteMarketingEventByNo operation.
     * @callback module:api/SellerhubApi~deleteMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 삭제
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/SellerhubApi~deleteMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteMarketingEventByNo",
    value: function deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteMarketingEventByNo");
      }

      var pathParams = {
        'boardNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the deleteProviderBrandByNo operation.
     * @callback module:api/SellerhubApi~deleteProviderBrandByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 삭제하기
     * 공급사 브랜드 삭제하기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:api/SellerhubApi~deleteProviderBrandByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "deleteProviderBrandByNo",
    value: function deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling deleteProviderBrandByNo");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider/{brandNumber}', 'DELETE', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getAddress operation.
     * @callback module:api/SellerhubApi~getAddressCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주소조회
     * 주소조회
     * @param {String} keyword 검색어
     * @param {Object} opts Optional parameters
     * @param {Number} opts.currentPage 현재 페이지
     * @param {Number} opts.countPerPage 페이지당 검색
     * @param {module:model/String} opts.hstryYn 변동된 주소정보 포함 여부 (default to 'N')
     * @param {module:model/String} opts.firstSort 정확도순 정렬 (default to 'none')
     * @param {module:model/String} opts.addInfoYn 출력결과에 추가된 항목 (default to 'N')
     * @param {module:api/SellerhubApi~getAddressCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getAddress",
    value: function getAddress(keyword, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'keyword' is set

      if (keyword === undefined || keyword === null) {
        throw new Error("Missing the required parameter 'keyword' when calling getAddress");
      }

      var pathParams = {};
      var queryParams = {
        'currentPage': opts['currentPage'],
        'countPerPage': opts['countPerPage'],
        'keyword': keyword,
        'hstryYn': opts['hstryYn'],
        'firstSort': opts['firstSort'],
        'addInfoYn': opts['addInfoYn']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/address/search', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getBoard operation.
     * @callback module:api/SellerhubApi~getBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 판매자지원 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Date} opts.startDate 시작일
     * @param {Date} opts.endDate 종료일
     * @param {module:model/String} opts.sortBy 정렬 (default to 'none')
     * @param {module:model/String} opts.sortDirection 정렬 방향 (default to 'desc')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getBoard",
    value: function getBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getBoardReply operation.
     * @callback module:api/SellerhubApi~getBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 조회
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getBoardReply",
    value: function getBoardReply(boardNumber, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling getBoardReply");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/{boardNumber}/reply', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getBrandStatistics operation.
     * @callback module:api/SellerhubApi~getBrandStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 브랜드별 매출 통계
     * 브랜드 매출 통계
     * @param {Object} opts Optional parameters
     * @param {Number} opts.year 년
     * @param {Number} opts.month 월
     * @param {Number} opts.day 일
     * @param {module:api/SellerhubApi~getBrandStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getBrandStatistics",
    value: function getBrandStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'year': opts['year'],
        'month': opts['month'],
        'day': opts['day']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics/brand', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCancelOrder operation.
     * @callback module:api/SellerhubApi~getCancelOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문취소 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/Array} opts.cancelType 주문상태
     * @param {module:model/String} opts.settlekind 결제방법 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getCancelOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCancelOrder",
    value: function getCancelOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'cancelType': opts['cancelType'],
        'settlekind': opts['settlekind'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/cancel', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCode operation.
     * @callback module:api/SellerhubApi~getCodeCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/CodeResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 그룹코드 조회
     * 상위 그룹코드 조회
     * @param {module:api/SellerhubApi~getCodeCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/CodeResource>}
     */

  }, {
    key: "getCode",
    value: function getCode(callback) {
      var postBody = null;
      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_CodeResource["default"]];
      return this.apiClient.callApi('/shapi/v1/codes', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCodeByGroupCd operation.
     * @callback module:api/SellerhubApi~getCodeByGroupCdCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/CodeResource>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 그룹코드 조회
     * 그룹코드로 코드 조회
     * @param {String} groupCd 그룹코드
     * @param {module:api/SellerhubApi~getCodeByGroupCdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/CodeResource>}
     */

  }, {
    key: "getCodeByGroupCd",
    value: function getCodeByGroupCd(groupCd, callback) {
      var postBody = null; // verify the required parameter 'groupCd' is set

      if (groupCd === undefined || groupCd === null) {
        throw new Error("Missing the required parameter 'groupCd' when calling getCodeByGroupCd");
      }

      var pathParams = {
        'groupCd': groupCd
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_CodeResource["default"]];
      return this.apiClient.callApi('/shapi/v1/codes/{groupCd}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCsQna operation.
     * @callback module:api/SellerhubApi~getCsQnaCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * cs 리스트
     * cs목록
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.dateType 기간검색 옵션
     * @param {Date} opts.startDate 시작 일자
     * @param {Date} opts.endDate 종료 일자
     * @param {Number} opts.status 처리상태
     * @param {String} opts.inquireType 문의유형
     * @param {module:model/String} opts.searchType 상세검색 옵션
     * @param {String} opts.searchWord 검색어
     * @param {module:model/String} opts.sortBy 정렬 옵션
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getCsQnaCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCsQna",
    value: function getCsQna(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'dateType': opts['dateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'status': opts['status'],
        'inquireType': opts['inquireType'],
        'searchType': opts['searchType'],
        'searchWord': opts['searchWord'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csQna', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getCsQnaByNo operation.
     * @callback module:api/SellerhubApi~getCsQnaByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * CS문의 상세내역 조회
     * CS문의 상세내역
     * @param {Number} csNumber cs문의 번호
     * @param {module:api/SellerhubApi~getCsQnaByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getCsQnaByNo",
    value: function getCsQnaByNo(csNumber, callback) {
      var postBody = null; // verify the required parameter 'csNumber' is set

      if (csNumber === undefined || csNumber === null) {
        throw new Error("Missing the required parameter 'csNumber' when calling getCsQnaByNo");
      }

      var pathParams = {};
      var queryParams = {
        'csNumber': csNumber
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csQna/{csNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getExchangeOrder operation.
     * @callback module:api/SellerhubApi~getExchangeOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 교환접수/완료 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.exchangeType 교환상태 (default to '')
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/String} opts.cancelkey 상태 (default to 'all')
     * @param {module:model/String} opts.exchange_flag 교환상태 (default to 'all')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getExchangeOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getExchangeOrder",
    value: function getExchangeOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'exchangeType': opts['exchangeType'],
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'cancelkey': opts['cancelkey'],
        'exchange_flag': opts['exchange_flag'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/exchnage', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getFaqBoard operation.
     * @callback module:api/SellerhubApi~getFaqBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * FAQ 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Date} opts.startDate 시작일
     * @param {Date} opts.endDate 종료일
     * @param {module:model/String} opts.sortBy 정렬 (default to 'none')
     * @param {module:model/String} opts.sortDirection 정렬 방향 (default to 'desc')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getFaqBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getFaqBoard",
    value: function getFaqBoard(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/faq', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getForbiddenWords operation.
     * @callback module:api/SellerhubApi~getForbiddenWordsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 금칙어 목록
     * @param {Object} opts Optional parameters
     * @param {String} opts.categoryCode 셀러허브 카테고리 코드
     * @param {String} opts.word 검색 금칙어
     * @param {module:model/String} opts.isAppliedSubCategory 하위카테고리 적용여부
     * @param {Array.<module:model/String>} opts._with WITH Model
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getForbiddenWordsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getForbiddenWords",
    value: function getForbiddenWords(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'categoryCode': opts['categoryCode'],
        'word': opts['word'],
        'isAppliedSubCategory': opts['isAppliedSubCategory'],
        'with[]': this.apiClient.buildCollectionParam(opts['_with'], 'multi'),
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/forbiddenWords', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEvent operation.
     * @callback module:api/SellerhubApi~getMarketingEventCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 목록 조회
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.isApprove 승인 여부 (default to 'all')
     * @param {module:model/String} opts.isAnswer 답변 여부 (default to 'all')
     * @param {module:model/String} opts.searchDateType 검색 일자 설정 (default to 'registed')
     * @param {Number} opts.startDate 시작일
     * @param {Number} opts.endDate 종료일
     * @param {module:model/String} opts.searchKey 검색키 (default to 'none')
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getMarketingEventCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEvent",
    value: function getMarketingEvent(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'isApprove': opts['isApprove'],
        'isAnswer': opts['isAnswer'],
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventApprovalByNo operation.
     * @callback module:api/SellerhubApi~getMarketingEventApprovalByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 재신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/SellerhubApi~getMarketingEventApprovalByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventApprovalByNo",
    value: function getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventApprovalByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/approval/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventByNo operation.
     * @callback module:api/SellerhubApi~getMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {module:api/SellerhubApi~getMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventByNo",
    value: function getMarketingEventByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposal operation.
     * @callback module:api/SellerhubApi~getMarketingEventProposalCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 목록 조회
     * @param {Object} opts Optional parameters
     * @param {Array.<module:model/String>} opts.channel 행사 채널
     * @param {module:model/String} opts.searchDateType 검색 일자 설정 (default to 'startDate')
     * @param {Number} opts.startDate 시작일
     * @param {Number} opts.endDate 종료일
     * @param {Array.<String>} opts.category 카테고리
     * @param {module:model/String} opts.eventStatus 행사진행여부 (default to 'all')
     * @param {module:model/String} opts.requestStatus 행사신청여부 (default to 'all')
     * @param {module:model/String} opts.isApprove 답변여부 (default to 'all')
     * @param {module:model/String} opts.searchKey 검색키 (default to 'title')
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getMarketingEventProposalCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposal",
    value: function getMarketingEventProposal(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'channel': this.apiClient.buildCollectionParam(opts['channel'], 'multi'),
        'searchDateType': opts['searchDateType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'category': this.apiClient.buildCollectionParam(opts['category'], 'multi'),
        'eventStatus': opts['eventStatus'],
        'requestStatus': opts['requestStatus'],
        'isApprove': opts['isApprove'],
        'searchKey': opts['searchKey'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposalByNo operation.
     * @callback module:api/SellerhubApi~getMarketingEventProposalByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 내역
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {module:api/SellerhubApi~getMarketingEventProposalByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposalByNo",
    value: function getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventProposalByNo");
      }

      var pathParams = {
        'eventProposalNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/{eventProposalNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventProposalRequestByNo operation.
     * @callback module:api/SellerhubApi~getMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청내역
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 게시물 번호
     * @param {module:api/SellerhubApi~getMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventProposalRequestByNo",
    value: function getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingEventReplyByNo operation.
     * @callback module:api/SellerhubApi~getMarketingEventReplyByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 댓글 목록 조회
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getMarketingEventReplyByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingEventReplyByNo",
    value: function getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getMarketingEventReplyByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/reply/{eventNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getMarketingPromotioon operation.
     * @callback module:api/SellerhubApi~getMarketingPromotioonCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 마케팅 프로모션 리스트
     * 프로모션목록
     * @param {Object} opts Optional parameters
     * @param {Date} opts.startDate 시작 일자
     * @param {Date} opts.endDate 종료 일자
     * @param {module:model/String} opts.searchType 검색 옵션
     * @param {String} opts.searchWord 검색어
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getMarketingPromotioonCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getMarketingPromotioon",
    value: function getMarketingPromotioon(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchType': opts['searchType'],
        'searchWord': opts['searchWord'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/promotion', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getOrder operation.
     * @callback module:api/SellerhubApi~getOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to '')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/String} opts.sgkey 상품검색 옵션 (default to '')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 상품검색 키워드
     * @param {module:model/String} opts.dtkind 처리일자 (옵션) (default to 'orddt')
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/String} opts.step2false 정상주문만 조회 (default to '')
     * @param {module:model/String} opts.step 주문상태 (default to '')
     * @param {module:model/String} opts.step2 주문상태 (default to '')
     * @param {module:model/String} opts.exchange_flag 주문상태 (default to '')
     * @param {module:model/String} opts.bundle_deli_cnt 주문상태 (default to '')
     * @param {module:model/String} opts.saleType 판매처 (default to '')
     * @param {module:model/String} opts.order_delayed 배송지연 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getOrder",
    value: function getOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'sgkey': opts['sgkey'],
        'sgword': opts['UNKNOWN_PARAMETER_NAME2'],
        'dtkind': opts['dtkind'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'step2false': opts['step2false'],
        'step': opts['step'],
        'step2': opts['step2'],
        'exchange_flag': opts['exchange_flag'],
        'bundle_deli_cnt': opts['bundle_deli_cnt'],
        'saleType': opts['saleType'],
        'order_delayed': opts['order_delayed'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getOrderCsMemoByNo operation.
     * @callback module:api/SellerhubApi~getOrderCsMemoByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * cs 메모
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 주문번호
     * @param {module:api/SellerhubApi~getOrderCsMemoByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getOrderCsMemoByNo",
    value: function getOrderCsMemoByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getOrderCsMemoByNo");
      }

      var pathParams = {
        'ordno': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/csMemo/{ordno}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPopularCategoryStatistics operation.
     * @callback module:api/SellerhubApi~getPopularCategoryStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 인기 카테고리 분석
     * 인기 카테고리 분석
     * @param {Object} opts Optional parameters
     * @param {Number} opts.year 년
     * @param {Number} opts.month 월
     * @param {Number} opts.category 카테고리
     * @param {module:api/SellerhubApi~getPopularCategoryStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPopularCategoryStatistics",
    value: function getPopularCategoryStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'year': opts['year'],
        'month': opts['month'],
        'category': opts['category']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics/popularCategory', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPopularProductStatistics operation.
     * @callback module:api/SellerhubApi~getPopularProductStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 인기 상품 분석
     * 인기 상품 분석
     * @param {Object} opts Optional parameters
     * @param {Number} opts.year 년
     * @param {Number} opts.month 월
     * @param {module:api/SellerhubApi~getPopularProductStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPopularProductStatistics",
    value: function getPopularProductStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'year': opts['year'],
        'month': opts['month']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics/popularGoods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProcessingOrder operation.
     * @callback module:api/SellerhubApi~getProcessingOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 배송준비 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/String} opts.dtkind 처리일자 (옵션) (default to 'orddt')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 처리일자 (키워드)
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 배송준비상태
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getProcessingOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProcessingOrder",
    value: function getProcessingOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'dtkind': opts['dtkind'],
        'regdt': opts['UNKNOWN_PARAMETER_NAME2'],
        'srstatus': opts['UNKNOWN_PARAMETER_NAME3'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/processing', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderBrand operation.
     * @callback module:api/SellerhubApi~getProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 목록 가져오기
     * 공급사 브랜드 목록 가져오기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME2 브랜드명
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.sortBy 정렬 타입
     * @param {module:model/String} opts.sortDirection 정렬 방향
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지당 노출 개수
     * @param {module:api/SellerhubApi~getProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProviderBrand",
    value: function getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getProviderBrand");
      } // verify the required parameter 'UNKNOWN_PARAMETER_NAME2' is set


      if (UNKNOWN_PARAMETER_NAME2 === undefined || UNKNOWN_PARAMETER_NAME2 === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME2' when calling getProviderBrand");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME,
        'brandName': UNKNOWN_PARAMETER_NAME2
      };
      var queryParams = {
        'sortBy': opts['sortBy'],
        'sortDirection': opts['sortDirection'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getProviderBrandByNo operation.
     * @callback module:api/SellerhubApi~getProviderBrandByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 특정 브랜드 가져오기
     * 공급사 특정 브랜드 가져오기
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 브랜드 번호
     * @param {module:api/SellerhubApi~getProviderBrandByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getProviderBrandByNo",
    value: function getProviderBrandByNo(UNKNOWN_PARAMETER_NAME, callback) {
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling getProviderBrandByNo");
      }

      var pathParams = {
        'brandNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider/{$brandNumber}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getRefundOrder operation.
     * @callback module:api/SellerhubApi~getRefundOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 반품접수/완료 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to 'all')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/String} opts.dtkind 반품요청일 (옵션) (default to 'regdt')
     * @param {Date} opts.startDate 시작 시간
     * @param {Date} opts.endDate 종료 시간
     * @param {module:model/Array} opts.cancelstatus 상태
     * @param {module:model/String} opts.cls 반품완료 여부 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/SellerhubApi~getRefundOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getRefundOrder",
    value: function getRefundOrder(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'dtkind': opts['dtkind'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'cancelstatus': opts['cancelstatus'],
        'cls': opts['cls'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/list/refund', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getStatistics operation.
     * @callback module:api/SellerhubApi~getStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 매출 통계 리스트
     * 매출 통계 목록
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 카테고리
     * @param {module:model/String} opts.orderStep 주문 상태
     * @param {module:model/String} opts.orderStep2 주문 상태
     * @param {Number} opts.brandNumber 브랜드 번호
     * @param {Number} opts.goodsBrandNubmer 상품 브랜드 번호
     * @param {module:model/String} opts.searchType 처리일자 (키워드)
     * @param {Date} opts.startDate 시작일자
     * @param {Date} opts.endDate 종료일자
     * @param {module:api/SellerhubApi~getStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getStatistics",
    value: function getStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'category': opts['UNKNOWN_PARAMETER_NAME'],
        'orderStep': opts['orderStep'],
        'orderStep2': opts['orderStep2'],
        'brandNumber': opts['brandNumber'],
        'goodsBrandNubmer': opts['goodsBrandNubmer'],
        'searchType': opts['searchType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateBoard operation.
     * @callback module:api/SellerhubApi~updateBoardCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 수정
     * @param {Number} boardNumber 판매자지원 게시물 번호
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/SellerhubApi~updateBoardCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateBoard",
    value: function updateBoard(boardNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'boardNumber' is set

      if (boardNumber === undefined || boardNumber === null) {
        throw new Error("Missing the required parameter 'boardNumber' when calling updateBoard");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling updateBoard");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateBoard");
      }

      var pathParams = {
        'boardNumber': boardNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateBoardReply operation.
     * @callback module:api/SellerhubApi~updateBoardReplyCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 판매자지원 게시물 답변 수정
     * @param {Number} replyNumber 판매자지원 답변 번호
     * @param {Object} subject 제목
     * @param {Object} contents 내용
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.category 구분값 카테고리
     * @param {module:model/String} opts.secret 비밀글 여부
     * @param {module:api/SellerhubApi~updateBoardReplyCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateBoardReply",
    value: function updateBoardReply(replyNumber, subject, contents, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'replyNumber' is set

      if (replyNumber === undefined || replyNumber === null) {
        throw new Error("Missing the required parameter 'replyNumber' when calling updateBoardReply");
      } // verify the required parameter 'subject' is set


      if (subject === undefined || subject === null) {
        throw new Error("Missing the required parameter 'subject' when calling updateBoardReply");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateBoardReply");
      }

      var pathParams = {
        'replyNumber': replyNumber
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'subject': subject,
        'contents': contents,
        'category': opts['category'],
        'secret': opts['secret']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/board/reply/{replyNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateCsQna operation.
     * @callback module:api/SellerhubApi~updateCsQnaCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * CS 답변 등록
     * CS 답변 등록
     * @param {Number} csNumber cs문의 번호
     * @param {module:api/SellerhubApi~updateCsQnaCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateCsQna",
    value: function updateCsQna(csNumber, callback) {
      var postBody = null; // verify the required parameter 'csNumber' is set

      if (csNumber === undefined || csNumber === null) {
        throw new Error("Missing the required parameter 'csNumber' when calling updateCsQna");
      }

      var pathParams = {};
      var queryParams = {
        'csNumber': csNumber
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/csQna/{csNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateMarketingEventByNo operation.
     * @callback module:api/SellerhubApi~updateMarketingEventByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사신청 게시물 등록
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사신청 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Number} returnDeliveryCharge 반품배송비(편도)
     * @param {Number} exchangeDeliveryCharge 교환배송비(왕복)
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/SellerhubApi~updateMarketingEventByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateMarketingEventByNo",
    value: function updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling updateMarketingEventByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateMarketingEventByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling updateMarketingEventByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling updateMarketingEventByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling updateMarketingEventByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateMarketingEventByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling updateMarketingEventByNo");
      } // verify the required parameter 'returnDeliveryCharge' is set


      if (returnDeliveryCharge === undefined || returnDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'returnDeliveryCharge' when calling updateMarketingEventByNo");
      } // verify the required parameter 'exchangeDeliveryCharge' is set


      if (exchangeDeliveryCharge === undefined || exchangeDeliveryCharge === null) {
        throw new Error("Missing the required parameter 'exchangeDeliveryCharge' when calling updateMarketingEventByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate'],
        'returnDeliveryCharge': returnDeliveryCharge,
        'exchangeDeliveryCharge': exchangeDeliveryCharge
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/event/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateMarketingEventProposalRequestByNo operation.
     * @callback module:api/SellerhubApi~updateMarketingEventProposalRequestByNoCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 행사제안 신청내용 재신청
     * @param {module:model/null} UNKNOWN_PARAMETER_NAME 행사제안 신청 게시물 번호
     * @param {Object} name 신청자이름
     * @param {Object} email 신청자이메일
     * @param {Object} phoneNumber 신청자연락처
     * @param {Object} title 행사제목
     * @param {Object} contents 행사내용
     * @param {module:model/String} isCoupon 쿠폰사용여부
     * @param {Object} opts Optional parameters
     * @param {Number} opts.couponDiscountRate 판매자 부담 쿠폰 할인율(%)
     * @param {module:api/SellerhubApi~updateMarketingEventProposalRequestByNoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateMarketingEventProposalRequestByNo",
    value: function updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, callback) {
      opts = opts || {};
      var postBody = null; // verify the required parameter 'UNKNOWN_PARAMETER_NAME' is set

      if (UNKNOWN_PARAMETER_NAME === undefined || UNKNOWN_PARAMETER_NAME === null) {
        throw new Error("Missing the required parameter 'UNKNOWN_PARAMETER_NAME' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'name' is set


      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'email' is set


      if (email === undefined || email === null) {
        throw new Error("Missing the required parameter 'email' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'phoneNumber' is set


      if (phoneNumber === undefined || phoneNumber === null) {
        throw new Error("Missing the required parameter 'phoneNumber' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'title' is set


      if (title === undefined || title === null) {
        throw new Error("Missing the required parameter 'title' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'contents' is set


      if (contents === undefined || contents === null) {
        throw new Error("Missing the required parameter 'contents' when calling updateMarketingEventProposalRequestByNo");
      } // verify the required parameter 'isCoupon' is set


      if (isCoupon === undefined || isCoupon === null) {
        throw new Error("Missing the required parameter 'isCoupon' when calling updateMarketingEventProposalRequestByNo");
      }

      var pathParams = {
        'eventNumber': UNKNOWN_PARAMETER_NAME
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {
        'name': name,
        'email': email,
        'phoneNumber': phoneNumber,
        'title': title,
        'contents': contents,
        'isCoupon': isCoupon,
        'couponDiscountRate': opts['couponDiscountRate']
      };
      var authNames = ['oauth2'];
      var contentTypes = ['multipart/form-data'];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/marketing/eventProposal/request/{eventNumber}', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateOrderDeliveryStatus operation.
     * @callback module:api/SellerhubApi~updateOrderDeliveryStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 배송상태 변경
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 아이템 sno
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 현재 istep
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 현재 istep2
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME4 발송예정일
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME5 발송지연 예정일
     * @param {module:model/String} opts.rstatus 배송상태 변경값 (default to '')
     * @param {module:api/SellerhubApi~updateOrderDeliveryStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateOrderDeliveryStatus",
    value: function updateOrderDeliveryStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'itemsSno': opts['UNKNOWN_PARAMETER_NAME'],
        'viewIstep': opts['UNKNOWN_PARAMETER_NAME2'],
        'viewIstep2': opts['UNKNOWN_PARAMETER_NAME3'],
        'rdelivery': opts['UNKNOWN_PARAMETER_NAME4'],
        'rdelivery2': opts['UNKNOWN_PARAMETER_NAME5'],
        'rstatus': opts['rstatus']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/deliveryStatus', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateOrderStatus operation.
     * @callback module:api/SellerhubApi~updateOrderStatusCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문상태 변경
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문상태값
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 아이템 sno
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 현재 istep
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME4 현재 istep2
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME5 발송예정일
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME6 발송지연 예정일
     * @param {module:model/String} opts.rstatus 배송상태 변경값 (default to '')
     * @param {module:api/SellerhubApi~updateOrderStatusCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateOrderStatus",
    value: function updateOrderStatus(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'case': opts['UNKNOWN_PARAMETER_NAME'],
        'itemsSno': opts['UNKNOWN_PARAMETER_NAME2'],
        'viewIstep': opts['UNKNOWN_PARAMETER_NAME3'],
        'viewIstep2': opts['UNKNOWN_PARAMETER_NAME4'],
        'rdelivery': opts['UNKNOWN_PARAMETER_NAME5'],
        'rdelivery2': opts['UNKNOWN_PARAMETER_NAME6'],
        'rstatus': opts['rstatus']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/orderStatus', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateOrderToRefund operation.
     * @callback module:api/SellerhubApi~updateOrderToRefundCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 반품처리
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문취소sno
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 반품택배사
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME3 반품택배 코드
     * @param {module:api/SellerhubApi~updateOrderToRefundCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateOrderToRefund",
    value: function updateOrderToRefund(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'cancelSno': opts['UNKNOWN_PARAMETER_NAME'],
        'delivery': opts['UNKNOWN_PARAMETER_NAME2'],
        'delivery_code': opts['UNKNOWN_PARAMETER_NAME3']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/order/refund', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the updateProviderBrand operation.
     * @callback module:api/SellerhubApi~updateProviderBrandCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 공급사 브랜드 변경하기
     * 공급사 브랜드 변경하기
     * @param {Array.<module:model/UpdateProviderBrandBrandDatasParameterInner>} brandDatas 브랜드 데이터
     * @param {module:api/SellerhubApi~updateProviderBrandCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "updateProviderBrand",
    value: function updateProviderBrand(brandDatas, callback) {
      var postBody = null; // verify the required parameter 'brandDatas' is set

      if (brandDatas === undefined || brandDatas === null) {
        throw new Error("Missing the required parameter 'brandDatas' when calling updateProviderBrand");
      }

      var pathParams = {
        'brandDatas': brandDatas
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/brands/provider', 'PUT', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return SellerhubApi;
}();

exports["default"] = SellerhubApi;
},{"../ApiClient":1,"../model/CodeResource":43,"../model/UpdateProviderBrandBrandDatasParameterInner":63}],36:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Statistics service.
* @module api/StatisticsApi
* @version 0.0.1
*/
var StatisticsApi = /*#__PURE__*/function () {
  /**
  * Constructs a new StatisticsApi. 
  * @alias module:api/StatisticsApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function StatisticsApi(apiClient) {
    _classCallCheck(this, StatisticsApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getBrandStatistics operation.
   * @callback module:api/StatisticsApi~getBrandStatisticsCallback
   * @param {String} error Error message, if any.
   * @param {Object} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 브랜드별 매출 통계
   * 브랜드 매출 통계
   * @param {Object} opts Optional parameters
   * @param {Number} opts.year 년
   * @param {Number} opts.month 월
   * @param {Number} opts.day 일
   * @param {module:api/StatisticsApi~getBrandStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Object}
   */


  _createClass(StatisticsApi, [{
    key: "getBrandStatistics",
    value: function getBrandStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'year': opts['year'],
        'month': opts['month'],
        'day': opts['day']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics/brand', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPopularCategoryStatistics operation.
     * @callback module:api/StatisticsApi~getPopularCategoryStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 인기 카테고리 분석
     * 인기 카테고리 분석
     * @param {Object} opts Optional parameters
     * @param {Number} opts.year 년
     * @param {Number} opts.month 월
     * @param {Number} opts.category 카테고리
     * @param {module:api/StatisticsApi~getPopularCategoryStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPopularCategoryStatistics",
    value: function getPopularCategoryStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'year': opts['year'],
        'month': opts['month'],
        'category': opts['category']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics/popularCategory', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getPopularProductStatistics operation.
     * @callback module:api/StatisticsApi~getPopularProductStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 인기 상품 분석
     * 인기 상품 분석
     * @param {Object} opts Optional parameters
     * @param {Number} opts.year 년
     * @param {Number} opts.month 월
     * @param {module:api/StatisticsApi~getPopularProductStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getPopularProductStatistics",
    value: function getPopularProductStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'year': opts['year'],
        'month': opts['month']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics/popularGoods', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getStatistics operation.
     * @callback module:api/StatisticsApi~getStatisticsCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 매출 통계 리스트
     * 매출 통계 목록
     * @param {Object} opts Optional parameters
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 카테고리
     * @param {module:model/String} opts.orderStep 주문 상태
     * @param {module:model/String} opts.orderStep2 주문 상태
     * @param {Number} opts.brandNumber 브랜드 번호
     * @param {Number} opts.goodsBrandNubmer 상품 브랜드 번호
     * @param {module:model/String} opts.searchType 처리일자 (키워드)
     * @param {Date} opts.startDate 시작일자
     * @param {Date} opts.endDate 종료일자
     * @param {module:api/StatisticsApi~getStatisticsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */

  }, {
    key: "getStatistics",
    value: function getStatistics(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'category': opts['UNKNOWN_PARAMETER_NAME'],
        'orderStep': opts['orderStep'],
        'orderStep2': opts['orderStep2'],
        'brandNumber': opts['brandNumber'],
        'goodsBrandNubmer': opts['goodsBrandNubmer'],
        'searchType': opts['searchType'],
        'startDate': opts['startDate'],
        'endDate': opts['endDate']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = Object;
      return this.apiClient.callApi('/shapi/v1/statistics', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return StatisticsApi;
}();

exports["default"] = StatisticsApi;
},{"../ApiClient":1}],37:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _UrgentMessage = _interopRequireDefault(require("../model/UrgentMessage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* UrgentMessage service.
* @module api/UrgentMessageApi
* @version 0.0.1
*/
var UrgentMessageApi = /*#__PURE__*/function () {
  /**
  * Constructs a new UrgentMessageApi. 
  * @alias module:api/UrgentMessageApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function UrgentMessageApi(apiClient) {
    _classCallCheck(this, UrgentMessageApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the getUrgentMessage operation.
   * @callback module:api/UrgentMessageApi~getUrgentMessageCallback
   * @param {String} error Error message, if any.
   * @param {Array.<module:model/UrgentMessage>} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * 긴급 알림 메시지 목록
   * @param {Object} opts Optional parameters
   * @param {Date} opts.startDate 알람 등록 시작일
   * @param {Date} opts.endDate 알람 등록 종료일
   * @param {module:model/String} opts.searchType 검색 타입
   * @param {String} opts.searchContents 검색 내용
   * @param {Array.<Number>} opts.brandNumbers 브랜드 번호
   * @param {Array.<module:model/String>} opts.alarmTypes 알람 타입
   * @param {module:model/String} opts.isReceived 수신 여부
   * @param {module:model/String} opts.isFinished 종료 여부
   * @param {Number} opts.page 페이지 번호
   * @param {Number} opts.pageSize 페이지당 노출 개수
   * @param {module:api/UrgentMessageApi~getUrgentMessageCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link Array.<module:model/UrgentMessage>}
   */


  _createClass(UrgentMessageApi, [{
    key: "getUrgentMessage",
    value: function getUrgentMessage(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'startDate': opts['startDate'],
        'endDate': opts['endDate'],
        'searchType': opts['searchType'],
        'searchContents': opts['searchContents'],
        'brandNumbers': this.apiClient.buildCollectionParam(opts['brandNumbers'], 'multi'),
        'alarmTypes': this.apiClient.buildCollectionParam(opts['alarmTypes'], 'multi'),
        'isReceived': opts['isReceived'],
        'isFinished': opts['isFinished'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = [_UrgentMessage["default"]];
      return this.apiClient.callApi('/shapi/v1/urgentMessages', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the getUrgentMessageById operation.
     * @callback module:api/UrgentMessageApi~getUrgentMessageByIdCallback
     * @param {String} error Error message, if any.
     * @param {module:model/UrgentMessage} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 긴급 알림 메시지
     * @param {Number} id 알림 ID
     * @param {module:api/UrgentMessageApi~getUrgentMessageByIdCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/UrgentMessage}
     */

  }, {
    key: "getUrgentMessageById",
    value: function getUrgentMessageById(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling getUrgentMessageById");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = ['oauth2'];
      var contentTypes = [];
      var accepts = ['application/json'];
      var returnType = _UrgentMessage["default"];
      return this.apiClient.callApi('/shapi/v1/urgentMessages/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return UrgentMessageApi;
}();

exports["default"] = UrgentMessageApi;
},{"../ApiClient":1,"../model/UrgentMessage":64}],38:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "A8a15b67230f3e43ed1bb29ee377c809Request", {
  enumerable: true,
  get: function get() {
    return _A8a15b67230f3e43ed1bb29ee377c809Request["default"];
  }
});
Object.defineProperty(exports, "AddressApi", {
  enumerable: true,
  get: function get() {
    return _AddressApi["default"];
  }
});
Object.defineProperty(exports, "AlarmApi", {
  enumerable: true,
  get: function get() {
    return _AlarmApi["default"];
  }
});
Object.defineProperty(exports, "ApiClient", {
  enumerable: true,
  get: function get() {
    return _ApiClient["default"];
  }
});
Object.defineProperty(exports, "AttemptMobileAuthRequest", {
  enumerable: true,
  get: function get() {
    return _AttemptMobileAuthRequest["default"];
  }
});
Object.defineProperty(exports, "AuthenticationApi", {
  enumerable: true,
  get: function get() {
    return _AuthenticationApi["default"];
  }
});
Object.defineProperty(exports, "BoardApi", {
  enumerable: true,
  get: function get() {
    return _BoardApi["default"];
  }
});
Object.defineProperty(exports, "BoardManual", {
  enumerable: true,
  get: function get() {
    return _BoardManual["default"];
  }
});
Object.defineProperty(exports, "BoardNotice", {
  enumerable: true,
  get: function get() {
    return _BoardNotice["default"];
  }
});
Object.defineProperty(exports, "BrandApi", {
  enumerable: true,
  get: function get() {
    return _BrandApi["default"];
  }
});
Object.defineProperty(exports, "CalculateApi", {
  enumerable: true,
  get: function get() {
    return _CalculateApi["default"];
  }
});
Object.defineProperty(exports, "CategoryApi", {
  enumerable: true,
  get: function get() {
    return _CategoryApi["default"];
  }
});
Object.defineProperty(exports, "CodeApi", {
  enumerable: true,
  get: function get() {
    return _CodeApi["default"];
  }
});
Object.defineProperty(exports, "CodeResource", {
  enumerable: true,
  get: function get() {
    return _CodeResource["default"];
  }
});
Object.defineProperty(exports, "CommonApi", {
  enumerable: true,
  get: function get() {
    return _CommonApi["default"];
  }
});
Object.defineProperty(exports, "ConfirmMobileAuthRequest", {
  enumerable: true,
  get: function get() {
    return _ConfirmMobileAuthRequest["default"];
  }
});
Object.defineProperty(exports, "CreateDetailInfoOfProviderRequest", {
  enumerable: true,
  get: function get() {
    return _CreateDetailInfoOfProviderRequest["default"];
  }
});
Object.defineProperty(exports, "CreateDetailInfoOfProviderRequestBrandDatasInnerInner", {
  enumerable: true,
  get: function get() {
    return _CreateDetailInfoOfProviderRequestBrandDatasInnerInner["default"];
  }
});
Object.defineProperty(exports, "CreateProductRequest", {
  enumerable: true,
  get: function get() {
    return _CreateProductRequest["default"];
  }
});
Object.defineProperty(exports, "CreateProductRequestCertificationDatasInnerInner", {
  enumerable: true,
  get: function get() {
    return _CreateProductRequestCertificationDatasInnerInner["default"];
  }
});
Object.defineProperty(exports, "CreateProductRequestOptionsInnerInner", {
  enumerable: true,
  get: function get() {
    return _CreateProductRequestOptionsInnerInner["default"];
  }
});
Object.defineProperty(exports, "CreateProductRequestProductImageDatasInnerInner", {
  enumerable: true,
  get: function get() {
    return _CreateProductRequestProductImageDatasInnerInner["default"];
  }
});
Object.defineProperty(exports, "CreateProductRequestProductInformationInner", {
  enumerable: true,
  get: function get() {
    return _CreateProductRequestProductInformationInner["default"];
  }
});
Object.defineProperty(exports, "CreateStaffInfoOfProviderRequest", {
  enumerable: true,
  get: function get() {
    return _CreateStaffInfoOfProviderRequest["default"];
  }
});
Object.defineProperty(exports, "CreateStaffInfoOfProviderRequestSellerInner", {
  enumerable: true,
  get: function get() {
    return _CreateStaffInfoOfProviderRequestSellerInner["default"];
  }
});
Object.defineProperty(exports, "CreateStaffInfoOfProviderRequestStaffMembersInner", {
  enumerable: true,
  get: function get() {
    return _CreateStaffInfoOfProviderRequestStaffMembersInner["default"];
  }
});
Object.defineProperty(exports, "CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner", {
  enumerable: true,
  get: function get() {
    return _CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner["default"];
  }
});
Object.defineProperty(exports, "CsQnaApi", {
  enumerable: true,
  get: function get() {
    return _CsQnaApi["default"];
  }
});
Object.defineProperty(exports, "DashboardApi", {
  enumerable: true,
  get: function get() {
    return _DashboardApi["default"];
  }
});
Object.defineProperty(exports, "DeliveryApi", {
  enumerable: true,
  get: function get() {
    return _DeliveryApi["default"];
  }
});
Object.defineProperty(exports, "DeliveryResource", {
  enumerable: true,
  get: function get() {
    return _DeliveryResource["default"];
  }
});
Object.defineProperty(exports, "EventApi", {
  enumerable: true,
  get: function get() {
    return _EventApi["default"];
  }
});
Object.defineProperty(exports, "EventProposalApi", {
  enumerable: true,
  get: function get() {
    return _EventProposalApi["default"];
  }
});
Object.defineProperty(exports, "ExitApi", {
  enumerable: true,
  get: function get() {
    return _ExitApi["default"];
  }
});
Object.defineProperty(exports, "FaqApi", {
  enumerable: true,
  get: function get() {
    return _FaqApi["default"];
  }
});
Object.defineProperty(exports, "ForbiddenApi", {
  enumerable: true,
  get: function get() {
    return _ForbiddenApi["default"];
  }
});
Object.defineProperty(exports, "HolidayNoticeApi", {
  enumerable: true,
  get: function get() {
    return _HolidayNoticeApi["default"];
  }
});
Object.defineProperty(exports, "KakaoPayApi", {
  enumerable: true,
  get: function get() {
    return _KakaoPayApi["default"];
  }
});
Object.defineProperty(exports, "LoginApi", {
  enumerable: true,
  get: function get() {
    return _LoginApi["default"];
  }
});
Object.defineProperty(exports, "ManageApi", {
  enumerable: true,
  get: function get() {
    return _ManageApi["default"];
  }
});
Object.defineProperty(exports, "ManualApi", {
  enumerable: true,
  get: function get() {
    return _ManualApi["default"];
  }
});
Object.defineProperty(exports, "MarketingApi", {
  enumerable: true,
  get: function get() {
    return _MarketingApi["default"];
  }
});
Object.defineProperty(exports, "Model219f3041d050108b1a2e51838473f6d8Request", {
  enumerable: true,
  get: function get() {
    return _Model219f3041d050108b1a2e51838473f6d8Request["default"];
  }
});
Object.defineProperty(exports, "Model69ec8f3e26fa2b1b02ce4897936e91e4Request", {
  enumerable: true,
  get: function get() {
    return _Model69ec8f3e26fa2b1b02ce4897936e91e4Request["default"];
  }
});
Object.defineProperty(exports, "Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner", {
  enumerable: true,
  get: function get() {
    return _Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner["default"];
  }
});
Object.defineProperty(exports, "NoticeApi", {
  enumerable: true,
  get: function get() {
    return _NoticeApi["default"];
  }
});
Object.defineProperty(exports, "OrderApi", {
  enumerable: true,
  get: function get() {
    return _OrderApi["default"];
  }
});
Object.defineProperty(exports, "PauseApi", {
  enumerable: true,
  get: function get() {
    return _PauseApi["default"];
  }
});
Object.defineProperty(exports, "PaymentApi", {
  enumerable: true,
  get: function get() {
    return _PaymentApi["default"];
  }
});
Object.defineProperty(exports, "PenaltyApi", {
  enumerable: true,
  get: function get() {
    return _PenaltyApi["default"];
  }
});
Object.defineProperty(exports, "PopularProduct", {
  enumerable: true,
  get: function get() {
    return _PopularProduct["default"];
  }
});
Object.defineProperty(exports, "PreCalculateApi", {
  enumerable: true,
  get: function get() {
    return _PreCalculateApi["default"];
  }
});
Object.defineProperty(exports, "ProductApi", {
  enumerable: true,
  get: function get() {
    return _ProductApi["default"];
  }
});
Object.defineProperty(exports, "PromotionApi", {
  enumerable: true,
  get: function get() {
    return _PromotionApi["default"];
  }
});
Object.defineProperty(exports, "ProviderApi", {
  enumerable: true,
  get: function get() {
    return _ProviderApi["default"];
  }
});
Object.defineProperty(exports, "ProviderDeliveryResource", {
  enumerable: true,
  get: function get() {
    return _ProviderDeliveryResource["default"];
  }
});
Object.defineProperty(exports, "ReplyApi", {
  enumerable: true,
  get: function get() {
    return _ReplyApi["default"];
  }
});
Object.defineProperty(exports, "SellerhubApi", {
  enumerable: true,
  get: function get() {
    return _SellerhubApi["default"];
  }
});
Object.defineProperty(exports, "StatisticsApi", {
  enumerable: true,
  get: function get() {
    return _StatisticsApi["default"];
  }
});
Object.defineProperty(exports, "UpdateProductByNoRequest", {
  enumerable: true,
  get: function get() {
    return _UpdateProductByNoRequest["default"];
  }
});
Object.defineProperty(exports, "UpdateProviderBrandBrandDatasParameterInner", {
  enumerable: true,
  get: function get() {
    return _UpdateProviderBrandBrandDatasParameterInner["default"];
  }
});
Object.defineProperty(exports, "UrgentMessage", {
  enumerable: true,
  get: function get() {
    return _UrgentMessage["default"];
  }
});
Object.defineProperty(exports, "UrgentMessageAlarm", {
  enumerable: true,
  get: function get() {
    return _UrgentMessageAlarm["default"];
  }
});
Object.defineProperty(exports, "UrgentMessageApi", {
  enumerable: true,
  get: function get() {
    return _UrgentMessageApi["default"];
  }
});

var _ApiClient = _interopRequireDefault(require("./ApiClient"));

var _A8a15b67230f3e43ed1bb29ee377c809Request = _interopRequireDefault(require("./model/A8a15b67230f3e43ed1bb29ee377c809Request"));

var _AttemptMobileAuthRequest = _interopRequireDefault(require("./model/AttemptMobileAuthRequest"));

var _BoardManual = _interopRequireDefault(require("./model/BoardManual"));

var _BoardNotice = _interopRequireDefault(require("./model/BoardNotice"));

var _CodeResource = _interopRequireDefault(require("./model/CodeResource"));

var _ConfirmMobileAuthRequest = _interopRequireDefault(require("./model/ConfirmMobileAuthRequest"));

var _CreateDetailInfoOfProviderRequest = _interopRequireDefault(require("./model/CreateDetailInfoOfProviderRequest"));

var _CreateDetailInfoOfProviderRequestBrandDatasInnerInner = _interopRequireDefault(require("./model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner"));

var _CreateProductRequest = _interopRequireDefault(require("./model/CreateProductRequest"));

var _CreateProductRequestCertificationDatasInnerInner = _interopRequireDefault(require("./model/CreateProductRequestCertificationDatasInnerInner"));

var _CreateProductRequestOptionsInnerInner = _interopRequireDefault(require("./model/CreateProductRequestOptionsInnerInner"));

var _CreateProductRequestProductImageDatasInnerInner = _interopRequireDefault(require("./model/CreateProductRequestProductImageDatasInnerInner"));

var _CreateProductRequestProductInformationInner = _interopRequireDefault(require("./model/CreateProductRequestProductInformationInner"));

var _CreateStaffInfoOfProviderRequest = _interopRequireDefault(require("./model/CreateStaffInfoOfProviderRequest"));

var _CreateStaffInfoOfProviderRequestSellerInner = _interopRequireDefault(require("./model/CreateStaffInfoOfProviderRequestSellerInner"));

var _CreateStaffInfoOfProviderRequestStaffMembersInner = _interopRequireDefault(require("./model/CreateStaffInfoOfProviderRequestStaffMembersInner"));

var _CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner = _interopRequireDefault(require("./model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner"));

var _DeliveryResource = _interopRequireDefault(require("./model/DeliveryResource"));

var _Model219f3041d050108b1a2e51838473f6d8Request = _interopRequireDefault(require("./model/Model219f3041d050108b1a2e51838473f6d8Request"));

var _Model69ec8f3e26fa2b1b02ce4897936e91e4Request = _interopRequireDefault(require("./model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request"));

var _Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner = _interopRequireDefault(require("./model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner"));

var _PopularProduct = _interopRequireDefault(require("./model/PopularProduct"));

var _ProviderDeliveryResource = _interopRequireDefault(require("./model/ProviderDeliveryResource"));

var _UpdateProductByNoRequest = _interopRequireDefault(require("./model/UpdateProductByNoRequest"));

var _UpdateProviderBrandBrandDatasParameterInner = _interopRequireDefault(require("./model/UpdateProviderBrandBrandDatasParameterInner"));

var _UrgentMessage = _interopRequireDefault(require("./model/UrgentMessage"));

var _UrgentMessageAlarm = _interopRequireDefault(require("./model/UrgentMessageAlarm"));

var _AddressApi = _interopRequireDefault(require("./api/AddressApi"));

var _AlarmApi = _interopRequireDefault(require("./api/AlarmApi"));

var _AuthenticationApi = _interopRequireDefault(require("./api/AuthenticationApi"));

var _BoardApi = _interopRequireDefault(require("./api/BoardApi"));

var _BrandApi = _interopRequireDefault(require("./api/BrandApi"));

var _CalculateApi = _interopRequireDefault(require("./api/CalculateApi"));

var _CategoryApi = _interopRequireDefault(require("./api/CategoryApi"));

var _CodeApi = _interopRequireDefault(require("./api/CodeApi"));

var _CommonApi = _interopRequireDefault(require("./api/CommonApi"));

var _CsQnaApi = _interopRequireDefault(require("./api/CsQnaApi"));

var _DashboardApi = _interopRequireDefault(require("./api/DashboardApi"));

var _DeliveryApi = _interopRequireDefault(require("./api/DeliveryApi"));

var _EventApi = _interopRequireDefault(require("./api/EventApi"));

var _EventProposalApi = _interopRequireDefault(require("./api/EventProposalApi"));

var _ExitApi = _interopRequireDefault(require("./api/ExitApi"));

var _FaqApi = _interopRequireDefault(require("./api/FaqApi"));

var _ForbiddenApi = _interopRequireDefault(require("./api/ForbiddenApi"));

var _HolidayNoticeApi = _interopRequireDefault(require("./api/HolidayNoticeApi"));

var _KakaoPayApi = _interopRequireDefault(require("./api/KakaoPayApi"));

var _LoginApi = _interopRequireDefault(require("./api/LoginApi"));

var _ManageApi = _interopRequireDefault(require("./api/ManageApi"));

var _ManualApi = _interopRequireDefault(require("./api/ManualApi"));

var _MarketingApi = _interopRequireDefault(require("./api/MarketingApi"));

var _NoticeApi = _interopRequireDefault(require("./api/NoticeApi"));

var _OrderApi = _interopRequireDefault(require("./api/OrderApi"));

var _PauseApi = _interopRequireDefault(require("./api/PauseApi"));

var _PaymentApi = _interopRequireDefault(require("./api/PaymentApi"));

var _PenaltyApi = _interopRequireDefault(require("./api/PenaltyApi"));

var _PreCalculateApi = _interopRequireDefault(require("./api/PreCalculateApi"));

var _ProductApi = _interopRequireDefault(require("./api/ProductApi"));

var _PromotionApi = _interopRequireDefault(require("./api/PromotionApi"));

var _ProviderApi = _interopRequireDefault(require("./api/ProviderApi"));

var _ReplyApi = _interopRequireDefault(require("./api/ReplyApi"));

var _SellerhubApi = _interopRequireDefault(require("./api/SellerhubApi"));

var _StatisticsApi = _interopRequireDefault(require("./api/StatisticsApi"));

var _UrgentMessageApi = _interopRequireDefault(require("./api/UrgentMessageApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
},{"./ApiClient":1,"./api/AddressApi":2,"./api/AlarmApi":3,"./api/AuthenticationApi":4,"./api/BoardApi":5,"./api/BrandApi":6,"./api/CalculateApi":7,"./api/CategoryApi":8,"./api/CodeApi":9,"./api/CommonApi":10,"./api/CsQnaApi":11,"./api/DashboardApi":12,"./api/DeliveryApi":13,"./api/EventApi":14,"./api/EventProposalApi":15,"./api/ExitApi":16,"./api/FaqApi":17,"./api/ForbiddenApi":18,"./api/HolidayNoticeApi":19,"./api/KakaoPayApi":20,"./api/LoginApi":21,"./api/ManageApi":22,"./api/ManualApi":23,"./api/MarketingApi":24,"./api/NoticeApi":25,"./api/OrderApi":26,"./api/PauseApi":27,"./api/PaymentApi":28,"./api/PenaltyApi":29,"./api/PreCalculateApi":30,"./api/ProductApi":31,"./api/PromotionApi":32,"./api/ProviderApi":33,"./api/ReplyApi":34,"./api/SellerhubApi":35,"./api/StatisticsApi":36,"./api/UrgentMessageApi":37,"./model/A8a15b67230f3e43ed1bb29ee377c809Request":39,"./model/AttemptMobileAuthRequest":40,"./model/BoardManual":41,"./model/BoardNotice":42,"./model/CodeResource":43,"./model/ConfirmMobileAuthRequest":44,"./model/CreateDetailInfoOfProviderRequest":45,"./model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner":46,"./model/CreateProductRequest":47,"./model/CreateProductRequestCertificationDatasInnerInner":48,"./model/CreateProductRequestOptionsInnerInner":49,"./model/CreateProductRequestProductImageDatasInnerInner":50,"./model/CreateProductRequestProductInformationInner":51,"./model/CreateStaffInfoOfProviderRequest":52,"./model/CreateStaffInfoOfProviderRequestSellerInner":53,"./model/CreateStaffInfoOfProviderRequestStaffMembersInner":54,"./model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner":55,"./model/DeliveryResource":56,"./model/Model219f3041d050108b1a2e51838473f6d8Request":57,"./model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request":58,"./model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner":59,"./model/PopularProduct":60,"./model/ProviderDeliveryResource":61,"./model/UpdateProductByNoRequest":62,"./model/UpdateProviderBrandBrandDatasParameterInner":63,"./model/UrgentMessage":64,"./model/UrgentMessageAlarm":65}],39:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The A8a15b67230f3e43ed1bb29ee377c809Request model module.
 * @module model/A8a15b67230f3e43ed1bb29ee377c809Request
 * @version 0.0.1
 */
var A8a15b67230f3e43ed1bb29ee377c809Request = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>A8a15b67230f3e43ed1bb29ee377c809Request</code>.
   * @alias module:model/A8a15b67230f3e43ed1bb29ee377c809Request
   */
  function A8a15b67230f3e43ed1bb29ee377c809Request() {
    _classCallCheck(this, A8a15b67230f3e43ed1bb29ee377c809Request);

    A8a15b67230f3e43ed1bb29ee377c809Request.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(A8a15b67230f3e43ed1bb29ee377c809Request, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>A8a15b67230f3e43ed1bb29ee377c809Request</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/A8a15b67230f3e43ed1bb29ee377c809Request} obj Optional instance to populate.
     * @return {module:model/A8a15b67230f3e43ed1bb29ee377c809Request} The populated <code>A8a15b67230f3e43ed1bb29ee377c809Request</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new A8a15b67230f3e43ed1bb29ee377c809Request();

        if (data.hasOwnProperty('productPrice')) {
          obj['productPrice'] = _ApiClient["default"].convertToType(data['productPrice'], 'Number');
        }

        if (data.hasOwnProperty('productSupplyPrice')) {
          obj['productSupplyPrice'] = _ApiClient["default"].convertToType(data['productSupplyPrice'], 'Number');
        }

        if (data.hasOwnProperty('productConsumerPrice')) {
          obj['productConsumerPrice'] = _ApiClient["default"].convertToType(data['productConsumerPrice'], 'Number');
        }

        if (data.hasOwnProperty('productAbidePrice')) {
          obj['productAbidePrice'] = _ApiClient["default"].convertToType(data['productAbidePrice'], 'Number');
        }

        if (data.hasOwnProperty('productWolesalePrice')) {
          obj['productWolesalePrice'] = _ApiClient["default"].convertToType(data['productWolesalePrice'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return A8a15b67230f3e43ed1bb29ee377c809Request;
}();
/**
 * 상품 판매 가격
 * @member {Number} productPrice
 */


A8a15b67230f3e43ed1bb29ee377c809Request.prototype['productPrice'] = undefined;
/**
 * 상품 공급 가격
 * @member {Number} productSupplyPrice
 */

A8a15b67230f3e43ed1bb29ee377c809Request.prototype['productSupplyPrice'] = undefined;
/**
 * 소비자 가격
 * @member {Number} productConsumerPrice
 */

A8a15b67230f3e43ed1bb29ee377c809Request.prototype['productConsumerPrice'] = undefined;
/**
 * 준수 판매 가격
 * @member {Number} productAbidePrice
 */

A8a15b67230f3e43ed1bb29ee377c809Request.prototype['productAbidePrice'] = undefined;
/**
 * 도매 가격
 * @member {Number} productWolesalePrice
 */

A8a15b67230f3e43ed1bb29ee377c809Request.prototype['productWolesalePrice'] = undefined;
var _default = A8a15b67230f3e43ed1bb29ee377c809Request;
exports["default"] = _default;
},{"../ApiClient":1}],40:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The AttemptMobileAuthRequest model module.
 * @module model/AttemptMobileAuthRequest
 * @version 0.0.1
 */
var AttemptMobileAuthRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>AttemptMobileAuthRequest</code>.
   * @alias module:model/AttemptMobileAuthRequest
   * @param firstNumber {String} 처음번호
   * @param middleNumber {String} 중간번호
   * @param lastNumber {String} 끝번호
   */
  function AttemptMobileAuthRequest(firstNumber, middleNumber, lastNumber) {
    _classCallCheck(this, AttemptMobileAuthRequest);

    AttemptMobileAuthRequest.initialize(this, firstNumber, middleNumber, lastNumber);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(AttemptMobileAuthRequest, null, [{
    key: "initialize",
    value: function initialize(obj, firstNumber, middleNumber, lastNumber) {
      obj['firstNumber'] = firstNumber;
      obj['middleNumber'] = middleNumber;
      obj['lastNumber'] = lastNumber;
    }
    /**
     * Constructs a <code>AttemptMobileAuthRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AttemptMobileAuthRequest} obj Optional instance to populate.
     * @return {module:model/AttemptMobileAuthRequest} The populated <code>AttemptMobileAuthRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new AttemptMobileAuthRequest();

        if (data.hasOwnProperty('firstNumber')) {
          obj['firstNumber'] = _ApiClient["default"].convertToType(data['firstNumber'], 'String');
        }

        if (data.hasOwnProperty('middleNumber')) {
          obj['middleNumber'] = _ApiClient["default"].convertToType(data['middleNumber'], 'String');
        }

        if (data.hasOwnProperty('lastNumber')) {
          obj['lastNumber'] = _ApiClient["default"].convertToType(data['lastNumber'], 'String');
        }
      }

      return obj;
    }
  }]);

  return AttemptMobileAuthRequest;
}();
/**
 * 처음번호
 * @member {String} firstNumber
 */


AttemptMobileAuthRequest.prototype['firstNumber'] = undefined;
/**
 * 중간번호
 * @member {String} middleNumber
 */

AttemptMobileAuthRequest.prototype['middleNumber'] = undefined;
/**
 * 끝번호
 * @member {String} lastNumber
 */

AttemptMobileAuthRequest.prototype['lastNumber'] = undefined;
var _default = AttemptMobileAuthRequest;
exports["default"] = _default;
},{"../ApiClient":1}],41:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The BoardManual model module.
 * @module model/BoardManual
 * @version 0.0.1
 */
var BoardManual = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>BoardManual</code>.
   * @alias module:model/BoardManual
   */
  function BoardManual() {
    _classCallCheck(this, BoardManual);

    BoardManual.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(BoardManual, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>BoardManual</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BoardManual} obj Optional instance to populate.
     * @return {module:model/BoardManual} The populated <code>BoardManual</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new BoardManual();

        if (data.hasOwnProperty('manualNumber')) {
          obj['manualNumber'] = _ApiClient["default"].convertToType(data['manualNumber'], 'Number');
        }

        if (data.hasOwnProperty('subject')) {
          obj['subject'] = _ApiClient["default"].convertToType(data['subject'], 'String');
        }

        if (data.hasOwnProperty('contents')) {
          obj['contents'] = _ApiClient["default"].convertToType(data['contents'], 'String');
        }

        if (data.hasOwnProperty('category')) {
          obj['category'] = _ApiClient["default"].convertToType(data['category'], 'String');
        }

        if (data.hasOwnProperty('isSecret')) {
          obj['isSecret'] = _ApiClient["default"].convertToType(data['isSecret'], 'String');
        }

        if (data.hasOwnProperty('isNotice')) {
          obj['isNotice'] = _ApiClient["default"].convertToType(data['isNotice'], 'String');
        }

        if (data.hasOwnProperty('originalFileName')) {
          obj['originalFileName'] = _ApiClient["default"].convertToType(data['originalFileName'], 'String');
        }

        if (data.hasOwnProperty('attachmentsFileName')) {
          obj['attachmentsFileName'] = _ApiClient["default"].convertToType(data['attachmentsFileName'], 'String');
        }

        if (data.hasOwnProperty('creator')) {
          obj['creator'] = _ApiClient["default"].convertToType(data['creator'], 'String');
        }

        if (data.hasOwnProperty('createdAt')) {
          obj['createdAt'] = _ApiClient["default"].convertToType(data['createdAt'], 'String');
        }
      }

      return obj;
    }
  }]);

  return BoardManual;
}();
/**
 * 공급사 매뉴얼 번호
 * @member {Number} manualNumber
 */


BoardManual.prototype['manualNumber'] = undefined;
/**
 * 제목
 * @member {String} subject
 */

BoardManual.prototype['subject'] = undefined;
/**
 * 내용
 * @member {String} contents
 */

BoardManual.prototype['contents'] = undefined;
/**
 * 카테고리
 * @member {String} category
 */

BoardManual.prototype['category'] = undefined;
/**
 * 비밀글 여부
 * @member {module:model/BoardManual.IsSecretEnum} isSecret
 */

BoardManual.prototype['isSecret'] = undefined;
/**
 * 공지 여부
 * @member {module:model/BoardManual.IsNoticeEnum} isNotice
 */

BoardManual.prototype['isNotice'] = undefined;
/**
 * 원본 첨부 파일명
 * @member {String} originalFileName
 */

BoardManual.prototype['originalFileName'] = undefined;
/**
 * 첨부 파일명
 * @member {String} attachmentsFileName
 */

BoardManual.prototype['attachmentsFileName'] = undefined;
/**
 * 등록자
 * @member {String} creator
 */

BoardManual.prototype['creator'] = undefined;
/**
 * 생성 일자
 * @member {String} createdAt
 */

BoardManual.prototype['createdAt'] = undefined;
/**
 * Allowed values for the <code>isSecret</code> property.
 * @enum {String}
 * @readonly
 */

BoardManual['IsSecretEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isNotice</code> property.
 * @enum {String}
 * @readonly
 */

BoardManual['IsNoticeEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = BoardManual;
exports["default"] = _default;
},{"../ApiClient":1}],42:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The BoardNotice model module.
 * @module model/BoardNotice
 * @version 0.0.1
 */
var BoardNotice = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>BoardNotice</code>.
   * @alias module:model/BoardNotice
   */
  function BoardNotice() {
    _classCallCheck(this, BoardNotice);

    BoardNotice.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(BoardNotice, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>BoardNotice</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/BoardNotice} obj Optional instance to populate.
     * @return {module:model/BoardNotice} The populated <code>BoardNotice</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new BoardNotice();

        if (data.hasOwnProperty('noticeNumber')) {
          obj['noticeNumber'] = _ApiClient["default"].convertToType(data['noticeNumber'], 'Number');
        }

        if (data.hasOwnProperty('contentsType')) {
          obj['contentsType'] = _ApiClient["default"].convertToType(data['contentsType'], 'String');
        }

        if (data.hasOwnProperty('subject')) {
          obj['subject'] = _ApiClient["default"].convertToType(data['subject'], 'String');
        }

        if (data.hasOwnProperty('contents')) {
          obj['contents'] = _ApiClient["default"].convertToType(data['contents'], 'String');
        }

        if (data.hasOwnProperty('category')) {
          obj['category'] = _ApiClient["default"].convertToType(data['category'], 'String');
        }

        if (data.hasOwnProperty('popupLink')) {
          obj['popupLink'] = _ApiClient["default"].convertToType(data['popupLink'], 'String');
        }

        if (data.hasOwnProperty('isSecret')) {
          obj['isSecret'] = _ApiClient["default"].convertToType(data['isSecret'], 'String');
        }

        if (data.hasOwnProperty('isNotice')) {
          obj['isNotice'] = _ApiClient["default"].convertToType(data['isNotice'], 'String');
        }

        if (data.hasOwnProperty('isPopup')) {
          obj['isPopup'] = _ApiClient["default"].convertToType(data['isPopup'], 'String');
        }

        if (data.hasOwnProperty('isForcedToRead')) {
          obj['isForcedToRead'] = _ApiClient["default"].convertToType(data['isForcedToRead'], 'String');
        }

        if (data.hasOwnProperty('boardStatus')) {
          obj['boardStatus'] = _ApiClient["default"].convertToType(data['boardStatus'], 'String');
        }

        if (data.hasOwnProperty('boardAfterStatus')) {
          obj['boardAfterStatus'] = _ApiClient["default"].convertToType(data['boardAfterStatus'], 'String');
        }

        if (data.hasOwnProperty('priority')) {
          obj['priority'] = _ApiClient["default"].convertToType(data['priority'], 'Number');
        }

        if (data.hasOwnProperty('popupPosition')) {
          obj['popupPosition'] = _ApiClient["default"].convertToType(data['popupPosition'], 'String');
        }

        if (data.hasOwnProperty('popupStyle')) {
          obj['popupStyle'] = _ApiClient["default"].convertToType(data['popupStyle'], Object);
        }

        if (data.hasOwnProperty('isAvailableAgree')) {
          obj['isAvailableAgree'] = _ApiClient["default"].convertToType(data['isAvailableAgree'], 'String');
        }

        if (data.hasOwnProperty('isAvailableEventAgree')) {
          obj['isAvailableEventAgree'] = _ApiClient["default"].convertToType(data['isAvailableEventAgree'], 'String');
        }

        if (data.hasOwnProperty('eventAgreeItems')) {
          obj['eventAgreeItems'] = _ApiClient["default"].convertToType(data['eventAgreeItems'], 'String');
        }

        if (data.hasOwnProperty('eventAgreedText')) {
          obj['eventAgreedText'] = _ApiClient["default"].convertToType(data['eventAgreedText'], 'String');
        }

        if (data.hasOwnProperty('eventAgreeItemsFlag')) {
          obj['eventAgreeItemsFlag'] = _ApiClient["default"].convertToType(data['eventAgreeItemsFlag'], 'String');
        }

        if (data.hasOwnProperty('originalFileName')) {
          obj['originalFileName'] = _ApiClient["default"].convertToType(data['originalFileName'], 'String');
        }

        if (data.hasOwnProperty('attachmentsFileName')) {
          obj['attachmentsFileName'] = _ApiClient["default"].convertToType(data['attachmentsFileName'], 'String');
        }

        if (data.hasOwnProperty('creator')) {
          obj['creator'] = _ApiClient["default"].convertToType(data['creator'], 'String');
        }

        if (data.hasOwnProperty('createdAt')) {
          obj['createdAt'] = _ApiClient["default"].convertToType(data['createdAt'], 'Date');
        }

        if (data.hasOwnProperty('updator')) {
          obj['updator'] = _ApiClient["default"].convertToType(data['updator'], 'String');
        }

        if (data.hasOwnProperty('updatedAt')) {
          obj['updatedAt'] = _ApiClient["default"].convertToType(data['updatedAt'], 'Date');
        }

        if (data.hasOwnProperty('isSetPeriod')) {
          obj['isSetPeriod'] = _ApiClient["default"].convertToType(data['isSetPeriod'], 'String');
        }

        if (data.hasOwnProperty('popupStartedAt')) {
          obj['popupStartedAt'] = _ApiClient["default"].convertToType(data['popupStartedAt'], 'Date');
        }

        if (data.hasOwnProperty('popupEndedAt')) {
          obj['popupEndedAt'] = _ApiClient["default"].convertToType(data['popupEndedAt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return BoardNotice;
}();
/**
 * 공급사 공지사항 번호
 * @member {Number} noticeNumber
 */


BoardNotice.prototype['noticeNumber'] = undefined;
/**
 * 글 타입
 * @member {module:model/BoardNotice.ContentsTypeEnum} contentsType
 */

BoardNotice.prototype['contentsType'] = undefined;
/**
 * 제목
 * @member {String} subject
 */

BoardNotice.prototype['subject'] = undefined;
/**
 * 내용
 * @member {String} contents
 */

BoardNotice.prototype['contents'] = undefined;
/**
 * 카테고리
 * @member {String} category
 */

BoardNotice.prototype['category'] = undefined;
/**
 * 링크
 * @member {String} popupLink
 */

BoardNotice.prototype['popupLink'] = undefined;
/**
 * 비밀글 여부
 * @member {module:model/BoardNotice.IsSecretEnum} isSecret
 */

BoardNotice.prototype['isSecret'] = undefined;
/**
 * 공지 여부
 * @member {module:model/BoardNotice.IsNoticeEnum} isNotice
 */

BoardNotice.prototype['isNotice'] = undefined;
/**
 * 팝업 여부
 * @member {module:model/BoardNotice.IsPopupEnum} isPopup
 */

BoardNotice.prototype['isPopup'] = undefined;
/**
 * 필독 여부
 * @member {module:model/BoardNotice.IsForcedToReadEnum} isForcedToRead
 */

BoardNotice.prototype['isForcedToRead'] = undefined;
/**
 * 게시물 상태
 * @member {module:model/BoardNotice.BoardStatusEnum} boardStatus
 */

BoardNotice.prototype['boardStatus'] = undefined;
/**
 * 게시기간 종료후 상태
 * @member {module:model/BoardNotice.BoardAfterStatusEnum} boardAfterStatus
 */

BoardNotice.prototype['boardAfterStatus'] = undefined;
/**
 * 우선순위
 * @member {Number} priority
 */

BoardNotice.prototype['priority'] = undefined;
/**
 * 팝업 노출 위치
 * @member {module:model/BoardNotice.PopupPositionEnum} popupPosition
 */

BoardNotice.prototype['popupPosition'] = undefined;
/**
 * 팝업 스타일
 * @member {Object} popupStyle
 */

BoardNotice.prototype['popupStyle'] = undefined;
/**
 * 동의기능 사용 여부
 * @member {module:model/BoardNotice.IsAvailableAgreeEnum} isAvailableAgree
 */

BoardNotice.prototype['isAvailableAgree'] = undefined;
/**
 * 이벤트 동의 팝업 여부
 * @member {module:model/BoardNotice.IsAvailableEventAgreeEnum} isAvailableEventAgree
 */

BoardNotice.prototype['isAvailableEventAgree'] = undefined;
/**
 * 이벤트 동의 항목
 * @member {String} eventAgreeItems
 */

BoardNotice.prototype['eventAgreeItems'] = undefined;
/**
 * 이벤트 동의/미동의 내용
 * @member {String} eventAgreedText
 */

BoardNotice.prototype['eventAgreedText'] = undefined;
/**
 * 이벤트 참여여부
 * @member {module:model/BoardNotice.EventAgreeItemsFlagEnum} eventAgreeItemsFlag
 */

BoardNotice.prototype['eventAgreeItemsFlag'] = undefined;
/**
 * 원본 첨부 파일명
 * @member {String} originalFileName
 */

BoardNotice.prototype['originalFileName'] = undefined;
/**
 * 첨부 파일명
 * @member {String} attachmentsFileName
 */

BoardNotice.prototype['attachmentsFileName'] = undefined;
/**
 * 등록자
 * @member {String} creator
 */

BoardNotice.prototype['creator'] = undefined;
/**
 * 생성 일자
 * @member {Date} createdAt
 */

BoardNotice.prototype['createdAt'] = undefined;
/**
 * 수정자
 * @member {String} updator
 */

BoardNotice.prototype['updator'] = undefined;
/**
 * 수정 일자
 * @member {Date} updatedAt
 */

BoardNotice.prototype['updatedAt'] = undefined;
/**
 * 기간설정 여부
 * @member {module:model/BoardNotice.IsSetPeriodEnum} isSetPeriod
 */

BoardNotice.prototype['isSetPeriod'] = undefined;
/**
 * 팝업노출기간 - 시작
 * @member {Date} popupStartedAt
 */

BoardNotice.prototype['popupStartedAt'] = undefined;
/**
 * 팝업노출기간 - 종료
 * @member {Date} popupEndedAt
 */

BoardNotice.prototype['popupEndedAt'] = undefined;
/**
 * Allowed values for the <code>contentsType</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['ContentsTypeEnum'] = {
  /**
   * value: "promotion"
   * @const
   */
  "promotion": "promotion",

  /**
   * value: "etc"
   * @const
   */
  "etc": "etc",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isSecret</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsSecretEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isNotice</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsNoticeEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isPopup</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsPopupEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isForcedToRead</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsForcedToReadEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>boardStatus</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['BoardStatusEnum'] = {
  /**
   * value: "posting"
   * @const
   */
  "posting": "posting",

  /**
   * value: "exit"
   * @const
   */
  "exit": "exit",

  /**
   * value: "reservation"
   * @const
   */
  "reservation": "reservation",

  /**
   * value: "hide"
   * @const
   */
  "hide": "hide",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>boardAfterStatus</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['BoardAfterStatusEnum'] = {
  /**
   * value: "posting"
   * @const
   */
  "posting": "posting",

  /**
   * value: "exit"
   * @const
   */
  "exit": "exit",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>popupPosition</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['PopupPositionEnum'] = {
  /**
   * value: "all"
   * @const
   */
  "all": "all",

  /**
   * value: "dashboard"
   * @const
   */
  "dashboard": "dashboard",

  /**
   * value: "productRegist"
   * @const
   */
  "productRegist": "productRegist",

  /**
   * value: "productApprove"
   * @const
   */
  "productApprove": "productApprove",

  /**
   * value: "calculate"
   * @const
   */
  "calculate": "calculate",

  /**
   * value: "order"
   * @const
   */
  "order": "order",

  /**
   * value: "board"
   * @const
   */
  "board": "board",

  /**
   * value: "linkage"
   * @const
   */
  "linkage": "linkage",

  /**
   * value: "statistics"
   * @const
   */
  "statistics": "statistics",

  /**
   * value: "event"
   * @const
   */
  "event": "event",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isAvailableAgree</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsAvailableAgreeEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isAvailableEventAgree</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsAvailableEventAgreeEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>eventAgreeItemsFlag</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['EventAgreeItemsFlagEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isSetPeriod</code> property.
 * @enum {String}
 * @readonly
 */

BoardNotice['IsSetPeriodEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = BoardNotice;
exports["default"] = _default;
},{"../ApiClient":1}],43:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CodeResource model module.
 * @module model/CodeResource
 * @version 0.0.1
 */
var CodeResource = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CodeResource</code>.
   * @alias module:model/CodeResource
   */
  function CodeResource() {
    _classCallCheck(this, CodeResource);

    CodeResource.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CodeResource, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CodeResource</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CodeResource} obj Optional instance to populate.
     * @return {module:model/CodeResource} The populated <code>CodeResource</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CodeResource();

        if (data.hasOwnProperty('groupCode')) {
          obj['groupCode'] = _ApiClient["default"].convertToType(data['groupCode'], 'String');
        }

        if (data.hasOwnProperty('itemCode')) {
          obj['itemCode'] = _ApiClient["default"].convertToType(data['itemCode'], 'String');
        }

        if (data.hasOwnProperty('itemName')) {
          obj['itemName'] = _ApiClient["default"].convertToType(data['itemName'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CodeResource;
}();
/**
 * Group Code
 * @member {String} groupCode
 */


CodeResource.prototype['groupCode'] = undefined;
/**
 * Group Code
 * @member {String} itemCode
 */

CodeResource.prototype['itemCode'] = undefined;
/**
 * Group Code
 * @member {String} itemName
 */

CodeResource.prototype['itemName'] = undefined;
var _default = CodeResource;
exports["default"] = _default;
},{"../ApiClient":1}],44:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The ConfirmMobileAuthRequest model module.
 * @module model/ConfirmMobileAuthRequest
 * @version 0.0.1
 */
var ConfirmMobileAuthRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ConfirmMobileAuthRequest</code>.
   * @alias module:model/ConfirmMobileAuthRequest
   * @param firstNumber {String} 처음번호
   * @param middleNumber {String} 중간번호
   * @param lastNumber {String} 끝번호
   * @param mobileAuthToken {String} 휴대폰 인증 토큰
   * @param authKey {String} 휴대폰 인증 번호
   */
  function ConfirmMobileAuthRequest(firstNumber, middleNumber, lastNumber, mobileAuthToken, authKey) {
    _classCallCheck(this, ConfirmMobileAuthRequest);

    ConfirmMobileAuthRequest.initialize(this, firstNumber, middleNumber, lastNumber, mobileAuthToken, authKey);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ConfirmMobileAuthRequest, null, [{
    key: "initialize",
    value: function initialize(obj, firstNumber, middleNumber, lastNumber, mobileAuthToken, authKey) {
      obj['firstNumber'] = firstNumber;
      obj['middleNumber'] = middleNumber;
      obj['lastNumber'] = lastNumber;
      obj['mobileAuthToken'] = mobileAuthToken;
      obj['authKey'] = authKey;
    }
    /**
     * Constructs a <code>ConfirmMobileAuthRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ConfirmMobileAuthRequest} obj Optional instance to populate.
     * @return {module:model/ConfirmMobileAuthRequest} The populated <code>ConfirmMobileAuthRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ConfirmMobileAuthRequest();

        if (data.hasOwnProperty('firstNumber')) {
          obj['firstNumber'] = _ApiClient["default"].convertToType(data['firstNumber'], 'String');
        }

        if (data.hasOwnProperty('middleNumber')) {
          obj['middleNumber'] = _ApiClient["default"].convertToType(data['middleNumber'], 'String');
        }

        if (data.hasOwnProperty('lastNumber')) {
          obj['lastNumber'] = _ApiClient["default"].convertToType(data['lastNumber'], 'String');
        }

        if (data.hasOwnProperty('mobileAuthToken')) {
          obj['mobileAuthToken'] = _ApiClient["default"].convertToType(data['mobileAuthToken'], 'String');
        }

        if (data.hasOwnProperty('authKey')) {
          obj['authKey'] = _ApiClient["default"].convertToType(data['authKey'], 'String');
        }
      }

      return obj;
    }
  }]);

  return ConfirmMobileAuthRequest;
}();
/**
 * 처음번호
 * @member {String} firstNumber
 */


ConfirmMobileAuthRequest.prototype['firstNumber'] = undefined;
/**
 * 중간번호
 * @member {String} middleNumber
 */

ConfirmMobileAuthRequest.prototype['middleNumber'] = undefined;
/**
 * 끝번호
 * @member {String} lastNumber
 */

ConfirmMobileAuthRequest.prototype['lastNumber'] = undefined;
/**
 * 휴대폰 인증 토큰
 * @member {String} mobileAuthToken
 */

ConfirmMobileAuthRequest.prototype['mobileAuthToken'] = undefined;
/**
 * 휴대폰 인증 번호
 * @member {String} authKey
 */

ConfirmMobileAuthRequest.prototype['authKey'] = undefined;
var _default = ConfirmMobileAuthRequest;
exports["default"] = _default;
},{"../ApiClient":1}],45:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateDetailInfoOfProviderRequestBrandDatasInnerInner = _interopRequireDefault(require("./CreateDetailInfoOfProviderRequestBrandDatasInnerInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateDetailInfoOfProviderRequest model module.
 * @module model/CreateDetailInfoOfProviderRequest
 * @version 0.0.1
 */
var CreateDetailInfoOfProviderRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateDetailInfoOfProviderRequest</code>.
   * @alias module:model/CreateDetailInfoOfProviderRequest
   */
  function CreateDetailInfoOfProviderRequest() {
    _classCallCheck(this, CreateDetailInfoOfProviderRequest);

    CreateDetailInfoOfProviderRequest.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateDetailInfoOfProviderRequest, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateDetailInfoOfProviderRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateDetailInfoOfProviderRequest} obj Optional instance to populate.
     * @return {module:model/CreateDetailInfoOfProviderRequest} The populated <code>CreateDetailInfoOfProviderRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateDetailInfoOfProviderRequest();

        if (data.hasOwnProperty('bankName')) {
          obj['bankName'] = _ApiClient["default"].convertToType(data['bankName'], 'String');
        }

        if (data.hasOwnProperty('account')) {
          obj['account'] = _ApiClient["default"].convertToType(data['account'], 'String');
        }

        if (data.hasOwnProperty('accountHolder')) {
          obj['accountHolder'] = _ApiClient["default"].convertToType(data['accountHolder'], 'String');
        }

        if (data.hasOwnProperty('bankbook')) {
          obj['bankbook'] = _ApiClient["default"].convertToType(data['bankbook'], File);
        }

        if (data.hasOwnProperty('businessCondition')) {
          obj['businessCondition'] = _ApiClient["default"].convertToType(data['businessCondition'], 'String');
        }

        if (data.hasOwnProperty('businessType')) {
          obj['businessType'] = _ApiClient["default"].convertToType(data['businessType'], 'String');
        }

        if (data.hasOwnProperty('taxPayerType')) {
          obj['taxPayerType'] = _ApiClient["default"].convertToType(data['taxPayerType'], 'String');
        }

        if (data.hasOwnProperty('zipCode')) {
          obj['zipCode'] = _ApiClient["default"].convertToType(data['zipCode'], 'String');
        }

        if (data.hasOwnProperty('address')) {
          obj['address'] = _ApiClient["default"].convertToType(data['address'], 'String');
        }

        if (data.hasOwnProperty('detailAddress')) {
          obj['detailAddress'] = _ApiClient["default"].convertToType(data['detailAddress'], 'String');
        }

        if (data.hasOwnProperty('businessLicense')) {
          obj['businessLicense'] = _ApiClient["default"].convertToType(data['businessLicense'], File);
        }

        if (data.hasOwnProperty('isAutoDiscounthoppingMall')) {
          obj['isAutoDiscounthoppingMall'] = _ApiClient["default"].convertToType(data['isAutoDiscounthoppingMall'], 'String');
        }

        if (data.hasOwnProperty('isAutoExposureNaverShopping')) {
          obj['isAutoExposureNaverShopping'] = _ApiClient["default"].convertToType(data['isAutoExposureNaverShopping'], 'String');
        }

        if (data.hasOwnProperty('brandDatas')) {
          obj['brandDatas'] = _ApiClient["default"].convertToType(data['brandDatas'], [[_CreateDetailInfoOfProviderRequestBrandDatasInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('categoryCodes')) {
          obj['categoryCodes'] = _ApiClient["default"].convertToType(data['categoryCodes'], ['String']);
        }

        if (data.hasOwnProperty('companyLogo')) {
          obj['companyLogo'] = _ApiClient["default"].convertToType(data['companyLogo'], File);
        }
      }

      return obj;
    }
  }]);

  return CreateDetailInfoOfProviderRequest;
}();
/**
 * 은행명
 * @member {String} bankName
 */


CreateDetailInfoOfProviderRequest.prototype['bankName'] = undefined;
/**
 * 예금주
 * @member {String} account
 */

CreateDetailInfoOfProviderRequest.prototype['account'] = undefined;
/**
 * 계좌번호
 * @member {String} accountHolder
 */

CreateDetailInfoOfProviderRequest.prototype['accountHolder'] = undefined;
/**
 * 통장사본
 * @member {File} bankbook
 */

CreateDetailInfoOfProviderRequest.prototype['bankbook'] = undefined;
/**
 * 업태
 * @member {String} businessCondition
 */

CreateDetailInfoOfProviderRequest.prototype['businessCondition'] = undefined;
/**
 * 업종
 * @member {String} businessType
 */

CreateDetailInfoOfProviderRequest.prototype['businessType'] = undefined;
/**
 * 과세 타입
 * @member {module:model/CreateDetailInfoOfProviderRequest.TaxPayerTypeEnum} taxPayerType
 */

CreateDetailInfoOfProviderRequest.prototype['taxPayerType'] = undefined;
/**
 * 우편번호
 * @member {String} zipCode
 */

CreateDetailInfoOfProviderRequest.prototype['zipCode'] = undefined;
/**
 * 주소
 * @member {String} address
 */

CreateDetailInfoOfProviderRequest.prototype['address'] = undefined;
/**
 * 상세주소
 * @member {String} detailAddress
 */

CreateDetailInfoOfProviderRequest.prototype['detailAddress'] = undefined;
/**
 * 사업자 등록증 사본
 * @member {File} businessLicense
 */

CreateDetailInfoOfProviderRequest.prototype['businessLicense'] = undefined;
/**
 * 자동 할인율 적용여부
 * @member {module:model/CreateDetailInfoOfProviderRequest.IsAutoDiscounthoppingMallEnum} isAutoDiscounthoppingMall
 */

CreateDetailInfoOfProviderRequest.prototype['isAutoDiscounthoppingMall'] = undefined;
/**
 * 자동 네이버 지식쇼핑 노출여부
 * @member {module:model/CreateDetailInfoOfProviderRequest.IsAutoExposureNaverShoppingEnum} isAutoExposureNaverShopping
 */

CreateDetailInfoOfProviderRequest.prototype['isAutoExposureNaverShopping'] = undefined;
/**
 * 브랜드 데이터
 * @member {Array.<Array.<module:model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner>>} brandDatas
 */

CreateDetailInfoOfProviderRequest.prototype['brandDatas'] = undefined;
/**
 * 카테고리 코드
 * @member {Array.<String>} categoryCodes
 */

CreateDetailInfoOfProviderRequest.prototype['categoryCodes'] = undefined;
/**
 * 회사 대표 이미지/로고
 * @member {File} companyLogo
 */

CreateDetailInfoOfProviderRequest.prototype['companyLogo'] = undefined;
/**
 * Allowed values for the <code>taxPayerType</code> property.
 * @enum {String}
 * @readonly
 */

CreateDetailInfoOfProviderRequest['TaxPayerTypeEnum'] = {
  /**
   * value: "general"
   * @const
   */
  "general": "general",

  /**
   * value: "simplified"
   * @const
   */
  "simplified": "simplified",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isAutoDiscounthoppingMall</code> property.
 * @enum {String}
 * @readonly
 */

CreateDetailInfoOfProviderRequest['IsAutoDiscounthoppingMallEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isAutoExposureNaverShopping</code> property.
 * @enum {String}
 * @readonly
 */

CreateDetailInfoOfProviderRequest['IsAutoExposureNaverShoppingEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = CreateDetailInfoOfProviderRequest;
exports["default"] = _default;
},{"../ApiClient":1,"./CreateDetailInfoOfProviderRequestBrandDatasInnerInner":46}],46:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateDetailInfoOfProviderRequestBrandDatasInnerInner model module.
 * @module model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner
 * @version 0.0.1
 */
var CreateDetailInfoOfProviderRequestBrandDatasInnerInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateDetailInfoOfProviderRequestBrandDatasInnerInner</code>.
   * @alias module:model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner
   * @param brandName {String} 브랜드명
   */
  function CreateDetailInfoOfProviderRequestBrandDatasInnerInner(brandName) {
    _classCallCheck(this, CreateDetailInfoOfProviderRequestBrandDatasInnerInner);

    CreateDetailInfoOfProviderRequestBrandDatasInnerInner.initialize(this, brandName);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateDetailInfoOfProviderRequestBrandDatasInnerInner, null, [{
    key: "initialize",
    value: function initialize(obj, brandName) {
      obj['brandName'] = brandName;
    }
    /**
     * Constructs a <code>CreateDetailInfoOfProviderRequestBrandDatasInnerInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner} obj Optional instance to populate.
     * @return {module:model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner} The populated <code>CreateDetailInfoOfProviderRequestBrandDatasInnerInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateDetailInfoOfProviderRequestBrandDatasInnerInner();

        if (data.hasOwnProperty('brandNumber')) {
          obj['brandNumber'] = _ApiClient["default"].convertToType(data['brandNumber'], 'Number');
        }

        if (data.hasOwnProperty('brandName')) {
          obj['brandName'] = _ApiClient["default"].convertToType(data['brandName'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateDetailInfoOfProviderRequestBrandDatasInnerInner;
}();
/**
 * 브랜드번호
 * @member {Number} brandNumber
 */


CreateDetailInfoOfProviderRequestBrandDatasInnerInner.prototype['brandNumber'] = undefined;
/**
 * 브랜드명
 * @member {String} brandName
 */

CreateDetailInfoOfProviderRequestBrandDatasInnerInner.prototype['brandName'] = undefined;
var _default = CreateDetailInfoOfProviderRequestBrandDatasInnerInner;
exports["default"] = _default;
},{"../ApiClient":1}],47:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateProductRequestCertificationDatasInnerInner = _interopRequireDefault(require("./CreateProductRequestCertificationDatasInnerInner"));

var _CreateProductRequestOptionsInnerInner = _interopRequireDefault(require("./CreateProductRequestOptionsInnerInner"));

var _CreateProductRequestProductImageDatasInnerInner = _interopRequireDefault(require("./CreateProductRequestProductImageDatasInnerInner"));

var _CreateProductRequestProductInformationInner = _interopRequireDefault(require("./CreateProductRequestProductInformationInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateProductRequest model module.
 * @module model/CreateProductRequest
 * @version 0.0.1
 */
var CreateProductRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProductRequest</code>.
   * @alias module:model/CreateProductRequest
   * @param categoryCode {String} 카테고리 코드
   * @param brandNumber {Number} 브랜드 번호
   * @param productName {String} 상품명
   * @param productPrice {Number} 상품 판매 가격
   * @param productConsumerPrice {Number} 소비자 가격
   * @param optionTitles {Array.<String>} 상품 옵션 제목
   * @param options {Array.<Array.<module:model/CreateProductRequestOptionsInnerInner>>} 상품 옵션
   * @param productImageDatas {Array.<Array.<module:model/CreateProductRequestProductImageDatasInnerInner>>} 상품 이미지
   * @param description {String} 상품 상세 내용
   * @param productInformation {Array.<module:model/CreateProductRequestProductInformationInner>} 상품 정보 고시
   */
  function CreateProductRequest(categoryCode, brandNumber, productName, productPrice, productConsumerPrice, optionTitles, options, productImageDatas, description, productInformation) {
    _classCallCheck(this, CreateProductRequest);

    CreateProductRequest.initialize(this, categoryCode, brandNumber, productName, productPrice, productConsumerPrice, optionTitles, options, productImageDatas, description, productInformation);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProductRequest, null, [{
    key: "initialize",
    value: function initialize(obj, categoryCode, brandNumber, productName, productPrice, productConsumerPrice, optionTitles, options, productImageDatas, description, productInformation) {
      obj['categoryCode'] = categoryCode;
      obj['brandNumber'] = brandNumber;
      obj['productName'] = productName;
      obj['productPrice'] = productPrice;
      obj['productConsumerPrice'] = productConsumerPrice;
      obj['optionTitles'] = optionTitles;
      obj['options'] = options;
      obj['productImageDatas'] = productImageDatas;
      obj['description'] = description;
      obj['productInformation'] = productInformation;
    }
    /**
     * Constructs a <code>CreateProductRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequest} obj Optional instance to populate.
     * @return {module:model/CreateProductRequest} The populated <code>CreateProductRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProductRequest();

        if (data.hasOwnProperty('categoryCode')) {
          obj['categoryCode'] = _ApiClient["default"].convertToType(data['categoryCode'], 'String');
        }

        if (data.hasOwnProperty('brandNumber')) {
          obj['brandNumber'] = _ApiClient["default"].convertToType(data['brandNumber'], 'Number');
        }

        if (data.hasOwnProperty('maker')) {
          obj['maker'] = _ApiClient["default"].convertToType(data['maker'], 'String');
        }

        if (data.hasOwnProperty('origin')) {
          obj['origin'] = _ApiClient["default"].convertToType(data['origin'], 'String');
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('productName')) {
          obj['productName'] = _ApiClient["default"].convertToType(data['productName'], 'String');
        }

        if (data.hasOwnProperty('keywords')) {
          obj['keywords'] = _ApiClient["default"].convertToType(data['keywords'], ['String']);
        }

        if (data.hasOwnProperty('isUsed')) {
          obj['isUsed'] = _ApiClient["default"].convertToType(data['isUsed'], 'String');
        }

        if (data.hasOwnProperty('isSellerDeal')) {
          obj['isSellerDeal'] = _ApiClient["default"].convertToType(data['isSellerDeal'], 'String');
        }

        if (data.hasOwnProperty('isTemporary')) {
          obj['isTemporary'] = _ApiClient["default"].convertToType(data['isTemporary'], 'String');
        }

        if (data.hasOwnProperty('deliveryType')) {
          obj['deliveryType'] = _ApiClient["default"].convertToType(data['deliveryType'], 'String');
        }

        if (data.hasOwnProperty('deliveryPrice')) {
          obj['deliveryPrice'] = _ApiClient["default"].convertToType(data['deliveryPrice'], 'Number');
        }

        if (data.hasOwnProperty('isManufactured')) {
          obj['isManufactured'] = _ApiClient["default"].convertToType(data['isManufactured'], 'String');
        }

        if (data.hasOwnProperty('manufacturingPeriod')) {
          obj['manufacturingPeriod'] = _ApiClient["default"].convertToType(data['manufacturingPeriod'], 'String');
        }

        if (data.hasOwnProperty('isReservationDelivery')) {
          obj['isReservationDelivery'] = _ApiClient["default"].convertToType(data['isReservationDelivery'], 'String');
        }

        if (data.hasOwnProperty('reservationDeliveryDate')) {
          obj['reservationDeliveryDate'] = _ApiClient["default"].convertToType(data['reservationDeliveryDate'], 'String');
        }

        if (data.hasOwnProperty('isOverseasDelivery')) {
          obj['isOverseasDelivery'] = _ApiClient["default"].convertToType(data['isOverseasDelivery'], 'String');
        }

        if (data.hasOwnProperty('overseasDeliveryPeriod')) {
          obj['overseasDeliveryPeriod'] = _ApiClient["default"].convertToType(data['overseasDeliveryPeriod'], 'String');
        }

        if (data.hasOwnProperty('productPrice')) {
          obj['productPrice'] = _ApiClient["default"].convertToType(data['productPrice'], 'Number');
        }

        if (data.hasOwnProperty('productSupplyPrice')) {
          obj['productSupplyPrice'] = _ApiClient["default"].convertToType(data['productSupplyPrice'], 'Number');
        }

        if (data.hasOwnProperty('productConsumerPrice')) {
          obj['productConsumerPrice'] = _ApiClient["default"].convertToType(data['productConsumerPrice'], 'Number');
        }

        if (data.hasOwnProperty('productAbidePrice')) {
          obj['productAbidePrice'] = _ApiClient["default"].convertToType(data['productAbidePrice'], 'Number');
        }

        if (data.hasOwnProperty('productWolesalePrice')) {
          obj['productWolesalePrice'] = _ApiClient["default"].convertToType(data['productWolesalePrice'], 'Number');
        }

        if (data.hasOwnProperty('isUsingStock')) {
          obj['isUsingStock'] = _ApiClient["default"].convertToType(data['isUsingStock'], 'String');
        }

        if (data.hasOwnProperty('isTaxable')) {
          obj['isTaxable'] = _ApiClient["default"].convertToType(data['isTaxable'], 'String');
        }

        if (data.hasOwnProperty('optionTitles')) {
          obj['optionTitles'] = _ApiClient["default"].convertToType(data['optionTitles'], ['String']);
        }

        if (data.hasOwnProperty('optionType')) {
          obj['optionType'] = _ApiClient["default"].convertToType(data['optionType'], 'String');
        }

        if (data.hasOwnProperty('options')) {
          obj['options'] = _ApiClient["default"].convertToType(data['options'], [[_CreateProductRequestOptionsInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('isRunouted')) {
          obj['isRunouted'] = _ApiClient["default"].convertToType(data['isRunouted'], 'String');
        }

        if (data.hasOwnProperty('productImageDatas')) {
          obj['productImageDatas'] = _ApiClient["default"].convertToType(data['productImageDatas'], [[_CreateProductRequestProductImageDatasInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('kcCertificationType')) {
          obj['kcCertificationType'] = _ApiClient["default"].convertToType(data['kcCertificationType'], 'String');
        }

        if (data.hasOwnProperty('kcCertificationInfo')) {
          obj['kcCertificationInfo'] = _ApiClient["default"].convertToType(data['kcCertificationInfo'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationType')) {
          obj['ecoCertificationType'] = _ApiClient["default"].convertToType(data['ecoCertificationType'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationAuth')) {
          obj['ecoCertificationAuth'] = _ApiClient["default"].convertToType(data['ecoCertificationAuth'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationCode')) {
          obj['ecoCertificationCode'] = _ApiClient["default"].convertToType(data['ecoCertificationCode'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationMark')) {
          obj['ecoCertificationMark'] = _ApiClient["default"].convertToType(data['ecoCertificationMark'], 'String');
        }

        if (data.hasOwnProperty('certificationDatas')) {
          obj['certificationDatas'] = _ApiClient["default"].convertToType(data['certificationDatas'], [[_CreateProductRequestCertificationDatasInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('productInformation')) {
          obj['productInformation'] = _ApiClient["default"].convertToType(data['productInformation'], [_CreateProductRequestProductInformationInner["default"]]);
        }

        if (data.hasOwnProperty('manageMemo')) {
          obj['manageMemo'] = _ApiClient["default"].convertToType(data['manageMemo'], 'String');
        }

        if (data.hasOwnProperty('productMemo')) {
          obj['productMemo'] = _ApiClient["default"].convertToType(data['productMemo'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateProductRequest;
}();
/**
 * 카테고리 코드
 * @member {String} categoryCode
 */


CreateProductRequest.prototype['categoryCode'] = undefined;
/**
 * 브랜드 번호
 * @member {Number} brandNumber
 */

CreateProductRequest.prototype['brandNumber'] = undefined;
/**
 * 메이커
 * @member {String} maker
 */

CreateProductRequest.prototype['maker'] = undefined;
/**
 * 원산지
 * @member {String} origin
 */

CreateProductRequest.prototype['origin'] = undefined;
/**
 * 브랜드 번호
 * @member {String} productCode
 */

CreateProductRequest.prototype['productCode'] = undefined;
/**
 * 상품명
 * @member {String} productName
 */

CreateProductRequest.prototype['productName'] = undefined;
/**
 * 키워드
 * @member {Array.<String>} keywords
 */

CreateProductRequest.prototype['keywords'] = undefined;
/**
 * 중고여부
 * @member {module:model/CreateProductRequest.IsUsedEnum} isUsed
 */

CreateProductRequest.prototype['isUsed'] = undefined;
/**
 * 셀러딜 상품여부
 * @member {module:model/CreateProductRequest.IsSellerDealEnum} isSellerDeal
 */

CreateProductRequest.prototype['isSellerDeal'] = undefined;
/**
 * 임시상품 여부
 * @member {module:model/CreateProductRequest.IsTemporaryEnum} isTemporary
 */

CreateProductRequest.prototype['isTemporary'] = undefined;
/**
 * 배송 유형
 * @member {module:model/CreateProductRequest.DeliveryTypeEnum} deliveryType
 */

CreateProductRequest.prototype['deliveryType'] = undefined;
/**
 * 배송비
 * @member {Number} deliveryPrice
 */

CreateProductRequest.prototype['deliveryPrice'] = undefined;
/**
 * 제조 상품 여부
 * @member {module:model/CreateProductRequest.IsManufacturedEnum} isManufactured
 */

CreateProductRequest.prototype['isManufactured'] = undefined;
/**
 * 제조기간
 * @member {String} manufacturingPeriod
 */

CreateProductRequest.prototype['manufacturingPeriod'] = undefined;
/**
 * 예약 상품 여부
 * @member {module:model/CreateProductRequest.IsReservationDeliveryEnum} isReservationDelivery
 */

CreateProductRequest.prototype['isReservationDelivery'] = undefined;
/**
 * 예약발송일자
 * @member {String} reservationDeliveryDate
 */

CreateProductRequest.prototype['reservationDeliveryDate'] = undefined;
/**
 * 해외 상품 여부
 * @member {module:model/CreateProductRequest.IsOverseasDeliveryEnum} isOverseasDelivery
 */

CreateProductRequest.prototype['isOverseasDelivery'] = undefined;
/**
 * 해외배송기간
 * @member {String} overseasDeliveryPeriod
 */

CreateProductRequest.prototype['overseasDeliveryPeriod'] = undefined;
/**
 * 상품 판매 가격
 * @member {Number} productPrice
 */

CreateProductRequest.prototype['productPrice'] = undefined;
/**
 * 상품 공급 가격
 * @member {Number} productSupplyPrice
 */

CreateProductRequest.prototype['productSupplyPrice'] = undefined;
/**
 * 소비자 가격
 * @member {Number} productConsumerPrice
 */

CreateProductRequest.prototype['productConsumerPrice'] = undefined;
/**
 * 준수 판매 가격
 * @member {Number} productAbidePrice
 */

CreateProductRequest.prototype['productAbidePrice'] = undefined;
/**
 * 도매 가격
 * @member {Number} productWolesalePrice
 */

CreateProductRequest.prototype['productWolesalePrice'] = undefined;
/**
 * 재고량 연동 여부
 * @member {module:model/CreateProductRequest.IsUsingStockEnum} isUsingStock
 */

CreateProductRequest.prototype['isUsingStock'] = undefined;
/**
 * 과세 여부
 * @member {module:model/CreateProductRequest.IsTaxableEnum} isTaxable
 */

CreateProductRequest.prototype['isTaxable'] = undefined;
/**
 * 상품 옵션 제목
 * @member {Array.<String>} optionTitles
 */

CreateProductRequest.prototype['optionTitles'] = undefined;
/**
 * 옵션 유형
 * @member {module:model/CreateProductRequest.OptionTypeEnum} optionType
 */

CreateProductRequest.prototype['optionType'] = undefined;
/**
 * 상품 옵션
 * @member {Array.<Array.<module:model/CreateProductRequestOptionsInnerInner>>} options
 */

CreateProductRequest.prototype['options'] = undefined;
/**
 * 품절 여부
 * @member {module:model/CreateProductRequest.IsRunoutedEnum} isRunouted
 */

CreateProductRequest.prototype['isRunouted'] = undefined;
/**
 * 상품 이미지
 * @member {Array.<Array.<module:model/CreateProductRequestProductImageDatasInnerInner>>} productImageDatas
 */

CreateProductRequest.prototype['productImageDatas'] = undefined;
/**
 * 상품 상세 내용
 * @member {String} description
 */

CreateProductRequest.prototype['description'] = undefined;
/**
 * KC 인증 유형
 * @member {module:model/CreateProductRequest.KcCertificationTypeEnum} kcCertificationType
 */

CreateProductRequest.prototype['kcCertificationType'] = undefined;
/**
 * KC 인증 정보
 * @member {String} kcCertificationInfo
 */

CreateProductRequest.prototype['kcCertificationInfo'] = undefined;
/**
 * 친환경 인증 유형
 * @member {module:model/CreateProductRequest.EcoCertificationTypeEnum} ecoCertificationType
 */

CreateProductRequest.prototype['ecoCertificationType'] = undefined;
/**
 * 친환경 인증 기관
 * @member {String} ecoCertificationAuth
 */

CreateProductRequest.prototype['ecoCertificationAuth'] = undefined;
/**
 * 친환경 인증 번호
 * @member {String} ecoCertificationCode
 */

CreateProductRequest.prototype['ecoCertificationCode'] = undefined;
/**
 * 친환경 인증 마크 사용여부
 * @member {module:model/CreateProductRequest.EcoCertificationMarkEnum} ecoCertificationMark
 */

CreateProductRequest.prototype['ecoCertificationMark'] = undefined;
/**
 * 상품 인증자료 첨부
 * @member {Array.<Array.<module:model/CreateProductRequestCertificationDatasInnerInner>>} certificationDatas
 */

CreateProductRequest.prototype['certificationDatas'] = undefined;
/**
 * 상품 정보 고시
 * @member {Array.<module:model/CreateProductRequestProductInformationInner>} productInformation
 */

CreateProductRequest.prototype['productInformation'] = undefined;
/**
 * 관리 메모
 * @member {String} manageMemo
 */

CreateProductRequest.prototype['manageMemo'] = undefined;
/**
 * 상품 메모
 * @member {String} productMemo
 */

CreateProductRequest.prototype['productMemo'] = undefined;
/**
 * Allowed values for the <code>isUsed</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsUsedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isSellerDeal</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsSellerDealEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isTemporary</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsTemporaryEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>deliveryType</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['DeliveryTypeEnum'] = {
  /**
   * value: "wait"
   * @const
   */
  "wait": "wait",

  /**
   * value: "request"
   * @const
   */
  "request": "request",

  /**
   * value: "approved"
   * @const
   */
  "approved": "approved",

  /**
   * value: "hold"
   * @const
   */
  "hold": "hold",

  /**
   * value: "reject"
   * @const
   */
  "reject": "reject",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isManufactured</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsManufacturedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isReservationDelivery</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsReservationDeliveryEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isOverseasDelivery</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsOverseasDeliveryEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isUsingStock</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsUsingStockEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isTaxable</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsTaxableEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>optionType</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['OptionTypeEnum'] = {
  /**
   * value: "single"
   * @const
   */
  "single": "single",

  /**
   * value: "double"
   * @const
   */
  "double": "double",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isRunouted</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['IsRunoutedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>kcCertificationType</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['KcCertificationTypeEnum'] = {
  /**
   * value: "해당없음"
   * @const
   */
  "해당없음": "해당없음",

  /**
   * value: "상세설명참조"
   * @const
   */
  "상세설명참조": "상세설명참조",

  /**
   * value: "[생활용품]안전인증"
   * @const
   */
  "[생활용품]안전인증": "[생활용품]안전인증",

  /**
   * value: "[생활용품]안전확인"
   * @const
   */
  "[생활용품]안전확인": "[생활용품]안전확인",

  /**
   * value: "[생활용품]공급자적합성확인"
   * @const
   */
  "[생활용품]공급자적합성확인": "[생활용품]공급자적합성확인",

  /**
   * value: "[생활용품]어린이보호포장"
   * @const
   */
  "[생활용품]어린이보호포장": "[생활용품]어린이보호포장",

  /**
   * value: "[어린이제품]안전인증"
   * @const
   */
  "[어린이제품]안전인증": "[어린이제품]안전인증",

  /**
   * value: "[어린이제품]안전확인"
   * @const
   */
  "[어린이제품]안전확인": "[어린이제품]안전확인",

  /**
   * value: "[어린이제품]공급자적합성확인"
   * @const
   */
  "[어린이제품]공급자적합성확인": "[어린이제품]공급자적합성확인",

  /**
   * value: "[전기용품]안전인증"
   * @const
   */
  "[전기용품]안전인증": "[전기용품]안전인증",

  /**
   * value: "[전기용품]안전확인"
   * @const
   */
  "[전기용품]안전확인": "[전기용품]안전확인",

  /**
   * value: "[전기용품]공급자적합성확인"
   * @const
   */
  "[전기용품]공급자적합성확인": "[전기용품]공급자적합성확인",

  /**
   * value: "[방송통신기자재]적합성평가"
   * @const
   */
  "[방송통신기자재]적합성평가": "[방송통신기자재]적합성평가",

  /**
   * value: "[위해우려제품]자가검사번호"
   * @const
   */
  "[위해우려제품]자가검사번호": "[위해우려제품]자가검사번호",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>ecoCertificationType</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['EcoCertificationTypeEnum'] = {
  /**
   * value: "[친환경]유기축산물"
   * @const
   */
  "유기축산물": "[친환경]유기축산물",

  /**
   * value: "[친환경]유기농산물"
   * @const
   */
  "유기농산물": "[친환경]유기농산물",

  /**
   * value: "[친환경]무항생제축산물"
   * @const
   */
  "무항생제축산물": "[친환경]무항생제축산물",

  /**
   * value: "[친환경]무농약농산물"
   * @const
   */
  "무농약농산물": "[친환경]무농약농산물",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>ecoCertificationMark</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequest['EcoCertificationMarkEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = CreateProductRequest;
exports["default"] = _default;
},{"../ApiClient":1,"./CreateProductRequestCertificationDatasInnerInner":48,"./CreateProductRequestOptionsInnerInner":49,"./CreateProductRequestProductImageDatasInnerInner":50,"./CreateProductRequestProductInformationInner":51}],48:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateProductRequestCertificationDatasInnerInner model module.
 * @module model/CreateProductRequestCertificationDatasInnerInner
 * @version 0.0.1
 */
var CreateProductRequestCertificationDatasInnerInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProductRequestCertificationDatasInnerInner</code>.
   * @alias module:model/CreateProductRequestCertificationDatasInnerInner
   * @param type {module:model/CreateProductRequestCertificationDatasInnerInner.TypeEnum} 
   */
  function CreateProductRequestCertificationDatasInnerInner(type) {
    _classCallCheck(this, CreateProductRequestCertificationDatasInnerInner);

    CreateProductRequestCertificationDatasInnerInner.initialize(this, type);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProductRequestCertificationDatasInnerInner, null, [{
    key: "initialize",
    value: function initialize(obj, type) {
      obj['type'] = type;
    }
    /**
     * Constructs a <code>CreateProductRequestCertificationDatasInnerInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequestCertificationDatasInnerInner} obj Optional instance to populate.
     * @return {module:model/CreateProductRequestCertificationDatasInnerInner} The populated <code>CreateProductRequestCertificationDatasInnerInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProductRequestCertificationDatasInnerInner();

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('url')) {
          obj['url'] = _ApiClient["default"].convertToType(data['url'], 'String');
        }

        if (data.hasOwnProperty('attachment')) {
          obj['attachment'] = _ApiClient["default"].convertToType(data['attachment'], 'Blob');
        }

        if (data.hasOwnProperty('memo')) {
          obj['memo'] = _ApiClient["default"].convertToType(data['memo'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateProductRequestCertificationDatasInnerInner;
}();
/**
 * @member {module:model/CreateProductRequestCertificationDatasInnerInner.TypeEnum} type
 */


CreateProductRequestCertificationDatasInnerInner.prototype['type'] = undefined;
/**
 * @member {String} url
 */

CreateProductRequestCertificationDatasInnerInner.prototype['url'] = undefined;
/**
 * @member {Blob} attachment
 */

CreateProductRequestCertificationDatasInnerInner.prototype['attachment'] = undefined;
/**
 * @member {String} memo
 */

CreateProductRequestCertificationDatasInnerInner.prototype['memo'] = undefined;
/**
 * Allowed values for the <code>type</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequestCertificationDatasInnerInner['TypeEnum'] = {
  /**
   * value: "url"
   * @const
   */
  "url": "url",

  /**
   * value: "base64"
   * @const
   */
  "base64": "base64",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = CreateProductRequestCertificationDatasInnerInner;
exports["default"] = _default;
},{"../ApiClient":1}],49:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateProductRequestOptionsInnerInner model module.
 * @module model/CreateProductRequestOptionsInnerInner
 * @version 0.0.1
 */
var CreateProductRequestOptionsInnerInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProductRequestOptionsInnerInner</code>.
   * @alias module:model/CreateProductRequestOptionsInnerInner
   * @param optionName1 {String} 
   * @param stock {Number} 
   */
  function CreateProductRequestOptionsInnerInner(optionName1, stock) {
    _classCallCheck(this, CreateProductRequestOptionsInnerInner);

    CreateProductRequestOptionsInnerInner.initialize(this, optionName1, stock);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProductRequestOptionsInnerInner, null, [{
    key: "initialize",
    value: function initialize(obj, optionName1, stock) {
      obj['optionName1'] = optionName1;
      obj['stock'] = stock;
    }
    /**
     * Constructs a <code>CreateProductRequestOptionsInnerInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequestOptionsInnerInner} obj Optional instance to populate.
     * @return {module:model/CreateProductRequestOptionsInnerInner} The populated <code>CreateProductRequestOptionsInnerInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProductRequestOptionsInnerInner();

        if (data.hasOwnProperty('optionName1')) {
          obj['optionName1'] = _ApiClient["default"].convertToType(data['optionName1'], 'String');
        }

        if (data.hasOwnProperty('optionName2')) {
          obj['optionName2'] = _ApiClient["default"].convertToType(data['optionName2'], 'String');
        }

        if (data.hasOwnProperty('stock')) {
          obj['stock'] = _ApiClient["default"].convertToType(data['stock'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return CreateProductRequestOptionsInnerInner;
}();
/**
 * @member {String} optionName1
 */


CreateProductRequestOptionsInnerInner.prototype['optionName1'] = undefined;
/**
 * @member {String} optionName2
 */

CreateProductRequestOptionsInnerInner.prototype['optionName2'] = undefined;
/**
 * @member {Number} stock
 */

CreateProductRequestOptionsInnerInner.prototype['stock'] = undefined;
var _default = CreateProductRequestOptionsInnerInner;
exports["default"] = _default;
},{"../ApiClient":1}],50:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateProductRequestProductImageDatasInnerInner model module.
 * @module model/CreateProductRequestProductImageDatasInnerInner
 * @version 0.0.1
 */
var CreateProductRequestProductImageDatasInnerInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProductRequestProductImageDatasInnerInner</code>.
   * @alias module:model/CreateProductRequestProductImageDatasInnerInner
   * @param type {module:model/CreateProductRequestProductImageDatasInnerInner.TypeEnum} 
   */
  function CreateProductRequestProductImageDatasInnerInner(type) {
    _classCallCheck(this, CreateProductRequestProductImageDatasInnerInner);

    CreateProductRequestProductImageDatasInnerInner.initialize(this, type);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProductRequestProductImageDatasInnerInner, null, [{
    key: "initialize",
    value: function initialize(obj, type) {
      obj['type'] = type;
    }
    /**
     * Constructs a <code>CreateProductRequestProductImageDatasInnerInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequestProductImageDatasInnerInner} obj Optional instance to populate.
     * @return {module:model/CreateProductRequestProductImageDatasInnerInner} The populated <code>CreateProductRequestProductImageDatasInnerInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProductRequestProductImageDatasInnerInner();

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('url')) {
          obj['url'] = _ApiClient["default"].convertToType(data['url'], 'String');
        }

        if (data.hasOwnProperty('attachment')) {
          obj['attachment'] = _ApiClient["default"].convertToType(data['attachment'], 'Blob');
        }
      }

      return obj;
    }
  }]);

  return CreateProductRequestProductImageDatasInnerInner;
}();
/**
 * @member {module:model/CreateProductRequestProductImageDatasInnerInner.TypeEnum} type
 */


CreateProductRequestProductImageDatasInnerInner.prototype['type'] = undefined;
/**
 * @member {String} url
 */

CreateProductRequestProductImageDatasInnerInner.prototype['url'] = undefined;
/**
 * @member {Blob} attachment
 */

CreateProductRequestProductImageDatasInnerInner.prototype['attachment'] = undefined;
/**
 * Allowed values for the <code>type</code> property.
 * @enum {String}
 * @readonly
 */

CreateProductRequestProductImageDatasInnerInner['TypeEnum'] = {
  /**
   * value: "url"
   * @const
   */
  "url": "url",

  /**
   * value: "base64"
   * @const
   */
  "base64": "base64",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = CreateProductRequestProductImageDatasInnerInner;
exports["default"] = _default;
},{"../ApiClient":1}],51:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateProductRequestProductInformationInner model module.
 * @module model/CreateProductRequestProductInformationInner
 * @version 0.0.1
 */
var CreateProductRequestProductInformationInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateProductRequestProductInformationInner</code>.
   * @alias module:model/CreateProductRequestProductInformationInner
   * @param goodsRequired {String} 
   */
  function CreateProductRequestProductInformationInner(goodsRequired) {
    _classCallCheck(this, CreateProductRequestProductInformationInner);

    CreateProductRequestProductInformationInner.initialize(this, goodsRequired);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateProductRequestProductInformationInner, null, [{
    key: "initialize",
    value: function initialize(obj, goodsRequired) {
      obj['goodsRequired'] = goodsRequired;
    }
    /**
     * Constructs a <code>CreateProductRequestProductInformationInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequestProductInformationInner} obj Optional instance to populate.
     * @return {module:model/CreateProductRequestProductInformationInner} The populated <code>CreateProductRequestProductInformationInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateProductRequestProductInformationInner();

        if (data.hasOwnProperty('goodsRequired')) {
          obj['goodsRequired'] = _ApiClient["default"].convertToType(data['goodsRequired'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateProductRequestProductInformationInner;
}();
/**
 * @member {String} goodsRequired
 */


CreateProductRequestProductInformationInner.prototype['goodsRequired'] = undefined;
var _default = CreateProductRequestProductInformationInner;
exports["default"] = _default;
},{"../ApiClient":1}],52:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateStaffInfoOfProviderRequestSellerInner = _interopRequireDefault(require("./CreateStaffInfoOfProviderRequestSellerInner"));

var _CreateStaffInfoOfProviderRequestStaffMembersInner = _interopRequireDefault(require("./CreateStaffInfoOfProviderRequestStaffMembersInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateStaffInfoOfProviderRequest model module.
 * @module model/CreateStaffInfoOfProviderRequest
 * @version 0.0.1
 */
var CreateStaffInfoOfProviderRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateStaffInfoOfProviderRequest</code>.
   * @alias module:model/CreateStaffInfoOfProviderRequest
   */
  function CreateStaffInfoOfProviderRequest() {
    _classCallCheck(this, CreateStaffInfoOfProviderRequest);

    CreateStaffInfoOfProviderRequest.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateStaffInfoOfProviderRequest, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateStaffInfoOfProviderRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStaffInfoOfProviderRequest} obj Optional instance to populate.
     * @return {module:model/CreateStaffInfoOfProviderRequest} The populated <code>CreateStaffInfoOfProviderRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateStaffInfoOfProviderRequest();

        if (data.hasOwnProperty('staffMembers')) {
          obj['staffMembers'] = _ApiClient["default"].convertToType(data['staffMembers'], [_CreateStaffInfoOfProviderRequestStaffMembersInner["default"]]);
        }

        if (data.hasOwnProperty('seller')) {
          obj['seller'] = _ApiClient["default"].convertToType(data['seller'], [_CreateStaffInfoOfProviderRequestSellerInner["default"]]);
        }
      }

      return obj;
    }
  }]);

  return CreateStaffInfoOfProviderRequest;
}();
/**
 * 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInner>} staffMembers
 */


CreateStaffInfoOfProviderRequest.prototype['staffMembers'] = undefined;
/**
 * 판매자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestSellerInner>} seller
 */

CreateStaffInfoOfProviderRequest.prototype['seller'] = undefined;
var _default = CreateStaffInfoOfProviderRequest;
exports["default"] = _default;
},{"../ApiClient":1,"./CreateStaffInfoOfProviderRequestSellerInner":53,"./CreateStaffInfoOfProviderRequestStaffMembersInner":54}],53:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateStaffInfoOfProviderRequestSellerInner model module.
 * @module model/CreateStaffInfoOfProviderRequestSellerInner
 * @version 0.0.1
 */
var CreateStaffInfoOfProviderRequestSellerInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateStaffInfoOfProviderRequestSellerInner</code>.
   * @alias module:model/CreateStaffInfoOfProviderRequestSellerInner
   */
  function CreateStaffInfoOfProviderRequestSellerInner() {
    _classCallCheck(this, CreateStaffInfoOfProviderRequestSellerInner);

    CreateStaffInfoOfProviderRequestSellerInner.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateStaffInfoOfProviderRequestSellerInner, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateStaffInfoOfProviderRequestSellerInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStaffInfoOfProviderRequestSellerInner} obj Optional instance to populate.
     * @return {module:model/CreateStaffInfoOfProviderRequestSellerInner} The populated <code>CreateStaffInfoOfProviderRequestSellerInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateStaffInfoOfProviderRequestSellerInner();

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('serviceCenterTel')) {
          obj['serviceCenterTel'] = _ApiClient["default"].convertToType(data['serviceCenterTel'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateStaffInfoOfProviderRequestSellerInner;
}();
/**
 * 판매자명
 * @member {String} name
 */


CreateStaffInfoOfProviderRequestSellerInner.prototype['name'] = undefined;
/**
 * 고객센터 번호
 * @member {String} serviceCenterTel
 */

CreateStaffInfoOfProviderRequestSellerInner.prototype['serviceCenterTel'] = undefined;
var _default = CreateStaffInfoOfProviderRequestSellerInner;
exports["default"] = _default;
},{"../ApiClient":1}],54:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner = _interopRequireDefault(require("./CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateStaffInfoOfProviderRequestStaffMembersInner model module.
 * @module model/CreateStaffInfoOfProviderRequestStaffMembersInner
 * @version 0.0.1
 */
var CreateStaffInfoOfProviderRequestStaffMembersInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateStaffInfoOfProviderRequestStaffMembersInner</code>.
   * @alias module:model/CreateStaffInfoOfProviderRequestStaffMembersInner
   */
  function CreateStaffInfoOfProviderRequestStaffMembersInner() {
    _classCallCheck(this, CreateStaffInfoOfProviderRequestStaffMembersInner);

    CreateStaffInfoOfProviderRequestStaffMembersInner.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateStaffInfoOfProviderRequestStaffMembersInner, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>CreateStaffInfoOfProviderRequestStaffMembersInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStaffInfoOfProviderRequestStaffMembersInner} obj Optional instance to populate.
     * @return {module:model/CreateStaffInfoOfProviderRequestStaffMembersInner} The populated <code>CreateStaffInfoOfProviderRequestStaffMembersInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateStaffInfoOfProviderRequestStaffMembersInner();

        if (data.hasOwnProperty('sales')) {
          obj['sales'] = _ApiClient["default"].convertToType(data['sales'], [_CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner["default"]]);
        }

        if (data.hasOwnProperty('cs')) {
          obj['cs'] = _ApiClient["default"].convertToType(data['cs'], [_CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner["default"]]);
        }

        if (data.hasOwnProperty('delivery')) {
          obj['delivery'] = _ApiClient["default"].convertToType(data['delivery'], [_CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner["default"]]);
        }

        if (data.hasOwnProperty('settle')) {
          obj['settle'] = _ApiClient["default"].convertToType(data['settle'], [_CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner["default"]]);
        }
      }

      return obj;
    }
  }]);

  return CreateStaffInfoOfProviderRequestStaffMembersInner;
}();
/**
 * 영업 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} sales
 */


CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['sales'] = undefined;
/**
 * CS 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} cs
 */

CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['cs'] = undefined;
/**
 * 배송 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} delivery
 */

CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['delivery'] = undefined;
/**
 * 정산 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} settle
 */

CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['settle'] = undefined;
var _default = CreateStaffInfoOfProviderRequestStaffMembersInner;
exports["default"] = _default;
},{"../ApiClient":1,"./CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner":55}],55:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner model module.
 * @module model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner
 * @version 0.0.1
 */
var CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner</code>.
   * @alias module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner
   * @param name {String} 담당자명
   * @param email {String} 담당자 이메일
   * @param tel {String} 담당자 전화번호
   * @param phone {String} 담당자 휴대폰번호
   */
  function CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner(name, email, tel, phone) {
    _classCallCheck(this, CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner);

    CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.initialize(this, name, email, tel, phone);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner, null, [{
    key: "initialize",
    value: function initialize(obj, name, email, tel, phone) {
      obj['name'] = name;
      obj['email'] = email;
      obj['tel'] = tel;
      obj['phone'] = phone;
    }
    /**
     * Constructs a <code>CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner} obj Optional instance to populate.
     * @return {module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner} The populated <code>CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner();

        if (data.hasOwnProperty('name')) {
          obj['name'] = _ApiClient["default"].convertToType(data['name'], 'String');
        }

        if (data.hasOwnProperty('email')) {
          obj['email'] = _ApiClient["default"].convertToType(data['email'], 'String');
        }

        if (data.hasOwnProperty('tel')) {
          obj['tel'] = _ApiClient["default"].convertToType(data['tel'], 'String');
        }

        if (data.hasOwnProperty('phone')) {
          obj['phone'] = _ApiClient["default"].convertToType(data['phone'], 'String');
        }
      }

      return obj;
    }
  }]);

  return CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner;
}();
/**
 * 담당자명
 * @member {String} name
 */


CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.prototype['name'] = undefined;
/**
 * 담당자 이메일
 * @member {String} email
 */

CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.prototype['email'] = undefined;
/**
 * 담당자 전화번호
 * @member {String} tel
 */

CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.prototype['tel'] = undefined;
/**
 * 담당자 휴대폰번호
 * @member {String} phone
 */

CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.prototype['phone'] = undefined;
var _default = CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner;
exports["default"] = _default;
},{"../ApiClient":1}],56:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The DeliveryResource model module.
 * @module model/DeliveryResource
 * @version 0.0.1
 */
var DeliveryResource = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>DeliveryResource</code>.
   * @alias module:model/DeliveryResource
   */
  function DeliveryResource() {
    _classCallCheck(this, DeliveryResource);

    DeliveryResource.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(DeliveryResource, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>DeliveryResource</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/DeliveryResource} obj Optional instance to populate.
     * @return {module:model/DeliveryResource} The populated <code>DeliveryResource</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new DeliveryResource();

        if (data.hasOwnProperty('deliveryNumber')) {
          obj['deliveryNumber'] = _ApiClient["default"].convertToType(data['deliveryNumber'], 'Number');
        }

        if (data.hasOwnProperty('deliveryCompany')) {
          obj['deliveryCompany'] = _ApiClient["default"].convertToType(data['deliveryCompany'], 'String');
        }

        if (data.hasOwnProperty('deliveryUrl')) {
          obj['deliveryUrl'] = _ApiClient["default"].convertToType(data['deliveryUrl'], 'String');
        }

        if (data.hasOwnProperty('useYn')) {
          obj['useYn'] = _ApiClient["default"].convertToType(data['useYn'], 'String');
        }

        if (data.hasOwnProperty('courier')) {
          obj['courier'] = _ApiClient["default"].convertToType(data['courier'], 'String');
        }

        if (data.hasOwnProperty('trackerId')) {
          obj['trackerId'] = _ApiClient["default"].convertToType(data['trackerId'], 'String');
        }

        if (data.hasOwnProperty('validLength')) {
          obj['validLength'] = _ApiClient["default"].convertToType(data['validLength'], 'String');
        }
      }

      return obj;
    }
  }]);

  return DeliveryResource;
}();
/**
 * 택배사 번호
 * @member {Number} deliveryNumber
 */


DeliveryResource.prototype['deliveryNumber'] = undefined;
/**
 * 택배사명
 * @member {String} deliveryCompany
 */

DeliveryResource.prototype['deliveryCompany'] = undefined;
/**
 * 택배사 트래킹 주소
 * @member {String} deliveryUrl
 */

DeliveryResource.prototype['deliveryUrl'] = undefined;
/**
 * 사용여부
 * @member {module:model/DeliveryResource.UseYnEnum} useYn
 */

DeliveryResource.prototype['useYn'] = undefined;
/**
 * Group Code
 * @member {String} courier
 */

DeliveryResource.prototype['courier'] = undefined;
/**
 * 트래킹 아이디
 * @member {String} trackerId
 */

DeliveryResource.prototype['trackerId'] = undefined;
/**
 * 검증 길이
 * @member {String} validLength
 */

DeliveryResource.prototype['validLength'] = undefined;
/**
 * Allowed values for the <code>useYn</code> property.
 * @enum {String}
 * @readonly
 */

DeliveryResource['UseYnEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = DeliveryResource;
exports["default"] = _default;
},{"../ApiClient":1}],57:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The Model219f3041d050108b1a2e51838473f6d8Request model module.
 * @module model/Model219f3041d050108b1a2e51838473f6d8Request
 * @version 0.0.1
 */
var Model219f3041d050108b1a2e51838473f6d8Request = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>Model219f3041d050108b1a2e51838473f6d8Request</code>.
   * @alias module:model/Model219f3041d050108b1a2e51838473f6d8Request
   * @param isRunouted {module:model/Model219f3041d050108b1a2e51838473f6d8Request.IsRunoutedEnum} 품절 여부
   */
  function Model219f3041d050108b1a2e51838473f6d8Request(isRunouted) {
    _classCallCheck(this, Model219f3041d050108b1a2e51838473f6d8Request);

    Model219f3041d050108b1a2e51838473f6d8Request.initialize(this, isRunouted);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(Model219f3041d050108b1a2e51838473f6d8Request, null, [{
    key: "initialize",
    value: function initialize(obj, isRunouted) {
      obj['isRunouted'] = isRunouted;
    }
    /**
     * Constructs a <code>Model219f3041d050108b1a2e51838473f6d8Request</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Model219f3041d050108b1a2e51838473f6d8Request} obj Optional instance to populate.
     * @return {module:model/Model219f3041d050108b1a2e51838473f6d8Request} The populated <code>Model219f3041d050108b1a2e51838473f6d8Request</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new Model219f3041d050108b1a2e51838473f6d8Request();

        if (data.hasOwnProperty('isRunouted')) {
          obj['isRunouted'] = _ApiClient["default"].convertToType(data['isRunouted'], 'String');
        }
      }

      return obj;
    }
  }]);

  return Model219f3041d050108b1a2e51838473f6d8Request;
}();
/**
 * 품절 여부
 * @member {module:model/Model219f3041d050108b1a2e51838473f6d8Request.IsRunoutedEnum} isRunouted
 */


Model219f3041d050108b1a2e51838473f6d8Request.prototype['isRunouted'] = undefined;
/**
 * Allowed values for the <code>isRunouted</code> property.
 * @enum {String}
 * @readonly
 */

Model219f3041d050108b1a2e51838473f6d8Request['IsRunoutedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = Model219f3041d050108b1a2e51838473f6d8Request;
exports["default"] = _default;
},{"../ApiClient":1}],58:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner = _interopRequireDefault(require("./Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The Model69ec8f3e26fa2b1b02ce4897936e91e4Request model module.
 * @module model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request
 * @version 0.0.1
 */
var Model69ec8f3e26fa2b1b02ce4897936e91e4Request = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>Model69ec8f3e26fa2b1b02ce4897936e91e4Request</code>.
   * @alias module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request
   * @param options {Array.<Array.<module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner>>} 상품 옵션
   */
  function Model69ec8f3e26fa2b1b02ce4897936e91e4Request(options) {
    _classCallCheck(this, Model69ec8f3e26fa2b1b02ce4897936e91e4Request);

    Model69ec8f3e26fa2b1b02ce4897936e91e4Request.initialize(this, options);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(Model69ec8f3e26fa2b1b02ce4897936e91e4Request, null, [{
    key: "initialize",
    value: function initialize(obj, options) {
      obj['options'] = options;
    }
    /**
     * Constructs a <code>Model69ec8f3e26fa2b1b02ce4897936e91e4Request</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request} obj Optional instance to populate.
     * @return {module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4Request} The populated <code>Model69ec8f3e26fa2b1b02ce4897936e91e4Request</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new Model69ec8f3e26fa2b1b02ce4897936e91e4Request();

        if (data.hasOwnProperty('options')) {
          obj['options'] = _ApiClient["default"].convertToType(data['options'], [[_Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner["default"]]]);
        }
      }

      return obj;
    }
  }]);

  return Model69ec8f3e26fa2b1b02ce4897936e91e4Request;
}();
/**
 * 상품 옵션
 * @member {Array.<Array.<module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner>>} options
 */


Model69ec8f3e26fa2b1b02ce4897936e91e4Request.prototype['options'] = undefined;
var _default = Model69ec8f3e26fa2b1b02ce4897936e91e4Request;
exports["default"] = _default;
},{"../ApiClient":1,"./Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner":59}],59:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner model module.
 * @module model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner
 * @version 0.0.1
 */
var Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner</code>.
   * @alias module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner
   * @param optionName1 {String} 
   * @param stock {Number} 
   */
  function Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner(optionName1, stock) {
    _classCallCheck(this, Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner);

    Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner.initialize(this, optionName1, stock);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner, null, [{
    key: "initialize",
    value: function initialize(obj, optionName1, stock) {
      obj['optionName1'] = optionName1;
      obj['stock'] = stock;
    }
    /**
     * Constructs a <code>Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner} obj Optional instance to populate.
     * @return {module:model/Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner} The populated <code>Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner();

        if (data.hasOwnProperty('optionNumber')) {
          obj['optionNumber'] = _ApiClient["default"].convertToType(data['optionNumber'], 'Number');
        }

        if (data.hasOwnProperty('optionName1')) {
          obj['optionName1'] = _ApiClient["default"].convertToType(data['optionName1'], 'String');
        }

        if (data.hasOwnProperty('optionName2')) {
          obj['optionName2'] = _ApiClient["default"].convertToType(data['optionName2'], 'String');
        }

        if (data.hasOwnProperty('stock')) {
          obj['stock'] = _ApiClient["default"].convertToType(data['stock'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner;
}();
/**
 * @member {Number} optionNumber
 */


Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner.prototype['optionNumber'] = undefined;
/**
 * @member {String} optionName1
 */

Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner.prototype['optionName1'] = undefined;
/**
 * @member {String} optionName2
 */

Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner.prototype['optionName2'] = undefined;
/**
 * @member {Number} stock
 */

Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner.prototype['stock'] = undefined;
var _default = Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner;
exports["default"] = _default;
},{"../ApiClient":1}],60:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The PopularProduct model module.
 * @module model/PopularProduct
 * @version 0.0.1
 */
var PopularProduct = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>PopularProduct</code>.
   * @alias module:model/PopularProduct
   */
  function PopularProduct() {
    _classCallCheck(this, PopularProduct);

    PopularProduct.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(PopularProduct, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>PopularProduct</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/PopularProduct} obj Optional instance to populate.
     * @return {module:model/PopularProduct} The populated <code>PopularProduct</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new PopularProduct();

        if (data.hasOwnProperty('productNumber')) {
          obj['productNumber'] = _ApiClient["default"].convertToType(data['productNumber'], 'Number');
        }

        if (data.hasOwnProperty('cnt')) {
          obj['cnt'] = _ApiClient["default"].convertToType(data['cnt'], 'Number');
        }

        if (data.hasOwnProperty('price')) {
          obj['price'] = _ApiClient["default"].convertToType(data['price'], 'Number');
        }

        if (data.hasOwnProperty('productName')) {
          obj['productName'] = _ApiClient["default"].convertToType(data['productName'], 'String');
        }

        if (data.hasOwnProperty('productImageSmall')) {
          obj['productImageSmall'] = _ApiClient["default"].convertToType(data['productImageSmall'], 'String');
        }

        if (data.hasOwnProperty('productImageLarge')) {
          obj['productImageLarge'] = _ApiClient["default"].convertToType(data['productImageLarge'], 'String');
        }
      }

      return obj;
    }
  }]);

  return PopularProduct;
}();
/**
 * 상품번호
 * @member {Number} productNumber
 */


PopularProduct.prototype['productNumber'] = undefined;
/**
 * 판매량
 * @member {Number} cnt
 */

PopularProduct.prototype['cnt'] = undefined;
/**
 * 판매가격
 * @member {Number} price
 */

PopularProduct.prototype['price'] = undefined;
/**
 * 상품명
 * @member {String} productName
 */

PopularProduct.prototype['productName'] = undefined;
/**
 * 상품대표이미지 - 작은사이즈
 * @member {String} productImageSmall
 */

PopularProduct.prototype['productImageSmall'] = undefined;
/**
 * 상품대표이미지 - 큰사이즈
 * @member {String} productImageLarge
 */

PopularProduct.prototype['productImageLarge'] = undefined;
var _default = PopularProduct;
exports["default"] = _default;
},{"../ApiClient":1}],61:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The ProviderDeliveryResource model module.
 * @module model/ProviderDeliveryResource
 * @version 0.0.1
 */
var ProviderDeliveryResource = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ProviderDeliveryResource</code>.
   * @alias module:model/ProviderDeliveryResource
   */
  function ProviderDeliveryResource() {
    _classCallCheck(this, ProviderDeliveryResource);

    ProviderDeliveryResource.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ProviderDeliveryResource, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>ProviderDeliveryResource</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ProviderDeliveryResource} obj Optional instance to populate.
     * @return {module:model/ProviderDeliveryResource} The populated <code>ProviderDeliveryResource</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ProviderDeliveryResource();

        if (data.hasOwnProperty('brandNumber')) {
          obj['brandNumber'] = _ApiClient["default"].convertToType(data['brandNumber'], 'Number');
        }

        if (data.hasOwnProperty('deliveryCompanyNumber')) {
          obj['deliveryCompanyNumber'] = _ApiClient["default"].convertToType(data['deliveryCompanyNumber'], 'Number');
        }

        if (data.hasOwnProperty('deliveryCompanyName')) {
          obj['deliveryCompanyName'] = _ApiClient["default"].convertToType(data['deliveryCompanyName'], 'String');
        }

        if (data.hasOwnProperty('deliveryContractCode')) {
          obj['deliveryContractCode'] = _ApiClient["default"].convertToType(data['deliveryContractCode'], 'String');
        }

        if (data.hasOwnProperty('postOfficeCustomerCode')) {
          obj['postOfficeCustomerCode'] = _ApiClient["default"].convertToType(data['postOfficeCustomerCode'], 'String');
        }

        if (data.hasOwnProperty('isAvaliableInvoiceLinking')) {
          obj['isAvaliableInvoiceLinking'] = _ApiClient["default"].convertToType(data['isAvaliableInvoiceLinking'], 'String');
        }
      }

      return obj;
    }
  }]);

  return ProviderDeliveryResource;
}();
/**
 * 브랜드번호
 * @member {Number} brandNumber
 */


ProviderDeliveryResource.prototype['brandNumber'] = undefined;
/**
 * 택배회사번호
 * @member {Number} deliveryCompanyNumber
 */

ProviderDeliveryResource.prototype['deliveryCompanyNumber'] = undefined;
/**
 * 택배사명
 * @member {String} deliveryCompanyName
 */

ProviderDeliveryResource.prototype['deliveryCompanyName'] = undefined;
/**
 * 택배사 계약코드
 * @member {String} deliveryContractCode
 */

ProviderDeliveryResource.prototype['deliveryContractCode'] = undefined;
/**
 * 우체국 고객코드
 * @member {String} postOfficeCustomerCode
 */

ProviderDeliveryResource.prototype['postOfficeCustomerCode'] = undefined;
/**
 * 송장연동 출력기능 가능
 * @member {module:model/ProviderDeliveryResource.IsAvaliableInvoiceLinkingEnum} isAvaliableInvoiceLinking
 */

ProviderDeliveryResource.prototype['isAvaliableInvoiceLinking'] = undefined;
/**
 * Allowed values for the <code>isAvaliableInvoiceLinking</code> property.
 * @enum {String}
 * @readonly
 */

ProviderDeliveryResource['IsAvaliableInvoiceLinkingEnum'] = {
  /**
   * value: "y"
   * @const
   */
  "y": "y",

  /**
   * value: "n"
   * @const
   */
  "n": "n",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = ProviderDeliveryResource;
exports["default"] = _default;
},{"../ApiClient":1}],62:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _CreateProductRequestCertificationDatasInnerInner = _interopRequireDefault(require("./CreateProductRequestCertificationDatasInnerInner"));

var _CreateProductRequestOptionsInnerInner = _interopRequireDefault(require("./CreateProductRequestOptionsInnerInner"));

var _CreateProductRequestProductImageDatasInnerInner = _interopRequireDefault(require("./CreateProductRequestProductImageDatasInnerInner"));

var _CreateProductRequestProductInformationInner = _interopRequireDefault(require("./CreateProductRequestProductInformationInner"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The UpdateProductByNoRequest model module.
 * @module model/UpdateProductByNoRequest
 * @version 0.0.1
 */
var UpdateProductByNoRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateProductByNoRequest</code>.
   * @alias module:model/UpdateProductByNoRequest
   */
  function UpdateProductByNoRequest() {
    _classCallCheck(this, UpdateProductByNoRequest);

    UpdateProductByNoRequest.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateProductByNoRequest, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>UpdateProductByNoRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateProductByNoRequest} obj Optional instance to populate.
     * @return {module:model/UpdateProductByNoRequest} The populated <code>UpdateProductByNoRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateProductByNoRequest();

        if (data.hasOwnProperty('categoryCode')) {
          obj['categoryCode'] = _ApiClient["default"].convertToType(data['categoryCode'], 'String');
        }

        if (data.hasOwnProperty('brandNumber')) {
          obj['brandNumber'] = _ApiClient["default"].convertToType(data['brandNumber'], 'Number');
        }

        if (data.hasOwnProperty('maker')) {
          obj['maker'] = _ApiClient["default"].convertToType(data['maker'], 'String');
        }

        if (data.hasOwnProperty('origin')) {
          obj['origin'] = _ApiClient["default"].convertToType(data['origin'], 'String');
        }

        if (data.hasOwnProperty('productCode')) {
          obj['productCode'] = _ApiClient["default"].convertToType(data['productCode'], 'String');
        }

        if (data.hasOwnProperty('productName')) {
          obj['productName'] = _ApiClient["default"].convertToType(data['productName'], 'String');
        }

        if (data.hasOwnProperty('keywords')) {
          obj['keywords'] = _ApiClient["default"].convertToType(data['keywords'], ['String']);
        }

        if (data.hasOwnProperty('isUsed')) {
          obj['isUsed'] = _ApiClient["default"].convertToType(data['isUsed'], 'String');
        }

        if (data.hasOwnProperty('isSellerDeal')) {
          obj['isSellerDeal'] = _ApiClient["default"].convertToType(data['isSellerDeal'], 'String');
        }

        if (data.hasOwnProperty('isTemporary')) {
          obj['isTemporary'] = _ApiClient["default"].convertToType(data['isTemporary'], 'String');
        }

        if (data.hasOwnProperty('deliveryType')) {
          obj['deliveryType'] = _ApiClient["default"].convertToType(data['deliveryType'], 'String');
        }

        if (data.hasOwnProperty('deliveryPrice')) {
          obj['deliveryPrice'] = _ApiClient["default"].convertToType(data['deliveryPrice'], 'Number');
        }

        if (data.hasOwnProperty('isManufactured')) {
          obj['isManufactured'] = _ApiClient["default"].convertToType(data['isManufactured'], 'String');
        }

        if (data.hasOwnProperty('manufacturingPeriod')) {
          obj['manufacturingPeriod'] = _ApiClient["default"].convertToType(data['manufacturingPeriod'], 'String');
        }

        if (data.hasOwnProperty('isReservationDelivery')) {
          obj['isReservationDelivery'] = _ApiClient["default"].convertToType(data['isReservationDelivery'], 'String');
        }

        if (data.hasOwnProperty('reservationDeliveryDate')) {
          obj['reservationDeliveryDate'] = _ApiClient["default"].convertToType(data['reservationDeliveryDate'], 'String');
        }

        if (data.hasOwnProperty('isOverseasDelivery')) {
          obj['isOverseasDelivery'] = _ApiClient["default"].convertToType(data['isOverseasDelivery'], 'String');
        }

        if (data.hasOwnProperty('overseasDeliveryPeriod')) {
          obj['overseasDeliveryPeriod'] = _ApiClient["default"].convertToType(data['overseasDeliveryPeriod'], 'String');
        }

        if (data.hasOwnProperty('productPrice')) {
          obj['productPrice'] = _ApiClient["default"].convertToType(data['productPrice'], 'Number');
        }

        if (data.hasOwnProperty('productSupplyPrice')) {
          obj['productSupplyPrice'] = _ApiClient["default"].convertToType(data['productSupplyPrice'], 'Number');
        }

        if (data.hasOwnProperty('productConsumerPrice')) {
          obj['productConsumerPrice'] = _ApiClient["default"].convertToType(data['productConsumerPrice'], 'Number');
        }

        if (data.hasOwnProperty('productAbidePrice')) {
          obj['productAbidePrice'] = _ApiClient["default"].convertToType(data['productAbidePrice'], 'Number');
        }

        if (data.hasOwnProperty('productWolesalePrice')) {
          obj['productWolesalePrice'] = _ApiClient["default"].convertToType(data['productWolesalePrice'], 'Number');
        }

        if (data.hasOwnProperty('isUsingStock')) {
          obj['isUsingStock'] = _ApiClient["default"].convertToType(data['isUsingStock'], 'String');
        }

        if (data.hasOwnProperty('isTaxable')) {
          obj['isTaxable'] = _ApiClient["default"].convertToType(data['isTaxable'], 'String');
        }

        if (data.hasOwnProperty('optionTitles')) {
          obj['optionTitles'] = _ApiClient["default"].convertToType(data['optionTitles'], ['String']);
        }

        if (data.hasOwnProperty('optionType')) {
          obj['optionType'] = _ApiClient["default"].convertToType(data['optionType'], 'String');
        }

        if (data.hasOwnProperty('options')) {
          obj['options'] = _ApiClient["default"].convertToType(data['options'], [[_CreateProductRequestOptionsInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('isRunouted')) {
          obj['isRunouted'] = _ApiClient["default"].convertToType(data['isRunouted'], 'String');
        }

        if (data.hasOwnProperty('productImageDatas')) {
          obj['productImageDatas'] = _ApiClient["default"].convertToType(data['productImageDatas'], [[_CreateProductRequestProductImageDatasInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('kcCertificationType')) {
          obj['kcCertificationType'] = _ApiClient["default"].convertToType(data['kcCertificationType'], 'String');
        }

        if (data.hasOwnProperty('kcCertificationInfo')) {
          obj['kcCertificationInfo'] = _ApiClient["default"].convertToType(data['kcCertificationInfo'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationType')) {
          obj['ecoCertificationType'] = _ApiClient["default"].convertToType(data['ecoCertificationType'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationAuth')) {
          obj['ecoCertificationAuth'] = _ApiClient["default"].convertToType(data['ecoCertificationAuth'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationCode')) {
          obj['ecoCertificationCode'] = _ApiClient["default"].convertToType(data['ecoCertificationCode'], 'String');
        }

        if (data.hasOwnProperty('ecoCertificationMark')) {
          obj['ecoCertificationMark'] = _ApiClient["default"].convertToType(data['ecoCertificationMark'], 'String');
        }

        if (data.hasOwnProperty('certificationDatas')) {
          obj['certificationDatas'] = _ApiClient["default"].convertToType(data['certificationDatas'], [[_CreateProductRequestCertificationDatasInnerInner["default"]]]);
        }

        if (data.hasOwnProperty('productInformation')) {
          obj['productInformation'] = _ApiClient["default"].convertToType(data['productInformation'], [_CreateProductRequestProductInformationInner["default"]]);
        }

        if (data.hasOwnProperty('manageMemo')) {
          obj['manageMemo'] = _ApiClient["default"].convertToType(data['manageMemo'], 'String');
        }

        if (data.hasOwnProperty('productMemo')) {
          obj['productMemo'] = _ApiClient["default"].convertToType(data['productMemo'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UpdateProductByNoRequest;
}();
/**
 * 카테고리 코드
 * @member {String} categoryCode
 */


UpdateProductByNoRequest.prototype['categoryCode'] = undefined;
/**
 * 브랜드 번호
 * @member {Number} brandNumber
 */

UpdateProductByNoRequest.prototype['brandNumber'] = undefined;
/**
 * 메이커
 * @member {String} maker
 */

UpdateProductByNoRequest.prototype['maker'] = undefined;
/**
 * 원산지
 * @member {String} origin
 */

UpdateProductByNoRequest.prototype['origin'] = undefined;
/**
 * 브랜드 번호
 * @member {String} productCode
 */

UpdateProductByNoRequest.prototype['productCode'] = undefined;
/**
 * 상품명
 * @member {String} productName
 */

UpdateProductByNoRequest.prototype['productName'] = undefined;
/**
 * 키워드
 * @member {Array.<String>} keywords
 */

UpdateProductByNoRequest.prototype['keywords'] = undefined;
/**
 * 중고여부
 * @member {module:model/UpdateProductByNoRequest.IsUsedEnum} isUsed
 */

UpdateProductByNoRequest.prototype['isUsed'] = undefined;
/**
 * 셀러딜 상품여부
 * @member {module:model/UpdateProductByNoRequest.IsSellerDealEnum} isSellerDeal
 */

UpdateProductByNoRequest.prototype['isSellerDeal'] = undefined;
/**
 * 임시상품 여부
 * @member {module:model/UpdateProductByNoRequest.IsTemporaryEnum} isTemporary
 */

UpdateProductByNoRequest.prototype['isTemporary'] = undefined;
/**
 * 배송 유형
 * @member {module:model/UpdateProductByNoRequest.DeliveryTypeEnum} deliveryType
 */

UpdateProductByNoRequest.prototype['deliveryType'] = undefined;
/**
 * 배송비
 * @member {Number} deliveryPrice
 */

UpdateProductByNoRequest.prototype['deliveryPrice'] = undefined;
/**
 * 제조 상품 여부
 * @member {module:model/UpdateProductByNoRequest.IsManufacturedEnum} isManufactured
 */

UpdateProductByNoRequest.prototype['isManufactured'] = undefined;
/**
 * 제조기간
 * @member {String} manufacturingPeriod
 */

UpdateProductByNoRequest.prototype['manufacturingPeriod'] = undefined;
/**
 * 예약 상품 여부
 * @member {module:model/UpdateProductByNoRequest.IsReservationDeliveryEnum} isReservationDelivery
 */

UpdateProductByNoRequest.prototype['isReservationDelivery'] = undefined;
/**
 * 예약발송일자
 * @member {String} reservationDeliveryDate
 */

UpdateProductByNoRequest.prototype['reservationDeliveryDate'] = undefined;
/**
 * 해외 상품 여부
 * @member {module:model/UpdateProductByNoRequest.IsOverseasDeliveryEnum} isOverseasDelivery
 */

UpdateProductByNoRequest.prototype['isOverseasDelivery'] = undefined;
/**
 * 해외배송기간
 * @member {String} overseasDeliveryPeriod
 */

UpdateProductByNoRequest.prototype['overseasDeliveryPeriod'] = undefined;
/**
 * 상품 판매 가격
 * @member {Number} productPrice
 */

UpdateProductByNoRequest.prototype['productPrice'] = undefined;
/**
 * 상품 공급 가격
 * @member {Number} productSupplyPrice
 */

UpdateProductByNoRequest.prototype['productSupplyPrice'] = undefined;
/**
 * 소비자 가격
 * @member {Number} productConsumerPrice
 */

UpdateProductByNoRequest.prototype['productConsumerPrice'] = undefined;
/**
 * 준수 판매 가격
 * @member {Number} productAbidePrice
 */

UpdateProductByNoRequest.prototype['productAbidePrice'] = undefined;
/**
 * 도매 가격
 * @member {Number} productWolesalePrice
 */

UpdateProductByNoRequest.prototype['productWolesalePrice'] = undefined;
/**
 * 재고량 연동 여부
 * @member {module:model/UpdateProductByNoRequest.IsUsingStockEnum} isUsingStock
 */

UpdateProductByNoRequest.prototype['isUsingStock'] = undefined;
/**
 * 과세 여부
 * @member {module:model/UpdateProductByNoRequest.IsTaxableEnum} isTaxable
 */

UpdateProductByNoRequest.prototype['isTaxable'] = undefined;
/**
 * 상품 옵션 제목
 * @member {Array.<String>} optionTitles
 */

UpdateProductByNoRequest.prototype['optionTitles'] = undefined;
/**
 * 옵션 유형
 * @member {module:model/UpdateProductByNoRequest.OptionTypeEnum} optionType
 */

UpdateProductByNoRequest.prototype['optionType'] = undefined;
/**
 * 상품 옵션
 * @member {Array.<Array.<module:model/CreateProductRequestOptionsInnerInner>>} options
 */

UpdateProductByNoRequest.prototype['options'] = undefined;
/**
 * 품절 여부
 * @member {module:model/UpdateProductByNoRequest.IsRunoutedEnum} isRunouted
 */

UpdateProductByNoRequest.prototype['isRunouted'] = undefined;
/**
 * 상품 이미지
 * @member {Array.<Array.<module:model/CreateProductRequestProductImageDatasInnerInner>>} productImageDatas
 */

UpdateProductByNoRequest.prototype['productImageDatas'] = undefined;
/**
 * 상품 상세 내용
 * @member {String} description
 */

UpdateProductByNoRequest.prototype['description'] = undefined;
/**
 * KC 인증 유형
 * @member {module:model/UpdateProductByNoRequest.KcCertificationTypeEnum} kcCertificationType
 */

UpdateProductByNoRequest.prototype['kcCertificationType'] = undefined;
/**
 * KC 인증 정보
 * @member {String} kcCertificationInfo
 */

UpdateProductByNoRequest.prototype['kcCertificationInfo'] = undefined;
/**
 * 친환경 인증 유형
 * @member {module:model/UpdateProductByNoRequest.EcoCertificationTypeEnum} ecoCertificationType
 */

UpdateProductByNoRequest.prototype['ecoCertificationType'] = undefined;
/**
 * 친환경 인증 기관
 * @member {String} ecoCertificationAuth
 */

UpdateProductByNoRequest.prototype['ecoCertificationAuth'] = undefined;
/**
 * 친환경 인증 번호
 * @member {String} ecoCertificationCode
 */

UpdateProductByNoRequest.prototype['ecoCertificationCode'] = undefined;
/**
 * 친환경 인증 마크 사용여부
 * @member {module:model/UpdateProductByNoRequest.EcoCertificationMarkEnum} ecoCertificationMark
 */

UpdateProductByNoRequest.prototype['ecoCertificationMark'] = undefined;
/**
 * 상품 인증자료 첨부
 * @member {Array.<Array.<module:model/CreateProductRequestCertificationDatasInnerInner>>} certificationDatas
 */

UpdateProductByNoRequest.prototype['certificationDatas'] = undefined;
/**
 * 상품 정보 고시
 * @member {Array.<module:model/CreateProductRequestProductInformationInner>} productInformation
 */

UpdateProductByNoRequest.prototype['productInformation'] = undefined;
/**
 * 관리 메모
 * @member {String} manageMemo
 */

UpdateProductByNoRequest.prototype['manageMemo'] = undefined;
/**
 * 상품 메모
 * @member {String} productMemo
 */

UpdateProductByNoRequest.prototype['productMemo'] = undefined;
/**
 * Allowed values for the <code>isUsed</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsUsedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isSellerDeal</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsSellerDealEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isTemporary</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsTemporaryEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>deliveryType</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['DeliveryTypeEnum'] = {
  /**
   * value: "wait"
   * @const
   */
  "wait": "wait",

  /**
   * value: "request"
   * @const
   */
  "request": "request",

  /**
   * value: "approved"
   * @const
   */
  "approved": "approved",

  /**
   * value: "hold"
   * @const
   */
  "hold": "hold",

  /**
   * value: "reject"
   * @const
   */
  "reject": "reject",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isManufactured</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsManufacturedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isReservationDelivery</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsReservationDeliveryEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isOverseasDelivery</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsOverseasDeliveryEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isUsingStock</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsUsingStockEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isTaxable</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsTaxableEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>optionType</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['OptionTypeEnum'] = {
  /**
   * value: "single"
   * @const
   */
  "single": "single",

  /**
   * value: "double"
   * @const
   */
  "double": "double",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isRunouted</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['IsRunoutedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>kcCertificationType</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['KcCertificationTypeEnum'] = {
  /**
   * value: "해당없음"
   * @const
   */
  "해당없음": "해당없음",

  /**
   * value: "상세설명참조"
   * @const
   */
  "상세설명참조": "상세설명참조",

  /**
   * value: "[생활용품]안전인증"
   * @const
   */
  "[생활용품]안전인증": "[생활용품]안전인증",

  /**
   * value: "[생활용품]안전확인"
   * @const
   */
  "[생활용품]안전확인": "[생활용품]안전확인",

  /**
   * value: "[생활용품]공급자적합성확인"
   * @const
   */
  "[생활용품]공급자적합성확인": "[생활용품]공급자적합성확인",

  /**
   * value: "[생활용품]어린이보호포장"
   * @const
   */
  "[생활용품]어린이보호포장": "[생활용품]어린이보호포장",

  /**
   * value: "[어린이제품]안전인증"
   * @const
   */
  "[어린이제품]안전인증": "[어린이제품]안전인증",

  /**
   * value: "[어린이제품]안전확인"
   * @const
   */
  "[어린이제품]안전확인": "[어린이제품]안전확인",

  /**
   * value: "[어린이제품]공급자적합성확인"
   * @const
   */
  "[어린이제품]공급자적합성확인": "[어린이제품]공급자적합성확인",

  /**
   * value: "[전기용품]안전인증"
   * @const
   */
  "[전기용품]안전인증": "[전기용품]안전인증",

  /**
   * value: "[전기용품]안전확인"
   * @const
   */
  "[전기용품]안전확인": "[전기용품]안전확인",

  /**
   * value: "[전기용품]공급자적합성확인"
   * @const
   */
  "[전기용품]공급자적합성확인": "[전기용품]공급자적합성확인",

  /**
   * value: "[방송통신기자재]적합성평가"
   * @const
   */
  "[방송통신기자재]적합성평가": "[방송통신기자재]적합성평가",

  /**
   * value: "[위해우려제품]자가검사번호"
   * @const
   */
  "[위해우려제품]자가검사번호": "[위해우려제품]자가검사번호",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>ecoCertificationType</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['EcoCertificationTypeEnum'] = {
  /**
   * value: "[친환경]유기축산물"
   * @const
   */
  "유기축산물": "[친환경]유기축산물",

  /**
   * value: "[친환경]유기농산물"
   * @const
   */
  "유기농산물": "[친환경]유기농산물",

  /**
   * value: "[친환경]무항생제축산물"
   * @const
   */
  "무항생제축산물": "[친환경]무항생제축산물",

  /**
   * value: "[친환경]무농약농산물"
   * @const
   */
  "무농약농산물": "[친환경]무농약농산물",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>ecoCertificationMark</code> property.
 * @enum {String}
 * @readonly
 */

UpdateProductByNoRequest['EcoCertificationMarkEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = UpdateProductByNoRequest;
exports["default"] = _default;
},{"../ApiClient":1,"./CreateProductRequestCertificationDatasInnerInner":48,"./CreateProductRequestOptionsInnerInner":49,"./CreateProductRequestProductImageDatasInnerInner":50,"./CreateProductRequestProductInformationInner":51}],63:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The UpdateProviderBrandBrandDatasParameterInner model module.
 * @module model/UpdateProviderBrandBrandDatasParameterInner
 * @version 0.0.1
 */
var UpdateProviderBrandBrandDatasParameterInner = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UpdateProviderBrandBrandDatasParameterInner</code>.
   * @alias module:model/UpdateProviderBrandBrandDatasParameterInner
   */
  function UpdateProviderBrandBrandDatasParameterInner() {
    _classCallCheck(this, UpdateProviderBrandBrandDatasParameterInner);

    UpdateProviderBrandBrandDatasParameterInner.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UpdateProviderBrandBrandDatasParameterInner, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>UpdateProviderBrandBrandDatasParameterInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateProviderBrandBrandDatasParameterInner} obj Optional instance to populate.
     * @return {module:model/UpdateProviderBrandBrandDatasParameterInner} The populated <code>UpdateProviderBrandBrandDatasParameterInner</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UpdateProviderBrandBrandDatasParameterInner();

        if (data.hasOwnProperty('brandName')) {
          obj['brandName'] = _ApiClient["default"].convertToType(data['brandName'], 'String');
        }

        if (data.hasOwnProperty('brandNumber')) {
          obj['brandNumber'] = _ApiClient["default"].convertToType(data['brandNumber'], 'Number');
        }
      }

      return obj;
    }
  }]);

  return UpdateProviderBrandBrandDatasParameterInner;
}();
/**
 * 브랜드명
 * @member {String} brandName
 */


UpdateProviderBrandBrandDatasParameterInner.prototype['brandName'] = undefined;
/**
 * 브랜드 번호
 * @member {Number} brandNumber
 */

UpdateProviderBrandBrandDatasParameterInner.prototype['brandNumber'] = undefined;
var _default = UpdateProviderBrandBrandDatasParameterInner;
exports["default"] = _default;
},{"../ApiClient":1}],64:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _UrgentMessageAlarm = _interopRequireDefault(require("./UrgentMessageAlarm"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The UrgentMessage model module.
 * @module model/UrgentMessage
 * @version 0.0.1
 */
var UrgentMessage = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UrgentMessage</code>.
   * @alias module:model/UrgentMessage
   */
  function UrgentMessage() {
    _classCallCheck(this, UrgentMessage);

    UrgentMessage.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UrgentMessage, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>UrgentMessage</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UrgentMessage} obj Optional instance to populate.
     * @return {module:model/UrgentMessage} The populated <code>UrgentMessage</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UrgentMessage();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'Number');
        }

        if (data.hasOwnProperty('orderNumber')) {
          obj['orderNumber'] = _ApiClient["default"].convertToType(data['orderNumber'], 'Number');
        }

        if (data.hasOwnProperty('orderItemNumber')) {
          obj['orderItemNumber'] = _ApiClient["default"].convertToType(data['orderItemNumber'], 'Number');
        }

        if (data.hasOwnProperty('productNumber')) {
          obj['productNumber'] = _ApiClient["default"].convertToType(data['productNumber'], 'Number');
        }

        if (data.hasOwnProperty('productName')) {
          obj['productName'] = _ApiClient["default"].convertToType(data['productName'], 'String');
        }

        if (data.hasOwnProperty('brandNumber')) {
          obj['brandNumber'] = _ApiClient["default"].convertToType(data['brandNumber'], 'String');
        }

        if (data.hasOwnProperty('brandName')) {
          obj['brandName'] = _ApiClient["default"].convertToType(data['brandName'], 'String');
        }

        if (data.hasOwnProperty('alarmType')) {
          obj['alarmType'] = _ApiClient["default"].convertToType(data['alarmType'], 'String');
        }

        if (data.hasOwnProperty('alarmTypeKor')) {
          obj['alarmTypeKor'] = _ApiClient["default"].convertToType(data['alarmTypeKor'], 'String');
        }

        if (data.hasOwnProperty('alarms')) {
          obj['alarms'] = _ApiClient["default"].convertToType(data['alarms'], [_UrgentMessageAlarm["default"]]);
        }

        if (data.hasOwnProperty('isReceived')) {
          obj['isReceived'] = _ApiClient["default"].convertToType(data['isReceived'], 'String');
        }

        if (data.hasOwnProperty('isFinished')) {
          obj['isFinished'] = _ApiClient["default"].convertToType(data['isFinished'], 'String');
        }

        if (data.hasOwnProperty('createdAt')) {
          obj['createdAt'] = _ApiClient["default"].convertToType(data['createdAt'], 'String');
        }

        if (data.hasOwnProperty('finishedAt')) {
          obj['finishedAt'] = _ApiClient["default"].convertToType(data['finishedAt'], 'String');
        }

        if (data.hasOwnProperty('isAvailableToReply')) {
          obj['isAvailableToReply'] = _ApiClient["default"].convertToType(data['isAvailableToReply'], 'String');
        }
      }

      return obj;
    }
  }]);

  return UrgentMessage;
}();
/**
 * 긴급 알람 ID
 * @member {Number} id
 */


UrgentMessage.prototype['id'] = undefined;
/**
 * 주문 번호
 * @member {Number} orderNumber
 */

UrgentMessage.prototype['orderNumber'] = undefined;
/**
 * 주문 아이템 번호
 * @member {Number} orderItemNumber
 */

UrgentMessage.prototype['orderItemNumber'] = undefined;
/**
 * 상품 번호
 * @member {Number} productNumber
 */

UrgentMessage.prototype['productNumber'] = undefined;
/**
 * 상품명
 * @member {String} productName
 */

UrgentMessage.prototype['productName'] = undefined;
/**
 * 브랜드 번호
 * @member {String} brandNumber
 */

UrgentMessage.prototype['brandNumber'] = undefined;
/**
 * 브랜드명
 * @member {String} brandName
 */

UrgentMessage.prototype['brandName'] = undefined;
/**
 * 알림 타입 코드
 * @member {String} alarmType
 */

UrgentMessage.prototype['alarmType'] = undefined;
/**
 * 알람 타입
 * @member {String} alarmTypeKor
 */

UrgentMessage.prototype['alarmTypeKor'] = undefined;
/**
 * 알람 목록
 * @member {Array.<module:model/UrgentMessageAlarm>} alarms
 */

UrgentMessage.prototype['alarms'] = undefined;
/**
 * 수신 여부
 * @member {module:model/UrgentMessage.IsReceivedEnum} isReceived
 */

UrgentMessage.prototype['isReceived'] = undefined;
/**
 * 종료 여부
 * @member {module:model/UrgentMessage.IsFinishedEnum} isFinished
 */

UrgentMessage.prototype['isFinished'] = undefined;
/**
 * 생성 일자
 * @member {String} createdAt
 */

UrgentMessage.prototype['createdAt'] = undefined;
/**
 * 종료 일자
 * @member {String} finishedAt
 */

UrgentMessage.prototype['finishedAt'] = undefined;
/**
 * 답변 가능 여부
 * @member {module:model/UrgentMessage.IsAvailableToReplyEnum} isAvailableToReply
 */

UrgentMessage.prototype['isAvailableToReply'] = undefined;
/**
 * Allowed values for the <code>isReceived</code> property.
 * @enum {String}
 * @readonly
 */

UrgentMessage['IsReceivedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isFinished</code> property.
 * @enum {String}
 * @readonly
 */

UrgentMessage['IsFinishedEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
/**
 * Allowed values for the <code>isAvailableToReply</code> property.
 * @enum {String}
 * @readonly
 */

UrgentMessage['IsAvailableToReplyEnum'] = {
  /**
   * value: "Y"
   * @const
   */
  "Y": "Y",

  /**
   * value: "N"
   * @const
   */
  "N": "N",

  /**
   * value: "unknown_default_open_api"
   * @const
   */
  "unknown_default_open_api": "unknown_default_open_api"
};
var _default = UrgentMessage;
exports["default"] = _default;
},{"../ApiClient":1,"./UrgentMessageAlarm":65}],65:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The UrgentMessageAlarm model module.
 * @module model/UrgentMessageAlarm
 * @version 0.0.1
 */
var UrgentMessageAlarm = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>UrgentMessageAlarm</code>.
   * @alias module:model/UrgentMessageAlarm
   */
  function UrgentMessageAlarm() {
    _classCallCheck(this, UrgentMessageAlarm);

    UrgentMessageAlarm.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(UrgentMessageAlarm, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>UrgentMessageAlarm</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UrgentMessageAlarm} obj Optional instance to populate.
     * @return {module:model/UrgentMessageAlarm} The populated <code>UrgentMessageAlarm</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new UrgentMessageAlarm();

        if (data.hasOwnProperty('writerNumber')) {
          obj['writerNumber'] = _ApiClient["default"].convertToType(data['writerNumber'], 'Number');
        }

        if (data.hasOwnProperty('writerName')) {
          obj['writerName'] = _ApiClient["default"].convertToType(data['writerName'], 'String');
        }

        if (data.hasOwnProperty('message')) {
          obj['message'] = _ApiClient["default"].convertToType(data['message'], 'String');
        }

        if (data.hasOwnProperty('answer')) {
          obj['answer'] = _ApiClient["default"].convertToType(data['answer'], 'String');
        }

        if (data.hasOwnProperty('createdAt')) {
          obj['createdAt'] = _ApiClient["default"].convertToType(data['createdAt'], 'Date');
        }

        if (data.hasOwnProperty('answeredAt')) {
          obj['answeredAt'] = _ApiClient["default"].convertToType(data['answeredAt'], 'Date');
        }
      }

      return obj;
    }
  }]);

  return UrgentMessageAlarm;
}();
/**
 * 작성자 번호
 * @member {Number} writerNumber
 */


UrgentMessageAlarm.prototype['writerNumber'] = undefined;
/**
 * 작성자명
 * @member {String} writerName
 */

UrgentMessageAlarm.prototype['writerName'] = undefined;
/**
 * 알람 내용
 * @member {String} message
 */

UrgentMessageAlarm.prototype['message'] = undefined;
/**
 * 답변
 * @member {String} answer
 */

UrgentMessageAlarm.prototype['answer'] = undefined;
/**
 * 알람 등록일
 * @member {Date} createdAt
 */

UrgentMessageAlarm.prototype['createdAt'] = undefined;
/**
 * 답변일
 * @member {Date} answeredAt
 */

UrgentMessageAlarm.prototype['answeredAt'] = undefined;
var _default = UrgentMessageAlarm;
exports["default"] = _default;
},{"../ApiClient":1}],66:[function(require,module,exports){

/**
 * Expose `Emitter`.
 */

if (typeof module !== 'undefined') {
  module.exports = Emitter;
}

/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */

function Emitter(obj) {
  if (obj) return mixin(obj);
};

/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
  for (var key in Emitter.prototype) {
    obj[key] = Emitter.prototype[key];
  }
  return obj;
}

/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.on =
Emitter.prototype.addEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};
  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
    .push(fn);
  return this;
};

/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.once = function(event, fn){
  function on() {
    this.off(event, on);
    fn.apply(this, arguments);
  }

  on.fn = fn;
  this.on(event, on);
  return this;
};

/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.off =
Emitter.prototype.removeListener =
Emitter.prototype.removeAllListeners =
Emitter.prototype.removeEventListener = function(event, fn){
  this._callbacks = this._callbacks || {};

  // all
  if (0 == arguments.length) {
    this._callbacks = {};
    return this;
  }

  // specific event
  var callbacks = this._callbacks['$' + event];
  if (!callbacks) return this;

  // remove all handlers
  if (1 == arguments.length) {
    delete this._callbacks['$' + event];
    return this;
  }

  // remove specific handler
  var cb;
  for (var i = 0; i < callbacks.length; i++) {
    cb = callbacks[i];
    if (cb === fn || cb.fn === fn) {
      callbacks.splice(i, 1);
      break;
    }
  }

  // Remove event specific arrays for event types that no
  // one is subscribed for to avoid memory leak.
  if (callbacks.length === 0) {
    delete this._callbacks['$' + event];
  }

  return this;
};

/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */

Emitter.prototype.emit = function(event){
  this._callbacks = this._callbacks || {};

  var args = new Array(arguments.length - 1)
    , callbacks = this._callbacks['$' + event];

  for (var i = 1; i < arguments.length; i++) {
    args[i - 1] = arguments[i];
  }

  if (callbacks) {
    callbacks = callbacks.slice(0);
    for (var i = 0, len = callbacks.length; i < len; ++i) {
      callbacks[i].apply(this, args);
    }
  }

  return this;
};

/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */

Emitter.prototype.listeners = function(event){
  this._callbacks = this._callbacks || {};
  return this._callbacks['$' + event] || [];
};

/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */

Emitter.prototype.hasListeners = function(event){
  return !! this.listeners(event).length;
};

},{}],67:[function(require,module,exports){
module.exports = stringify
stringify.default = stringify
stringify.stable = deterministicStringify
stringify.stableStringify = deterministicStringify

var LIMIT_REPLACE_NODE = '[...]'
var CIRCULAR_REPLACE_NODE = '[Circular]'

var arr = []
var replacerStack = []

function defaultOptions () {
  return {
    depthLimit: Number.MAX_SAFE_INTEGER,
    edgesLimit: Number.MAX_SAFE_INTEGER
  }
}

// Regular stringify
function stringify (obj, replacer, spacer, options) {
  if (typeof options === 'undefined') {
    options = defaultOptions()
  }

  decirc(obj, '', 0, [], undefined, 0, options)
  var res
  try {
    if (replacerStack.length === 0) {
      res = JSON.stringify(obj, replacer, spacer)
    } else {
      res = JSON.stringify(obj, replaceGetterValues(replacer), spacer)
    }
  } catch (_) {
    return JSON.stringify('[unable to serialize, circular reference is too complex to analyze]')
  } finally {
    while (arr.length !== 0) {
      var part = arr.pop()
      if (part.length === 4) {
        Object.defineProperty(part[0], part[1], part[3])
      } else {
        part[0][part[1]] = part[2]
      }
    }
  }
  return res
}

function setReplace (replace, val, k, parent) {
  var propertyDescriptor = Object.getOwnPropertyDescriptor(parent, k)
  if (propertyDescriptor.get !== undefined) {
    if (propertyDescriptor.configurable) {
      Object.defineProperty(parent, k, { value: replace })
      arr.push([parent, k, val, propertyDescriptor])
    } else {
      replacerStack.push([val, k, replace])
    }
  } else {
    parent[k] = replace
    arr.push([parent, k, val])
  }
}

function decirc (val, k, edgeIndex, stack, parent, depth, options) {
  depth += 1
  var i
  if (typeof val === 'object' && val !== null) {
    for (i = 0; i < stack.length; i++) {
      if (stack[i] === val) {
        setReplace(CIRCULAR_REPLACE_NODE, val, k, parent)
        return
      }
    }

    if (
      typeof options.depthLimit !== 'undefined' &&
      depth > options.depthLimit
    ) {
      setReplace(LIMIT_REPLACE_NODE, val, k, parent)
      return
    }

    if (
      typeof options.edgesLimit !== 'undefined' &&
      edgeIndex + 1 > options.edgesLimit
    ) {
      setReplace(LIMIT_REPLACE_NODE, val, k, parent)
      return
    }

    stack.push(val)
    // Optimize for Arrays. Big arrays could kill the performance otherwise!
    if (Array.isArray(val)) {
      for (i = 0; i < val.length; i++) {
        decirc(val[i], i, i, stack, val, depth, options)
      }
    } else {
      var keys = Object.keys(val)
      for (i = 0; i < keys.length; i++) {
        var key = keys[i]
        decirc(val[key], key, i, stack, val, depth, options)
      }
    }
    stack.pop()
  }
}

// Stable-stringify
function compareFunction (a, b) {
  if (a < b) {
    return -1
  }
  if (a > b) {
    return 1
  }
  return 0
}

function deterministicStringify (obj, replacer, spacer, options) {
  if (typeof options === 'undefined') {
    options = defaultOptions()
  }

  var tmp = deterministicDecirc(obj, '', 0, [], undefined, 0, options) || obj
  var res
  try {
    if (replacerStack.length === 0) {
      res = JSON.stringify(tmp, replacer, spacer)
    } else {
      res = JSON.stringify(tmp, replaceGetterValues(replacer), spacer)
    }
  } catch (_) {
    return JSON.stringify('[unable to serialize, circular reference is too complex to analyze]')
  } finally {
    // Ensure that we restore the object as it was.
    while (arr.length !== 0) {
      var part = arr.pop()
      if (part.length === 4) {
        Object.defineProperty(part[0], part[1], part[3])
      } else {
        part[0][part[1]] = part[2]
      }
    }
  }
  return res
}

function deterministicDecirc (val, k, edgeIndex, stack, parent, depth, options) {
  depth += 1
  var i
  if (typeof val === 'object' && val !== null) {
    for (i = 0; i < stack.length; i++) {
      if (stack[i] === val) {
        setReplace(CIRCULAR_REPLACE_NODE, val, k, parent)
        return
      }
    }
    try {
      if (typeof val.toJSON === 'function') {
        return
      }
    } catch (_) {
      return
    }

    if (
      typeof options.depthLimit !== 'undefined' &&
      depth > options.depthLimit
    ) {
      setReplace(LIMIT_REPLACE_NODE, val, k, parent)
      return
    }

    if (
      typeof options.edgesLimit !== 'undefined' &&
      edgeIndex + 1 > options.edgesLimit
    ) {
      setReplace(LIMIT_REPLACE_NODE, val, k, parent)
      return
    }

    stack.push(val)
    // Optimize for Arrays. Big arrays could kill the performance otherwise!
    if (Array.isArray(val)) {
      for (i = 0; i < val.length; i++) {
        deterministicDecirc(val[i], i, i, stack, val, depth, options)
      }
    } else {
      // Create a temporary object in the required way
      var tmp = {}
      var keys = Object.keys(val).sort(compareFunction)
      for (i = 0; i < keys.length; i++) {
        var key = keys[i]
        deterministicDecirc(val[key], key, i, stack, val, depth, options)
        tmp[key] = val[key]
      }
      if (typeof parent !== 'undefined') {
        arr.push([parent, k, val])
        parent[k] = tmp
      } else {
        return tmp
      }
    }
    stack.pop()
  }
}

// wraps replacer function to handle values we couldn't replace
// and mark them as replaced value
function replaceGetterValues (replacer) {
  replacer =
    typeof replacer !== 'undefined'
      ? replacer
      : function (k, v) {
        return v
      }
  return function (key, val) {
    if (replacerStack.length > 0) {
      for (var i = 0; i < replacerStack.length; i++) {
        var part = replacerStack[i]
        if (part[1] === key && part[0] === val) {
          val = part[2]
          replacerStack.splice(i, 1)
          break
        }
      }
    }
    return replacer.call(this, key, val)
  }
}

},{}],68:[function(require,module,exports){
"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function Agent() {
  this._defaults = [];
}

['use', 'on', 'once', 'set', 'query', 'type', 'accept', 'auth', 'withCredentials', 'sortQuery', 'retry', 'ok', 'redirects', 'timeout', 'buffer', 'serialize', 'parse', 'ca', 'key', 'pfx', 'cert', 'disableTLSCerts'].forEach(function (fn) {
  // Default setting for all requests from this agent
  Agent.prototype[fn] = function () {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    this._defaults.push({
      fn: fn,
      args: args
    });

    return this;
  };
});

Agent.prototype._setDefaults = function (req) {
  this._defaults.forEach(function (def) {
    req[def.fn].apply(req, _toConsumableArray(def.args));
  });
};

module.exports = Agent;

},{}],69:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * Root reference for iframes.
 */
var root;

if (typeof window !== 'undefined') {
  // Browser window
  root = window;
} else if (typeof self === 'undefined') {
  // Other environments
  console.warn('Using browser-only version of superagent in non-browser environment');
  root = void 0;
} else {
  // Web Worker
  root = self;
}

var Emitter = require('component-emitter');

var safeStringify = require('fast-safe-stringify');

var RequestBase = require('./request-base');

var isObject = require('./is-object');

var ResponseBase = require('./response-base');

var Agent = require('./agent-base');
/**
 * Noop.
 */


function noop() {}
/**
 * Expose `request`.
 */


module.exports = function (method, url) {
  // callback
  if (typeof url === 'function') {
    return new exports.Request('GET', method).end(url);
  } // url first


  if (arguments.length === 1) {
    return new exports.Request('GET', method);
  }

  return new exports.Request(method, url);
};

exports = module.exports;
var request = exports;
exports.Request = Request;
/**
 * Determine XHR.
 */

request.getXHR = function () {
  if (root.XMLHttpRequest && (!root.location || root.location.protocol !== 'file:' || !root.ActiveXObject)) {
    return new XMLHttpRequest();
  }

  try {
    return new ActiveXObject('Microsoft.XMLHTTP');
  } catch (_unused) {}

  try {
    return new ActiveXObject('Msxml2.XMLHTTP.6.0');
  } catch (_unused2) {}

  try {
    return new ActiveXObject('Msxml2.XMLHTTP.3.0');
  } catch (_unused3) {}

  try {
    return new ActiveXObject('Msxml2.XMLHTTP');
  } catch (_unused4) {}

  throw new Error('Browser-only version of superagent could not find XHR');
};
/**
 * Removes leading and trailing whitespace, added to support IE.
 *
 * @param {String} s
 * @return {String}
 * @api private
 */


var trim = ''.trim ? function (s) {
  return s.trim();
} : function (s) {
  return s.replace(/(^\s*|\s*$)/g, '');
};
/**
 * Serialize the given `obj`.
 *
 * @param {Object} obj
 * @return {String}
 * @api private
 */

function serialize(obj) {
  if (!isObject(obj)) return obj;
  var pairs = [];

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) pushEncodedKeyValuePair(pairs, key, obj[key]);
  }

  return pairs.join('&');
}
/**
 * Helps 'serialize' with serializing arrays.
 * Mutates the pairs array.
 *
 * @param {Array} pairs
 * @param {String} key
 * @param {Mixed} val
 */


function pushEncodedKeyValuePair(pairs, key, val) {
  if (val === undefined) return;

  if (val === null) {
    pairs.push(encodeURI(key));
    return;
  }

  if (Array.isArray(val)) {
    val.forEach(function (v) {
      pushEncodedKeyValuePair(pairs, key, v);
    });
  } else if (isObject(val)) {
    for (var subkey in val) {
      if (Object.prototype.hasOwnProperty.call(val, subkey)) pushEncodedKeyValuePair(pairs, "".concat(key, "[").concat(subkey, "]"), val[subkey]);
    }
  } else {
    pairs.push(encodeURI(key) + '=' + encodeURIComponent(val));
  }
}
/**
 * Expose serialization method.
 */


request.serializeObject = serialize;
/**
 * Parse the given x-www-form-urlencoded `str`.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function parseString(str) {
  var obj = {};
  var pairs = str.split('&');
  var pair;
  var pos;

  for (var i = 0, len = pairs.length; i < len; ++i) {
    pair = pairs[i];
    pos = pair.indexOf('=');

    if (pos === -1) {
      obj[decodeURIComponent(pair)] = '';
    } else {
      obj[decodeURIComponent(pair.slice(0, pos))] = decodeURIComponent(pair.slice(pos + 1));
    }
  }

  return obj;
}
/**
 * Expose parser.
 */


request.parseString = parseString;
/**
 * Default MIME type map.
 *
 *     superagent.types.xml = 'application/xml';
 *
 */

request.types = {
  html: 'text/html',
  json: 'application/json',
  xml: 'text/xml',
  urlencoded: 'application/x-www-form-urlencoded',
  form: 'application/x-www-form-urlencoded',
  'form-data': 'application/x-www-form-urlencoded'
};
/**
 * Default serialization map.
 *
 *     superagent.serialize['application/xml'] = function(obj){
 *       return 'generated xml here';
 *     };
 *
 */

request.serialize = {
  'application/x-www-form-urlencoded': serialize,
  'application/json': safeStringify
};
/**
 * Default parsers.
 *
 *     superagent.parse['application/xml'] = function(str){
 *       return { object parsed from str };
 *     };
 *
 */

request.parse = {
  'application/x-www-form-urlencoded': parseString,
  'application/json': JSON.parse
};
/**
 * Parse the given header `str` into
 * an object containing the mapped fields.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */

function parseHeader(str) {
  var lines = str.split(/\r?\n/);
  var fields = {};
  var index;
  var line;
  var field;
  var val;

  for (var i = 0, len = lines.length; i < len; ++i) {
    line = lines[i];
    index = line.indexOf(':');

    if (index === -1) {
      // could be empty line, just skip it
      continue;
    }

    field = line.slice(0, index).toLowerCase();
    val = trim(line.slice(index + 1));
    fields[field] = val;
  }

  return fields;
}
/**
 * Check if `mime` is json or has +json structured syntax suffix.
 *
 * @param {String} mime
 * @return {Boolean}
 * @api private
 */


function isJSON(mime) {
  // should match /json or +json
  // but not /json-seq
  return /[/+]json($|[^-\w])/.test(mime);
}
/**
 * Initialize a new `Response` with the given `xhr`.
 *
 *  - set flags (.ok, .error, etc)
 *  - parse header
 *
 * Examples:
 *
 *  Aliasing `superagent` as `request` is nice:
 *
 *      request = superagent;
 *
 *  We can use the promise-like API, or pass callbacks:
 *
 *      request.get('/').end(function(res){});
 *      request.get('/', function(res){});
 *
 *  Sending data can be chained:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' })
 *        .end(function(res){});
 *
 *  Or passed to `.send()`:
 *
 *      request
 *        .post('/user')
 *        .send({ name: 'tj' }, function(res){});
 *
 *  Or passed to `.post()`:
 *
 *      request
 *        .post('/user', { name: 'tj' })
 *        .end(function(res){});
 *
 * Or further reduced to a single call for simple cases:
 *
 *      request
 *        .post('/user', { name: 'tj' }, function(res){});
 *
 * @param {XMLHTTPRequest} xhr
 * @param {Object} options
 * @api private
 */


function Response(req) {
  this.req = req;
  this.xhr = this.req.xhr; // responseText is accessible only if responseType is '' or 'text' and on older browsers

  this.text = this.req.method !== 'HEAD' && (this.xhr.responseType === '' || this.xhr.responseType === 'text') || typeof this.xhr.responseType === 'undefined' ? this.xhr.responseText : null;
  this.statusText = this.req.xhr.statusText;
  var status = this.xhr.status; // handle IE9 bug: http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request

  if (status === 1223) {
    status = 204;
  }

  this._setStatusProperties(status);

  this.headers = parseHeader(this.xhr.getAllResponseHeaders());
  this.header = this.headers; // getAllResponseHeaders sometimes falsely returns "" for CORS requests, but
  // getResponseHeader still works. so we get content-type even if getting
  // other headers fails.

  this.header['content-type'] = this.xhr.getResponseHeader('content-type');

  this._setHeaderProperties(this.header);

  if (this.text === null && req._responseType) {
    this.body = this.xhr.response;
  } else {
    this.body = this.req.method === 'HEAD' ? null : this._parseBody(this.text ? this.text : this.xhr.response);
  }
} // eslint-disable-next-line new-cap


ResponseBase(Response.prototype);
/**
 * Parse the given body `str`.
 *
 * Used for auto-parsing of bodies. Parsers
 * are defined on the `superagent.parse` object.
 *
 * @param {String} str
 * @return {Mixed}
 * @api private
 */

Response.prototype._parseBody = function (str) {
  var parse = request.parse[this.type];

  if (this.req._parser) {
    return this.req._parser(this, str);
  }

  if (!parse && isJSON(this.type)) {
    parse = request.parse['application/json'];
  }

  return parse && str && (str.length > 0 || str instanceof Object) ? parse(str) : null;
};
/**
 * Return an `Error` representative of this response.
 *
 * @return {Error}
 * @api public
 */


Response.prototype.toError = function () {
  var req = this.req;
  var method = req.method;
  var url = req.url;
  var msg = "cannot ".concat(method, " ").concat(url, " (").concat(this.status, ")");
  var err = new Error(msg);
  err.status = this.status;
  err.method = method;
  err.url = url;
  return err;
};
/**
 * Expose `Response`.
 */


request.Response = Response;
/**
 * Initialize a new `Request` with the given `method` and `url`.
 *
 * @param {String} method
 * @param {String} url
 * @api public
 */

function Request(method, url) {
  var self = this;
  this._query = this._query || [];
  this.method = method;
  this.url = url;
  this.header = {}; // preserves header name case

  this._header = {}; // coerces header names to lowercase

  this.on('end', function () {
    var err = null;
    var res = null;

    try {
      res = new Response(self);
    } catch (err_) {
      err = new Error('Parser is unable to parse the response');
      err.parse = true;
      err.original = err_; // issue #675: return the raw response if the response parsing fails

      if (self.xhr) {
        // ie9 doesn't have 'response' property
        err.rawResponse = typeof self.xhr.responseType === 'undefined' ? self.xhr.responseText : self.xhr.response; // issue #876: return the http status code if the response parsing fails

        err.status = self.xhr.status ? self.xhr.status : null;
        err.statusCode = err.status; // backwards-compat only
      } else {
        err.rawResponse = null;
        err.status = null;
      }

      return self.callback(err);
    }

    self.emit('response', res);
    var new_err;

    try {
      if (!self._isResponseOK(res)) {
        new_err = new Error(res.statusText || res.text || 'Unsuccessful HTTP response');
      }
    } catch (err_) {
      new_err = err_; // ok() callback can throw
    } // #1000 don't catch errors from the callback to avoid double calling it


    if (new_err) {
      new_err.original = err;
      new_err.response = res;
      new_err.status = res.status;
      self.callback(new_err, res);
    } else {
      self.callback(null, res);
    }
  });
}
/**
 * Mixin `Emitter` and `RequestBase`.
 */
// eslint-disable-next-line new-cap


Emitter(Request.prototype); // eslint-disable-next-line new-cap

RequestBase(Request.prototype);
/**
 * Set Content-Type to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.xml = 'application/xml';
 *
 *      request.post('/')
 *        .type('xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 *      request.post('/')
 *        .type('application/xml')
 *        .send(xmlstring)
 *        .end(callback);
 *
 * @param {String} type
 * @return {Request} for chaining
 * @api public
 */

Request.prototype.type = function (type) {
  this.set('Content-Type', request.types[type] || type);
  return this;
};
/**
 * Set Accept to `type`, mapping values from `request.types`.
 *
 * Examples:
 *
 *      superagent.types.json = 'application/json';
 *
 *      request.get('/agent')
 *        .accept('json')
 *        .end(callback);
 *
 *      request.get('/agent')
 *        .accept('application/json')
 *        .end(callback);
 *
 * @param {String} accept
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.accept = function (type) {
  this.set('Accept', request.types[type] || type);
  return this;
};
/**
 * Set Authorization field value with `user` and `pass`.
 *
 * @param {String} user
 * @param {String} [pass] optional in case of using 'bearer' as type
 * @param {Object} options with 'type' property 'auto', 'basic' or 'bearer' (default 'basic')
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.auth = function (user, pass, options) {
  if (arguments.length === 1) pass = '';

  if (_typeof(pass) === 'object' && pass !== null) {
    // pass is optional and can be replaced with options
    options = pass;
    pass = '';
  }

  if (!options) {
    options = {
      type: typeof btoa === 'function' ? 'basic' : 'auto'
    };
  }

  var encoder = function encoder(string) {
    if (typeof btoa === 'function') {
      return btoa(string);
    }

    throw new Error('Cannot use basic auth, btoa is not a function');
  };

  return this._auth(user, pass, options, encoder);
};
/**
 * Add query-string `val`.
 *
 * Examples:
 *
 *   request.get('/shoes')
 *     .query('size=10')
 *     .query({ color: 'blue' })
 *
 * @param {Object|String} val
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.query = function (val) {
  if (typeof val !== 'string') val = serialize(val);
  if (val) this._query.push(val);
  return this;
};
/**
 * Queue the given `file` as an attachment to the specified `field`,
 * with optional `options` (or filename).
 *
 * ``` js
 * request.post('/upload')
 *   .attach('content', new Blob(['<a id="a"><b id="b">hey!</b></a>'], { type: "text/html"}))
 *   .end(callback);
 * ```
 *
 * @param {String} field
 * @param {Blob|File} file
 * @param {String|Object} options
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.attach = function (field, file, options) {
  if (file) {
    if (this._data) {
      throw new Error("superagent can't mix .send() and .attach()");
    }

    this._getFormData().append(field, file, options || file.name);
  }

  return this;
};

Request.prototype._getFormData = function () {
  if (!this._formData) {
    this._formData = new root.FormData();
  }

  return this._formData;
};
/**
 * Invoke the callback with `err` and `res`
 * and handle arity check.
 *
 * @param {Error} err
 * @param {Response} res
 * @api private
 */


Request.prototype.callback = function (err, res) {
  if (this._shouldRetry(err, res)) {
    return this._retry();
  }

  var fn = this._callback;
  this.clearTimeout();

  if (err) {
    if (this._maxRetries) err.retries = this._retries - 1;
    this.emit('error', err);
  }

  fn(err, res);
};
/**
 * Invoke callback with x-domain error.
 *
 * @api private
 */


Request.prototype.crossDomainError = function () {
  var err = new Error('Request has been terminated\nPossible causes: the network is offline, Origin is not allowed by Access-Control-Allow-Origin, the page is being unloaded, etc.');
  err.crossDomain = true;
  err.status = this.status;
  err.method = this.method;
  err.url = this.url;
  this.callback(err);
}; // This only warns, because the request is still likely to work


Request.prototype.agent = function () {
  console.warn('This is not supported in browser version of superagent');
  return this;
};

Request.prototype.ca = Request.prototype.agent;
Request.prototype.buffer = Request.prototype.ca; // This throws, because it can't send/receive data as expected

Request.prototype.write = function () {
  throw new Error('Streaming is not supported in browser version of superagent');
};

Request.prototype.pipe = Request.prototype.write;
/**
 * Check if `obj` is a host object,
 * we don't want to serialize these :)
 *
 * @param {Object} obj host object
 * @return {Boolean} is a host object
 * @api private
 */

Request.prototype._isHost = function (obj) {
  // Native objects stringify to [object File], [object Blob], [object FormData], etc.
  return obj && _typeof(obj) === 'object' && !Array.isArray(obj) && Object.prototype.toString.call(obj) !== '[object Object]';
};
/**
 * Initiate request, invoking callback `fn(res)`
 * with an instanceof `Response`.
 *
 * @param {Function} fn
 * @return {Request} for chaining
 * @api public
 */


Request.prototype.end = function (fn) {
  if (this._endCalled) {
    console.warn('Warning: .end() was called twice. This is not supported in superagent');
  }

  this._endCalled = true; // store callback

  this._callback = fn || noop; // querystring

  this._finalizeQueryString();

  this._end();
};

Request.prototype._setUploadTimeout = function () {
  var self = this; // upload timeout it's wokrs only if deadline timeout is off

  if (this._uploadTimeout && !this._uploadTimeoutTimer) {
    this._uploadTimeoutTimer = setTimeout(function () {
      self._timeoutError('Upload timeout of ', self._uploadTimeout, 'ETIMEDOUT');
    }, this._uploadTimeout);
  }
}; // eslint-disable-next-line complexity


Request.prototype._end = function () {
  if (this._aborted) return this.callback(new Error('The request has been aborted even before .end() was called'));
  var self = this;
  this.xhr = request.getXHR();
  var xhr = this.xhr;
  var data = this._formData || this._data;

  this._setTimeouts(); // state change


  xhr.onreadystatechange = function () {
    var readyState = xhr.readyState;

    if (readyState >= 2 && self._responseTimeoutTimer) {
      clearTimeout(self._responseTimeoutTimer);
    }

    if (readyState !== 4) {
      return;
    } // In IE9, reads to any property (e.g. status) off of an aborted XHR will
    // result in the error "Could not complete the operation due to error c00c023f"


    var status;

    try {
      status = xhr.status;
    } catch (_unused5) {
      status = 0;
    }

    if (!status) {
      if (self.timedout || self._aborted) return;
      return self.crossDomainError();
    }

    self.emit('end');
  }; // progress


  var handleProgress = function handleProgress(direction, e) {
    if (e.total > 0) {
      e.percent = e.loaded / e.total * 100;

      if (e.percent === 100) {
        clearTimeout(self._uploadTimeoutTimer);
      }
    }

    e.direction = direction;
    self.emit('progress', e);
  };

  if (this.hasListeners('progress')) {
    try {
      xhr.addEventListener('progress', handleProgress.bind(null, 'download'));

      if (xhr.upload) {
        xhr.upload.addEventListener('progress', handleProgress.bind(null, 'upload'));
      }
    } catch (_unused6) {// Accessing xhr.upload fails in IE from a web worker, so just pretend it doesn't exist.
      // Reported here:
      // https://connect.microsoft.com/IE/feedback/details/837245/xmlhttprequest-upload-throws-invalid-argument-when-used-from-web-worker-context
    }
  }

  if (xhr.upload) {
    this._setUploadTimeout();
  } // initiate request


  try {
    if (this.username && this.password) {
      xhr.open(this.method, this.url, true, this.username, this.password);
    } else {
      xhr.open(this.method, this.url, true);
    }
  } catch (err) {
    // see #1149
    return this.callback(err);
  } // CORS


  if (this._withCredentials) xhr.withCredentials = true; // body

  if (!this._formData && this.method !== 'GET' && this.method !== 'HEAD' && typeof data !== 'string' && !this._isHost(data)) {
    // serialize stuff
    var contentType = this._header['content-type'];

    var _serialize = this._serializer || request.serialize[contentType ? contentType.split(';')[0] : ''];

    if (!_serialize && isJSON(contentType)) {
      _serialize = request.serialize['application/json'];
    }

    if (_serialize) data = _serialize(data);
  } // set header fields


  for (var field in this.header) {
    if (this.header[field] === null) continue;
    if (Object.prototype.hasOwnProperty.call(this.header, field)) xhr.setRequestHeader(field, this.header[field]);
  }

  if (this._responseType) {
    xhr.responseType = this._responseType;
  } // send stuff


  this.emit('request', this); // IE11 xhr.send(undefined) sends 'undefined' string as POST payload (instead of nothing)
  // We need null here if data is undefined

  xhr.send(typeof data === 'undefined' ? null : data);
};

request.agent = function () {
  return new Agent();
};

['GET', 'POST', 'OPTIONS', 'PATCH', 'PUT', 'DELETE'].forEach(function (method) {
  Agent.prototype[method.toLowerCase()] = function (url, fn) {
    var req = new request.Request(method, url);

    this._setDefaults(req);

    if (fn) {
      req.end(fn);
    }

    return req;
  };
});
Agent.prototype.del = Agent.prototype.delete;
/**
 * GET `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.get = function (url, data, fn) {
  var req = request('GET', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.query(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * HEAD `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.head = function (url, data, fn) {
  var req = request('HEAD', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.query(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * OPTIONS query to `url` with optional callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.options = function (url, data, fn) {
  var req = request('OPTIONS', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * DELETE `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


function del(url, data, fn) {
  var req = request('DELETE', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
}

request.del = del;
request.delete = del;
/**
 * PATCH `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */

request.patch = function (url, data, fn) {
  var req = request('PATCH', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * POST `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed} [data]
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.post = function (url, data, fn) {
  var req = request('POST', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};
/**
 * PUT `url` with optional `data` and callback `fn(res)`.
 *
 * @param {String} url
 * @param {Mixed|Function} [data] or fn
 * @param {Function} [fn]
 * @return {Request}
 * @api public
 */


request.put = function (url, data, fn) {
  var req = request('PUT', url);

  if (typeof data === 'function') {
    fn = data;
    data = null;
  }

  if (data) req.send(data);
  if (fn) req.end(fn);
  return req;
};

},{"./agent-base":68,"./is-object":70,"./request-base":71,"./response-base":72,"component-emitter":66,"fast-safe-stringify":67}],70:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * Check if `obj` is an object.
 *
 * @param {Object} obj
 * @return {Boolean}
 * @api private
 */
function isObject(obj) {
  return obj !== null && _typeof(obj) === 'object';
}

module.exports = isObject;

},{}],71:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * Module of mixed-in functions shared between node and client code
 */
var isObject = require('./is-object');
/**
 * Expose `RequestBase`.
 */


module.exports = RequestBase;
/**
 * Initialize a new `RequestBase`.
 *
 * @api public
 */

function RequestBase(obj) {
  if (obj) return mixin(obj);
}
/**
 * Mixin the prototype properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */


function mixin(obj) {
  for (var key in RequestBase.prototype) {
    if (Object.prototype.hasOwnProperty.call(RequestBase.prototype, key)) obj[key] = RequestBase.prototype[key];
  }

  return obj;
}
/**
 * Clear previous timeout.
 *
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.clearTimeout = function () {
  clearTimeout(this._timer);
  clearTimeout(this._responseTimeoutTimer);
  clearTimeout(this._uploadTimeoutTimer);
  delete this._timer;
  delete this._responseTimeoutTimer;
  delete this._uploadTimeoutTimer;
  return this;
};
/**
 * Override default response body parser
 *
 * This function will be called to convert incoming data into request.body
 *
 * @param {Function}
 * @api public
 */


RequestBase.prototype.parse = function (fn) {
  this._parser = fn;
  return this;
};
/**
 * Set format of binary response body.
 * In browser valid formats are 'blob' and 'arraybuffer',
 * which return Blob and ArrayBuffer, respectively.
 *
 * In Node all values result in Buffer.
 *
 * Examples:
 *
 *      req.get('/')
 *        .responseType('blob')
 *        .end(callback);
 *
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.responseType = function (val) {
  this._responseType = val;
  return this;
};
/**
 * Override default request body serializer
 *
 * This function will be called to convert data set via .send or .attach into payload to send
 *
 * @param {Function}
 * @api public
 */


RequestBase.prototype.serialize = function (fn) {
  this._serializer = fn;
  return this;
};
/**
 * Set timeouts.
 *
 * - response timeout is time between sending request and receiving the first byte of the response. Includes DNS and connection time.
 * - deadline is the time from start of the request to receiving response body in full. If the deadline is too short large files may not load at all on slow connections.
 * - upload is the time  since last bit of data was sent or received. This timeout works only if deadline timeout is off
 *
 * Value of 0 or false means no timeout.
 *
 * @param {Number|Object} ms or {response, deadline}
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.timeout = function (options) {
  if (!options || _typeof(options) !== 'object') {
    this._timeout = options;
    this._responseTimeout = 0;
    this._uploadTimeout = 0;
    return this;
  }

  for (var option in options) {
    if (Object.prototype.hasOwnProperty.call(options, option)) {
      switch (option) {
        case 'deadline':
          this._timeout = options.deadline;
          break;

        case 'response':
          this._responseTimeout = options.response;
          break;

        case 'upload':
          this._uploadTimeout = options.upload;
          break;

        default:
          console.warn('Unknown timeout option', option);
      }
    }
  }

  return this;
};
/**
 * Set number of retry attempts on error.
 *
 * Failed requests will be retried 'count' times if timeout or err.code >= 500.
 *
 * @param {Number} count
 * @param {Function} [fn]
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.retry = function (count, fn) {
  // Default to 1 if no count passed or true
  if (arguments.length === 0 || count === true) count = 1;
  if (count <= 0) count = 0;
  this._maxRetries = count;
  this._retries = 0;
  this._retryCallback = fn;
  return this;
};

var ERROR_CODES = ['ECONNRESET', 'ETIMEDOUT', 'EADDRINFO', 'ESOCKETTIMEDOUT'];
/**
 * Determine if a request should be retried.
 * (Borrowed from segmentio/superagent-retry)
 *
 * @param {Error} err an error
 * @param {Response} [res] response
 * @returns {Boolean} if segment should be retried
 */

RequestBase.prototype._shouldRetry = function (err, res) {
  if (!this._maxRetries || this._retries++ >= this._maxRetries) {
    return false;
  }

  if (this._retryCallback) {
    try {
      var override = this._retryCallback(err, res);

      if (override === true) return true;
      if (override === false) return false; // undefined falls back to defaults
    } catch (err_) {
      console.error(err_);
    }
  }

  if (res && res.status && res.status >= 500 && res.status !== 501) return true;

  if (err) {
    if (err.code && ERROR_CODES.includes(err.code)) return true; // Superagent timeout

    if (err.timeout && err.code === 'ECONNABORTED') return true;
    if (err.crossDomain) return true;
  }

  return false;
};
/**
 * Retry request
 *
 * @return {Request} for chaining
 * @api private
 */


RequestBase.prototype._retry = function () {
  this.clearTimeout(); // node

  if (this.req) {
    this.req = null;
    this.req = this.request();
  }

  this._aborted = false;
  this.timedout = false;
  this.timedoutError = null;
  return this._end();
};
/**
 * Promise support
 *
 * @param {Function} resolve
 * @param {Function} [reject]
 * @return {Request}
 */


RequestBase.prototype.then = function (resolve, reject) {
  var _this = this;

  if (!this._fullfilledPromise) {
    var self = this;

    if (this._endCalled) {
      console.warn('Warning: superagent request was sent twice, because both .end() and .then() were called. Never call .end() if you use promises');
    }

    this._fullfilledPromise = new Promise(function (resolve, reject) {
      self.on('abort', function () {
        if (_this._maxRetries && _this._maxRetries > _this._retries) {
          return;
        }

        if (_this.timedout && _this.timedoutError) {
          reject(_this.timedoutError);
          return;
        }

        var err = new Error('Aborted');
        err.code = 'ABORTED';
        err.status = _this.status;
        err.method = _this.method;
        err.url = _this.url;
        reject(err);
      });
      self.end(function (err, res) {
        if (err) reject(err);else resolve(res);
      });
    });
  }

  return this._fullfilledPromise.then(resolve, reject);
};

RequestBase.prototype.catch = function (cb) {
  return this.then(undefined, cb);
};
/**
 * Allow for extension
 */


RequestBase.prototype.use = function (fn) {
  fn(this);
  return this;
};

RequestBase.prototype.ok = function (cb) {
  if (typeof cb !== 'function') throw new Error('Callback required');
  this._okCallback = cb;
  return this;
};

RequestBase.prototype._isResponseOK = function (res) {
  if (!res) {
    return false;
  }

  if (this._okCallback) {
    return this._okCallback(res);
  }

  return res.status >= 200 && res.status < 300;
};
/**
 * Get request header `field`.
 * Case-insensitive.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */


RequestBase.prototype.get = function (field) {
  return this._header[field.toLowerCase()];
};
/**
 * Get case-insensitive header `field` value.
 * This is a deprecated internal API. Use `.get(field)` instead.
 *
 * (getHeader is no longer used internally by the superagent code base)
 *
 * @param {String} field
 * @return {String}
 * @api private
 * @deprecated
 */


RequestBase.prototype.getHeader = RequestBase.prototype.get;
/**
 * Set header `field` to `val`, or multiple fields with one object.
 * Case-insensitive.
 *
 * Examples:
 *
 *      req.get('/')
 *        .set('Accept', 'application/json')
 *        .set('X-API-Key', 'foobar')
 *        .end(callback);
 *
 *      req.get('/')
 *        .set({ Accept: 'application/json', 'X-API-Key': 'foobar' })
 *        .end(callback);
 *
 * @param {String|Object} field
 * @param {String} val
 * @return {Request} for chaining
 * @api public
 */

RequestBase.prototype.set = function (field, val) {
  if (isObject(field)) {
    for (var key in field) {
      if (Object.prototype.hasOwnProperty.call(field, key)) this.set(key, field[key]);
    }

    return this;
  }

  this._header[field.toLowerCase()] = val;
  this.header[field] = val;
  return this;
};
/**
 * Remove header `field`.
 * Case-insensitive.
 *
 * Example:
 *
 *      req.get('/')
 *        .unset('User-Agent')
 *        .end(callback);
 *
 * @param {String} field field name
 */


RequestBase.prototype.unset = function (field) {
  delete this._header[field.toLowerCase()];
  delete this.header[field];
  return this;
};
/**
 * Write the field `name` and `val`, or multiple fields with one object
 * for "multipart/form-data" request bodies.
 *
 * ``` js
 * request.post('/upload')
 *   .field('foo', 'bar')
 *   .end(callback);
 *
 * request.post('/upload')
 *   .field({ foo: 'bar', baz: 'qux' })
 *   .end(callback);
 * ```
 *
 * @param {String|Object} name name of field
 * @param {String|Blob|File|Buffer|fs.ReadStream} val value of field
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.field = function (name, val) {
  // name should be either a string or an object.
  if (name === null || undefined === name) {
    throw new Error('.field(name, val) name can not be empty');
  }

  if (this._data) {
    throw new Error(".field() can't be used if .send() is used. Please use only .send() or only .field() & .attach()");
  }

  if (isObject(name)) {
    for (var key in name) {
      if (Object.prototype.hasOwnProperty.call(name, key)) this.field(key, name[key]);
    }

    return this;
  }

  if (Array.isArray(val)) {
    for (var i in val) {
      if (Object.prototype.hasOwnProperty.call(val, i)) this.field(name, val[i]);
    }

    return this;
  } // val should be defined now


  if (val === null || undefined === val) {
    throw new Error('.field(name, val) val can not be empty');
  }

  if (typeof val === 'boolean') {
    val = String(val);
  }

  this._getFormData().append(name, val);

  return this;
};
/**
 * Abort the request, and clear potential timeout.
 *
 * @return {Request} request
 * @api public
 */


RequestBase.prototype.abort = function () {
  if (this._aborted) {
    return this;
  }

  this._aborted = true;
  if (this.xhr) this.xhr.abort(); // browser

  if (this.req) this.req.abort(); // node

  this.clearTimeout();
  this.emit('abort');
  return this;
};

RequestBase.prototype._auth = function (user, pass, options, base64Encoder) {
  switch (options.type) {
    case 'basic':
      this.set('Authorization', "Basic ".concat(base64Encoder("".concat(user, ":").concat(pass))));
      break;

    case 'auto':
      this.username = user;
      this.password = pass;
      break;

    case 'bearer':
      // usage would be .auth(accessToken, { type: 'bearer' })
      this.set('Authorization', "Bearer ".concat(user));
      break;

    default:
      break;
  }

  return this;
};
/**
 * Enable transmission of cookies with x-domain requests.
 *
 * Note that for this to work the origin must not be
 * using "Access-Control-Allow-Origin" with a wildcard,
 * and also must set "Access-Control-Allow-Credentials"
 * to "true".
 *
 * @api public
 */


RequestBase.prototype.withCredentials = function (on) {
  // This is browser-only functionality. Node side is no-op.
  if (on === undefined) on = true;
  this._withCredentials = on;
  return this;
};
/**
 * Set the max redirects to `n`. Does nothing in browser XHR implementation.
 *
 * @param {Number} n
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.redirects = function (n) {
  this._maxRedirects = n;
  return this;
};
/**
 * Maximum size of buffered response body, in bytes. Counts uncompressed size.
 * Default 200MB.
 *
 * @param {Number} n number of bytes
 * @return {Request} for chaining
 */


RequestBase.prototype.maxResponseSize = function (n) {
  if (typeof n !== 'number') {
    throw new TypeError('Invalid argument');
  }

  this._maxResponseSize = n;
  return this;
};
/**
 * Convert to a plain javascript object (not JSON string) of scalar properties.
 * Note as this method is designed to return a useful non-this value,
 * it cannot be chained.
 *
 * @return {Object} describing method, url, and data of this request
 * @api public
 */


RequestBase.prototype.toJSON = function () {
  return {
    method: this.method,
    url: this.url,
    data: this._data,
    headers: this._header
  };
};
/**
 * Send `data` as the request body, defaulting the `.type()` to "json" when
 * an object is given.
 *
 * Examples:
 *
 *       // manual json
 *       request.post('/user')
 *         .type('json')
 *         .send('{"name":"tj"}')
 *         .end(callback)
 *
 *       // auto json
 *       request.post('/user')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // manual x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send('name=tj')
 *         .end(callback)
 *
 *       // auto x-www-form-urlencoded
 *       request.post('/user')
 *         .type('form')
 *         .send({ name: 'tj' })
 *         .end(callback)
 *
 *       // defaults to x-www-form-urlencoded
 *      request.post('/user')
 *        .send('name=tobi')
 *        .send('species=ferret')
 *        .end(callback)
 *
 * @param {String|Object} data
 * @return {Request} for chaining
 * @api public
 */
// eslint-disable-next-line complexity


RequestBase.prototype.send = function (data) {
  var isObj = isObject(data);
  var type = this._header['content-type'];

  if (this._formData) {
    throw new Error(".send() can't be used if .attach() or .field() is used. Please use only .send() or only .field() & .attach()");
  }

  if (isObj && !this._data) {
    if (Array.isArray(data)) {
      this._data = [];
    } else if (!this._isHost(data)) {
      this._data = {};
    }
  } else if (data && this._data && this._isHost(this._data)) {
    throw new Error("Can't merge these send calls");
  } // merge


  if (isObj && isObject(this._data)) {
    for (var key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) this._data[key] = data[key];
    }
  } else if (typeof data === 'string') {
    // default to x-www-form-urlencoded
    if (!type) this.type('form');
    type = this._header['content-type'];

    if (type === 'application/x-www-form-urlencoded') {
      this._data = this._data ? "".concat(this._data, "&").concat(data) : data;
    } else {
      this._data = (this._data || '') + data;
    }
  } else {
    this._data = data;
  }

  if (!isObj || this._isHost(data)) {
    return this;
  } // default to json


  if (!type) this.type('json');
  return this;
};
/**
 * Sort `querystring` by the sort function
 *
 *
 * Examples:
 *
 *       // default order
 *       request.get('/user')
 *         .query('name=Nick')
 *         .query('search=Manny')
 *         .sortQuery()
 *         .end(callback)
 *
 *       // customized sort function
 *       request.get('/user')
 *         .query('name=Nick')
 *         .query('search=Manny')
 *         .sortQuery(function(a, b){
 *           return a.length - b.length;
 *         })
 *         .end(callback)
 *
 *
 * @param {Function} sort
 * @return {Request} for chaining
 * @api public
 */


RequestBase.prototype.sortQuery = function (sort) {
  // _sort default to true but otherwise can be a function or boolean
  this._sort = typeof sort === 'undefined' ? true : sort;
  return this;
};
/**
 * Compose querystring to append to req.url
 *
 * @api private
 */


RequestBase.prototype._finalizeQueryString = function () {
  var query = this._query.join('&');

  if (query) {
    this.url += (this.url.includes('?') ? '&' : '?') + query;
  }

  this._query.length = 0; // Makes the call idempotent

  if (this._sort) {
    var index = this.url.indexOf('?');

    if (index >= 0) {
      var queryArr = this.url.slice(index + 1).split('&');

      if (typeof this._sort === 'function') {
        queryArr.sort(this._sort);
      } else {
        queryArr.sort();
      }

      this.url = this.url.slice(0, index) + '?' + queryArr.join('&');
    }
  }
}; // For backwards compat only


RequestBase.prototype._appendQueryString = function () {
  console.warn('Unsupported');
};
/**
 * Invoke callback with timeout error.
 *
 * @api private
 */


RequestBase.prototype._timeoutError = function (reason, timeout, errno) {
  if (this._aborted) {
    return;
  }

  var err = new Error("".concat(reason + timeout, "ms exceeded"));
  err.timeout = timeout;
  err.code = 'ECONNABORTED';
  err.errno = errno;
  this.timedout = true;
  this.timedoutError = err;
  this.abort();
  this.callback(err);
};

RequestBase.prototype._setTimeouts = function () {
  var self = this; // deadline

  if (this._timeout && !this._timer) {
    this._timer = setTimeout(function () {
      self._timeoutError('Timeout of ', self._timeout, 'ETIME');
    }, this._timeout);
  } // response timeout


  if (this._responseTimeout && !this._responseTimeoutTimer) {
    this._responseTimeoutTimer = setTimeout(function () {
      self._timeoutError('Response timeout of ', self._responseTimeout, 'ETIMEDOUT');
    }, this._responseTimeout);
  }
};

},{"./is-object":70}],72:[function(require,module,exports){
"use strict";

/**
 * Module dependencies.
 */
var utils = require('./utils');
/**
 * Expose `ResponseBase`.
 */


module.exports = ResponseBase;
/**
 * Initialize a new `ResponseBase`.
 *
 * @api public
 */

function ResponseBase(obj) {
  if (obj) return mixin(obj);
}
/**
 * Mixin the prototype properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */


function mixin(obj) {
  for (var key in ResponseBase.prototype) {
    if (Object.prototype.hasOwnProperty.call(ResponseBase.prototype, key)) obj[key] = ResponseBase.prototype[key];
  }

  return obj;
}
/**
 * Get case-insensitive `field` value.
 *
 * @param {String} field
 * @return {String}
 * @api public
 */


ResponseBase.prototype.get = function (field) {
  return this.header[field.toLowerCase()];
};
/**
 * Set header related properties:
 *
 *   - `.type` the content type without params
 *
 * A response of "Content-Type: text/plain; charset=utf-8"
 * will provide you with a `.type` of "text/plain".
 *
 * @param {Object} header
 * @api private
 */


ResponseBase.prototype._setHeaderProperties = function (header) {
  // TODO: moar!
  // TODO: make this a util
  // content-type
  var ct = header['content-type'] || '';
  this.type = utils.type(ct); // params

  var params = utils.params(ct);

  for (var key in params) {
    if (Object.prototype.hasOwnProperty.call(params, key)) this[key] = params[key];
  }

  this.links = {}; // links

  try {
    if (header.link) {
      this.links = utils.parseLinks(header.link);
    }
  } catch (_unused) {// ignore
  }
};
/**
 * Set flags such as `.ok` based on `status`.
 *
 * For example a 2xx response will give you a `.ok` of __true__
 * whereas 5xx will be __false__ and `.error` will be __true__. The
 * `.clientError` and `.serverError` are also available to be more
 * specific, and `.statusType` is the class of error ranging from 1..5
 * sometimes useful for mapping respond colors etc.
 *
 * "sugar" properties are also defined for common cases. Currently providing:
 *
 *   - .noContent
 *   - .badRequest
 *   - .unauthorized
 *   - .notAcceptable
 *   - .notFound
 *
 * @param {Number} status
 * @api private
 */


ResponseBase.prototype._setStatusProperties = function (status) {
  var type = status / 100 | 0; // status / class

  this.statusCode = status;
  this.status = this.statusCode;
  this.statusType = type; // basics

  this.info = type === 1;
  this.ok = type === 2;
  this.redirect = type === 3;
  this.clientError = type === 4;
  this.serverError = type === 5;
  this.error = type === 4 || type === 5 ? this.toError() : false; // sugar

  this.created = status === 201;
  this.accepted = status === 202;
  this.noContent = status === 204;
  this.badRequest = status === 400;
  this.unauthorized = status === 401;
  this.notAcceptable = status === 406;
  this.forbidden = status === 403;
  this.notFound = status === 404;
  this.unprocessableEntity = status === 422;
};

},{"./utils":73}],73:[function(require,module,exports){
"use strict";

/**
 * Return the mime type for the given `str`.
 *
 * @param {String} str
 * @return {String}
 * @api private
 */
exports.type = function (str) {
  return str.split(/ *; */).shift();
};
/**
 * Return header field parameters.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */


exports.params = function (str) {
  return str.split(/ *; */).reduce(function (obj, str) {
    var parts = str.split(/ *= */);
    var key = parts.shift();
    var val = parts.shift();
    if (key && val) obj[key] = val;
    return obj;
  }, {});
};
/**
 * Parse Link header fields.
 *
 * @param {String} str
 * @return {Object}
 * @api private
 */


exports.parseLinks = function (str) {
  return str.split(/ *, */).reduce(function (obj, str) {
    var parts = str.split(/ *; */);
    var url = parts[0].slice(1, -1);
    var rel = parts[1].split(/ *= */)[1].slice(1, -1);
    obj[rel] = url;
    return obj;
  }, {});
};
/**
 * Strip content related fields from `header`.
 *
 * @param {Object} header
 * @return {Object} header
 * @api private
 */


exports.cleanHeader = function (header, changesOrigin) {
  delete header['content-type'];
  delete header['content-length'];
  delete header['transfer-encoding'];
  delete header.host; // secuirty

  if (changesOrigin) {
    delete header.authorization;
    delete header.cookie;
  }

  return header;
};

},{}],74:[function(require,module,exports){
'use strict'

exports.byteLength = byteLength
exports.toByteArray = toByteArray
exports.fromByteArray = fromByteArray

var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i]
  revLookup[code.charCodeAt(i)] = i
}

// Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications
revLookup['-'.charCodeAt(0)] = 62
revLookup['_'.charCodeAt(0)] = 63

function getLens (b64) {
  var len = b64.length

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42
  var validLen = b64.indexOf('=')
  if (validLen === -1) validLen = len

  var placeHoldersLen = validLen === len
    ? 0
    : 4 - (validLen % 4)

  return [validLen, placeHoldersLen]
}

// base64 is 4/3 + up to two characters of the original data
function byteLength (b64) {
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function _byteLength (b64, validLen, placeHoldersLen) {
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function toByteArray (b64) {
  var tmp
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]

  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen))

  var curByte = 0

  // if there are placeholders, only get up to the last complete 4 chars
  var len = placeHoldersLen > 0
    ? validLen - 4
    : validLen

  var i
  for (i = 0; i < len; i += 4) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 18) |
      (revLookup[b64.charCodeAt(i + 1)] << 12) |
      (revLookup[b64.charCodeAt(i + 2)] << 6) |
      revLookup[b64.charCodeAt(i + 3)]
    arr[curByte++] = (tmp >> 16) & 0xFF
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 2) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 2) |
      (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 1) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 10) |
      (revLookup[b64.charCodeAt(i + 1)] << 4) |
      (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  return arr
}

function tripletToBase64 (num) {
  return lookup[num >> 18 & 0x3F] +
    lookup[num >> 12 & 0x3F] +
    lookup[num >> 6 & 0x3F] +
    lookup[num & 0x3F]
}

function encodeChunk (uint8, start, end) {
  var tmp
  var output = []
  for (var i = start; i < end; i += 3) {
    tmp =
      ((uint8[i] << 16) & 0xFF0000) +
      ((uint8[i + 1] << 8) & 0xFF00) +
      (uint8[i + 2] & 0xFF)
    output.push(tripletToBase64(tmp))
  }
  return output.join('')
}

function fromByteArray (uint8) {
  var tmp
  var len = uint8.length
  var extraBytes = len % 3 // if we have 1 byte left, pad 2 bytes
  var parts = []
  var maxChunkLength = 16383 // must be multiple of 3

  // go through the array every three bytes, we'll deal with trailing stuff later
  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)))
  }

  // pad the end with zeros, but make sure to not forget the extra bytes
  if (extraBytes === 1) {
    tmp = uint8[len - 1]
    parts.push(
      lookup[tmp >> 2] +
      lookup[(tmp << 4) & 0x3F] +
      '=='
    )
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + uint8[len - 1]
    parts.push(
      lookup[tmp >> 10] +
      lookup[(tmp >> 4) & 0x3F] +
      lookup[(tmp << 2) & 0x3F] +
      '='
    )
  }

  return parts.join('')
}

},{}],75:[function(require,module,exports){

},{}],76:[function(require,module,exports){
(function (Buffer){(function (){
/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <https://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */

'use strict'

var base64 = require('base64-js')
var ieee754 = require('ieee754')

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50

var K_MAX_LENGTH = 0x7fffffff
exports.kMaxLength = K_MAX_LENGTH

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Print warning and recommend using `buffer` v4.x which has an Object
 *               implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * We report that the browser does not support typed arrays if the are not subclassable
 * using __proto__. Firefox 4-29 lacks support for adding new properties to `Uint8Array`
 * (See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438). IE 10 lacks support
 * for __proto__ and has a buggy typed array implementation.
 */
Buffer.TYPED_ARRAY_SUPPORT = typedArraySupport()

if (!Buffer.TYPED_ARRAY_SUPPORT && typeof console !== 'undefined' &&
    typeof console.error === 'function') {
  console.error(
    'This browser lacks typed array (Uint8Array) support which is required by ' +
    '`buffer` v5.x. Use `buffer` v4.x if you require old browser support.'
  )
}

function typedArraySupport () {
  // Can typed array instances can be augmented?
  try {
    var arr = new Uint8Array(1)
    arr.__proto__ = { __proto__: Uint8Array.prototype, foo: function () { return 42 } }
    return arr.foo() === 42
  } catch (e) {
    return false
  }
}

Object.defineProperty(Buffer.prototype, 'parent', {
  enumerable: true,
  get: function () {
    if (!Buffer.isBuffer(this)) return undefined
    return this.buffer
  }
})

Object.defineProperty(Buffer.prototype, 'offset', {
  enumerable: true,
  get: function () {
    if (!Buffer.isBuffer(this)) return undefined
    return this.byteOffset
  }
})

function createBuffer (length) {
  if (length > K_MAX_LENGTH) {
    throw new RangeError('The value "' + length + '" is invalid for option "size"')
  }
  // Return an augmented `Uint8Array` instance
  var buf = new Uint8Array(length)
  buf.__proto__ = Buffer.prototype
  return buf
}

/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */

function Buffer (arg, encodingOrOffset, length) {
  // Common case.
  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new TypeError(
        'The "string" argument must be of type string. Received type number'
      )
    }
    return allocUnsafe(arg)
  }
  return from(arg, encodingOrOffset, length)
}

// Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
if (typeof Symbol !== 'undefined' && Symbol.species != null &&
    Buffer[Symbol.species] === Buffer) {
  Object.defineProperty(Buffer, Symbol.species, {
    value: null,
    configurable: true,
    enumerable: false,
    writable: false
  })
}

Buffer.poolSize = 8192 // not used by this implementation

function from (value, encodingOrOffset, length) {
  if (typeof value === 'string') {
    return fromString(value, encodingOrOffset)
  }

  if (ArrayBuffer.isView(value)) {
    return fromArrayLike(value)
  }

  if (value == null) {
    throw TypeError(
      'The first argument must be one of type string, Buffer, ArrayBuffer, Array, ' +
      'or Array-like Object. Received type ' + (typeof value)
    )
  }

  if (isInstance(value, ArrayBuffer) ||
      (value && isInstance(value.buffer, ArrayBuffer))) {
    return fromArrayBuffer(value, encodingOrOffset, length)
  }

  if (typeof value === 'number') {
    throw new TypeError(
      'The "value" argument must not be of type number. Received type number'
    )
  }

  var valueOf = value.valueOf && value.valueOf()
  if (valueOf != null && valueOf !== value) {
    return Buffer.from(valueOf, encodingOrOffset, length)
  }

  var b = fromObject(value)
  if (b) return b

  if (typeof Symbol !== 'undefined' && Symbol.toPrimitive != null &&
      typeof value[Symbol.toPrimitive] === 'function') {
    return Buffer.from(
      value[Symbol.toPrimitive]('string'), encodingOrOffset, length
    )
  }

  throw new TypeError(
    'The first argument must be one of type string, Buffer, ArrayBuffer, Array, ' +
    'or Array-like Object. Received type ' + (typeof value)
  )
}

/**
 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
 * if value is a number.
 * Buffer.from(str[, encoding])
 * Buffer.from(array)
 * Buffer.from(buffer)
 * Buffer.from(arrayBuffer[, byteOffset[, length]])
 **/
Buffer.from = function (value, encodingOrOffset, length) {
  return from(value, encodingOrOffset, length)
}

// Note: Change prototype *after* Buffer.from is defined to workaround Chrome bug:
// https://github.com/feross/buffer/pull/148
Buffer.prototype.__proto__ = Uint8Array.prototype
Buffer.__proto__ = Uint8Array

function assertSize (size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be of type number')
  } else if (size < 0) {
    throw new RangeError('The value "' + size + '" is invalid for option "size"')
  }
}

function alloc (size, fill, encoding) {
  assertSize(size)
  if (size <= 0) {
    return createBuffer(size)
  }
  if (fill !== undefined) {
    // Only pay attention to encoding if it's a string. This
    // prevents accidentally sending in a number that would
    // be interpretted as a start offset.
    return typeof encoding === 'string'
      ? createBuffer(size).fill(fill, encoding)
      : createBuffer(size).fill(fill)
  }
  return createBuffer(size)
}

/**
 * Creates a new filled Buffer instance.
 * alloc(size[, fill[, encoding]])
 **/
Buffer.alloc = function (size, fill, encoding) {
  return alloc(size, fill, encoding)
}

function allocUnsafe (size) {
  assertSize(size)
  return createBuffer(size < 0 ? 0 : checked(size) | 0)
}

/**
 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
 * */
Buffer.allocUnsafe = function (size) {
  return allocUnsafe(size)
}
/**
 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
 */
Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(size)
}

function fromString (string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8'
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('Unknown encoding: ' + encoding)
  }

  var length = byteLength(string, encoding) | 0
  var buf = createBuffer(length)

  var actual = buf.write(string, encoding)

  if (actual !== length) {
    // Writing a hex string, for example, that contains invalid characters will
    // cause everything after the first invalid character to be ignored. (e.g.
    // 'abxxcd' will be treated as 'ab')
    buf = buf.slice(0, actual)
  }

  return buf
}

function fromArrayLike (array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0
  var buf = createBuffer(length)
  for (var i = 0; i < length; i += 1) {
    buf[i] = array[i] & 255
  }
  return buf
}

function fromArrayBuffer (array, byteOffset, length) {
  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('"offset" is outside of buffer bounds')
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('"length" is outside of buffer bounds')
  }

  var buf
  if (byteOffset === undefined && length === undefined) {
    buf = new Uint8Array(array)
  } else if (length === undefined) {
    buf = new Uint8Array(array, byteOffset)
  } else {
    buf = new Uint8Array(array, byteOffset, length)
  }

  // Return an augmented `Uint8Array` instance
  buf.__proto__ = Buffer.prototype
  return buf
}

function fromObject (obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0
    var buf = createBuffer(len)

    if (buf.length === 0) {
      return buf
    }

    obj.copy(buf, 0, 0, len)
    return buf
  }

  if (obj.length !== undefined) {
    if (typeof obj.length !== 'number' || numberIsNaN(obj.length)) {
      return createBuffer(0)
    }
    return fromArrayLike(obj)
  }

  if (obj.type === 'Buffer' && Array.isArray(obj.data)) {
    return fromArrayLike(obj.data)
  }
}

function checked (length) {
  // Note: cannot use `length < K_MAX_LENGTH` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= K_MAX_LENGTH) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + K_MAX_LENGTH.toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (length) {
  if (+length != length) { // eslint-disable-line eqeqeq
    length = 0
  }
  return Buffer.alloc(+length)
}

Buffer.isBuffer = function isBuffer (b) {
  return b != null && b._isBuffer === true &&
    b !== Buffer.prototype // so Buffer.isBuffer(Buffer.prototype) will be false
}

Buffer.compare = function compare (a, b) {
  if (isInstance(a, Uint8Array)) a = Buffer.from(a, a.offset, a.byteLength)
  if (isInstance(b, Uint8Array)) b = Buffer.from(b, b.offset, b.byteLength)
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError(
      'The "buf1", "buf2" arguments must be one of type Buffer or Uint8Array'
    )
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i]
      y = b[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!Array.isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers')
  }

  if (list.length === 0) {
    return Buffer.alloc(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; ++i) {
      length += list[i].length
    }
  }

  var buffer = Buffer.allocUnsafe(length)
  var pos = 0
  for (i = 0; i < list.length; ++i) {
    var buf = list[i]
    if (isInstance(buf, Uint8Array)) {
      buf = Buffer.from(buf)
    }
    if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers')
    }
    buf.copy(buffer, pos)
    pos += buf.length
  }
  return buffer
}

function byteLength (string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length
  }
  if (ArrayBuffer.isView(string) || isInstance(string, ArrayBuffer)) {
    return string.byteLength
  }
  if (typeof string !== 'string') {
    throw new TypeError(
      'The "string" argument must be one of type string, Buffer, or ArrayBuffer. ' +
      'Received type ' + typeof string
    )
  }

  var len = string.length
  var mustMatch = (arguments.length > 2 && arguments[2] === true)
  if (!mustMatch && len === 0) return 0

  // Use a for loop to avoid recursion
  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len
      case 'utf8':
      case 'utf-8':
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) {
          return mustMatch ? -1 : utf8ToBytes(string).length // assume utf8
        }
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

function slowToString (encoding, start, end) {
  var loweredCase = false

  // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
  // property of a typed array.

  // This behaves neither like String nor Uint8Array in that we set start/end
  // to their upper/lower bounds if the value passed is out of range.
  // undefined is handled specially as per ECMA-262 6th Edition,
  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
  if (start === undefined || start < 0) {
    start = 0
  }
  // Return early if start > this.length. Done here to prevent potential uint32
  // coercion fail below.
  if (start > this.length) {
    return ''
  }

  if (end === undefined || end > this.length) {
    end = this.length
  }

  if (end <= 0) {
    return ''
  }

  // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
  end >>>= 0
  start >>>= 0

  if (end <= start) {
    return ''
  }

  if (!encoding) encoding = 'utf8'

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

// This property is used by `Buffer.isBuffer` (and the `is-buffer` npm package)
// to detect a Buffer instance. It's not possible to use `instanceof Buffer`
// reliably in a browserify context because there could be multiple different
// copies of the 'buffer' package in use. This method works even for Buffer
// instances that were created from another copy of the `buffer` package.
// See: https://github.com/feross/buffer/issues/154
Buffer.prototype._isBuffer = true

function swap (b, n, m) {
  var i = b[n]
  b[n] = b[m]
  b[m] = i
}

Buffer.prototype.swap16 = function swap16 () {
  var len = this.length
  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits')
  }
  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1)
  }
  return this
}

Buffer.prototype.swap32 = function swap32 () {
  var len = this.length
  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits')
  }
  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3)
    swap(this, i + 1, i + 2)
  }
  return this
}

Buffer.prototype.swap64 = function swap64 () {
  var len = this.length
  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits')
  }
  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7)
    swap(this, i + 1, i + 6)
    swap(this, i + 2, i + 5)
    swap(this, i + 3, i + 4)
  }
  return this
}

Buffer.prototype.toString = function toString () {
  var length = this.length
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.toLocaleString = Buffer.prototype.toString

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  str = this.toString('hex', 0, max).replace(/(.{2})/g, '$1 ').trim()
  if (this.length > max) str += ' ... '
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
  if (isInstance(target, Uint8Array)) {
    target = Buffer.from(target, target.offset, target.byteLength)
  }
  if (!Buffer.isBuffer(target)) {
    throw new TypeError(
      'The "target" argument must be one of type Buffer or Uint8Array. ' +
      'Received type ' + (typeof target)
    )
  }

  if (start === undefined) {
    start = 0
  }
  if (end === undefined) {
    end = target ? target.length : 0
  }
  if (thisStart === undefined) {
    thisStart = 0
  }
  if (thisEnd === undefined) {
    thisEnd = this.length
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index')
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0
  }
  if (thisStart >= thisEnd) {
    return -1
  }
  if (start >= end) {
    return 1
  }

  start >>>= 0
  end >>>= 0
  thisStart >>>= 0
  thisEnd >>>= 0

  if (this === target) return 0

  var x = thisEnd - thisStart
  var y = end - start
  var len = Math.min(x, y)

  var thisCopy = this.slice(thisStart, thisEnd)
  var targetCopy = target.slice(start, end)

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i]
      y = targetCopy[i]
      break
    }
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

// Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
//
// Arguments:
// - buffer - a Buffer to search
// - val - a string, Buffer, or number
// - byteOffset - an index into `buffer`; will be clamped to an int32
// - encoding - an optional encoding, relevant is val is a string
// - dir - true for indexOf, false for lastIndexOf
function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
  // Empty buffer means no match
  if (buffer.length === 0) return -1

  // Normalize byteOffset
  if (typeof byteOffset === 'string') {
    encoding = byteOffset
    byteOffset = 0
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000
  }
  byteOffset = +byteOffset // Coerce to Number.
  if (numberIsNaN(byteOffset)) {
    // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
    byteOffset = dir ? 0 : (buffer.length - 1)
  }

  // Normalize byteOffset: negative offsets start from the end of the buffer
  if (byteOffset < 0) byteOffset = buffer.length + byteOffset
  if (byteOffset >= buffer.length) {
    if (dir) return -1
    else byteOffset = buffer.length - 1
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0
    else return -1
  }

  // Normalize val
  if (typeof val === 'string') {
    val = Buffer.from(val, encoding)
  }

  // Finally, search either indexOf (if dir is true) or lastIndexOf
  if (Buffer.isBuffer(val)) {
    // Special case: looking for empty string/buffer always fails
    if (val.length === 0) {
      return -1
    }
    return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
  } else if (typeof val === 'number') {
    val = val & 0xFF // Search for a byte value [0-255]
    if (typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
      }
    }
    return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
  }

  throw new TypeError('val must be string, number or Buffer')
}

function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
  var indexSize = 1
  var arrLength = arr.length
  var valLength = val.length

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase()
    if (encoding === 'ucs2' || encoding === 'ucs-2' ||
        encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1
      }
      indexSize = 2
      arrLength /= 2
      valLength /= 2
      byteOffset /= 2
    }
  }

  function read (buf, i) {
    if (indexSize === 1) {
      return buf[i]
    } else {
      return buf.readUInt16BE(i * indexSize)
    }
  }

  var i
  if (dir) {
    var foundIndex = -1
    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
      } else {
        if (foundIndex !== -1) i -= i - foundIndex
        foundIndex = -1
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength
    for (i = byteOffset; i >= 0; i--) {
      var found = true
      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false
          break
        }
      }
      if (found) return i
    }
  }

  return -1
}

Buffer.prototype.includes = function includes (val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
}

Buffer.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  var strLen = string.length

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (numberIsNaN(parsed)) return i
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function latin1Write (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset >>> 0
    if (isFinite(length)) {
      length = length >>> 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  } else {
    throw new Error(
      'Buffer.write(string, encoding, offset[, length]) is no longer supported'
    )
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'latin1':
      case 'binary':
        return latin1Write(this, string, offset, length)

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
        : (firstByte > 0xBF) ? 2
          : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
  }

  // Decode in chunks to avoid "call stack size exceeded".
  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function latin1Slice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; ++i) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + (bytes[i + 1] * 256))
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf = this.subarray(start, end)
  // Return an augmented `Uint8Array` instance
  newBuf.__proto__ = Buffer.prototype
  return newBuf
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset >>> 0
  byteLength = byteLength >>> 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset >>> 0
  byteLength = byteLength >>> 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset >>> 0
  byteLength = byteLength >>> 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset >>> 0
  byteLength = byteLength >>> 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  offset = offset >>> 0
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset >>> 0
  byteLength = byteLength >>> 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset >>> 0
  byteLength = byteLength >>> 0
  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1
    checkInt(this, value, offset, byteLength, maxBytes, 0)
  }

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  this[offset] = (value & 0xff)
  this[offset + 1] = (value >>> 8)
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  this[offset] = (value >>> 8)
  this[offset + 1] = (value & 0xff)
  return offset + 2
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  this[offset + 3] = (value >>> 24)
  this[offset + 2] = (value >>> 16)
  this[offset + 1] = (value >>> 8)
  this[offset] = (value & 0xff)
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  this[offset] = (value >>> 24)
  this[offset + 1] = (value >>> 16)
  this[offset + 2] = (value >>> 8)
  this[offset + 3] = (value & 0xff)
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) {
    var limit = Math.pow(2, (8 * byteLength) - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) {
    var limit = Math.pow(2, (8 * byteLength) - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1
    }
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  this[offset] = (value & 0xff)
  this[offset + 1] = (value >>> 8)
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  this[offset] = (value >>> 8)
  this[offset + 1] = (value & 0xff)
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  this[offset] = (value & 0xff)
  this[offset + 1] = (value >>> 8)
  this[offset + 2] = (value >>> 16)
  this[offset + 3] = (value >>> 24)
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  this[offset] = (value >>> 24)
  this[offset + 1] = (value >>> 16)
  this[offset + 2] = (value >>> 8)
  this[offset + 3] = (value & 0xff)
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range')
  if (offset < 0) throw new RangeError('Index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!Buffer.isBuffer(target)) throw new TypeError('argument should be a Buffer')
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  // Copy 0 bytes; we're done
  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  // Fatal error conditions
  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('Index out of range')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start

  if (this === target && typeof Uint8Array.prototype.copyWithin === 'function') {
    // Use built-in when available, missing from IE11
    this.copyWithin(targetStart, start, end)
  } else if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (var i = len - 1; i >= 0; --i) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    Uint8Array.prototype.set.call(
      target,
      this.subarray(start, end),
      targetStart
    )
  }

  return len
}

// Usage:
//    buffer.fill(number[, offset[, end]])
//    buffer.fill(buffer[, offset[, end]])
//    buffer.fill(string[, offset[, end]][, encoding])
Buffer.prototype.fill = function fill (val, start, end, encoding) {
  // Handle string cases:
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start
      start = 0
      end = this.length
    } else if (typeof end === 'string') {
      encoding = end
      end = this.length
    }
    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string')
    }
    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding)
    }
    if (val.length === 1) {
      var code = val.charCodeAt(0)
      if ((encoding === 'utf8' && code < 128) ||
          encoding === 'latin1') {
        // Fast path: If `val` fits into a single byte, use that numeric value.
        val = code
      }
    }
  } else if (typeof val === 'number') {
    val = val & 255
  }

  // Invalid ranges are not set to a default, so can range check early.
  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index')
  }

  if (end <= start) {
    return this
  }

  start = start >>> 0
  end = end === undefined ? this.length : end >>> 0

  if (!val) val = 0

  var i
  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val
    }
  } else {
    var bytes = Buffer.isBuffer(val)
      ? val
      : Buffer.from(val, encoding)
    var len = bytes.length
    if (len === 0) {
      throw new TypeError('The value "' + val +
        '" is invalid for argument "value"')
    }
    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len]
    }
  }

  return this
}

// HELPER FUNCTIONS
// ================

var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g

function base64clean (str) {
  // Node takes equal signs as end of the Base64 encoding
  str = str.split('=')[0]
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = str.trim().replace(INVALID_BASE64_RE, '')
  // Node converts strings with length < 2 to ''
  if (str.length < 2) return ''
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i)

    // is surrogate component
    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        // valid lead
        leadSurrogate = codePoint

        continue
      }

      // 2 leads in a row
      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      // valid surrogate pair
      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    // encode utf8
    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; ++i) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

// ArrayBuffer or Uint8Array objects from other contexts (i.e. iframes) do not pass
// the `instanceof` check but they should be treated as of that type.
// See: https://github.com/feross/buffer/issues/166
function isInstance (obj, type) {
  return obj instanceof type ||
    (obj != null && obj.constructor != null && obj.constructor.name != null &&
      obj.constructor.name === type.name)
}
function numberIsNaN (obj) {
  // For IE11 support
  return obj !== obj // eslint-disable-line no-self-compare
}

}).call(this)}).call(this,require("buffer").Buffer)
},{"base64-js":74,"buffer":76,"ieee754":77}],77:[function(require,module,exports){
/*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = (nBytes * 8) - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = (e * 256) + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = (m * 256) + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = (nBytes * 8) - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = ((value * c) - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}

},{}],78:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

'use strict';

// If obj.hasOwnProperty has been overridden, then calling
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707
function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

module.exports = function(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);

  var maxKeys = 1000;
  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length;
  // maxKeys <= 0 means that we should not limit keys count
  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr, vstr, k, v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

},{}],79:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

'use strict';

var stringifyPrimitive = function(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

module.exports = function(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';
  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return map(objectKeys(obj), function(k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
      if (isArray(obj[k])) {
        return map(obj[k], function(v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);

  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq +
         encodeURIComponent(stringifyPrimitive(obj));
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

function map (xs, f) {
  if (xs.map) return xs.map(f);
  var res = [];
  for (var i = 0; i < xs.length; i++) {
    res.push(f(xs[i], i));
  }
  return res;
}

var objectKeys = Object.keys || function (obj) {
  var res = [];
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
  }
  return res;
};

},{}],80:[function(require,module,exports){
'use strict';

exports.decode = exports.parse = require('./decode');
exports.encode = exports.stringify = require('./encode');

},{"./decode":78,"./encode":79}]},{},[38])(38)
});
