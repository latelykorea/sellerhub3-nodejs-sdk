# SellerhubApi.UrgentMessageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUrgentMessage**](UrgentMessageApi.md#getUrgentMessage) | **GET** /shapi/v1/urgentMessages | 긴급 알림 메시지 목록
[**getUrgentMessageById**](UrgentMessageApi.md#getUrgentMessageById) | **GET** /shapi/v1/urgentMessages/{id} | 긴급 알림 메시지



## getUrgentMessage

> [UrgentMessage] getUrgentMessage(opts)

긴급 알림 메시지 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.UrgentMessageApi();
let opts = {
  'startDate': 20220101, // Date | 알람 등록 시작일
  'endDate': 20220101, // Date | 알람 등록 종료일
  'searchType': orderNumber, // String | 검색 타입
  'searchContents': , // String | 검색 내용
  'brandNumbers': [1,2,3], // [Number] | 브랜드 번호
  'alarmTypes': ["product"], // [String] | 알람 타입
  'isReceived': Y, // String | 수신 여부
  'isFinished': Y, // String | 종료 여부
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getUrgentMessage(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | [**Date**](.md)| 알람 등록 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 알람 등록 종료일 | [optional] 
 **searchType** | **String**| 검색 타입 | [optional] 
 **searchContents** | **String**| 검색 내용 | [optional] 
 **brandNumbers** | [**[Number]**](Number.md)| 브랜드 번호 | [optional] 
 **alarmTypes** | [**[String]**](String.md)| 알람 타입 | [optional] 
 **isReceived** | **String**| 수신 여부 | [optional] 
 **isFinished** | **String**| 종료 여부 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

[**[UrgentMessage]**](UrgentMessage.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getUrgentMessageById

> UrgentMessage getUrgentMessageById(id)

긴급 알림 메시지

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.UrgentMessageApi();
let id = 1; // Number | 알림 ID
apiInstance.getUrgentMessageById(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| 알림 ID | 

### Return type

[**UrgentMessage**](UrgentMessage.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

