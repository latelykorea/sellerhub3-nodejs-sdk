# SellerhubApi.CategoryApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCategory**](CategoryApi.md#getCategory) | **GET** /shapi/v1/categorys | 카테고리 목록
[**getCategoryByNo**](CategoryApi.md#getCategoryByNo) | **GET** /shapi/v1/categorys/{categoryNumber} | 카테고리 조회
[**getViolation**](CategoryApi.md#getViolation) | **GET** /shapi/v1/violations | 상품정보위반 목록
[**getViolationById**](CategoryApi.md#getViolationById) | **GET** /shapi/v1/violations/{violationId} | 상품정보위반 조회



## getCategory

> Object getCategory(opts)

카테고리 목록

카테고리 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CategoryApi();
let opts = {
  'categoryNumber': 56, // Number | 카테고리 번호
  'categoryName': "categoryName_example", // String | 카테고리 이름
  'categoryCode': "categoryCode_example", // String | 카테고리 코드
  'isDesigner': "isDesigner_example", // String | 디자이너 카테고리 여부
  'withSubCategory': true, // Boolean | 하위 카테고리 포함
  'maxDepth': 56, // Number | 최대 계층
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example" // String | 정렬 방향
};
apiInstance.getCategory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryNumber** | **Number**| 카테고리 번호 | [optional] 
 **categoryName** | **String**| 카테고리 이름 | [optional] 
 **categoryCode** | **String**| 카테고리 코드 | [optional] 
 **isDesigner** | **String**| 디자이너 카테고리 여부 | [optional] 
 **withSubCategory** | **Boolean**| 하위 카테고리 포함 | [optional] 
 **maxDepth** | **Number**| 최대 계층 | [optional] 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCategoryByNo

> Object getCategoryByNo(opts)

카테고리 조회

카테고리 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CategoryApi();
let opts = {
  'withSubCategory': true, // Boolean | 하위 카테고리 포함
  'maxDepth': 56 // Number | 최대 계층
};
apiInstance.getCategoryByNo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **withSubCategory** | **Boolean**| 하위 카테고리 포함 | [optional] 
 **maxDepth** | **Number**| 최대 계층 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getViolation

> Object getViolation(opts)

상품정보위반 목록

상품정보위반 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CategoryApi();
let opts = {
  'violationId': 56, // Number | 상품정보위반 ID
  'shoppingMallProductNumber': 56, // Number | 제휴몰 등록 상품 번호
  'shoppingMall': "shoppingMall_example", // String | 제휴몰
  'violationType': "violationType_example", // String | 위반 타입
  'violationStartDate': null, // Date | 위반일시 시작일자
  'violationEndDate': null, // Date | 위반일시 종료일자
  'registedViolationStartDate': null, // Date | 위반등록일시 시작일자
  'registedViolationEndDate': null, // Date | 위반등록일시 종료일자
  'updatedViolationStartDate': null, // Date | 위반수정일시 시작일자
  'updatedViolationEndDate': null, // Date | 위반수정일시 종료일자
  '_with': ["null"], // [String] | WITH Model
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getViolation(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **violationId** | **Number**| 상품정보위반 ID | [optional] 
 **shoppingMallProductNumber** | **Number**| 제휴몰 등록 상품 번호 | [optional] 
 **shoppingMall** | **String**| 제휴몰 | [optional] 
 **violationType** | **String**| 위반 타입 | [optional] 
 **violationStartDate** | [**Date**](.md)| 위반일시 시작일자 | [optional] 
 **violationEndDate** | [**Date**](.md)| 위반일시 종료일자 | [optional] 
 **registedViolationStartDate** | [**Date**](.md)| 위반등록일시 시작일자 | [optional] 
 **registedViolationEndDate** | [**Date**](.md)| 위반등록일시 종료일자 | [optional] 
 **updatedViolationStartDate** | [**Date**](.md)| 위반수정일시 시작일자 | [optional] 
 **updatedViolationEndDate** | [**Date**](.md)| 위반수정일시 종료일자 | [optional] 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getViolationById

> Object getViolationById(opts)

상품정보위반 조회

상품정보위반 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CategoryApi();
let opts = {
  '_with': ["null"] // [String] | WITH Model
};
apiInstance.getViolationById(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

