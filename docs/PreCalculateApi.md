# SellerhubApi.PreCalculateApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getApplicablePreCalculate**](PreCalculateApi.md#getApplicablePreCalculate) | **GET** /shapi/v1/preCalculate/applicable | 신청가능금액
[**getPreCalculate**](PreCalculateApi.md#getPreCalculate) | **GET** /shapi/v1/preCalculate | 빠른정산 목록
[**getPreCalculateRequest**](PreCalculateApi.md#getPreCalculateRequest) | **GET** /shapi/v1/preCalculate/request | 신청 목록



## getApplicablePreCalculate

> Object getApplicablePreCalculate()

신청가능금액

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.PreCalculateApi();
apiInstance.getApplicablePreCalculate((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPreCalculate

> Object getPreCalculate()

빠른정산 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.PreCalculateApi();
apiInstance.getPreCalculate((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPreCalculateRequest

> Object getPreCalculateRequest()

신청 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.PreCalculateApi();
apiInstance.getPreCalculateRequest((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

