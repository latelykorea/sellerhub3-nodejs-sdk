# SellerhubApi.DeliveryApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProviderDelivery**](DeliveryApi.md#createProviderDelivery) | **POST** /shapi/v1/delivery/provider | 공급사의 택배사 정보 추가
[**deleteProviderDeliveryByNo**](DeliveryApi.md#deleteProviderDeliveryByNo) | **DELETE** /shapi/v1/delivery/provider/{deliveryNumer} | 공급사가 사용중인 택배사 정보 삭제
[**getDelivery**](DeliveryApi.md#getDelivery) | **GET** /shapi/v1/delivery | 사용 택배사 조회
[**getProviderDelivery**](DeliveryApi.md#getProviderDelivery) | **GET** /shapi/v1/delivery/provider | 공급사가 사용중인 택배사 가져오기
[**updateProviderDelivery**](DeliveryApi.md#updateProviderDelivery) | **PUT** /shapi/v1/delivery/provider | 공급사가 사용중인 택배사 정보 수정



## createProviderDelivery

> ProviderDeliveryResource createProviderDelivery(brandNumber, deliveryCompanyNumber, isUsed, sort, opts)

공급사의 택배사 정보 추가

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.DeliveryApi();
let brandNumber = 56; // Number | 브랜드번호
let deliveryCompanyNumber = 56; // Number | 택배사 번호
let isUsed = "isUsed_example"; // String | 사용여부
let sort = 56; // Number | 순서
let opts = {
  'deliveryCompanyUrl': "deliveryCompanyUrl_example", // String | 택배사 주소
  'deliveryContractCode': "deliveryContractCode_example", // String | 택배사 계약번호
  'isAvaliableInvoiceLinking': "isAvaliableInvoiceLinking_example" // String | 송장연동 출력기능 사용여부
};
apiInstance.createProviderDelivery(brandNumber, deliveryCompanyNumber, isUsed, sort, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brandNumber** | **Number**| 브랜드번호 | 
 **deliveryCompanyNumber** | **Number**| 택배사 번호 | 
 **isUsed** | **String**| 사용여부 | 
 **sort** | **Number**| 순서 | 
 **deliveryCompanyUrl** | **String**| 택배사 주소 | [optional] 
 **deliveryContractCode** | **String**| 택배사 계약번호 | [optional] 
 **isAvaliableInvoiceLinking** | **String**| 송장연동 출력기능 사용여부 | [optional] 

### Return type

[**ProviderDeliveryResource**](ProviderDeliveryResource.md)

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## deleteProviderDeliveryByNo

> Object deleteProviderDeliveryByNo(UNKNOWN_PARAMETER_NAME)

공급사가 사용중인 택배사 정보 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DeliveryApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 공급사의 택배사번호
apiInstance.deleteProviderDeliveryByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 공급사의 택배사번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDelivery

> [DeliveryResource] getDelivery(opts)

사용 택배사 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DeliveryApi();
let opts = {
  'isUsed': "''" // String | 사용여부
};
apiInstance.getDelivery(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isUsed** | **String**| 사용여부 | [optional] [default to &#39;&#39;]

### Return type

[**[DeliveryResource]**](DeliveryResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderDelivery

> [ProviderDeliveryResource] getProviderDelivery(opts)

공급사가 사용중인 택배사 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DeliveryApi();
let opts = {
  'bramdId': 56, // Number | 브랜드 번호
  'deliveryCompanyNumber': 56, // Number | 택배사 번호
  'deliveryCompanyName': "deliveryCompanyName_example", // String | 택배사명
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getProviderDelivery(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bramdId** | **Number**| 브랜드 번호 | [optional] 
 **deliveryCompanyNumber** | **Number**| 택배사 번호 | [optional] 
 **deliveryCompanyName** | **String**| 택배사명 | [optional] 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

[**[ProviderDeliveryResource]**](ProviderDeliveryResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateProviderDelivery

> Object updateProviderDelivery(deliveryCompanyNumber, brandNumber, deliveryCompanyNumber2, isUsed, sort, opts)

공급사가 사용중인 택배사 정보 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DeliveryApi();
let deliveryCompanyNumber = 56; // Number | 공급사의 택배사번호
let brandNumber = 56; // Number | 브랜드번호
let deliveryCompanyNumber2 = 56; // Number | 택배사 번호
let isUsed = "isUsed_example"; // String | 사용여부
let sort = 56; // Number | 순서
let opts = {
  'deliveryCompanyUrl': "deliveryCompanyUrl_example", // String | 택배사 주소
  'deliveryContractCode': "deliveryContractCode_example", // String | 택배사 계약번호
  'isAvaliableInvoiceLinking': "isAvaliableInvoiceLinking_example" // String | 송장연동 출력기능 사용여부
};
apiInstance.updateProviderDelivery(deliveryCompanyNumber, brandNumber, deliveryCompanyNumber2, isUsed, sort, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deliveryCompanyNumber** | **Number**| 공급사의 택배사번호 | 
 **brandNumber** | **Number**| 브랜드번호 | 
 **deliveryCompanyNumber2** | **Number**| 택배사 번호 | 
 **isUsed** | **String**| 사용여부 | 
 **sort** | **Number**| 순서 | 
 **deliveryCompanyUrl** | **String**| 택배사 주소 | [optional] 
 **deliveryContractCode** | **String**| 택배사 계약번호 | [optional] 
 **isAvaliableInvoiceLinking** | **String**| 송장연동 출력기능 사용여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

