# SellerhubApi.CreateDetailInfoOfProviderRequestBrandDatasInnerInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandNumber** | **Number** | 브랜드번호 | [optional] 
**brandName** | **String** | 브랜드명 | 


