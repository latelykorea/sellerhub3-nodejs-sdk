# SellerhubApi.BoardNotice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**noticeNumber** | **Number** | 공급사 공지사항 번호 | [optional] 
**contentsType** | **String** | 글 타입 | [optional] 
**subject** | **String** | 제목 | [optional] 
**contents** | **String** | 내용 | [optional] 
**category** | **String** | 카테고리 | [optional] 
**popupLink** | **String** | 링크 | [optional] 
**isSecret** | **String** | 비밀글 여부 | [optional] 
**isNotice** | **String** | 공지 여부 | [optional] 
**isPopup** | **String** | 팝업 여부 | [optional] 
**isForcedToRead** | **String** | 필독 여부 | [optional] 
**boardStatus** | **String** | 게시물 상태 | [optional] 
**boardAfterStatus** | **String** | 게시기간 종료후 상태 | [optional] 
**priority** | **Number** | 우선순위 | [optional] 
**popupPosition** | **String** | 팝업 노출 위치 | [optional] 
**popupStyle** | **Object** | 팝업 스타일 | [optional] 
**isAvailableAgree** | **String** | 동의기능 사용 여부 | [optional] 
**isAvailableEventAgree** | **String** | 이벤트 동의 팝업 여부 | [optional] 
**eventAgreeItems** | **String** | 이벤트 동의 항목 | [optional] 
**eventAgreedText** | **String** | 이벤트 동의/미동의 내용 | [optional] 
**eventAgreeItemsFlag** | **String** | 이벤트 참여여부 | [optional] 
**originalFileName** | **String** | 원본 첨부 파일명 | [optional] 
**attachmentsFileName** | **String** | 첨부 파일명 | [optional] 
**creator** | **String** | 등록자 | [optional] 
**createdAt** | **Date** | 생성 일자 | [optional] 
**updator** | **String** | 수정자 | [optional] 
**updatedAt** | **Date** | 수정 일자 | [optional] 
**isSetPeriod** | **String** | 기간설정 여부 | [optional] 
**popupStartedAt** | **Date** | 팝업노출기간 - 시작 | [optional] 
**popupEndedAt** | **Date** | 팝업노출기간 - 종료 | [optional] 



## Enum: ContentsTypeEnum


* `promotion` (value: `"promotion"`)

* `etc` (value: `"etc"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsSecretEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsNoticeEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsPopupEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsForcedToReadEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: BoardStatusEnum


* `posting` (value: `"posting"`)

* `exit` (value: `"exit"`)

* `reservation` (value: `"reservation"`)

* `hide` (value: `"hide"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: BoardAfterStatusEnum


* `posting` (value: `"posting"`)

* `exit` (value: `"exit"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: PopupPositionEnum


* `all` (value: `"all"`)

* `dashboard` (value: `"dashboard"`)

* `productRegist` (value: `"productRegist"`)

* `productApprove` (value: `"productApprove"`)

* `calculate` (value: `"calculate"`)

* `order` (value: `"order"`)

* `board` (value: `"board"`)

* `linkage` (value: `"linkage"`)

* `statistics` (value: `"statistics"`)

* `event` (value: `"event"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsAvailableAgreeEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsAvailableEventAgreeEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: EventAgreeItemsFlagEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsSetPeriodEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




