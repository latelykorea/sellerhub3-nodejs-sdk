# SellerhubApi.OrderApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**call384c0bc85845dc729988acf253b757f2**](OrderApi.md#call384c0bc85845dc729988acf253b757f2) | **GET** /shapi/v1/order/{ordno} | 
[**getCancelOrder**](OrderApi.md#getCancelOrder) | **GET** /shapi/v1/order/list/cancel | 주문취소 리스트
[**getExchangeOrder**](OrderApi.md#getExchangeOrder) | **GET** /shapi/v1/order/list/exchnage | 교환접수/완료 리스트
[**getOrder**](OrderApi.md#getOrder) | **GET** /shapi/v1/order | 주문 리스트
[**getOrderCsMemoByNo**](OrderApi.md#getOrderCsMemoByNo) | **GET** /shapi/v1/order/csMemo/{ordno} | cs 메모
[**getProcessingOrder**](OrderApi.md#getProcessingOrder) | **GET** /shapi/v1/order/list/processing | 배송준비 리스트
[**getRefundOrder**](OrderApi.md#getRefundOrder) | **GET** /shapi/v1/order/list/refund | 반품접수/완료 리스트
[**updateOrderDeliveryStatus**](OrderApi.md#updateOrderDeliveryStatus) | **POST** /shapi/v1/order/deliveryStatus | 배송상태 변경
[**updateOrderStatus**](OrderApi.md#updateOrderStatus) | **POST** /shapi/v1/order/orderStatus | 주문상태 변경
[**updateOrderToRefund**](OrderApi.md#updateOrderToRefund) | **POST** /shapi/v1/order/refund | 반품처리



## call384c0bc85845dc729988acf253b757f2

> Object call384c0bc85845dc729988acf253b757f2(UNKNOWN_PARAMETER_NAME)



### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.OrderApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 주문번호
apiInstance.call384c0bc85845dc729988acf253b757f2(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문번호 | 

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCancelOrder

> Object getCancelOrder(opts)

주문취소 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'cancelType': null, // Array | 주문상태
  'settlekind': "''", // String | 결제방법
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getCancelOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **cancelType** | [**Array**](.md)| 주문상태 | [optional] 
 **settlekind** | **String**| 결제방법 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getExchangeOrder

> Object getExchangeOrder(opts)

교환접수/완료 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'exchangeType': "''", // String | 교환상태
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'cancelkey': "'all'", // String | 상태
  'exchange_flag': "'all'", // String | 교환상태
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getExchangeOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exchangeType** | **String**| 교환상태 | [optional] [default to &#39;&#39;]
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **cancelkey** | **String**| 상태 | [optional] [default to &#39;all&#39;]
 **exchange_flag** | **String**| 교환상태 | [optional] [default to &#39;all&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getOrder

> Object getOrder(opts)

주문 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'skey': "''", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'sgkey': "''", // String | 상품검색 옵션
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 상품검색 키워드
  'dtkind': "'orddt'", // String | 처리일자 (옵션)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'step2false': "''", // String | 정상주문만 조회
  'step': "''", // String | 주문상태
  'step2': "''", // String | 주문상태
  'exchange_flag': "''", // String | 주문상태
  'bundle_deli_cnt': "''", // String | 주문상태
  'saleType': "''", // String | 판매처
  'order_delayed': "''", // String | 배송지연
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **sgkey** | **String**| 상품검색 옵션 | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 상품검색 키워드 | [optional] 
 **dtkind** | **String**| 처리일자 (옵션) | [optional] [default to &#39;orddt&#39;]
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **step2false** | **String**| 정상주문만 조회 | [optional] [default to &#39;&#39;]
 **step** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **step2** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **exchange_flag** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **bundle_deli_cnt** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **saleType** | **String**| 판매처 | [optional] [default to &#39;&#39;]
 **order_delayed** | **String**| 배송지연 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getOrderCsMemoByNo

> Object getOrderCsMemoByNo(UNKNOWN_PARAMETER_NAME)

cs 메모

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 주문번호
apiInstance.getOrderCsMemoByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProcessingOrder

> Object getProcessingOrder(opts)

배송준비 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'dtkind': "'orddt'", // String | 처리일자 (옵션)
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 처리일자 (키워드)
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 배송준비상태
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getProcessingOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **dtkind** | **String**| 처리일자 (옵션) | [optional] [default to &#39;orddt&#39;]
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 처리일자 (키워드) | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 배송준비상태 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRefundOrder

> Object getRefundOrder(opts)

반품접수/완료 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'dtkind': "'regdt'", // String | 반품요청일 (옵션)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'cancelstatus': null, // Array | 상태
  'cls': "''", // String | 반품완료 여부
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getRefundOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **dtkind** | **String**| 반품요청일 (옵션) | [optional] [default to &#39;regdt&#39;]
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **cancelstatus** | [**Array**](.md)| 상태 | [optional] 
 **cls** | **String**| 반품완료 여부 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateOrderDeliveryStatus

> Object updateOrderDeliveryStatus(opts)

배송상태 변경

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 아이템 sno
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 현재 istep
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 현재 istep2
  'UNKNOWN_PARAMETER_NAME4': new SellerhubApi.null(), //  | 발송예정일
  'UNKNOWN_PARAMETER_NAME5': new SellerhubApi.null(), //  | 발송지연 예정일
  'rstatus': "''" // String | 배송상태 변경값
};
apiInstance.updateOrderDeliveryStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 아이템 sno | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 현재 istep | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 현재 istep2 | [optional] 
 **UNKNOWN_PARAMETER_NAME4** | [****](.md)| 발송예정일 | [optional] 
 **UNKNOWN_PARAMETER_NAME5** | [****](.md)| 발송지연 예정일 | [optional] 
 **rstatus** | **String**| 배송상태 변경값 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateOrderStatus

> Object updateOrderStatus(opts)

주문상태 변경

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문상태값
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 아이템 sno
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 현재 istep
  'UNKNOWN_PARAMETER_NAME4': new SellerhubApi.null(), //  | 현재 istep2
  'UNKNOWN_PARAMETER_NAME5': new SellerhubApi.null(), //  | 발송예정일
  'UNKNOWN_PARAMETER_NAME6': new SellerhubApi.null(), //  | 발송지연 예정일
  'rstatus': "''" // String | 배송상태 변경값
};
apiInstance.updateOrderStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문상태값 | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 아이템 sno | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 현재 istep | [optional] 
 **UNKNOWN_PARAMETER_NAME4** | [****](.md)| 현재 istep2 | [optional] 
 **UNKNOWN_PARAMETER_NAME5** | [****](.md)| 발송예정일 | [optional] 
 **UNKNOWN_PARAMETER_NAME6** | [****](.md)| 발송지연 예정일 | [optional] 
 **rstatus** | **String**| 배송상태 변경값 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateOrderToRefund

> Object updateOrderToRefund(opts)

반품처리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.OrderApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문취소sno
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 반품택배사
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null() //  | 반품택배 코드
};
apiInstance.updateOrderToRefund(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문취소sno | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 반품택배사 | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 반품택배 코드 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

