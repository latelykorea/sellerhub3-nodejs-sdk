# SellerhubApi.AddressApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAddress**](AddressApi.md#getAddress) | **GET** /shapi/v1/address/search | 주소조회



## getAddress

> Object getAddress(keyword, opts)

주소조회

주소조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.AddressApi();
let keyword = "keyword_example"; // String | 검색어
let opts = {
  'currentPage': 56, // Number | 현재 페이지
  'countPerPage': 56, // Number | 페이지당 검색
  'hstryYn': "'N'", // String | 변동된 주소정보 포함 여부
  'firstSort': "'none'", // String | 정확도순 정렬
  'addInfoYn': "'N'" // String | 출력결과에 추가된 항목
};
apiInstance.getAddress(keyword, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyword** | **String**| 검색어 | 
 **currentPage** | **Number**| 현재 페이지 | [optional] 
 **countPerPage** | **Number**| 페이지당 검색 | [optional] 
 **hstryYn** | **String**| 변동된 주소정보 포함 여부 | [optional] [default to &#39;N&#39;]
 **firstSort** | **String**| 정확도순 정렬 | [optional] [default to &#39;none&#39;]
 **addInfoYn** | **String**| 출력결과에 추가된 항목 | [optional] [default to &#39;N&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

