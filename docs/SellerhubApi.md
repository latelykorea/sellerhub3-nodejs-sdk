# SellerhubApi.SellerhubApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**call384c0bc85845dc729988acf253b757f2**](SellerhubApi.md#call384c0bc85845dc729988acf253b757f2) | **GET** /shapi/v1/order/{ordno} | 
[**createBoard**](SellerhubApi.md#createBoard) | **POST** /shapi/v1/board | 판매자지원 게시물 등록
[**createBoardReply**](SellerhubApi.md#createBoardReply) | **POST** /shapi/v1/board/{boardNumber}/reply | 판매자지원 게시물 답변 등록
[**createMarketingEvent**](SellerhubApi.md#createMarketingEvent) | **POST** /shapi/v1/marketing/event | 행사신청 게시물 등록
[**createMarketingEventProposalRequestByNo**](SellerhubApi.md#createMarketingEventProposalRequestByNo) | **POST** /shapi/v1/marketing/eventProposal/request/{eventProposalNumber} | 행사제안 신청
[**createMarketingEventReplyByNo**](SellerhubApi.md#createMarketingEventReplyByNo) | **PUT** /shapi/v1/marketing/event/reply/{eventNumber} | 행사신청 게시물 댓글 등록
[**createMarketingPromotioon**](SellerhubApi.md#createMarketingPromotioon) | **POST** /shapi/v1/marketing/promotion | 마케팅 프로모션 동의
[**createProviderBrand**](SellerhubApi.md#createProviderBrand) | **POST** /shapi/v1/brands/provider | 공급사 브랜드 추가하기
[**createToken**](SellerhubApi.md#createToken) | **POST** /shapi/v1/token | 토큰 생성
[**deleteBoardByNo**](SellerhubApi.md#deleteBoardByNo) | **DELETE** /shapi/v1/board/{boardNumber} | 판매자지원 게시물 삭제
[**deleteBoardReplyByNo**](SellerhubApi.md#deleteBoardReplyByNo) | **DELETE** /shapi/v1/board/reply/{replyNumber} | 판매자지원 게시물 답변 삭제
[**deleteMarketingEventByNo**](SellerhubApi.md#deleteMarketingEventByNo) | **DELETE** /shapi/v1/marketing/event/{eventNumber} | 행사신청 게시물 삭제
[**deleteProviderBrandByNo**](SellerhubApi.md#deleteProviderBrandByNo) | **DELETE** /shapi/v1/brands/provider/{brandNumber} | 공급사 브랜드 삭제하기
[**getAddress**](SellerhubApi.md#getAddress) | **GET** /shapi/v1/address/search | 주소조회
[**getBoard**](SellerhubApi.md#getBoard) | **GET** /shapi/v1/board | 공급사 판매자지원 조회
[**getBoardReply**](SellerhubApi.md#getBoardReply) | **GET** /shapi/v1/board/{boardNumber}/reply | 판매자지원 게시물 답변 조회
[**getBrandStatistics**](SellerhubApi.md#getBrandStatistics) | **GET** /shapi/v1/statistics/brand | 브랜드별 매출 통계
[**getCancelOrder**](SellerhubApi.md#getCancelOrder) | **GET** /shapi/v1/order/list/cancel | 주문취소 리스트
[**getCode**](SellerhubApi.md#getCode) | **GET** /shapi/v1/codes | 그룹코드 조회
[**getCodeByGroupCd**](SellerhubApi.md#getCodeByGroupCd) | **GET** /shapi/v1/codes/{groupCd} | 그룹코드 조회
[**getCsQna**](SellerhubApi.md#getCsQna) | **GET** /shapi/v1/csQna | cs 리스트
[**getCsQnaByNo**](SellerhubApi.md#getCsQnaByNo) | **GET** /shapi/v1/csQna/{csNumber} | CS문의 상세내역 조회
[**getExchangeOrder**](SellerhubApi.md#getExchangeOrder) | **GET** /shapi/v1/order/list/exchnage | 교환접수/완료 리스트
[**getFaqBoard**](SellerhubApi.md#getFaqBoard) | **GET** /shapi/v1/board/faq | FAQ 조회
[**getForbiddenWords**](SellerhubApi.md#getForbiddenWords) | **GET** /shapi/v1/forbiddenWords | 금칙어 목록
[**getMarketingEvent**](SellerhubApi.md#getMarketingEvent) | **GET** /shapi/v1/marketing/event | 행사신청 목록 조회
[**getMarketingEventApprovalByNo**](SellerhubApi.md#getMarketingEventApprovalByNo) | **PUT** /shapi/v1/marketing/event/approval/{eventNumber} | 행사신청 재신청
[**getMarketingEventByNo**](SellerhubApi.md#getMarketingEventByNo) | **GET** /shapi/v1/marketing/event/{eventNumber} | 행사신청 게시물
[**getMarketingEventProposal**](SellerhubApi.md#getMarketingEventProposal) | **GET** /shapi/v1/marketing/eventProposal | 행사제안 목록 조회
[**getMarketingEventProposalByNo**](SellerhubApi.md#getMarketingEventProposalByNo) | **GET** /shapi/v1/marketing/eventProposal/{eventProposalNumber} | 행사제안 내역
[**getMarketingEventProposalRequestByNo**](SellerhubApi.md#getMarketingEventProposalRequestByNo) | **GET** /shapi/v1/marketing/eventProposal/request/{eventNumber} | 행사제안 신청내역
[**getMarketingEventReplyByNo**](SellerhubApi.md#getMarketingEventReplyByNo) | **GET** /shapi/v1/marketing/event/reply/{eventNumber} | 행사신청 게시물 댓글 목록 조회
[**getMarketingPromotioon**](SellerhubApi.md#getMarketingPromotioon) | **GET** /shapi/v1/marketing/promotion | 마케팅 프로모션 리스트
[**getOrder**](SellerhubApi.md#getOrder) | **GET** /shapi/v1/order | 주문 리스트
[**getOrderCsMemoByNo**](SellerhubApi.md#getOrderCsMemoByNo) | **GET** /shapi/v1/order/csMemo/{ordno} | cs 메모
[**getPopularCategoryStatistics**](SellerhubApi.md#getPopularCategoryStatistics) | **GET** /shapi/v1/statistics/popularCategory | 인기 카테고리 분석
[**getPopularProductStatistics**](SellerhubApi.md#getPopularProductStatistics) | **GET** /shapi/v1/statistics/popularGoods | 인기 상품 분석
[**getProcessingOrder**](SellerhubApi.md#getProcessingOrder) | **GET** /shapi/v1/order/list/processing | 배송준비 리스트
[**getProviderBrand**](SellerhubApi.md#getProviderBrand) | **GET** /shapi/v1/brands/provider | 공급사 브랜드 목록 가져오기
[**getProviderBrandByNo**](SellerhubApi.md#getProviderBrandByNo) | **GET** /shapi/v1/brands/provider/{$brandNumber} | 공급사 특정 브랜드 가져오기
[**getRefundOrder**](SellerhubApi.md#getRefundOrder) | **GET** /shapi/v1/order/list/refund | 반품접수/완료 리스트
[**getStatistics**](SellerhubApi.md#getStatistics) | **GET** /shapi/v1/statistics | 매출 통계 리스트
[**updateBoard**](SellerhubApi.md#updateBoard) | **PUT** /shapi/v1/board | 판매자지원 게시물 수정
[**updateBoardReply**](SellerhubApi.md#updateBoardReply) | **PUT** /shapi/v1/board/reply/{replyNumber} | 판매자지원 게시물 답변 수정
[**updateCsQna**](SellerhubApi.md#updateCsQna) | **PUT** /shapi/v1/csQna/{csNumber} | CS 답변 등록
[**updateMarketingEventByNo**](SellerhubApi.md#updateMarketingEventByNo) | **PUT** /shapi/v1/marketing/event/{eventNumber} | 행사신청 게시물 등록
[**updateMarketingEventProposalRequestByNo**](SellerhubApi.md#updateMarketingEventProposalRequestByNo) | **PUT** /shapi/v1/marketing/eventProposal/request/{eventNumber} | 행사제안 신청내용 재신청
[**updateOrderDeliveryStatus**](SellerhubApi.md#updateOrderDeliveryStatus) | **POST** /shapi/v1/order/deliveryStatus | 배송상태 변경
[**updateOrderStatus**](SellerhubApi.md#updateOrderStatus) | **POST** /shapi/v1/order/orderStatus | 주문상태 변경
[**updateOrderToRefund**](SellerhubApi.md#updateOrderToRefund) | **POST** /shapi/v1/order/refund | 반품처리
[**updateProviderBrand**](SellerhubApi.md#updateProviderBrand) | **PUT** /shapi/v1/brands/provider | 공급사 브랜드 변경하기



## call384c0bc85845dc729988acf253b757f2

> Object call384c0bc85845dc729988acf253b757f2(UNKNOWN_PARAMETER_NAME)



### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 주문번호
apiInstance.call384c0bc85845dc729988acf253b757f2(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문번호 | 

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createBoard

> Object createBoard(subject, contents, opts)

판매자지원 게시물 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.createBoard(subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createBoardReply

> Object createBoardReply(boardNumber, subject, contents, opts)

판매자지원 게시물 답변 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.createBoardReply(boardNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingEvent

> Object createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts)

행사신청 게시물 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let returnDeliveryCharge = 56; // Number | 반품배송비(편도)
let exchangeDeliveryCharge = 56; // Number | 교환배송비(왕복)
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **returnDeliveryCharge** | **Number**| 반품배송비(편도) | 
 **exchangeDeliveryCharge** | **Number**| 교환배송비(왕복) | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingEventProposalRequestByNo

> Object createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts)

행사제안 신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingEventReplyByNo

> Object createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents)

행사신청 게시물 댓글 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let contents = null; // Object | 댓글내용
apiInstance.createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **contents** | [**Object**](Object.md)| 댓글내용 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingPromotioon

> Object createMarketingPromotioon(opts)

마케팅 프로모션 동의

프로모션 동의

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'agreeNumber': 56, // Number | 프로모션 번호
  'agreeCheck': "agreeCheck_example" // String | 프로모션 동의 여부
};
apiInstance.createMarketingPromotioon(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agreeNumber** | **Number**| 프로모션 번호 | [optional] 
 **agreeCheck** | **String**| 프로모션 동의 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createProviderBrand

> Object createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2)

공급사 브랜드 추가하기

공급사 브랜드 추가하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드명
let UNKNOWN_PARAMETER_NAME2 = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드명 | 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createToken

> Object createToken(name, opts)

토큰 생성

토큰 생성

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let name = "name_example"; // String | 토큰명
let opts = {
  'permissions': ["null"] // [String] | 권한
};
apiInstance.createToken(name, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| 토큰명 | 
 **permissions** | [**[String]**](String.md)| 권한 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## deleteBoardByNo

> Object deleteBoardByNo(UNKNOWN_PARAMETER_NAME)

판매자지원 게시물 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 판매자지원 게시물 번호
apiInstance.deleteBoardByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 판매자지원 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteBoardReplyByNo

> Object deleteBoardReplyByNo(replyNumber)

판매자지원 게시물 답변 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let replyNumber = 56; // Number | 판매자지원 답변 번호
apiInstance.deleteBoardReplyByNo(replyNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **replyNumber** | **Number**| 판매자지원 답변 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteMarketingEventByNo

> Object deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME)

행사신청 게시물 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
apiInstance.deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteProviderBrandByNo

> Object deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME)

공급사 브랜드 삭제하기

공급사 브랜드 삭제하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAddress

> Object getAddress(keyword, opts)

주소조회

주소조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let keyword = "keyword_example"; // String | 검색어
let opts = {
  'currentPage': 56, // Number | 현재 페이지
  'countPerPage': 56, // Number | 페이지당 검색
  'hstryYn': "'N'", // String | 변동된 주소정보 포함 여부
  'firstSort': "'none'", // String | 정확도순 정렬
  'addInfoYn': "'N'" // String | 출력결과에 추가된 항목
};
apiInstance.getAddress(keyword, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyword** | **String**| 검색어 | 
 **currentPage** | **Number**| 현재 페이지 | [optional] 
 **countPerPage** | **Number**| 페이지당 검색 | [optional] 
 **hstryYn** | **String**| 변동된 주소정보 포함 여부 | [optional] [default to &#39;N&#39;]
 **firstSort** | **String**| 정확도순 정렬 | [optional] [default to &#39;none&#39;]
 **addInfoYn** | **String**| 출력결과에 추가된 항목 | [optional] [default to &#39;N&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getBoard

> Object getBoard(opts)

공급사 판매자지원 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'startDate': null, // Date | 시작일
  'endDate': null, // Date | 종료일
  'sortBy': "'none'", // String | 정렬
  'sortDirection': "'desc'", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] [default to &#39;none&#39;]
 **sortDirection** | **String**| 정렬 방향 | [optional] [default to &#39;desc&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getBoardReply

> Object getBoardReply(boardNumber, opts)

판매자지원 게시물 답변 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let opts = {
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getBoardReply(boardNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getBrandStatistics

> Object getBrandStatistics(opts)

브랜드별 매출 통계

브랜드 매출 통계

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'year': 56, // Number | 년
  'month': 56, // Number | 월
  'day': 56 // Number | 일
};
apiInstance.getBrandStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Number**| 년 | [optional] 
 **month** | **Number**| 월 | [optional] 
 **day** | **Number**| 일 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCancelOrder

> Object getCancelOrder(opts)

주문취소 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'cancelType': null, // Array | 주문상태
  'settlekind': "''", // String | 결제방법
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getCancelOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **cancelType** | [**Array**](.md)| 주문상태 | [optional] 
 **settlekind** | **String**| 결제방법 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCode

> [CodeResource] getCode()

그룹코드 조회

상위 그룹코드 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
apiInstance.getCode((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[CodeResource]**](CodeResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCodeByGroupCd

> [CodeResource] getCodeByGroupCd(groupCd)

그룹코드 조회

그룹코드로 코드 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let groupCd = "groupCd_example"; // String | 그룹코드
apiInstance.getCodeByGroupCd(groupCd, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupCd** | **String**| 그룹코드 | 

### Return type

[**[CodeResource]**](CodeResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCsQna

> Object getCsQna(opts)

cs 리스트

cs목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'dateType': "dateType_example", // String | 기간검색 옵션
  'startDate': null, // Date | 시작 일자
  'endDate': null, // Date | 종료 일자
  'status': 56, // Number | 처리상태
  'inquireType': "inquireType_example", // String | 문의유형
  'searchType': "searchType_example", // String | 상세검색 옵션
  'searchWord': "searchWord_example", // String | 검색어
  'sortBy': "sortBy_example", // String | 정렬 옵션
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getCsQna(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dateType** | **String**| 기간검색 옵션 | [optional] 
 **startDate** | [**Date**](.md)| 시작 일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료 일자 | [optional] 
 **status** | **Number**| 처리상태 | [optional] 
 **inquireType** | **String**| 문의유형 | [optional] 
 **searchType** | **String**| 상세검색 옵션 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **sortBy** | **String**| 정렬 옵션 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCsQnaByNo

> Object getCsQnaByNo(csNumber)

CS문의 상세내역 조회

CS문의 상세내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let csNumber = 56; // Number | cs문의 번호
apiInstance.getCsQnaByNo(csNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **csNumber** | **Number**| cs문의 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getExchangeOrder

> Object getExchangeOrder(opts)

교환접수/완료 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'exchangeType': "''", // String | 교환상태
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'cancelkey': "'all'", // String | 상태
  'exchange_flag': "'all'", // String | 교환상태
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getExchangeOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exchangeType** | **String**| 교환상태 | [optional] [default to &#39;&#39;]
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **cancelkey** | **String**| 상태 | [optional] [default to &#39;all&#39;]
 **exchange_flag** | **String**| 교환상태 | [optional] [default to &#39;all&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getFaqBoard

> Object getFaqBoard(opts)

FAQ 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'startDate': null, // Date | 시작일
  'endDate': null, // Date | 종료일
  'sortBy': "'none'", // String | 정렬
  'sortDirection': "'desc'", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getFaqBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] [default to &#39;none&#39;]
 **sortDirection** | **String**| 정렬 방향 | [optional] [default to &#39;desc&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getForbiddenWords

> Object getForbiddenWords(opts)

금칙어 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'categoryCode': "categoryCode_example", // String | 셀러허브 카테고리 코드
  'word': "word_example", // String | 검색 금칙어
  'isAppliedSubCategory': "isAppliedSubCategory_example", // String | 하위카테고리 적용여부
  '_with': ["null"], // [String] | WITH Model
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getForbiddenWords(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **word** | **String**| 검색 금칙어 | [optional] 
 **isAppliedSubCategory** | **String**| 하위카테고리 적용여부 | [optional] 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEvent

> Object getMarketingEvent(opts)

행사신청 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'isApprove': "'all'", // String | 승인 여부
  'isAnswer': "'all'", // String | 답변 여부
  'searchDateType': "'registed'", // String | 검색 일자 설정
  'startDate': 56, // Number | 시작일
  'endDate': 56, // Number | 종료일
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEvent(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isApprove** | **String**| 승인 여부 | [optional] [default to &#39;all&#39;]
 **isAnswer** | **String**| 답변 여부 | [optional] [default to &#39;all&#39;]
 **searchDateType** | **String**| 검색 일자 설정 | [optional] [default to &#39;registed&#39;]
 **startDate** | **Number**| 시작일 | [optional] 
 **endDate** | **Number**| 종료일 | [optional] 
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventApprovalByNo

> Object getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME)

행사신청 재신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
apiInstance.getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventByNo

> Object getMarketingEventByNo(UNKNOWN_PARAMETER_NAME)

행사신청 게시물

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
apiInstance.getMarketingEventByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposal

> Object getMarketingEventProposal(opts)

행사제안 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'channel': ["null"], // [String] | 행사 채널
  'searchDateType': "'startDate'", // String | 검색 일자 설정
  'startDate': 56, // Number | 시작일
  'endDate': 56, // Number | 종료일
  'category': ["null"], // [String] | 카테고리
  'eventStatus': "'all'", // String | 행사진행여부
  'requestStatus': "'all'", // String | 행사신청여부
  'isApprove': "'all'", // String | 답변여부
  'searchKey': "'title'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEventProposal(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel** | [**[String]**](String.md)| 행사 채널 | [optional] 
 **searchDateType** | **String**| 검색 일자 설정 | [optional] [default to &#39;startDate&#39;]
 **startDate** | **Number**| 시작일 | [optional] 
 **endDate** | **Number**| 종료일 | [optional] 
 **category** | [**[String]**](String.md)| 카테고리 | [optional] 
 **eventStatus** | **String**| 행사진행여부 | [optional] [default to &#39;all&#39;]
 **requestStatus** | **String**| 행사신청여부 | [optional] [default to &#39;all&#39;]
 **isApprove** | **String**| 답변여부 | [optional] [default to &#39;all&#39;]
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;title&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposalByNo

> Object getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME)

행사제안 내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
apiInstance.getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposalRequestByNo

> Object getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME)

행사제안 신청내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
apiInstance.getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventReplyByNo

> Object getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts)

행사신청 게시물 댓글 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let opts = {
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingPromotioon

> Object getMarketingPromotioon(opts)

마케팅 프로모션 리스트

프로모션목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'startDate': null, // Date | 시작 일자
  'endDate': null, // Date | 종료 일자
  'searchType': "searchType_example", // String | 검색 옵션
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getMarketingPromotioon(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | [**Date**](.md)| 시작 일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료 일자 | [optional] 
 **searchType** | **String**| 검색 옵션 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getOrder

> Object getOrder(opts)

주문 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'skey': "''", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'sgkey': "''", // String | 상품검색 옵션
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 상품검색 키워드
  'dtkind': "'orddt'", // String | 처리일자 (옵션)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'step2false': "''", // String | 정상주문만 조회
  'step': "''", // String | 주문상태
  'step2': "''", // String | 주문상태
  'exchange_flag': "''", // String | 주문상태
  'bundle_deli_cnt': "''", // String | 주문상태
  'saleType': "''", // String | 판매처
  'order_delayed': "''", // String | 배송지연
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **sgkey** | **String**| 상품검색 옵션 | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 상품검색 키워드 | [optional] 
 **dtkind** | **String**| 처리일자 (옵션) | [optional] [default to &#39;orddt&#39;]
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **step2false** | **String**| 정상주문만 조회 | [optional] [default to &#39;&#39;]
 **step** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **step2** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **exchange_flag** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **bundle_deli_cnt** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **saleType** | **String**| 판매처 | [optional] [default to &#39;&#39;]
 **order_delayed** | **String**| 배송지연 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getOrderCsMemoByNo

> Object getOrderCsMemoByNo(UNKNOWN_PARAMETER_NAME)

cs 메모

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 주문번호
apiInstance.getOrderCsMemoByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPopularCategoryStatistics

> Object getPopularCategoryStatistics(opts)

인기 카테고리 분석

인기 카테고리 분석

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'year': 56, // Number | 년
  'month': 56, // Number | 월
  'category': 56 // Number | 카테고리
};
apiInstance.getPopularCategoryStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Number**| 년 | [optional] 
 **month** | **Number**| 월 | [optional] 
 **category** | **Number**| 카테고리 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPopularProductStatistics

> Object getPopularProductStatistics(opts)

인기 상품 분석

인기 상품 분석

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'year': 56, // Number | 년
  'month': 56 // Number | 월
};
apiInstance.getPopularProductStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Number**| 년 | [optional] 
 **month** | **Number**| 월 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProcessingOrder

> Object getProcessingOrder(opts)

배송준비 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'dtkind': "'orddt'", // String | 처리일자 (옵션)
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 처리일자 (키워드)
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 배송준비상태
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getProcessingOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **dtkind** | **String**| 처리일자 (옵션) | [optional] [default to &#39;orddt&#39;]
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 처리일자 (키워드) | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 배송준비상태 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderBrand

> Object getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts)

공급사 브랜드 목록 가져오기

공급사 브랜드 목록 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
let UNKNOWN_PARAMETER_NAME2 = new SellerhubApi.null(); //  | 브랜드명
let opts = {
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 브랜드명 | 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderBrandByNo

> Object getProviderBrandByNo(UNKNOWN_PARAMETER_NAME)

공급사 특정 브랜드 가져오기

공급사 특정 브랜드 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.getProviderBrandByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getRefundOrder

> Object getRefundOrder(opts)

반품접수/완료 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'skey': "'all'", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'dtkind': "'regdt'", // String | 반품요청일 (옵션)
  'startDate': 2022-01-01, // Date | 시작 시간
  'endDate': 2022-01-01, // Date | 종료 시간
  'cancelstatus': null, // Array | 상태
  'cls': "''", // String | 반품완료 여부
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getRefundOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;all&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **dtkind** | **String**| 반품요청일 (옵션) | [optional] [default to &#39;regdt&#39;]
 **startDate** | [**Date**](.md)| 시작 시간 | [optional] 
 **endDate** | [**Date**](.md)| 종료 시간 | [optional] 
 **cancelstatus** | [**Array**](.md)| 상태 | [optional] 
 **cls** | **String**| 반품완료 여부 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getStatistics

> Object getStatistics(opts)

매출 통계 리스트

매출 통계 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 카테고리
  'orderStep': "orderStep_example", // String | 주문 상태
  'orderStep2': "orderStep2_example", // String | 주문 상태
  'brandNumber': 56, // Number | 브랜드 번호
  'goodsBrandNubmer': 56, // Number | 상품 브랜드 번호
  'searchType': "searchType_example", // String | 처리일자 (키워드)
  'startDate': null, // Date | 시작일자
  'endDate': null // Date | 종료일자
};
apiInstance.getStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 카테고리 | [optional] 
 **orderStep** | **String**| 주문 상태 | [optional] 
 **orderStep2** | **String**| 주문 상태 | [optional] 
 **brandNumber** | **Number**| 브랜드 번호 | [optional] 
 **goodsBrandNubmer** | **Number**| 상품 브랜드 번호 | [optional] 
 **searchType** | **String**| 처리일자 (키워드) | [optional] 
 **startDate** | [**Date**](.md)| 시작일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료일자 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateBoard

> Object updateBoard(boardNumber, subject, contents, opts)

판매자지원 게시물 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.updateBoard(boardNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## updateBoardReply

> Object updateBoardReply(replyNumber, subject, contents, opts)

판매자지원 게시물 답변 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let replyNumber = 56; // Number | 판매자지원 답변 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.updateBoardReply(replyNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **replyNumber** | **Number**| 판매자지원 답변 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## updateCsQna

> Object updateCsQna(csNumber)

CS 답변 등록

CS 답변 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let csNumber = 56; // Number | cs문의 번호
apiInstance.updateCsQna(csNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **csNumber** | **Number**| cs문의 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateMarketingEventByNo

> Object updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts)

행사신청 게시물 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let returnDeliveryCharge = 56; // Number | 반품배송비(편도)
let exchangeDeliveryCharge = 56; // Number | 교환배송비(왕복)
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **returnDeliveryCharge** | **Number**| 반품배송비(편도) | 
 **exchangeDeliveryCharge** | **Number**| 교환배송비(왕복) | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## updateMarketingEventProposalRequestByNo

> Object updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts)

행사제안 신청내용 재신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 신청 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 신청 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## updateOrderDeliveryStatus

> Object updateOrderDeliveryStatus(opts)

배송상태 변경

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 아이템 sno
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 현재 istep
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 현재 istep2
  'UNKNOWN_PARAMETER_NAME4': new SellerhubApi.null(), //  | 발송예정일
  'UNKNOWN_PARAMETER_NAME5': new SellerhubApi.null(), //  | 발송지연 예정일
  'rstatus': "''" // String | 배송상태 변경값
};
apiInstance.updateOrderDeliveryStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 아이템 sno | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 현재 istep | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 현재 istep2 | [optional] 
 **UNKNOWN_PARAMETER_NAME4** | [****](.md)| 발송예정일 | [optional] 
 **UNKNOWN_PARAMETER_NAME5** | [****](.md)| 발송지연 예정일 | [optional] 
 **rstatus** | **String**| 배송상태 변경값 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateOrderStatus

> Object updateOrderStatus(opts)

주문상태 변경

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문상태값
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 아이템 sno
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 현재 istep
  'UNKNOWN_PARAMETER_NAME4': new SellerhubApi.null(), //  | 현재 istep2
  'UNKNOWN_PARAMETER_NAME5': new SellerhubApi.null(), //  | 발송예정일
  'UNKNOWN_PARAMETER_NAME6': new SellerhubApi.null(), //  | 발송지연 예정일
  'rstatus': "''" // String | 배송상태 변경값
};
apiInstance.updateOrderStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문상태값 | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 아이템 sno | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 현재 istep | [optional] 
 **UNKNOWN_PARAMETER_NAME4** | [****](.md)| 현재 istep2 | [optional] 
 **UNKNOWN_PARAMETER_NAME5** | [****](.md)| 발송예정일 | [optional] 
 **UNKNOWN_PARAMETER_NAME6** | [****](.md)| 발송지연 예정일 | [optional] 
 **rstatus** | **String**| 배송상태 변경값 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateOrderToRefund

> Object updateOrderToRefund(opts)

반품처리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문취소sno
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 반품택배사
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null() //  | 반품택배 코드
};
apiInstance.updateOrderToRefund(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문취소sno | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 반품택배사 | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 반품택배 코드 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateProviderBrand

> Object updateProviderBrand(brandDatas)

공급사 브랜드 변경하기

공급사 브랜드 변경하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.SellerhubApi();
let brandDatas = [new SellerhubApi.UpdateProviderBrandBrandDatasParameterInner()]; // [UpdateProviderBrandBrandDatasParameterInner] | 브랜드 데이터
apiInstance.updateProviderBrand(brandDatas, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brandDatas** | [**[UpdateProviderBrandBrandDatasParameterInner]**](UpdateProviderBrandBrandDatasParameterInner.md)| 브랜드 데이터 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

