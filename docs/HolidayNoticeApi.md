# SellerhubApi.HolidayNoticeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createHolidayNotice**](HolidayNoticeApi.md#createHolidayNotice) | **POST** /shapi/v1/holidayNotice | 휴무 공지 등록
[**deleteHolidayNoticeById**](HolidayNoticeApi.md#deleteHolidayNoticeById) | **DELETE** /shapi/v1/holidayNotice/{id} | 휴무 공지 삭제
[**getHolidayNotice**](HolidayNoticeApi.md#getHolidayNotice) | **GET** /shapi/v1/holidayNotice | 휴무 공지 목록
[**getHolidayNoticeById**](HolidayNoticeApi.md#getHolidayNoticeById) | **GET** /shapi/v1/holidayNotice/{id} | 휴무 공지
[**updateHolidayNoticeById**](HolidayNoticeApi.md#updateHolidayNoticeById) | **PUT** /shapi/v1/holidayNotice/{id} | 휴무 공지 수정



## createHolidayNotice

> Object createHolidayNotice(title, startDate, endDate, orderDeadLineDate, deliveryStartDate, opts)

휴무 공지 등록

휴무 공지 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.HolidayNoticeApi();
let title = "title_example"; // String | 제목
let startDate = new Date("2013-10-20"); // Date | 휴무 시작일
let endDate = new Date("2013-10-20"); // Date | 휴무 종료일
let orderDeadLineDate = new Date("2013-10-20"); // Date | 주문 마감 일자
let deliveryStartDate = new Date("2013-10-20"); // Date | 배송 시작일
let opts = {
  'contents': "contents_example", // String | 내용
  'image': "image_example", // String | 휴무 공지 이미지 base64
  'isShow': "isShow_example" // String | 노출 여부
};
apiInstance.createHolidayNotice(title, startDate, endDate, orderDeadLineDate, deliveryStartDate, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **title** | **String**| 제목 | 
 **startDate** | **Date**| 휴무 시작일 | 
 **endDate** | **Date**| 휴무 종료일 | 
 **orderDeadLineDate** | **Date**| 주문 마감 일자 | 
 **deliveryStartDate** | **Date**| 배송 시작일 | 
 **contents** | **String**| 내용 | [optional] 
 **image** | **String**| 휴무 공지 이미지 base64 | [optional] 
 **isShow** | **String**| 노출 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## deleteHolidayNoticeById

> Object deleteHolidayNoticeById()

휴무 공지 삭제

휴무 공지 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.HolidayNoticeApi();
apiInstance.deleteHolidayNoticeById((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getHolidayNotice

> Object getHolidayNotice()

휴무 공지 목록

휴무 공지 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.HolidayNoticeApi();
apiInstance.getHolidayNotice((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getHolidayNoticeById

> Object getHolidayNoticeById()

휴무 공지

휴무 공지

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.HolidayNoticeApi();
apiInstance.getHolidayNoticeById((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateHolidayNoticeById

> Object updateHolidayNoticeById(opts)

휴무 공지 수정

휴무 공지 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.HolidayNoticeApi();
let opts = {
  'title': "title_example", // String | 제목
  'contents': "contents_example", // String | 내용
  'image': "image_example", // String | 휴무 공지 이미지 base64
  'isShow': "isShow_example" // String | 노출 여부
};
apiInstance.updateHolidayNoticeById(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **title** | **String**| 제목 | [optional] 
 **contents** | **String**| 내용 | [optional] 
 **image** | **String**| 휴무 공지 이미지 base64 | [optional] 
 **isShow** | **String**| 노출 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

