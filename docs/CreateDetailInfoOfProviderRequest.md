# SellerhubApi.CreateDetailInfoOfProviderRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bankName** | **String** | 은행명 | [optional] 
**account** | **String** | 예금주 | [optional] 
**accountHolder** | **String** | 계좌번호 | [optional] 
**bankbook** | **File** | 통장사본 | [optional] 
**businessCondition** | **String** | 업태 | [optional] 
**businessType** | **String** | 업종 | [optional] 
**taxPayerType** | **String** | 과세 타입 | [optional] 
**zipCode** | **String** | 우편번호 | [optional] 
**address** | **String** | 주소 | [optional] 
**detailAddress** | **String** | 상세주소 | [optional] 
**businessLicense** | **File** | 사업자 등록증 사본 | [optional] 
**isAutoDiscounthoppingMall** | **String** | 자동 할인율 적용여부 | [optional] 
**isAutoExposureNaverShopping** | **String** | 자동 네이버 지식쇼핑 노출여부 | [optional] 
**brandDatas** | **[[CreateDetailInfoOfProviderRequestBrandDatasInnerInner]]** | 브랜드 데이터 | [optional] 
**categoryCodes** | **[String]** | 카테고리 코드 | [optional] 
**companyLogo** | **File** | 회사 대표 이미지/로고 | [optional] 



## Enum: TaxPayerTypeEnum


* `general` (value: `"general"`)

* `simplified` (value: `"simplified"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsAutoDiscounthoppingMallEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsAutoExposureNaverShoppingEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




