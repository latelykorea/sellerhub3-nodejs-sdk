# SellerhubApi.BoardApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBoard**](BoardApi.md#createBoard) | **POST** /shapi/v1/board | 판매자지원 게시물 등록
[**createBoardReply**](BoardApi.md#createBoardReply) | **POST** /shapi/v1/board/{boardNumber}/reply | 판매자지원 게시물 답변 등록
[**deleteBoardByNo**](BoardApi.md#deleteBoardByNo) | **DELETE** /shapi/v1/board/{boardNumber} | 판매자지원 게시물 삭제
[**deleteBoardReplyByNo**](BoardApi.md#deleteBoardReplyByNo) | **DELETE** /shapi/v1/board/reply/{replyNumber} | 판매자지원 게시물 답변 삭제
[**getBoard**](BoardApi.md#getBoard) | **GET** /shapi/v1/board | 공급사 판매자지원 조회
[**getBoardReply**](BoardApi.md#getBoardReply) | **GET** /shapi/v1/board/{boardNumber}/reply | 판매자지원 게시물 답변 조회
[**getFaqBoard**](BoardApi.md#getFaqBoard) | **GET** /shapi/v1/board/faq | FAQ 조회
[**getManualBoard**](BoardApi.md#getManualBoard) | **GET** /shapi/v1/board/manuals | 공급사 매뉴얼 목록
[**getManualBoardByNo**](BoardApi.md#getManualBoardByNo) | **GET** /shapi/v1/board/manuals/{manualNumber} | 공급사 매뉴얼
[**getNoticeBoard**](BoardApi.md#getNoticeBoard) | **GET** /shapi/v1/board/notices | 공급사 공지사항 조회
[**getNoticeBoardByNo**](BoardApi.md#getNoticeBoardByNo) | **GET** /shapi/v1/board/notices/{noticeNumber} | 공급사 공지사항
[**updateBoard**](BoardApi.md#updateBoard) | **PUT** /shapi/v1/board | 판매자지원 게시물 수정
[**updateBoardReply**](BoardApi.md#updateBoardReply) | **PUT** /shapi/v1/board/reply/{replyNumber} | 판매자지원 게시물 답변 수정



## createBoard

> Object createBoard(subject, contents, opts)

판매자지원 게시물 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.createBoard(subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createBoardReply

> Object createBoardReply(boardNumber, subject, contents, opts)

판매자지원 게시물 답변 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.createBoardReply(boardNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## deleteBoardByNo

> Object deleteBoardByNo(UNKNOWN_PARAMETER_NAME)

판매자지원 게시물 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 판매자지원 게시물 번호
apiInstance.deleteBoardByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 판매자지원 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteBoardReplyByNo

> Object deleteBoardReplyByNo(replyNumber)

판매자지원 게시물 답변 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let replyNumber = 56; // Number | 판매자지원 답변 번호
apiInstance.deleteBoardReplyByNo(replyNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **replyNumber** | **Number**| 판매자지원 답변 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getBoard

> Object getBoard(opts)

공급사 판매자지원 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let opts = {
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'startDate': null, // Date | 시작일
  'endDate': null, // Date | 종료일
  'sortBy': "'none'", // String | 정렬
  'sortDirection': "'desc'", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] [default to &#39;none&#39;]
 **sortDirection** | **String**| 정렬 방향 | [optional] [default to &#39;desc&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getBoardReply

> Object getBoardReply(boardNumber, opts)

판매자지원 게시물 답변 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let opts = {
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getBoardReply(boardNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getFaqBoard

> Object getFaqBoard(opts)

FAQ 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let opts = {
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'startDate': null, // Date | 시작일
  'endDate': null, // Date | 종료일
  'sortBy': "'none'", // String | 정렬
  'sortDirection': "'desc'", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getFaqBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] [default to &#39;none&#39;]
 **sortDirection** | **String**| 정렬 방향 | [optional] [default to &#39;desc&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getManualBoard

> [BoardManual] getManualBoard(opts)

공급사 매뉴얼 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let opts = {
  'startDate': 20220101, // Date | 등록 시작일
  'endDate': 20220101, // Date | 등록 종료일
  'searchType': subject, // String | 검색 타입
  'searchContents': , // String | 검색 내용
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getManualBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | [**Date**](.md)| 등록 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 등록 종료일 | [optional] 
 **searchType** | **String**| 검색 타입 | [optional] 
 **searchContents** | **String**| 검색 내용 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

[**[BoardManual]**](BoardManual.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getManualBoardByNo

> BoardManual getManualBoardByNo(manualNumber)

공급사 매뉴얼

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let manualNumber = 1; // Number | 공급사 매뉴얼 번호
apiInstance.getManualBoardByNo(manualNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **manualNumber** | **Number**| 공급사 매뉴얼 번호 | 

### Return type

[**BoardManual**](BoardManual.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getNoticeBoard

> [BoardNotice] getNoticeBoard(opts)

공급사 공지사항 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let opts = {
  'searchKey': subject, // String | 검색키
  'searchWord': , // String | 검색어
  'startDate': 20220101, // Date | 시작일
  'endDate': 20220101, // Date | 종료일
  'isPopup': Y, // String | 팝업 여부
  'isNotice': Y, // String | 공지 여부
  'popupPositions': ["all"], // [String] | 팝업 노출 위치
  'boardStatus': ["posting"], // String | 공지 상태
  'isAgree': Y, // String | 동의 여부
  'isCurrentVisible': Y, // String | 현재 노출 여부
  'sortBy': registed, // String | 정렬
  'sortDirection': desc, // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getNoticeBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **isPopup** | **String**| 팝업 여부 | [optional] 
 **isNotice** | **String**| 공지 여부 | [optional] 
 **popupPositions** | [**[String]**](String.md)| 팝업 노출 위치 | [optional] 
 **boardStatus** | **String**| 공지 상태 | [optional] 
 **isAgree** | **String**| 동의 여부 | [optional] 
 **isCurrentVisible** | **String**| 현재 노출 여부 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

[**[BoardNotice]**](BoardNotice.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getNoticeBoardByNo

> BoardNotice getNoticeBoardByNo(noticeNumber)

공급사 공지사항

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let noticeNumber = 1; // Number | 공급사 공지사항 번호
apiInstance.getNoticeBoardByNo(noticeNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **noticeNumber** | **Number**| 공급사 공지사항 번호 | 

### Return type

[**BoardNotice**](BoardNotice.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateBoard

> Object updateBoard(boardNumber, subject, contents, opts)

판매자지원 게시물 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.updateBoard(boardNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## updateBoardReply

> Object updateBoardReply(replyNumber, subject, contents, opts)

판매자지원 게시물 답변 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BoardApi();
let replyNumber = 56; // Number | 판매자지원 답변 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.updateBoardReply(replyNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **replyNumber** | **Number**| 판매자지원 답변 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

