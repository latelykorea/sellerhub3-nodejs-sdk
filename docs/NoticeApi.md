# SellerhubApi.NoticeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNoticeBoard**](NoticeApi.md#getNoticeBoard) | **GET** /shapi/v1/board/notices | 공급사 공지사항 조회
[**getNoticeBoardByNo**](NoticeApi.md#getNoticeBoardByNo) | **GET** /shapi/v1/board/notices/{noticeNumber} | 공급사 공지사항



## getNoticeBoard

> [BoardNotice] getNoticeBoard(opts)

공급사 공지사항 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.NoticeApi();
let opts = {
  'searchKey': subject, // String | 검색키
  'searchWord': , // String | 검색어
  'startDate': 20220101, // Date | 시작일
  'endDate': 20220101, // Date | 종료일
  'isPopup': Y, // String | 팝업 여부
  'isNotice': Y, // String | 공지 여부
  'popupPositions': ["all"], // [String] | 팝업 노출 위치
  'boardStatus': ["posting"], // String | 공지 상태
  'isAgree': Y, // String | 동의 여부
  'isCurrentVisible': Y, // String | 현재 노출 여부
  'sortBy': registed, // String | 정렬
  'sortDirection': desc, // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getNoticeBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **isPopup** | **String**| 팝업 여부 | [optional] 
 **isNotice** | **String**| 공지 여부 | [optional] 
 **popupPositions** | [**[String]**](String.md)| 팝업 노출 위치 | [optional] 
 **boardStatus** | **String**| 공지 상태 | [optional] 
 **isAgree** | **String**| 동의 여부 | [optional] 
 **isCurrentVisible** | **String**| 현재 노출 여부 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

[**[BoardNotice]**](BoardNotice.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getNoticeBoardByNo

> BoardNotice getNoticeBoardByNo(noticeNumber)

공급사 공지사항

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.NoticeApi();
let noticeNumber = 1; // Number | 공급사 공지사항 번호
apiInstance.getNoticeBoardByNo(noticeNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **noticeNumber** | **Number**| 공급사 공지사항 번호 | 

### Return type

[**BoardNotice**](BoardNotice.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

