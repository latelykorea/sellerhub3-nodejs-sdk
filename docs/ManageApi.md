# SellerhubApi.ManageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDeliveryNotice**](ManageApi.md#createDeliveryNotice) | **POST** /shapi/v1/manages/deliveryNotice | 기본관리 - 배송공지 등록
[**createDetailInfoOfProvider**](ManageApi.md#createDetailInfoOfProvider) | **POST** /shapi/v1/manages/detailInfo | 기본관리 - 입점사 상세정보 관리
[**createSellerPlanOfProvider**](ManageApi.md#createSellerPlanOfProvider) | **POST** /shapi/v1/manages/sellerPlan | 기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청
[**createStaffInfoOfProvider**](ManageApi.md#createStaffInfoOfProvider) | **POST** /shapi/v1/manages/staff | 기본관리 - 담당자 정보 설정
[**deleteDeliveryNotice**](ManageApi.md#deleteDeliveryNotice) | **DELETE** /shapi/v1/manages/deliveryNotice | 기본관리 - 배송공지 삭제



## createDeliveryNotice

> Object createDeliveryNotice(image, expiredAt)

기본관리 - 배송공지 등록

기본관리 - 배송공지 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManageApi();
let image = "image_example"; // String | 배송 공지 이미지 base64
let expiredAt = new Date("2013-10-20"); // Date | 만기일자
apiInstance.createDeliveryNotice(image, expiredAt, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image** | **String**| 배송 공지 이미지 base64 | 
 **expiredAt** | **Date**| 만기일자 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createDetailInfoOfProvider

> Object createDetailInfoOfProvider(CreateDetailInfoOfProviderRequest)

기본관리 - 입점사 상세정보 관리

기본관리 - 입점사 상세정보 관리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManageApi();
let CreateDetailInfoOfProviderRequest = new SellerhubApi.CreateDetailInfoOfProviderRequest(); // CreateDetailInfoOfProviderRequest | 
apiInstance.createDetailInfoOfProvider(CreateDetailInfoOfProviderRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **CreateDetailInfoOfProviderRequest** | [**CreateDetailInfoOfProviderRequest**](CreateDetailInfoOfProviderRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## createSellerPlanOfProvider

> Object createSellerPlanOfProvider(opts)

기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청

기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManageApi();
let opts = {
  'planCode': "planCode_example", // String | 셀러허브 카테고리 코드
  'doPlanUpgrade': true // Boolean | 플랜 업그레이드 진행여부
};
apiInstance.createSellerPlanOfProvider(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **doPlanUpgrade** | **Boolean**| 플랜 업그레이드 진행여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createStaffInfoOfProvider

> Object createStaffInfoOfProvider(CreateStaffInfoOfProviderRequest)

기본관리 - 담당자 정보 설정

기본관리 - 담당자 정보 설정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManageApi();
let CreateStaffInfoOfProviderRequest = new SellerhubApi.CreateStaffInfoOfProviderRequest(); // CreateStaffInfoOfProviderRequest | 
apiInstance.createStaffInfoOfProvider(CreateStaffInfoOfProviderRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **CreateStaffInfoOfProviderRequest** | [**CreateStaffInfoOfProviderRequest**](CreateStaffInfoOfProviderRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## deleteDeliveryNotice

> Object deleteDeliveryNotice()

기본관리 - 배송공지 삭제

기본관리 - 배송공지 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManageApi();
apiInstance.deleteDeliveryNotice((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

