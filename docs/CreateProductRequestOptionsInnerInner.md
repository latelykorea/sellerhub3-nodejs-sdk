# SellerhubApi.CreateProductRequestOptionsInnerInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optionName1** | **String** |  | 
**optionName2** | **String** |  | [optional] 
**stock** | **Number** |  | 


