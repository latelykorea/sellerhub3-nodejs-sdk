# SellerhubApi.CreateProductRequestCertificationDatasInnerInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**url** | **String** |  | [optional] 
**attachment** | **Blob** |  | [optional] 
**memo** | **String** |  | [optional] 



## Enum: TypeEnum


* `url` (value: `"url"`)

* `base64` (value: `"base64"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




