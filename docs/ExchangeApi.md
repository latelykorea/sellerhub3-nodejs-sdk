# SellerhubApi.ExchangeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getExchangeOrder**](ExchangeApi.md#getExchangeOrder) | **GET** /shapi/v1/order/list/exchnage | 교환접수/완료 리스트



## getExchangeOrder

> Object getExchangeOrder(opts)

교환접수/완료 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ExchangeApi();
let opts = {
  'exchangeType': "''", // String | 교환상태
  'skey': "''", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 교환요청일 (키워드)
  'cancelkey': "''", // String | 상태
  'exchangeFlag': "''", // String | 교환상태
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getExchangeOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **exchangeType** | **String**| 교환상태 | [optional] [default to &#39;&#39;]
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 교환요청일 (키워드) | [optional] 
 **cancelkey** | **String**| 상태 | [optional] [default to &#39;&#39;]
 **exchangeFlag** | **String**| 교환상태 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

