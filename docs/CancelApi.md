# SellerhubApi.CancelApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCancelOrder**](CancelApi.md#getCancelOrder) | **GET** /shapi/v1/order/list/cancel | 주문취소 리스트



## getCancelOrder

> Object getCancelOrder(opts)

주문취소 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CancelApi();
let opts = {
  'skey': "''", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 주문일 (키워드)
  'cancelType': "''", // String | 주문상태
  'settlekind': "''", // String | 결제방법
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getCancelOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 주문일 (키워드) | [optional] 
 **cancelType** | **String**| 주문상태 | [optional] [default to &#39;&#39;]
 **settlekind** | **String**| 결제방법 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

