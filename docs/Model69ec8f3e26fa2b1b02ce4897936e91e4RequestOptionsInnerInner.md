# SellerhubApi.Model69ec8f3e26fa2b1b02ce4897936e91e4RequestOptionsInnerInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**optionNumber** | **Number** |  | [optional] 
**optionName1** | **String** |  | 
**optionName2** | **String** |  | [optional] 
**stock** | **Number** |  | 


