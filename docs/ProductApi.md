# SellerhubApi.ProductApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**a8a15b67230f3e43ed1bb29ee377c809**](ProductApi.md#a8a15b67230f3e43ed1bb29ee377c809) | **PUT** /shapi/v1/products/{productNumber}/price | 상품 가격 수정
[**call219f3041d050108b1a2e51838473f6d8**](ProductApi.md#call219f3041d050108b1a2e51838473f6d8) | **PUT** /shapi/v1/products/{productNumber}/runout | 상품 품절 처리
[**call69ec8f3e26fa2b1b02ce4897936e91e4**](ProductApi.md#call69ec8f3e26fa2b1b02ce4897936e91e4) | **PUT** /shapi/v1/products/{productNumber}/stock | 상품 재고 수정
[**createProduct**](ProductApi.md#createProduct) | **POST** /shapi/v1/products | 상품 등록
[**getForbiddenWords**](ProductApi.md#getForbiddenWords) | **GET** /shapi/v1/forbiddenWords | 금칙어 목록
[**getProduct**](ProductApi.md#getProduct) | **GET** /shapi/v1/products | 상품 목록
[**getProductByNo**](ProductApi.md#getProductByNo) | **GET** /shapi/v1/products/{productNumber} | 상품 조회
[**getProductByTarget**](ProductApi.md#getProductByTarget) | **GET** /shapi/v1/products/target/{target} | 특정 상품 목록
[**updateProductByNo**](ProductApi.md#updateProductByNo) | **POST** /shapi/v1/products/{productNumber} | 상품 수정



## a8a15b67230f3e43ed1bb29ee377c809

> Object a8a15b67230f3e43ed1bb29ee377c809(productNumber, A8a15b67230f3e43ed1bb29ee377c809Request)

상품 가격 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let productNumber = 56; // Number | 셀러허브 상품 일련번호
let A8a15b67230f3e43ed1bb29ee377c809Request = new SellerhubApi.A8a15b67230f3e43ed1bb29ee377c809Request(); // A8a15b67230f3e43ed1bb29ee377c809Request | 
apiInstance.a8a15b67230f3e43ed1bb29ee377c809(productNumber, A8a15b67230f3e43ed1bb29ee377c809Request, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productNumber** | **Number**| 셀러허브 상품 일련번호 | 
 **A8a15b67230f3e43ed1bb29ee377c809Request** | [**A8a15b67230f3e43ed1bb29ee377c809Request**](A8a15b67230f3e43ed1bb29ee377c809Request.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## call219f3041d050108b1a2e51838473f6d8

> Object call219f3041d050108b1a2e51838473f6d8(productNumber, Model219f3041d050108b1a2e51838473f6d8Request)

상품 품절 처리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let productNumber = 56; // Number | 셀러허브 상품 일련번호
let Model219f3041d050108b1a2e51838473f6d8Request = new SellerhubApi.Model219f3041d050108b1a2e51838473f6d8Request(); // Model219f3041d050108b1a2e51838473f6d8Request | 
apiInstance.call219f3041d050108b1a2e51838473f6d8(productNumber, Model219f3041d050108b1a2e51838473f6d8Request, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productNumber** | **Number**| 셀러허브 상품 일련번호 | 
 **Model219f3041d050108b1a2e51838473f6d8Request** | [**Model219f3041d050108b1a2e51838473f6d8Request**](Model219f3041d050108b1a2e51838473f6d8Request.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## call69ec8f3e26fa2b1b02ce4897936e91e4

> Object call69ec8f3e26fa2b1b02ce4897936e91e4(productNumber, Model69ec8f3e26fa2b1b02ce4897936e91e4Request)

상품 재고 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let productNumber = 56; // Number | 셀러허브 상품 일련번호
let Model69ec8f3e26fa2b1b02ce4897936e91e4Request = new SellerhubApi.Model69ec8f3e26fa2b1b02ce4897936e91e4Request(); // Model69ec8f3e26fa2b1b02ce4897936e91e4Request | 
apiInstance.call69ec8f3e26fa2b1b02ce4897936e91e4(productNumber, Model69ec8f3e26fa2b1b02ce4897936e91e4Request, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productNumber** | **Number**| 셀러허브 상품 일련번호 | 
 **Model69ec8f3e26fa2b1b02ce4897936e91e4Request** | [**Model69ec8f3e26fa2b1b02ce4897936e91e4Request**](Model69ec8f3e26fa2b1b02ce4897936e91e4Request.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## createProduct

> Object createProduct(CreateProductRequest)

상품 등록

상품 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let CreateProductRequest = new SellerhubApi.CreateProductRequest(); // CreateProductRequest | 
apiInstance.createProduct(CreateProductRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **CreateProductRequest** | [**CreateProductRequest**](CreateProductRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## getForbiddenWords

> Object getForbiddenWords(opts)

금칙어 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let opts = {
  'categoryCode': "categoryCode_example", // String | 셀러허브 카테고리 코드
  'word': "word_example", // String | 검색 금칙어
  'isAppliedSubCategory': "isAppliedSubCategory_example", // String | 하위카테고리 적용여부
  '_with': ["null"], // [String] | WITH Model
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getForbiddenWords(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **word** | **String**| 검색 금칙어 | [optional] 
 **isAppliedSubCategory** | **String**| 하위카테고리 적용여부 | [optional] 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProduct

> Object getProduct(opts)

상품 목록

상품 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let opts = {
  'target': "target_example", // String | 상품 타입
  'approveTypes': ["null"], // [String] | 상품 승인 타입
  'categoryCode': "categoryCode_example", // String | 셀러허브 카테고리 코드
  'searchType': "searchType_example", // String | 검색 타겟
  'searchContent': "searchContent_example", // String | 검색 내용
  'isAbsoluteSearch': "isAbsoluteSearch_example", // String | 유사 검색 제외
  'brandNumber': 56, // Number | 브랜드 고유번호
  'minProductPrice': 56, // Number | 최소 상품 가격
  'maxProductPrice': 56, // Number | 최대 상품 가격
  'minProductSupplyPrice': 56, // Number | 최소 상품 공급가
  'maxProductSupplyPrice': 56, // Number | 최대 상품 공급가
  'isWholesale': "isWholesale_example", // String | 도매상품 여부
  'minProductWolesalePrice': 56, // Number | 최소 상품 도매가
  'maxProductWolesalePrice': 56, // Number | 최대 상품 도매가
  'searchDateType': "searchDateType_example", // String | 검색 일자 타입
  'startDate': null, // Date | 시작일자
  'endDate': null, // Date | 종료일자
  'salesStatus': "salesStatus_example", // String | 판매 상태
  'isOpened': "isOpened_example", // String | 상품 출력 여부
  'isPreRegistedProduct': "isPreRegistedProduct_example", // String | 선등록 상품 여부
  'isTemporary': "isTemporary_example", // String | 임시 상품 여부
  'isDeleted': "isDeleted_example", // String | 상품 삭제 여부
  'violationType': "violationType_example", // String | 위반 타입
  'violationStartDate': null, // Date | 위반일시 시작일자
  'violationEndDate': null, // Date | 위반일시 종료일자
  'registedViolationStartDate': null, // Date | 위반등록일시 시작일자
  'registedViolationEndDate': null, // Date | 위반등록일시 종료일자
  'updatedViolationStartDate': null, // Date | 위반수정일시 시작일자
  'updatedViolationEndDate': null, // Date | 위반수정일시 종료일자
  '_with': ["null"], // [String] | WITH Model
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getProduct(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **target** | **String**| 상품 타입 | [optional] 
 **approveTypes** | [**[String]**](String.md)| 상품 승인 타입 | [optional] 
 **categoryCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **searchType** | **String**| 검색 타겟 | [optional] 
 **searchContent** | **String**| 검색 내용 | [optional] 
 **isAbsoluteSearch** | **String**| 유사 검색 제외 | [optional] 
 **brandNumber** | **Number**| 브랜드 고유번호 | [optional] 
 **minProductPrice** | **Number**| 최소 상품 가격 | [optional] 
 **maxProductPrice** | **Number**| 최대 상품 가격 | [optional] 
 **minProductSupplyPrice** | **Number**| 최소 상품 공급가 | [optional] 
 **maxProductSupplyPrice** | **Number**| 최대 상품 공급가 | [optional] 
 **isWholesale** | **String**| 도매상품 여부 | [optional] 
 **minProductWolesalePrice** | **Number**| 최소 상품 도매가 | [optional] 
 **maxProductWolesalePrice** | **Number**| 최대 상품 도매가 | [optional] 
 **searchDateType** | **String**| 검색 일자 타입 | [optional] 
 **startDate** | [**Date**](.md)| 시작일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료일자 | [optional] 
 **salesStatus** | **String**| 판매 상태 | [optional] 
 **isOpened** | **String**| 상품 출력 여부 | [optional] 
 **isPreRegistedProduct** | **String**| 선등록 상품 여부 | [optional] 
 **isTemporary** | **String**| 임시 상품 여부 | [optional] 
 **isDeleted** | **String**| 상품 삭제 여부 | [optional] 
 **violationType** | **String**| 위반 타입 | [optional] 
 **violationStartDate** | [**Date**](.md)| 위반일시 시작일자 | [optional] 
 **violationEndDate** | [**Date**](.md)| 위반일시 종료일자 | [optional] 
 **registedViolationStartDate** | [**Date**](.md)| 위반등록일시 시작일자 | [optional] 
 **registedViolationEndDate** | [**Date**](.md)| 위반등록일시 종료일자 | [optional] 
 **updatedViolationStartDate** | [**Date**](.md)| 위반수정일시 시작일자 | [optional] 
 **updatedViolationEndDate** | [**Date**](.md)| 위반수정일시 종료일자 | [optional] 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProductByNo

> Object getProductByNo(productNumber, opts)

상품 조회

상품보기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let productNumber = 56; // Number | 셀러허브 상품 일련번호
let opts = {
  '_with': ["null"] // [String] | WITH Model
};
apiInstance.getProductByNo(productNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productNumber** | **Number**| 셀러허브 상품 일련번호 | 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProductByTarget

> Object getProductByTarget(opts)

특정 상품 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let opts = {
  'approveTypes': ["null"], // [String] | 상품 승인 타입
  'categoryCode': "categoryCode_example", // String | 셀러허브 카테고리 코드
  'searchType': "searchType_example", // String | 검색 타겟
  'searchContent': "searchContent_example", // String | 검색 내용
  'isAbsoluteSearch': "isAbsoluteSearch_example", // String | 유사 검색 제외
  'brandNumber': 56, // Number | 브랜드 고유번호
  'minProductPrice': 56, // Number | 최소 상품 가격
  'maxProductPrice': 56, // Number | 최대 상품 가격
  'minProductSupplyPrice': 56, // Number | 최소 상품 공급가
  'maxProductSupplyPrice': 56, // Number | 최대 상품 공급가
  'isWholesale': "isWholesale_example", // String | 도매상품 여부
  'minProductWolesalePrice': 56, // Number | 최소 상품 도매가
  'maxProductWolesalePrice': 56, // Number | 최대 상품 도매가
  'searchDateType': "searchDateType_example", // String | 검색 일자 타입
  'startDate': null, // Date | 시작일자
  'endDate': null, // Date | 종료일자
  'salesStatus': "salesStatus_example", // String | 판매 상태
  'isOpened': "isOpened_example", // String | 상품 출력 여부
  'isPreRegistedProduct': "isPreRegistedProduct_example", // String | 선등록 상품 여부
  'isTemporary': "isTemporary_example", // String | 임시 상품 여부
  'isDeleted': "isDeleted_example", // String | 상품 삭제 여부
  'violationType': "violationType_example", // String | 위반 타입
  'violationStartDate': null, // Date | 위반일시 시작일자
  'violationEndDate': null, // Date | 위반일시 종료일자
  'registedViolationStartDate': null, // Date | 위반등록일시 시작일자
  'registedViolationEndDate': null, // Date | 위반등록일시 종료일자
  'updatedViolationStartDate': null, // Date | 위반수정일시 시작일자
  'updatedViolationEndDate': null, // Date | 위반수정일시 종료일자
  '_with': ["null"], // [String] | WITH Model
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getProductByTarget(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **approveTypes** | [**[String]**](String.md)| 상품 승인 타입 | [optional] 
 **categoryCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **searchType** | **String**| 검색 타겟 | [optional] 
 **searchContent** | **String**| 검색 내용 | [optional] 
 **isAbsoluteSearch** | **String**| 유사 검색 제외 | [optional] 
 **brandNumber** | **Number**| 브랜드 고유번호 | [optional] 
 **minProductPrice** | **Number**| 최소 상품 가격 | [optional] 
 **maxProductPrice** | **Number**| 최대 상품 가격 | [optional] 
 **minProductSupplyPrice** | **Number**| 최소 상품 공급가 | [optional] 
 **maxProductSupplyPrice** | **Number**| 최대 상품 공급가 | [optional] 
 **isWholesale** | **String**| 도매상품 여부 | [optional] 
 **minProductWolesalePrice** | **Number**| 최소 상품 도매가 | [optional] 
 **maxProductWolesalePrice** | **Number**| 최대 상품 도매가 | [optional] 
 **searchDateType** | **String**| 검색 일자 타입 | [optional] 
 **startDate** | [**Date**](.md)| 시작일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료일자 | [optional] 
 **salesStatus** | **String**| 판매 상태 | [optional] 
 **isOpened** | **String**| 상품 출력 여부 | [optional] 
 **isPreRegistedProduct** | **String**| 선등록 상품 여부 | [optional] 
 **isTemporary** | **String**| 임시 상품 여부 | [optional] 
 **isDeleted** | **String**| 상품 삭제 여부 | [optional] 
 **violationType** | **String**| 위반 타입 | [optional] 
 **violationStartDate** | [**Date**](.md)| 위반일시 시작일자 | [optional] 
 **violationEndDate** | [**Date**](.md)| 위반일시 종료일자 | [optional] 
 **registedViolationStartDate** | [**Date**](.md)| 위반등록일시 시작일자 | [optional] 
 **registedViolationEndDate** | [**Date**](.md)| 위반등록일시 종료일자 | [optional] 
 **updatedViolationStartDate** | [**Date**](.md)| 위반수정일시 시작일자 | [optional] 
 **updatedViolationEndDate** | [**Date**](.md)| 위반수정일시 종료일자 | [optional] 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateProductByNo

> Object updateProductByNo(productNumber, UpdateProductByNoRequest)

상품 수정

상품 수정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProductApi();
let productNumber = 56; // Number | 셀러허브 상품 일련번호
let UpdateProductByNoRequest = new SellerhubApi.UpdateProductByNoRequest(); // UpdateProductByNoRequest | 
apiInstance.updateProductByNo(productNumber, UpdateProductByNoRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productNumber** | **Number**| 셀러허브 상품 일련번호 | 
 **UpdateProductByNoRequest** | [**UpdateProductByNoRequest**](UpdateProductByNoRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

