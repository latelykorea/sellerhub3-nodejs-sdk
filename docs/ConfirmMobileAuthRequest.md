# SellerhubApi.ConfirmMobileAuthRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstNumber** | **String** | 처음번호 | 
**middleNumber** | **String** | 중간번호 | 
**lastNumber** | **String** | 끝번호 | 
**mobileAuthToken** | **String** | 휴대폰 인증 토큰 | 
**authKey** | **String** | 휴대폰 인증 번호 | 


