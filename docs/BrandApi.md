# SellerhubApi.BrandApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProviderBrand**](BrandApi.md#createProviderBrand) | **POST** /shapi/v1/brands/provider | 공급사 브랜드 추가하기
[**deleteProviderBrandByNo**](BrandApi.md#deleteProviderBrandByNo) | **DELETE** /shapi/v1/brands/provider/{brandNumber} | 공급사 브랜드 삭제하기
[**getProviderBrand**](BrandApi.md#getProviderBrand) | **GET** /shapi/v1/brands/provider | 공급사 브랜드 목록 가져오기
[**getProviderBrandByNo**](BrandApi.md#getProviderBrandByNo) | **GET** /shapi/v1/brands/provider/{$brandNumber} | 공급사 특정 브랜드 가져오기
[**updateProviderBrand**](BrandApi.md#updateProviderBrand) | **PUT** /shapi/v1/brands/provider | 공급사 브랜드 변경하기



## createProviderBrand

> Object createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2)

공급사 브랜드 추가하기

공급사 브랜드 추가하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BrandApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드명
let UNKNOWN_PARAMETER_NAME2 = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드명 | 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteProviderBrandByNo

> Object deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME)

공급사 브랜드 삭제하기

공급사 브랜드 삭제하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BrandApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderBrand

> Object getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts)

공급사 브랜드 목록 가져오기

공급사 브랜드 목록 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BrandApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
let UNKNOWN_PARAMETER_NAME2 = new SellerhubApi.null(); //  | 브랜드명
let opts = {
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 브랜드명 | 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderBrandByNo

> Object getProviderBrandByNo(UNKNOWN_PARAMETER_NAME)

공급사 특정 브랜드 가져오기

공급사 특정 브랜드 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BrandApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.getProviderBrandByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateProviderBrand

> Object updateProviderBrand(brandDatas)

공급사 브랜드 변경하기

공급사 브랜드 변경하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.BrandApi();
let brandDatas = [new SellerhubApi.UpdateProviderBrandBrandDatasParameterInner()]; // [UpdateProviderBrandBrandDatasParameterInner] | 브랜드 데이터
apiInstance.updateProviderBrand(brandDatas, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brandDatas** | [**[UpdateProviderBrandBrandDatasParameterInner]**](UpdateProviderBrandBrandDatasParameterInner.md)| 브랜드 데이터 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

