# SellerhubApi.CalculateApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getApplicablePreCalculate**](CalculateApi.md#getApplicablePreCalculate) | **GET** /shapi/v1/preCalculate/applicable | 신청가능금액
[**getCalculateRequest**](CalculateApi.md#getCalculateRequest) | **GET** /shapi/v1/calculate/request | 정산 목록
[**getCalculateRequestDeduction**](CalculateApi.md#getCalculateRequestDeduction) | **GET** /shapi/v1/calculate/request/{calculateRequestNumber}/deductions | 정산 공제 목록
[**getCalculateRequestDetail**](CalculateApi.md#getCalculateRequestDetail) | **GET** /shapi/v1/calculate/request/{calculateRequestNumber}/items | 정산 상세 목록
[**getCalculateScheduled**](CalculateApi.md#getCalculateScheduled) | **GET** /shapi/v1/calculate/scheduled | 정산 예정
[**getCalculateScheduledDeduction**](CalculateApi.md#getCalculateScheduledDeduction) | **GET** /shapi/v1/calculate/scheduled/deductions | 정산 공제 예정 내역
[**getCalculateScheduledDelivery**](CalculateApi.md#getCalculateScheduledDelivery) | **GET** /shapi/v1/calculate/scheduled/deliveries | 정산 예정 패널티
[**getCalculateScheduledDetail**](CalculateApi.md#getCalculateScheduledDetail) | **GET** /shapi/v1/calculate/scheduled/items | 정산 예정 상품 상세
[**getCalculateSurtax**](CalculateApi.md#getCalculateSurtax) | **GET** /shapi/v1/calculate/surtax | 부가세 신고 참고자료
[**getPreCalculate**](CalculateApi.md#getPreCalculate) | **GET** /shapi/v1/preCalculate | 빠른정산 목록
[**getPreCalculateRequest**](CalculateApi.md#getPreCalculateRequest) | **GET** /shapi/v1/preCalculate/request | 신청 목록



## getApplicablePreCalculate

> Object getApplicablePreCalculate()

신청가능금액

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getApplicablePreCalculate((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateRequest

> Object getCalculateRequest()

정산 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getCalculateRequest((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateRequestDeduction

> Object getCalculateRequestDeduction(calculateRequestNumber)

정산 공제 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
let calculateRequestNumber = 56; // Number | 정산요청번호
apiInstance.getCalculateRequestDeduction(calculateRequestNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculateRequestNumber** | **Number**| 정산요청번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateRequestDetail

> Object getCalculateRequestDetail(calculateRequestNumber)

정산 상세 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
let calculateRequestNumber = 56; // Number | 정산요청번호
apiInstance.getCalculateRequestDetail(calculateRequestNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calculateRequestNumber** | **Number**| 정산요청번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateScheduled

> Object getCalculateScheduled()

정산 예정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getCalculateScheduled((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateScheduledDeduction

> Object getCalculateScheduledDeduction()

정산 공제 예정 내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getCalculateScheduledDeduction((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateScheduledDelivery

> Object getCalculateScheduledDelivery()

정산 예정 패널티

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getCalculateScheduledDelivery((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateScheduledDetail

> Object getCalculateScheduledDetail()

정산 예정 상품 상세

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getCalculateScheduledDetail((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCalculateSurtax

> Object getCalculateSurtax()

부가세 신고 참고자료

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getCalculateSurtax((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPreCalculate

> Object getPreCalculate()

빠른정산 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getPreCalculate((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPreCalculateRequest

> Object getPreCalculateRequest()

신청 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CalculateApi();
apiInstance.getPreCalculateRequest((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

