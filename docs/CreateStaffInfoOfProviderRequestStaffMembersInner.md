# SellerhubApi.CreateStaffInfoOfProviderRequestStaffMembersInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales** | [**[CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]**](CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.md) | 영업 담당자 | [optional] 
**cs** | [**[CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]**](CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.md) | CS 담당자 | [optional] 
**delivery** | [**[CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]**](CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.md) | 배송 담당자 | [optional] 
**settle** | [**[CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]**](CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner.md) | 정산 담당자 | [optional] 


