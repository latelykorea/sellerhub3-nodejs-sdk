# SellerhubApi.ManualApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getManualBoard**](ManualApi.md#getManualBoard) | **GET** /shapi/v1/board/manuals | 공급사 매뉴얼 목록
[**getManualBoardByNo**](ManualApi.md#getManualBoardByNo) | **GET** /shapi/v1/board/manuals/{manualNumber} | 공급사 매뉴얼



## getManualBoard

> [BoardManual] getManualBoard(opts)

공급사 매뉴얼 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManualApi();
let opts = {
  'startDate': 20220101, // Date | 등록 시작일
  'endDate': 20220101, // Date | 등록 종료일
  'searchType': subject, // String | 검색 타입
  'searchContents': , // String | 검색 내용
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getManualBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | [**Date**](.md)| 등록 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 등록 종료일 | [optional] 
 **searchType** | **String**| 검색 타입 | [optional] 
 **searchContents** | **String**| 검색 내용 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

[**[BoardManual]**](BoardManual.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getManualBoardByNo

> BoardManual getManualBoardByNo(manualNumber)

공급사 매뉴얼

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ManualApi();
let manualNumber = 1; // Number | 공급사 매뉴얼 번호
apiInstance.getManualBoardByNo(manualNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **manualNumber** | **Number**| 공급사 매뉴얼 번호 | 

### Return type

[**BoardManual**](BoardManual.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

