# SellerhubApi.EventProposalApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMarketingEventProposalRequestByNo**](EventProposalApi.md#createMarketingEventProposalRequestByNo) | **POST** /shapi/v1/marketing/eventProposal/request/{eventProposalNumber} | 행사제안 신청
[**getMarketingEventProposal**](EventProposalApi.md#getMarketingEventProposal) | **GET** /shapi/v1/marketing/eventProposal | 행사제안 목록 조회
[**getMarketingEventProposalByNo**](EventProposalApi.md#getMarketingEventProposalByNo) | **GET** /shapi/v1/marketing/eventProposal/{eventProposalNumber} | 행사제안 내역
[**getMarketingEventProposalRequestByNo**](EventProposalApi.md#getMarketingEventProposalRequestByNo) | **GET** /shapi/v1/marketing/eventProposal/request/{eventNumber} | 행사제안 신청내역
[**updateMarketingEventProposalRequestByNo**](EventProposalApi.md#updateMarketingEventProposalRequestByNo) | **PUT** /shapi/v1/marketing/eventProposal/request/{eventNumber} | 행사제안 신청내용 재신청



## createMarketingEventProposalRequestByNo

> Object createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts)

행사제안 신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.EventProposalApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## getMarketingEventProposal

> Object getMarketingEventProposal(opts)

행사제안 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.EventProposalApi();
let opts = {
  'channel': ["null"], // [String] | 행사 채널
  'searchDateType': "'startDate'", // String | 검색 일자 설정
  'startDate': 56, // Number | 시작일
  'endDate': 56, // Number | 종료일
  'category': ["null"], // [String] | 카테고리
  'eventStatus': "'all'", // String | 행사진행여부
  'requestStatus': "'all'", // String | 행사신청여부
  'isApprove': "'all'", // String | 답변여부
  'searchKey': "'title'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEventProposal(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel** | [**[String]**](String.md)| 행사 채널 | [optional] 
 **searchDateType** | **String**| 검색 일자 설정 | [optional] [default to &#39;startDate&#39;]
 **startDate** | **Number**| 시작일 | [optional] 
 **endDate** | **Number**| 종료일 | [optional] 
 **category** | [**[String]**](String.md)| 카테고리 | [optional] 
 **eventStatus** | **String**| 행사진행여부 | [optional] [default to &#39;all&#39;]
 **requestStatus** | **String**| 행사신청여부 | [optional] [default to &#39;all&#39;]
 **isApprove** | **String**| 답변여부 | [optional] [default to &#39;all&#39;]
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;title&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposalByNo

> Object getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME)

행사제안 내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.EventProposalApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
apiInstance.getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposalRequestByNo

> Object getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME)

행사제안 신청내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.EventProposalApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
apiInstance.getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateMarketingEventProposalRequestByNo

> Object updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts)

행사제안 신청내용 재신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.EventProposalApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 신청 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 신청 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

