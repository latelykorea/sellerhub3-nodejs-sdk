# SellerhubApi.UrgentMessageAlarm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**writerNumber** | **Number** | 작성자 번호 | [optional] 
**writerName** | **String** | 작성자명 | [optional] 
**message** | **String** | 알람 내용 | [optional] 
**answer** | **String** | 답변 | [optional] 
**createdAt** | **Date** | 알람 등록일 | [optional] 
**answeredAt** | **Date** | 답변일 | [optional] 


