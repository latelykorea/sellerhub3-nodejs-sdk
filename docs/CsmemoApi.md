# SellerhubApi.CsmemoApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOrderCsMemoByNo**](CsmemoApi.md#getOrderCsMemoByNo) | **GET** /shapi/v1/order/csMemo/{ordno} | cs 메모



## getOrderCsMemoByNo

> Object getOrderCsMemoByNo(opts)

cs 메모

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CsmemoApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null() //  | 주문번호
};
apiInstance.getOrderCsMemoByNo(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문번호 | [optional] 

### Return type

**Object**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

