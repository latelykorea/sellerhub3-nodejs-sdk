# SellerhubApi.AuthenticationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attemptMobileAuth**](AuthenticationApi.md#attemptMobileAuth) | **POST** /shapi/v1/mobileAuth/attempt | 휴대폰 인증 번호 요청
[**confirmMobileAuth**](AuthenticationApi.md#confirmMobileAuth) | **POST** /shapi/v1/mobileAuth/confirm | 휴대폰 인증 번호 검증



## attemptMobileAuth

> Object attemptMobileAuth(AttemptMobileAuthRequest)

휴대폰 인증 번호 요청

휴대폰 인증 번호 요청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.AuthenticationApi();
let AttemptMobileAuthRequest = new SellerhubApi.AttemptMobileAuthRequest(); // AttemptMobileAuthRequest | 
apiInstance.attemptMobileAuth(AttemptMobileAuthRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **AttemptMobileAuthRequest** | [**AttemptMobileAuthRequest**](AttemptMobileAuthRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## confirmMobileAuth

> Object confirmMobileAuth(ConfirmMobileAuthRequest)

휴대폰 인증 번호 검증

휴대폰 인증 번호 검증

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.AuthenticationApi();
let ConfirmMobileAuthRequest = new SellerhubApi.ConfirmMobileAuthRequest(); // ConfirmMobileAuthRequest | 
apiInstance.confirmMobileAuth(ConfirmMobileAuthRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ConfirmMobileAuthRequest** | [**ConfirmMobileAuthRequest**](ConfirmMobileAuthRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

