# SellerhubApi.UrgentMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | 긴급 알람 ID | [optional] 
**orderNumber** | **Number** | 주문 번호 | [optional] 
**orderItemNumber** | **Number** | 주문 아이템 번호 | [optional] 
**productNumber** | **Number** | 상품 번호 | [optional] 
**productName** | **String** | 상품명 | [optional] 
**brandNumber** | **String** | 브랜드 번호 | [optional] 
**brandName** | **String** | 브랜드명 | [optional] 
**alarmType** | **String** | 알림 타입 코드 | [optional] 
**alarmTypeKor** | **String** | 알람 타입 | [optional] 
**alarms** | [**[UrgentMessageAlarm]**](UrgentMessageAlarm.md) | 알람 목록 | [optional] 
**isReceived** | **String** | 수신 여부 | [optional] 
**isFinished** | **String** | 종료 여부 | [optional] 
**createdAt** | **String** | 생성 일자 | [optional] 
**finishedAt** | **String** | 종료 일자 | [optional] 
**isAvailableToReply** | **String** | 답변 가능 여부 | [optional] 



## Enum: IsReceivedEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsFinishedEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsAvailableToReplyEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




