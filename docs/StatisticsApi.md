# SellerhubApi.StatisticsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBrandStatistics**](StatisticsApi.md#getBrandStatistics) | **GET** /shapi/v1/statistics/brand | 브랜드별 매출 통계
[**getPopularCategoryStatistics**](StatisticsApi.md#getPopularCategoryStatistics) | **GET** /shapi/v1/statistics/popularCategory | 인기 카테고리 분석
[**getPopularProductStatistics**](StatisticsApi.md#getPopularProductStatistics) | **GET** /shapi/v1/statistics/popularGoods | 인기 상품 분석
[**getStatistics**](StatisticsApi.md#getStatistics) | **GET** /shapi/v1/statistics | 매출 통계 리스트



## getBrandStatistics

> Object getBrandStatistics(opts)

브랜드별 매출 통계

브랜드 매출 통계

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.StatisticsApi();
let opts = {
  'year': 56, // Number | 년
  'month': 56, // Number | 월
  'day': 56 // Number | 일
};
apiInstance.getBrandStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Number**| 년 | [optional] 
 **month** | **Number**| 월 | [optional] 
 **day** | **Number**| 일 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPopularCategoryStatistics

> Object getPopularCategoryStatistics(opts)

인기 카테고리 분석

인기 카테고리 분석

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.StatisticsApi();
let opts = {
  'year': 56, // Number | 년
  'month': 56, // Number | 월
  'category': 56 // Number | 카테고리
};
apiInstance.getPopularCategoryStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Number**| 년 | [optional] 
 **month** | **Number**| 월 | [optional] 
 **category** | **Number**| 카테고리 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPopularProductStatistics

> Object getPopularProductStatistics(opts)

인기 상품 분석

인기 상품 분석

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.StatisticsApi();
let opts = {
  'year': 56, // Number | 년
  'month': 56 // Number | 월
};
apiInstance.getPopularProductStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **year** | **Number**| 년 | [optional] 
 **month** | **Number**| 월 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getStatistics

> Object getStatistics(opts)

매출 통계 리스트

매출 통계 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.StatisticsApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 카테고리
  'orderStep': "orderStep_example", // String | 주문 상태
  'orderStep2': "orderStep2_example", // String | 주문 상태
  'brandNumber': 56, // Number | 브랜드 번호
  'goodsBrandNubmer': 56, // Number | 상품 브랜드 번호
  'searchType': "searchType_example", // String | 처리일자 (키워드)
  'startDate': null, // Date | 시작일자
  'endDate': null // Date | 종료일자
};
apiInstance.getStatistics(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 카테고리 | [optional] 
 **orderStep** | **String**| 주문 상태 | [optional] 
 **orderStep2** | **String**| 주문 상태 | [optional] 
 **brandNumber** | **Number**| 브랜드 번호 | [optional] 
 **goodsBrandNubmer** | **Number**| 상품 브랜드 번호 | [optional] 
 **searchType** | **String**| 처리일자 (키워드) | [optional] 
 **startDate** | [**Date**](.md)| 시작일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료일자 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

