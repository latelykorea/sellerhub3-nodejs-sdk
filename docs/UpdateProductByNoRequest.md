# SellerhubApi.UpdateProductByNoRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryCode** | **String** | 카테고리 코드 | [optional] 
**brandNumber** | **Number** | 브랜드 번호 | [optional] 
**maker** | **String** | 메이커 | [optional] 
**origin** | **String** | 원산지 | [optional] 
**productCode** | **String** | 브랜드 번호 | [optional] 
**productName** | **String** | 상품명 | [optional] 
**keywords** | **[String]** | 키워드 | [optional] 
**isUsed** | **String** | 중고여부 | [optional] 
**isSellerDeal** | **String** | 셀러딜 상품여부 | [optional] 
**isTemporary** | **String** | 임시상품 여부 | [optional] 
**deliveryType** | **String** | 배송 유형 | [optional] 
**deliveryPrice** | **Number** | 배송비 | [optional] 
**isManufactured** | **String** | 제조 상품 여부 | [optional] 
**manufacturingPeriod** | **String** | 제조기간 | [optional] 
**isReservationDelivery** | **String** | 예약 상품 여부 | [optional] 
**reservationDeliveryDate** | **String** | 예약발송일자 | [optional] 
**isOverseasDelivery** | **String** | 해외 상품 여부 | [optional] 
**overseasDeliveryPeriod** | **String** | 해외배송기간 | [optional] 
**productPrice** | **Number** | 상품 판매 가격 | [optional] 
**productSupplyPrice** | **Number** | 상품 공급 가격 | [optional] 
**productConsumerPrice** | **Number** | 소비자 가격 | [optional] 
**productAbidePrice** | **Number** | 준수 판매 가격 | [optional] 
**productWolesalePrice** | **Number** | 도매 가격 | [optional] 
**isUsingStock** | **String** | 재고량 연동 여부 | [optional] 
**isTaxable** | **String** | 과세 여부 | [optional] 
**optionTitles** | **[String]** | 상품 옵션 제목 | [optional] 
**optionType** | **String** | 옵션 유형 | [optional] 
**options** | **[[CreateProductRequestOptionsInnerInner]]** | 상품 옵션 | [optional] 
**isRunouted** | **String** | 품절 여부 | [optional] 
**productImageDatas** | **[[CreateProductRequestProductImageDatasInnerInner]]** | 상품 이미지 | [optional] 
**description** | **String** | 상품 상세 내용 | [optional] 
**kcCertificationType** | **String** | KC 인증 유형 | [optional] 
**kcCertificationInfo** | **String** | KC 인증 정보 | [optional] 
**ecoCertificationType** | **String** | 친환경 인증 유형 | [optional] 
**ecoCertificationAuth** | **String** | 친환경 인증 기관 | [optional] 
**ecoCertificationCode** | **String** | 친환경 인증 번호 | [optional] 
**ecoCertificationMark** | **String** | 친환경 인증 마크 사용여부 | [optional] 
**certificationDatas** | **[[CreateProductRequestCertificationDatasInnerInner]]** | 상품 인증자료 첨부 | [optional] 
**productInformation** | [**[CreateProductRequestProductInformationInner]**](CreateProductRequestProductInformationInner.md) | 상품 정보 고시 | [optional] 
**manageMemo** | **String** | 관리 메모 | [optional] 
**productMemo** | **String** | 상품 메모 | [optional] 



## Enum: IsUsedEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsSellerDealEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsTemporaryEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: DeliveryTypeEnum


* `wait` (value: `"wait"`)

* `request` (value: `"request"`)

* `approved` (value: `"approved"`)

* `hold` (value: `"hold"`)

* `reject` (value: `"reject"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsManufacturedEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsReservationDeliveryEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsOverseasDeliveryEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsUsingStockEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsTaxableEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: OptionTypeEnum


* `single` (value: `"single"`)

* `double` (value: `"double"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsRunoutedEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: KcCertificationTypeEnum


* `해당없음` (value: `"해당없음"`)

* `상세설명참조` (value: `"상세설명참조"`)

* `[생활용품]안전인증` (value: `"[생활용품]안전인증"`)

* `[생활용품]안전확인` (value: `"[생활용품]안전확인"`)

* `[생활용품]공급자적합성확인` (value: `"[생활용품]공급자적합성확인"`)

* `[생활용품]어린이보호포장` (value: `"[생활용품]어린이보호포장"`)

* `[어린이제품]안전인증` (value: `"[어린이제품]안전인증"`)

* `[어린이제품]안전확인` (value: `"[어린이제품]안전확인"`)

* `[어린이제품]공급자적합성확인` (value: `"[어린이제품]공급자적합성확인"`)

* `[전기용품]안전인증` (value: `"[전기용품]안전인증"`)

* `[전기용품]안전확인` (value: `"[전기용품]안전확인"`)

* `[전기용품]공급자적합성확인` (value: `"[전기용품]공급자적합성확인"`)

* `[방송통신기자재]적합성평가` (value: `"[방송통신기자재]적합성평가"`)

* `[위해우려제품]자가검사번호` (value: `"[위해우려제품]자가검사번호"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: EcoCertificationTypeEnum


* `유기축산물` (value: `"[친환경]유기축산물"`)

* `유기농산물` (value: `"[친환경]유기농산물"`)

* `무항생제축산물` (value: `"[친환경]무항생제축산물"`)

* `무농약농산물` (value: `"[친환경]무농약농산물"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: EcoCertificationMarkEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




