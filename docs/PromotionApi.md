# SellerhubApi.PromotionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMarketingPromotioon**](PromotionApi.md#createMarketingPromotioon) | **POST** /shapi/v1/marketing/promotion | 마케팅 프로모션 동의
[**getMarketingPromotioon**](PromotionApi.md#getMarketingPromotioon) | **GET** /shapi/v1/marketing/promotion | 마케팅 프로모션 리스트



## createMarketingPromotioon

> Object createMarketingPromotioon(opts)

마케팅 프로모션 동의

프로모션 동의

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PromotionApi();
let opts = {
  'agreeNumber': 56, // Number | 프로모션 번호
  'agreeCheck': "agreeCheck_example" // String | 프로모션 동의 여부
};
apiInstance.createMarketingPromotioon(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agreeNumber** | **Number**| 프로모션 번호 | [optional] 
 **agreeCheck** | **String**| 프로모션 동의 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingPromotioon

> Object getMarketingPromotioon(opts)

마케팅 프로모션 리스트

프로모션목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PromotionApi();
let opts = {
  'startDate': null, // Date | 시작 일자
  'endDate': null, // Date | 종료 일자
  'searchType': "searchType_example", // String | 검색 옵션
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getMarketingPromotioon(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | [**Date**](.md)| 시작 일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료 일자 | [optional] 
 **searchType** | **String**| 검색 옵션 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

