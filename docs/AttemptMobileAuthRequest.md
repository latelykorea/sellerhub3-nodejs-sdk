# SellerhubApi.AttemptMobileAuthRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstNumber** | **String** | 처음번호 | 
**middleNumber** | **String** | 중간번호 | 
**lastNumber** | **String** | 끝번호 | 


