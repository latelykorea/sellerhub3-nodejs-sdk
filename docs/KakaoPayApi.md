# SellerhubApi.KakaoPayApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**call283fca4e62590665f3db9e7ba1abf39c**](KakaoPayApi.md#call283fca4e62590665f3db9e7ba1abf39c) | **GET** /shapi/v1/kakaoPay/subscribe/cancel | 구독 결제 등록 준비 취소
[**call5e85b8719a105c8bd65b5eac7d84fdc4**](KakaoPayApi.md#call5e85b8719a105c8bd65b5eac7d84fdc4) | **POST** /shapi/v1/kakaoPay/subscribe/ready | 구독 결제 등록 준비
[**eee5ed068b07412eecc9079210555b9a**](KakaoPayApi.md#eee5ed068b07412eecc9079210555b9a) | **GET** /shapi/v1/kakaoPay/subscribe/fail | 구독 결제 등록 준비 실패
[**ef0c8611ac5670707a9f5f8b649e31c0**](KakaoPayApi.md#ef0c8611ac5670707a9f5f8b649e31c0) | **GET** /shapi/v1/kakaoPay/subscribe/approve | 구독 결제 승인 요청



## call283fca4e62590665f3db9e7ba1abf39c

> Object call283fca4e62590665f3db9e7ba1abf39c(shToken)

구독 결제 등록 준비 취소

구독 결제 등록 준비 취소

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.KakaoPayApi();
let shToken = "shToken_example"; // String | 결제 요청 관리 키값
apiInstance.call283fca4e62590665f3db9e7ba1abf39c(shToken, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shToken** | **String**| 결제 요청 관리 키값 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## call5e85b8719a105c8bd65b5eac7d84fdc4

> Object call5e85b8719a105c8bd65b5eac7d84fdc4(body)

구독 결제 등록 준비

구독 결제 등록 준비

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.KakaoPayApi();
let body = {key: null}; // Object | 
apiInstance.call5e85b8719a105c8bd65b5eac7d84fdc4(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## eee5ed068b07412eecc9079210555b9a

> Object eee5ed068b07412eecc9079210555b9a(shToken)

구독 결제 등록 준비 실패

구독 결제 등록 준비 실패

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.KakaoPayApi();
let shToken = "shToken_example"; // String | 결제 요청 관리 키값
apiInstance.eee5ed068b07412eecc9079210555b9a(shToken, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shToken** | **String**| 결제 요청 관리 키값 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## ef0c8611ac5670707a9f5f8b649e31c0

> Object ef0c8611ac5670707a9f5f8b649e31c0()

구독 결제 승인 요청

구독 결제 등록 요청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.KakaoPayApi();
apiInstance.ef0c8611ac5670707a9f5f8b649e31c0((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

