# SellerhubApi.PopularProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productNumber** | **Number** | 상품번호 | [optional] 
**cnt** | **Number** | 판매량 | [optional] 
**price** | **Number** | 판매가격 | [optional] 
**productName** | **String** | 상품명 | [optional] 
**productImageSmall** | **String** | 상품대표이미지 - 작은사이즈 | [optional] 
**productImageLarge** | **String** | 상품대표이미지 - 큰사이즈 | [optional] 


