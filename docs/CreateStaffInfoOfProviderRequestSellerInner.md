# SellerhubApi.CreateStaffInfoOfProviderRequestSellerInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | 판매자명 | [optional] 
**serviceCenterTel** | **String** | 고객센터 번호 | [optional] 


