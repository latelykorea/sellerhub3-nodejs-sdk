# SellerhubApi.PaymentApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAvailablePaymentMethods**](PaymentApi.md#getAvailablePaymentMethods) | **GET** /shapi/v1/paymentMethods | 결제 수단 등록 목록
[**getPaymentHistoryById**](PaymentApi.md#getPaymentHistoryById) | **GET** /shapi/v1/payments/{id} | 결제 내역
[**getPaymentMethodById**](PaymentApi.md#getPaymentMethodById) | **GET** /shapi/v1/paymentMethods/{id} | 결제 수단 등록 내용
[**geyPaymentHistory**](PaymentApi.md#geyPaymentHistory) | **GET** /shapi/v1/payments | 결제 내역 목록
[**registPaymentMethod**](PaymentApi.md#registPaymentMethod) | **POST** /shapi/v1/paymentMethods | 결제 수단 등록



## getAvailablePaymentMethods

> Object getAvailablePaymentMethods()

결제 수단 등록 목록

결제 수단 등록 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PaymentApi();
apiInstance.getAvailablePaymentMethods((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPaymentHistoryById

> Object getPaymentHistoryById(opts)

결제 내역

결제 내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PaymentApi();
let opts = {
  'id': 1 // Number | 결제 내역 고유 ID
};
apiInstance.getPaymentHistoryById(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| 결제 내역 고유 ID | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPaymentMethodById

> Object getPaymentMethodById()

결제 수단 등록 내용

결제 수단 등록 내용

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PaymentApi();
apiInstance.getPaymentMethodById((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## geyPaymentHistory

> Object geyPaymentHistory(opts)

결제 내역 목록

결제 내역 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PaymentApi();
let opts = {
  'body': {key: null} // Object | 
};
apiInstance.geyPaymentHistory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## registPaymentMethod

> Object registPaymentMethod(body)

결제 수단 등록

결제 수단 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.PaymentApi();
let body = {key: null}; // Object | 
apiInstance.registPaymentMethod(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

