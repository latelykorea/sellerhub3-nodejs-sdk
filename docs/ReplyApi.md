# SellerhubApi.ReplyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBoardReply**](ReplyApi.md#createBoardReply) | **POST** /shapi/v1/board/{boardNumber}/reply | 판매자지원 게시물 답변 등록
[**getBoardReply**](ReplyApi.md#getBoardReply) | **GET** /shapi/v1/board/{boardNumber}/reply | 판매자지원 게시물 답변 조회
[**getMarketingEventReplyByNo**](ReplyApi.md#getMarketingEventReplyByNo) | **GET** /shapi/v1/marketing/event/reply/{eventNumber} | 행사신청 게시물 댓글 목록 조회



## createBoardReply

> Object createBoardReply(boardNumber, subject, contents, opts)

판매자지원 게시물 답변 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ReplyApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let subject = null; // Object | 제목
let contents = null; // Object | 내용
let opts = {
  'category': "category_example", // String | 구분값 카테고리
  'secret': "secret_example" // String | 비밀글 여부
};
apiInstance.createBoardReply(boardNumber, subject, contents, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **subject** | [**Object**](Object.md)| 제목 | 
 **contents** | [**Object**](Object.md)| 내용 | 
 **category** | **String**| 구분값 카테고리 | [optional] 
 **secret** | **String**| 비밀글 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## getBoardReply

> Object getBoardReply(boardNumber, opts)

판매자지원 게시물 답변 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ReplyApi();
let boardNumber = 56; // Number | 판매자지원 게시물 번호
let opts = {
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getBoardReply(boardNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **boardNumber** | **Number**| 판매자지원 게시물 번호 | 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventReplyByNo

> Object getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts)

행사신청 게시물 댓글 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ReplyApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let opts = {
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

