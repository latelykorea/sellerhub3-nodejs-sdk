# SellerhubApi.ForbiddenApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getForbiddenWords**](ForbiddenApi.md#getForbiddenWords) | **GET** /shapi/v1/forbiddenWords | 금칙어 목록



## getForbiddenWords

> Object getForbiddenWords(opts)

금칙어 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ForbiddenApi();
let opts = {
  'categoryCode': "categoryCode_example", // String | 셀러허브 카테고리 코드
  'word': "word_example", // String | 검색 금칙어
  'isAppliedSubCategory': "isAppliedSubCategory_example", // String | 하위카테고리 적용여부
  '_with': ["null"], // [String] | WITH Model
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getForbiddenWords(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **word** | **String**| 검색 금칙어 | [optional] 
 **isAppliedSubCategory** | **String**| 하위카테고리 적용여부 | [optional] 
 **_with** | [**[String]**](String.md)| WITH Model | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

