# SellerhubApi.AlarmApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAlarm**](AlarmApi.md#getAlarm) | **GET** /shapi/v1/alarm | 알람 목록 조회
[**getAlarmByNo**](AlarmApi.md#getAlarmByNo) | **GET** /shapi/v1/alarm/read/{alarmNumber} | 알람읽기



## getAlarm

> Object getAlarm(opts)

알람 목록 조회

알람 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.AlarmApi();
let opts = {
  'regDateTime': new Date("2013-10-20T19:20:30+01:00"), // Date | 보낸시간
  'alarmType1': "alarmType1_example", // String | 알람 메인타입
  'alarmType2': "alarmType2_example", // String | 알람 타입
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | 시작 시간
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | 종료 시간
  'keyword': "keyword_example", // String | 검색어
  'isRead': "''" // String | 읽음 여부
};
apiInstance.getAlarm(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regDateTime** | **Date**| 보낸시간 | [optional] 
 **alarmType1** | **String**| 알람 메인타입 | [optional] 
 **alarmType2** | **String**| 알람 타입 | [optional] 
 **startDate** | **Date**| 시작 시간 | [optional] 
 **endDate** | **Date**| 종료 시간 | [optional] 
 **keyword** | **String**| 검색어 | [optional] 
 **isRead** | **String**| 읽음 여부 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAlarmByNo

> Object getAlarmByNo(alarmNumber)

알람읽기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.AlarmApi();
let alarmNumber = 56; // Number | 알람 번호
apiInstance.getAlarmByNo(alarmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alarmNumber** | **Number**| 알람 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

