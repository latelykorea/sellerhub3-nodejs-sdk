# SellerhubApi.RefundApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRefundOrder**](RefundApi.md#getRefundOrder) | **GET** /shapi/v1/order/list/refund | 반품접수/완료 리스트
[**updateOrderToRefund**](RefundApi.md#updateOrderToRefund) | **POST** /shapi/v1/order/refund | 반품처리



## getRefundOrder

> Object getRefundOrder(opts)

반품접수/완료 리스트

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.RefundApi();
let opts = {
  'skey': "''", // String | 주문검색 (옵션)
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문검색 (키워드)
  'dtkind': "''", // String | 반품요청일 (옵션)
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 반품요청일 (키워드)
  'cancelstatus': "''", // String | 상태
  'cls': "''", // String | 반품완료 여부
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getRefundOrder(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skey** | **String**| 주문검색 (옵션) | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문검색 (키워드) | [optional] 
 **dtkind** | **String**| 반품요청일 (옵션) | [optional] [default to &#39;&#39;]
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 반품요청일 (키워드) | [optional] 
 **cancelstatus** | **String**| 상태 | [optional] [default to &#39;&#39;]
 **cls** | **String**| 반품완료 여부 | [optional] [default to &#39;&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateOrderToRefund

> Object updateOrderToRefund(opts)

반품처리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.RefundApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 주문취소sno
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 반품택배사
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null() //  | 반품택배 코드
};
apiInstance.updateOrderToRefund(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 주문취소sno | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 반품택배사 | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 반품택배 코드 | [optional] 

### Return type

**Object**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

