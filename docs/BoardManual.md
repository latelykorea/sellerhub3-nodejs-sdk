# SellerhubApi.BoardManual

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manualNumber** | **Number** | 공급사 매뉴얼 번호 | [optional] 
**subject** | **String** | 제목 | [optional] 
**contents** | **String** | 내용 | [optional] 
**category** | **String** | 카테고리 | [optional] 
**isSecret** | **String** | 비밀글 여부 | [optional] 
**isNotice** | **String** | 공지 여부 | [optional] 
**originalFileName** | **String** | 원본 첨부 파일명 | [optional] 
**attachmentsFileName** | **String** | 첨부 파일명 | [optional] 
**creator** | **String** | 등록자 | [optional] 
**createdAt** | **String** | 생성 일자 | [optional] 



## Enum: IsSecretEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)





## Enum: IsNoticeEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




