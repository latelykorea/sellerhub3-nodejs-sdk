# SellerhubApi.FaqApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getFaqBoard**](FaqApi.md#getFaqBoard) | **GET** /shapi/v1/board/faq | FAQ 조회



## getFaqBoard

> Object getFaqBoard(opts)

FAQ 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.FaqApi();
let opts = {
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'startDate': null, // Date | 시작일
  'endDate': null, // Date | 종료일
  'sortBy': "'none'", // String | 정렬
  'sortDirection': "'desc'", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getFaqBoard(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **startDate** | [**Date**](.md)| 시작일 | [optional] 
 **endDate** | [**Date**](.md)| 종료일 | [optional] 
 **sortBy** | **String**| 정렬 | [optional] [default to &#39;none&#39;]
 **sortDirection** | **String**| 정렬 방향 | [optional] [default to &#39;desc&#39;]
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

