# SellerhubApi.DeliveryChangeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**updateOrderDeliveryStatus**](DeliveryChangeApi.md#updateOrderDeliveryStatus) | **POST** /shapi/v1/order/deliveryStatus | 배송상태 변경



## updateOrderDeliveryStatus

> Object updateOrderDeliveryStatus(opts)

배송상태 변경

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearer
let bearer = defaultClient.authentications['bearer'];
bearer.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DeliveryChangeApi();
let opts = {
  'UNKNOWN_PARAMETER_NAME': new SellerhubApi.null(), //  | 아이템 sno
  'UNKNOWN_PARAMETER_NAME2': new SellerhubApi.null(), //  | 현재 istep
  'UNKNOWN_PARAMETER_NAME3': new SellerhubApi.null(), //  | 현재 istep2
  'UNKNOWN_PARAMETER_NAME4': new SellerhubApi.null(), //  | 발송예정일
  'UNKNOWN_PARAMETER_NAME5': new SellerhubApi.null(), //  | 발송지연 예정일
  'rstatus': "''" // String | 배송상태 변경값
};
apiInstance.updateOrderDeliveryStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 아이템 sno | [optional] 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 현재 istep | [optional] 
 **UNKNOWN_PARAMETER_NAME3** | [****](.md)| 현재 istep2 | [optional] 
 **UNKNOWN_PARAMETER_NAME4** | [****](.md)| 발송예정일 | [optional] 
 **UNKNOWN_PARAMETER_NAME5** | [****](.md)| 발송지연 예정일 | [optional] 
 **rstatus** | **String**| 배송상태 변경값 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

