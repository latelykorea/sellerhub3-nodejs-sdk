# SellerhubApi.ProviderDeliveryResource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandNumber** | **Number** | 브랜드번호 | [optional] 
**deliveryCompanyNumber** | **Number** | 택배회사번호 | [optional] 
**deliveryCompanyName** | **String** | 택배사명 | [optional] 
**deliveryContractCode** | **String** | 택배사 계약코드 | [optional] 
**postOfficeCustomerCode** | **String** | 우체국 고객코드 | [optional] 
**isAvaliableInvoiceLinking** | **String** | 송장연동 출력기능 가능 | [optional] 



## Enum: IsAvaliableInvoiceLinkingEnum


* `y` (value: `"y"`)

* `n` (value: `"n"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




