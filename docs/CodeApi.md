# SellerhubApi.CodeApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCode**](CodeApi.md#getCode) | **GET** /shapi/v1/codes | 그룹코드 조회
[**getCodeByGroupCd**](CodeApi.md#getCodeByGroupCd) | **GET** /shapi/v1/codes/{groupCd} | 그룹코드 조회



## getCode

> [CodeResource] getCode()

그룹코드 조회

상위 그룹코드 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CodeApi();
apiInstance.getCode((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[CodeResource]**](CodeResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCodeByGroupCd

> [CodeResource] getCodeByGroupCd(groupCd)

그룹코드 조회

그룹코드로 코드 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CodeApi();
let groupCd = "groupCd_example"; // String | 그룹코드
apiInstance.getCodeByGroupCd(groupCd, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupCd** | **String**| 그룹코드 | 

### Return type

[**[CodeResource]**](CodeResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

