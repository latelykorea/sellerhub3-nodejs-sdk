# SellerhubApi.DeliveryResource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deliveryNumber** | **Number** | 택배사 번호 | [optional] 
**deliveryCompany** | **String** | 택배사명 | [optional] 
**deliveryUrl** | **String** | 택배사 트래킹 주소 | [optional] 
**useYn** | **String** | 사용여부 | [optional] 
**courier** | **String** | Group Code | [optional] 
**trackerId** | **String** | 트래킹 아이디 | [optional] 
**validLength** | **String** | 검증 길이 | [optional] 



## Enum: UseYnEnum


* `Y` (value: `"Y"`)

* `N` (value: `"N"`)

* `unknown_default_open_api` (value: `"unknown_default_open_api"`)




