# SellerhubApi.ExitApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkExitProvider**](ExitApi.md#checkExitProvider) | **GET** /shapi/v1/exit/check | 퇴점 가능여부 확인
[**doExitProvider**](ExitApi.md#doExitProvider) | **POST** /shapi/v1/exit/check | 퇴점신청



## checkExitProvider

> Object checkExitProvider()

퇴점 가능여부 확인

퇴점 가능여부 확인

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ExitApi();
apiInstance.checkExitProvider((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## doExitProvider

> Object doExitProvider(reasons, cancelPhoneNumber)

퇴점신청

퇴점신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ExitApi();
let reasons = {"reason_1":"reason 1","reason_2":"reason 2"}; // Object | 퇴점 사유
let cancelPhoneNumber = 1012345678; // String | 퇴점 안내 받을 연락처
apiInstance.doExitProvider(reasons, cancelPhoneNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reasons** | [**Object**](.md)| 퇴점 사유 | 
 **cancelPhoneNumber** | **String**| 퇴점 안내 받을 연락처 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

