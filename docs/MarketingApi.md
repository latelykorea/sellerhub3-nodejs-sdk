# SellerhubApi.MarketingApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMarketingEvent**](MarketingApi.md#createMarketingEvent) | **POST** /shapi/v1/marketing/event | 행사신청 게시물 등록
[**createMarketingEventProposalRequestByNo**](MarketingApi.md#createMarketingEventProposalRequestByNo) | **POST** /shapi/v1/marketing/eventProposal/request/{eventProposalNumber} | 행사제안 신청
[**createMarketingEventReplyByNo**](MarketingApi.md#createMarketingEventReplyByNo) | **PUT** /shapi/v1/marketing/event/reply/{eventNumber} | 행사신청 게시물 댓글 등록
[**createMarketingPromotioon**](MarketingApi.md#createMarketingPromotioon) | **POST** /shapi/v1/marketing/promotion | 마케팅 프로모션 동의
[**deleteMarketingEventByNo**](MarketingApi.md#deleteMarketingEventByNo) | **DELETE** /shapi/v1/marketing/event/{eventNumber} | 행사신청 게시물 삭제
[**getMarketingEvent**](MarketingApi.md#getMarketingEvent) | **GET** /shapi/v1/marketing/event | 행사신청 목록 조회
[**getMarketingEventApprovalByNo**](MarketingApi.md#getMarketingEventApprovalByNo) | **PUT** /shapi/v1/marketing/event/approval/{eventNumber} | 행사신청 재신청
[**getMarketingEventByNo**](MarketingApi.md#getMarketingEventByNo) | **GET** /shapi/v1/marketing/event/{eventNumber} | 행사신청 게시물
[**getMarketingEventProposal**](MarketingApi.md#getMarketingEventProposal) | **GET** /shapi/v1/marketing/eventProposal | 행사제안 목록 조회
[**getMarketingEventProposalByNo**](MarketingApi.md#getMarketingEventProposalByNo) | **GET** /shapi/v1/marketing/eventProposal/{eventProposalNumber} | 행사제안 내역
[**getMarketingEventProposalRequestByNo**](MarketingApi.md#getMarketingEventProposalRequestByNo) | **GET** /shapi/v1/marketing/eventProposal/request/{eventNumber} | 행사제안 신청내역
[**getMarketingEventReplyByNo**](MarketingApi.md#getMarketingEventReplyByNo) | **GET** /shapi/v1/marketing/event/reply/{eventNumber} | 행사신청 게시물 댓글 목록 조회
[**getMarketingPromotioon**](MarketingApi.md#getMarketingPromotioon) | **GET** /shapi/v1/marketing/promotion | 마케팅 프로모션 리스트
[**updateMarketingEventByNo**](MarketingApi.md#updateMarketingEventByNo) | **PUT** /shapi/v1/marketing/event/{eventNumber} | 행사신청 게시물 등록
[**updateMarketingEventProposalRequestByNo**](MarketingApi.md#updateMarketingEventProposalRequestByNo) | **PUT** /shapi/v1/marketing/eventProposal/request/{eventNumber} | 행사제안 신청내용 재신청



## createMarketingEvent

> Object createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts)

행사신청 게시물 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let returnDeliveryCharge = 56; // Number | 반품배송비(편도)
let exchangeDeliveryCharge = 56; // Number | 교환배송비(왕복)
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.createMarketingEvent(name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **returnDeliveryCharge** | **Number**| 반품배송비(편도) | 
 **exchangeDeliveryCharge** | **Number**| 교환배송비(왕복) | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingEventProposalRequestByNo

> Object createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts)

행사제안 신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.createMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingEventReplyByNo

> Object createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents)

행사신청 게시물 댓글 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let contents = null; // Object | 댓글내용
apiInstance.createMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, contents, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **contents** | [**Object**](Object.md)| 댓글내용 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createMarketingPromotioon

> Object createMarketingPromotioon(opts)

마케팅 프로모션 동의

프로모션 동의

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let opts = {
  'agreeNumber': 56, // Number | 프로모션 번호
  'agreeCheck': "agreeCheck_example" // String | 프로모션 동의 여부
};
apiInstance.createMarketingPromotioon(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agreeNumber** | **Number**| 프로모션 번호 | [optional] 
 **agreeCheck** | **String**| 프로모션 동의 여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteMarketingEventByNo

> Object deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME)

행사신청 게시물 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
apiInstance.deleteMarketingEventByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEvent

> Object getMarketingEvent(opts)

행사신청 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let opts = {
  'isApprove': "'all'", // String | 승인 여부
  'isAnswer': "'all'", // String | 답변 여부
  'searchDateType': "'registed'", // String | 검색 일자 설정
  'startDate': 56, // Number | 시작일
  'endDate': 56, // Number | 종료일
  'searchKey': "'none'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEvent(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isApprove** | **String**| 승인 여부 | [optional] [default to &#39;all&#39;]
 **isAnswer** | **String**| 답변 여부 | [optional] [default to &#39;all&#39;]
 **searchDateType** | **String**| 검색 일자 설정 | [optional] [default to &#39;registed&#39;]
 **startDate** | **Number**| 시작일 | [optional] 
 **endDate** | **Number**| 종료일 | [optional] 
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;none&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventApprovalByNo

> Object getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME)

행사신청 재신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
apiInstance.getMarketingEventApprovalByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventByNo

> Object getMarketingEventByNo(UNKNOWN_PARAMETER_NAME)

행사신청 게시물

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
apiInstance.getMarketingEventByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposal

> Object getMarketingEventProposal(opts)

행사제안 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let opts = {
  'channel': ["null"], // [String] | 행사 채널
  'searchDateType': "'startDate'", // String | 검색 일자 설정
  'startDate': 56, // Number | 시작일
  'endDate': 56, // Number | 종료일
  'category': ["null"], // [String] | 카테고리
  'eventStatus': "'all'", // String | 행사진행여부
  'requestStatus': "'all'", // String | 행사신청여부
  'isApprove': "'all'", // String | 답변여부
  'searchKey': "'title'", // String | 검색키
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEventProposal(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel** | [**[String]**](String.md)| 행사 채널 | [optional] 
 **searchDateType** | **String**| 검색 일자 설정 | [optional] [default to &#39;startDate&#39;]
 **startDate** | **Number**| 시작일 | [optional] 
 **endDate** | **Number**| 종료일 | [optional] 
 **category** | [**[String]**](String.md)| 카테고리 | [optional] 
 **eventStatus** | **String**| 행사진행여부 | [optional] [default to &#39;all&#39;]
 **requestStatus** | **String**| 행사신청여부 | [optional] [default to &#39;all&#39;]
 **isApprove** | **String**| 답변여부 | [optional] [default to &#39;all&#39;]
 **searchKey** | **String**| 검색키 | [optional] [default to &#39;title&#39;]
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposalByNo

> Object getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME)

행사제안 내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
apiInstance.getMarketingEventProposalByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventProposalRequestByNo

> Object getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME)

행사제안 신청내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 게시물 번호
apiInstance.getMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 게시물 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingEventReplyByNo

> Object getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts)

행사신청 게시물 댓글 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let opts = {
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getMarketingEventReplyByNo(UNKNOWN_PARAMETER_NAME, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMarketingPromotioon

> Object getMarketingPromotioon(opts)

마케팅 프로모션 리스트

프로모션목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let opts = {
  'startDate': null, // Date | 시작 일자
  'endDate': null, // Date | 종료 일자
  'searchType': "searchType_example", // String | 검색 옵션
  'searchWord': "searchWord_example", // String | 검색어
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getMarketingPromotioon(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startDate** | [**Date**](.md)| 시작 일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료 일자 | [optional] 
 **searchType** | **String**| 검색 옵션 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateMarketingEventByNo

> Object updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts)

행사신청 게시물 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사신청 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let returnDeliveryCharge = 56; // Number | 반품배송비(편도)
let exchangeDeliveryCharge = 56; // Number | 교환배송비(왕복)
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.updateMarketingEventByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, returnDeliveryCharge, exchangeDeliveryCharge, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사신청 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **returnDeliveryCharge** | **Number**| 반품배송비(편도) | 
 **exchangeDeliveryCharge** | **Number**| 교환배송비(왕복) | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## updateMarketingEventProposalRequestByNo

> Object updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts)

행사제안 신청내용 재신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.MarketingApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 행사제안 신청 게시물 번호
let name = null; // Object | 신청자이름
let email = null; // Object | 신청자이메일
let phoneNumber = null; // Object | 신청자연락처
let title = null; // Object | 행사제목
let contents = null; // Object | 행사내용
let isCoupon = "isCoupon_example"; // String | 쿠폰사용여부
let opts = {
  'couponDiscountRate': 56 // Number | 판매자 부담 쿠폰 할인율(%)
};
apiInstance.updateMarketingEventProposalRequestByNo(UNKNOWN_PARAMETER_NAME, name, email, phoneNumber, title, contents, isCoupon, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 행사제안 신청 게시물 번호 | 
 **name** | [**Object**](Object.md)| 신청자이름 | 
 **email** | [**Object**](Object.md)| 신청자이메일 | 
 **phoneNumber** | [**Object**](Object.md)| 신청자연락처 | 
 **title** | [**Object**](Object.md)| 행사제목 | 
 **contents** | [**Object**](Object.md)| 행사내용 | 
 **isCoupon** | **String**| 쿠폰사용여부 | 
 **couponDiscountRate** | **Number**| 판매자 부담 쿠폰 할인율(%) | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

