# SellerhubApi.CommonApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createToken**](CommonApi.md#createToken) | **POST** /shapi/v1/token | 토큰 생성
[**getAddress**](CommonApi.md#getAddress) | **GET** /shapi/v1/address/search | 주소조회
[**getAlarm**](CommonApi.md#getAlarm) | **GET** /shapi/v1/alarm | 알람 목록 조회
[**getAlarmByNo**](CommonApi.md#getAlarmByNo) | **GET** /shapi/v1/alarm/read/{alarmNumber} | 알람읽기
[**getCode**](CommonApi.md#getCode) | **GET** /shapi/v1/codes | 그룹코드 조회
[**getCodeByGroupCd**](CommonApi.md#getCodeByGroupCd) | **GET** /shapi/v1/codes/{groupCd} | 그룹코드 조회
[**getCsrfToken**](CommonApi.md#getCsrfToken) | **GET** /shapi/v1/csrfToken | CSRF 토큰가져오기
[**getScmUploadFileOfProvider**](CommonApi.md#getScmUploadFileOfProvider) | **GET** /shapi/v1/scm/{model}/{key}/{column} | 공급사의 업로드 파일 (사업자등록증 등) 보기
[**login**](CommonApi.md#login) | **POST** /shapi/v1/login | 로그인
[**logout**](CommonApi.md#logout) | **POST** /shapi/v1/logout | 로그아웃
[**subLogin**](CommonApi.md#subLogin) | **POST** /shapi/v1/subLogin | 로그인



## createToken

> Object createToken(name, opts)

토큰 생성

토큰 생성

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
let name = "name_example"; // String | 토큰명
let opts = {
  'permissions': ["null"] // [String] | 권한
};
apiInstance.createToken(name, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| 토큰명 | 
 **permissions** | [**[String]**](String.md)| 권한 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## getAddress

> Object getAddress(keyword, opts)

주소조회

주소조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
let keyword = "keyword_example"; // String | 검색어
let opts = {
  'currentPage': 56, // Number | 현재 페이지
  'countPerPage': 56, // Number | 페이지당 검색
  'hstryYn': "'N'", // String | 변동된 주소정보 포함 여부
  'firstSort': "'none'", // String | 정확도순 정렬
  'addInfoYn': "'N'" // String | 출력결과에 추가된 항목
};
apiInstance.getAddress(keyword, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keyword** | **String**| 검색어 | 
 **currentPage** | **Number**| 현재 페이지 | [optional] 
 **countPerPage** | **Number**| 페이지당 검색 | [optional] 
 **hstryYn** | **String**| 변동된 주소정보 포함 여부 | [optional] [default to &#39;N&#39;]
 **firstSort** | **String**| 정확도순 정렬 | [optional] [default to &#39;none&#39;]
 **addInfoYn** | **String**| 출력결과에 추가된 항목 | [optional] [default to &#39;N&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAlarm

> Object getAlarm(opts)

알람 목록 조회

알람 목록 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
let opts = {
  'regDateTime': new Date("2013-10-20T19:20:30+01:00"), // Date | 보낸시간
  'alarmType1': "alarmType1_example", // String | 알람 메인타입
  'alarmType2': "alarmType2_example", // String | 알람 타입
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | 시작 시간
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | 종료 시간
  'keyword': "keyword_example", // String | 검색어
  'isRead': "''" // String | 읽음 여부
};
apiInstance.getAlarm(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regDateTime** | **Date**| 보낸시간 | [optional] 
 **alarmType1** | **String**| 알람 메인타입 | [optional] 
 **alarmType2** | **String**| 알람 타입 | [optional] 
 **startDate** | **Date**| 시작 시간 | [optional] 
 **endDate** | **Date**| 종료 시간 | [optional] 
 **keyword** | **String**| 검색어 | [optional] 
 **isRead** | **String**| 읽음 여부 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAlarmByNo

> Object getAlarmByNo(alarmNumber)

알람읽기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
let alarmNumber = 56; // Number | 알람 번호
apiInstance.getAlarmByNo(alarmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **alarmNumber** | **Number**| 알람 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCode

> [CodeResource] getCode()

그룹코드 조회

상위 그룹코드 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
apiInstance.getCode((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[CodeResource]**](CodeResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCodeByGroupCd

> [CodeResource] getCodeByGroupCd(groupCd)

그룹코드 조회

그룹코드로 코드 조회

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
let groupCd = "groupCd_example"; // String | 그룹코드
apiInstance.getCodeByGroupCd(groupCd, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupCd** | **String**| 그룹코드 | 

### Return type

[**[CodeResource]**](CodeResource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCsrfToken

> Object getCsrfToken()

CSRF 토큰가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CommonApi();
apiInstance.getCsrfToken((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getScmUploadFileOfProvider

> getScmUploadFileOfProvider(opts)

공급사의 업로드 파일 (사업자등록증 등) 보기

공급사의 파일관리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CommonApi();
let opts = {
  'model': provider, // String | SCM 모델
  'key': 1634, // Number | SCM 모델의 키
  'column': accountImg // String | SCM 모델의 컬럼명
};
apiInstance.getScmUploadFileOfProvider(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | **String**| SCM 모델 | [optional] 
 **key** | **Number**| SCM 모델의 키 | [optional] 
 **column** | **String**| SCM 모델의 컬럼명 | [optional] 

### Return type

null (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## login

> Object login(opts)

로그인

사용자 로그인

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CommonApi();
let opts = {
  'm_id': "m_id_example", // String | 사용자아이디
  'password': "password_example" // String | 사용자비밀번호
};
apiInstance.login(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **m_id** | **String**| 사용자아이디 | [optional] 
 **password** | **String**| 사용자비밀번호 | [optional] 

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## logout

> Object logout()

로그아웃

Destroy an authenticated session.

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CommonApi();
apiInstance.logout((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## subLogin

> Object subLogin(opts)

로그인

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.CommonApi();
let opts = {
  'sadmid': "sadmid_example", // String | 사용자아이디
  'password': "password_example" // String | 사용자비밀번호
};
apiInstance.subLogin(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sadmid** | **String**| 사용자아이디 | [optional] 
 **password** | **String**| 사용자비밀번호 | [optional] 

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

