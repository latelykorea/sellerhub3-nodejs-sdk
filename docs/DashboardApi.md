# SellerhubApi.DashboardApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clearDashboardCache**](DashboardApi.md#clearDashboardCache) | **GET** /shapi/v1/dashboard/cacheClear | 대시보드 캐시삭제
[**getDashboardBanner**](DashboardApi.md#getDashboardBanner) | **GET** /shapi/v1/dashboard/banner | 하단 배너
[**getDashboardExchangeStatus**](DashboardApi.md#getDashboardExchangeStatus) | **GET** /shapi/v1/dashboard/exchangeStatus | 교환 현황
[**getDashboardLinkageStatus**](DashboardApi.md#getDashboardLinkageStatus) | **GET** /shapi/v1/dashboard/linkageStatus | 연동현황
[**getDashboardOrderStat**](DashboardApi.md#getDashboardOrderStat) | **GET** /shapi/v1/dashboard/orderStat | 매출 현황
[**getDashboardPenaltyStatus**](DashboardApi.md#getDashboardPenaltyStatus) | **GET** /shapi/v1/dashboard/penaltyStatus | 패널티 현황
[**getDashboardPopularProducts**](DashboardApi.md#getDashboardPopularProducts) | **GET** /shapi/v1/dashboard/popularProducts | 인기상품 현황
[**getDashboardProductState**](DashboardApi.md#getDashboardProductState) | **GET** /shapi/v1/dashboard/productState | 상품 현황
[**getDashboardProductViolation**](DashboardApi.md#getDashboardProductViolation) | **GET** /shapi/v1/dashboard/productViolation | 상품정보고시 위반 현황
[**getDashboardSalesStatus**](DashboardApi.md#getDashboardSalesStatus) | **GET** /shapi/v1/dashboard/salesStatus | 판매 현황



## clearDashboardCache

> Object clearDashboardCache()

대시보드 캐시삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
apiInstance.clearDashboardCache((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardBanner

> Object getDashboardBanner()

하단 배너

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
apiInstance.getDashboardBanner((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardExchangeStatus

> Object getDashboardExchangeStatus(opts)

교환 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardExchangeStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardLinkageStatus

> Object getDashboardLinkageStatus(opts)

연동현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardLinkageStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardOrderStat

> Object getDashboardOrderStat(opts)

매출 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardOrderStat(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardPenaltyStatus

> Object getDashboardPenaltyStatus(opts)

패널티 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardPenaltyStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardPopularProducts

> [PopularProduct] getDashboardPopularProducts(opts)

인기상품 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardPopularProducts(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

[**[PopularProduct]**](PopularProduct.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardProductState

> Object getDashboardProductState(opts)

상품 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardProductState(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardProductViolation

> Object getDashboardProductViolation(opts)

상품정보고시 위반 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardProductViolation(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getDashboardSalesStatus

> Object getDashboardSalesStatus(opts)

판매 현황

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.DashboardApi();
let opts = {
  'forceReflush': true // Boolean | 캐시 삭제
};
apiInstance.getDashboardSalesStatus(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forceReflush** | **Boolean**| 캐시 삭제 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

