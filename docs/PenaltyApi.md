# SellerhubApi.PenaltyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPenaltyObjectionByNo**](PenaltyApi.md#createPenaltyObjectionByNo) | **POST** /shapi/v1/penalty/{penaltySeq}/objections | 패널티 이이제기
[**getPenalty**](PenaltyApi.md#getPenalty) | **GET** /shapi/v1/penalty | 패널티 목록



## createPenaltyObjectionByNo

> Object createPenaltyObjectionByNo(penaltySeq, opts)

패널티 이이제기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.PenaltyApi();
let penaltySeq = 56; // Number | 패널티 번호
let opts = {
  'contents': "contents_example" // String | 이이제기 사유
};
apiInstance.createPenaltyObjectionByNo(penaltySeq, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **penaltySeq** | **Number**| 패널티 번호 | 
 **contents** | **String**| 이이제기 사유 | [optional] 

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## getPenalty

> Object getPenalty(opts)

패널티 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure API key authorization: sanctum
let sanctum = defaultClient.authentications['sanctum'];
sanctum.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//sanctum.apiKeyPrefix = 'Token';

let apiInstance = new SellerhubApi.PenaltyApi();
let opts = {
  'status': "''", // String | 상태
  'penaltyType': "''" // String | 패널티 타입
};
apiInstance.getPenalty(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| 상태 | [optional] [default to &#39;&#39;]
 **penaltyType** | **String**| 패널티 타입 | [optional] [default to &#39;&#39;]

### Return type

**Object**

### Authorization

[sanctum](../README.md#sanctum)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

