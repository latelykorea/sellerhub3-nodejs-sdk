# SellerhubApi.UpdateProviderBrandBrandDatasParameterInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandName** | **String** | 브랜드명 | [optional] 
**brandNumber** | **Number** | 브랜드 번호 | [optional] 


