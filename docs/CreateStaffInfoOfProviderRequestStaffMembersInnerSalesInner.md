# SellerhubApi.CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | 담당자명 | 
**email** | **String** | 담당자 이메일 | 
**tel** | **String** | 담당자 전화번호 | 
**phone** | **String** | 담당자 휴대폰번호 | 


