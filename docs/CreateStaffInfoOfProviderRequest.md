# SellerhubApi.CreateStaffInfoOfProviderRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**staffMembers** | [**[CreateStaffInfoOfProviderRequestStaffMembersInner]**](CreateStaffInfoOfProviderRequestStaffMembersInner.md) | 담당자 | [optional] 
**seller** | [**[CreateStaffInfoOfProviderRequestSellerInner]**](CreateStaffInfoOfProviderRequestSellerInner.md) | 판매자 | [optional] 


