# SellerhubApi.CsQnaApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCsQna**](CsQnaApi.md#getCsQna) | **GET** /shapi/v1/csQna | cs 리스트
[**getCsQnaByNo**](CsQnaApi.md#getCsQnaByNo) | **GET** /shapi/v1/csQna/{csNumber} | CS문의 상세내역 조회
[**updateCsQna**](CsQnaApi.md#updateCsQna) | **PUT** /shapi/v1/csQna/{csNumber} | CS 답변 등록



## getCsQna

> Object getCsQna(opts)

cs 리스트

cs목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CsQnaApi();
let opts = {
  'dateType': "dateType_example", // String | 기간검색 옵션
  'startDate': null, // Date | 시작 일자
  'endDate': null, // Date | 종료 일자
  'status': 56, // Number | 처리상태
  'inquireType': "inquireType_example", // String | 문의유형
  'searchType': "searchType_example", // String | 상세검색 옵션
  'searchWord': "searchWord_example", // String | 검색어
  'sortBy': "sortBy_example", // String | 정렬 옵션
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지 개수
};
apiInstance.getCsQna(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dateType** | **String**| 기간검색 옵션 | [optional] 
 **startDate** | [**Date**](.md)| 시작 일자 | [optional] 
 **endDate** | [**Date**](.md)| 종료 일자 | [optional] 
 **status** | **Number**| 처리상태 | [optional] 
 **inquireType** | **String**| 문의유형 | [optional] 
 **searchType** | **String**| 상세검색 옵션 | [optional] 
 **searchWord** | **String**| 검색어 | [optional] 
 **sortBy** | **String**| 정렬 옵션 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getCsQnaByNo

> Object getCsQnaByNo(csNumber)

CS문의 상세내역 조회

CS문의 상세내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CsQnaApi();
let csNumber = 56; // Number | cs문의 번호
apiInstance.getCsQnaByNo(csNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **csNumber** | **Number**| cs문의 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## updateCsQna

> Object updateCsQna(csNumber)

CS 답변 등록

CS 답변 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.CsQnaApi();
let csNumber = 56; // Number | cs문의 번호
apiInstance.updateCsQna(csNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **csNumber** | **Number**| cs문의 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

