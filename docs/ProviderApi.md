# SellerhubApi.ProviderApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkExitProvider**](ProviderApi.md#checkExitProvider) | **GET** /shapi/v1/exit/check | 퇴점 가능여부 확인
[**createDeliveryNotice**](ProviderApi.md#createDeliveryNotice) | **POST** /shapi/v1/manages/deliveryNotice | 기본관리 - 배송공지 등록
[**createDetailInfoOfProvider**](ProviderApi.md#createDetailInfoOfProvider) | **POST** /shapi/v1/manages/detailInfo | 기본관리 - 입점사 상세정보 관리
[**createProviderBrand**](ProviderApi.md#createProviderBrand) | **POST** /shapi/v1/brands/provider | 공급사 브랜드 추가하기
[**createSellerPlanOfProvider**](ProviderApi.md#createSellerPlanOfProvider) | **POST** /shapi/v1/manages/sellerPlan | 기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청
[**createStaffInfoOfProvider**](ProviderApi.md#createStaffInfoOfProvider) | **POST** /shapi/v1/manages/staff | 기본관리 - 담당자 정보 설정
[**deleteDeliveryNotice**](ProviderApi.md#deleteDeliveryNotice) | **DELETE** /shapi/v1/manages/deliveryNotice | 기본관리 - 배송공지 삭제
[**deleteProviderBrandByNo**](ProviderApi.md#deleteProviderBrandByNo) | **DELETE** /shapi/v1/brands/provider/{brandNumber} | 공급사 브랜드 삭제하기
[**doExitProvider**](ProviderApi.md#doExitProvider) | **POST** /shapi/v1/exit/check | 퇴점신청
[**getAvailablePaymentMethods**](ProviderApi.md#getAvailablePaymentMethods) | **GET** /shapi/v1/paymentMethods | 결제 수단 등록 목록
[**getPaymentHistoryById**](ProviderApi.md#getPaymentHistoryById) | **GET** /shapi/v1/payments/{id} | 결제 내역
[**getPaymentMethodById**](ProviderApi.md#getPaymentMethodById) | **GET** /shapi/v1/paymentMethods/{id} | 결제 수단 등록 내용
[**getPaymentMethodHistory**](ProviderApi.md#getPaymentMethodHistory) | **GET** /shapi/v1/manages/paymentMethodHistory | 결제방법 히스토리
[**getProviderBrand**](ProviderApi.md#getProviderBrand) | **GET** /shapi/v1/brands/provider | 공급사 브랜드 목록 가져오기
[**getProviderBrandByNo**](ProviderApi.md#getProviderBrandByNo) | **GET** /shapi/v1/brands/provider/{$brandNumber} | 공급사 특정 브랜드 가져오기
[**getScmUploadFileOfProvider**](ProviderApi.md#getScmUploadFileOfProvider) | **GET** /shapi/v1/scm/{model}/{key}/{column} | 공급사의 업로드 파일 (사업자등록증 등) 보기
[**geyPaymentHistory**](ProviderApi.md#geyPaymentHistory) | **GET** /shapi/v1/payments | 결제 내역 목록
[**pauseProvider**](ProviderApi.md#pauseProvider) | **POST** /shapi/v1/pause | 계약 일시정지
[**registPaymentMethod**](ProviderApi.md#registPaymentMethod) | **POST** /shapi/v1/paymentMethods | 결제 수단 등록
[**updateProviderBrand**](ProviderApi.md#updateProviderBrand) | **PUT** /shapi/v1/brands/provider | 공급사 브랜드 변경하기



## checkExitProvider

> Object checkExitProvider()

퇴점 가능여부 확인

퇴점 가능여부 확인

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
apiInstance.checkExitProvider((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createDeliveryNotice

> Object createDeliveryNotice(image, expiredAt)

기본관리 - 배송공지 등록

기본관리 - 배송공지 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let image = "image_example"; // String | 배송 공지 이미지 base64
let expiredAt = new Date("2013-10-20"); // Date | 만기일자
apiInstance.createDeliveryNotice(image, expiredAt, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image** | **String**| 배송 공지 이미지 base64 | 
 **expiredAt** | **Date**| 만기일자 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## createDetailInfoOfProvider

> Object createDetailInfoOfProvider(CreateDetailInfoOfProviderRequest)

기본관리 - 입점사 상세정보 관리

기본관리 - 입점사 상세정보 관리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let CreateDetailInfoOfProviderRequest = new SellerhubApi.CreateDetailInfoOfProviderRequest(); // CreateDetailInfoOfProviderRequest | 
apiInstance.createDetailInfoOfProvider(CreateDetailInfoOfProviderRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **CreateDetailInfoOfProviderRequest** | [**CreateDetailInfoOfProviderRequest**](CreateDetailInfoOfProviderRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## createProviderBrand

> Object createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2)

공급사 브랜드 추가하기

공급사 브랜드 추가하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드명
let UNKNOWN_PARAMETER_NAME2 = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.createProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드명 | 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createSellerPlanOfProvider

> Object createSellerPlanOfProvider(opts)

기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청

기본관리 - 계약정보 및 플랜 - 셀러 플랜 신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let opts = {
  'planCode': "planCode_example", // String | 셀러허브 카테고리 코드
  'doPlanUpgrade': true // Boolean | 플랜 업그레이드 진행여부
};
apiInstance.createSellerPlanOfProvider(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **planCode** | **String**| 셀러허브 카테고리 코드 | [optional] 
 **doPlanUpgrade** | **Boolean**| 플랜 업그레이드 진행여부 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## createStaffInfoOfProvider

> Object createStaffInfoOfProvider(CreateStaffInfoOfProviderRequest)

기본관리 - 담당자 정보 설정

기본관리 - 담당자 정보 설정

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let CreateStaffInfoOfProviderRequest = new SellerhubApi.CreateStaffInfoOfProviderRequest(); // CreateStaffInfoOfProviderRequest | 
apiInstance.createStaffInfoOfProvider(CreateStaffInfoOfProviderRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **CreateStaffInfoOfProviderRequest** | [**CreateStaffInfoOfProviderRequest**](CreateStaffInfoOfProviderRequest.md)|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## deleteDeliveryNotice

> Object deleteDeliveryNotice()

기본관리 - 배송공지 삭제

기본관리 - 배송공지 삭제

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
apiInstance.deleteDeliveryNotice((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## deleteProviderBrandByNo

> Object deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME)

공급사 브랜드 삭제하기

공급사 브랜드 삭제하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.deleteProviderBrandByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## doExitProvider

> Object doExitProvider(reasons, cancelPhoneNumber)

퇴점신청

퇴점신청

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let reasons = {"reason_1":"reason 1","reason_2":"reason 2"}; // Object | 퇴점 사유
let cancelPhoneNumber = 1012345678; // String | 퇴점 안내 받을 연락처
apiInstance.doExitProvider(reasons, cancelPhoneNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reasons** | [**Object**](.md)| 퇴점 사유 | 
 **cancelPhoneNumber** | **String**| 퇴점 안내 받을 연락처 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getAvailablePaymentMethods

> Object getAvailablePaymentMethods()

결제 수단 등록 목록

결제 수단 등록 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
apiInstance.getAvailablePaymentMethods((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPaymentHistoryById

> Object getPaymentHistoryById(opts)

결제 내역

결제 내역

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let opts = {
  'id': 1 // Number | 결제 내역 고유 ID
};
apiInstance.getPaymentHistoryById(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| 결제 내역 고유 ID | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPaymentMethodById

> Object getPaymentMethodById()

결제 수단 등록 내용

결제 수단 등록 내용

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
apiInstance.getPaymentMethodById((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPaymentMethodHistory

> Object getPaymentMethodHistory()

결제방법 히스토리

결제방법 변경 이력

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
apiInstance.getPaymentMethodHistory((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderBrand

> Object getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts)

공급사 브랜드 목록 가져오기

공급사 브랜드 목록 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
let UNKNOWN_PARAMETER_NAME2 = new SellerhubApi.null(); //  | 브랜드명
let opts = {
  'sortBy': "sortBy_example", // String | 정렬 타입
  'sortDirection': "sortDirection_example", // String | 정렬 방향
  'page': 56, // Number | 페이지 번호
  'pageSize': 56 // Number | 페이지당 노출 개수
};
apiInstance.getProviderBrand(UNKNOWN_PARAMETER_NAME, UNKNOWN_PARAMETER_NAME2, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 
 **UNKNOWN_PARAMETER_NAME2** | [****](.md)| 브랜드명 | 
 **sortBy** | **String**| 정렬 타입 | [optional] 
 **sortDirection** | **String**| 정렬 방향 | [optional] 
 **page** | **Number**| 페이지 번호 | [optional] 
 **pageSize** | **Number**| 페이지당 노출 개수 | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getProviderBrandByNo

> Object getProviderBrandByNo(UNKNOWN_PARAMETER_NAME)

공급사 특정 브랜드 가져오기

공급사 특정 브랜드 가져오기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let UNKNOWN_PARAMETER_NAME = new SellerhubApi.null(); //  | 브랜드 번호
apiInstance.getProviderBrandByNo(UNKNOWN_PARAMETER_NAME, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **UNKNOWN_PARAMETER_NAME** | [****](.md)| 브랜드 번호 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getScmUploadFileOfProvider

> getScmUploadFileOfProvider(opts)

공급사의 업로드 파일 (사업자등록증 등) 보기

공급사의 파일관리

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let opts = {
  'model': provider, // String | SCM 모델
  'key': 1634, // Number | SCM 모델의 키
  'column': accountImg // String | SCM 모델의 컬럼명
};
apiInstance.getScmUploadFileOfProvider(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | **String**| SCM 모델 | [optional] 
 **key** | **Number**| SCM 모델의 키 | [optional] 
 **column** | **String**| SCM 모델의 컬럼명 | [optional] 

### Return type

null (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## geyPaymentHistory

> Object geyPaymentHistory(opts)

결제 내역 목록

결제 내역 목록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let opts = {
  'body': {key: null} // Object | 
};
apiInstance.geyPaymentHistory(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | [optional] 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## pauseProvider

> Object pauseProvider()

계약 일시정지

계약 일시정지

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
apiInstance.pauseProvider((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## registPaymentMethod

> Object registPaymentMethod(body)

결제 수단 등록

결제 수단 등록

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let body = {key: null}; // Object | 
apiInstance.registPaymentMethod(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **Object**|  | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## updateProviderBrand

> Object updateProviderBrand(brandDatas)

공급사 브랜드 변경하기

공급사 브랜드 변경하기

### Example

```javascript
import SellerhubApi from 'sellerhub_api';
let defaultClient = SellerhubApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: oauth2
let oauth2 = defaultClient.authentications['oauth2'];
oauth2.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new SellerhubApi.ProviderApi();
let brandDatas = [new SellerhubApi.UpdateProviderBrandBrandDatasParameterInner()]; // [UpdateProviderBrandBrandDatasParameterInner] | 브랜드 데이터
apiInstance.updateProviderBrand(brandDatas, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **brandDatas** | [**[UpdateProviderBrandBrandDatasParameterInner]**](UpdateProviderBrandBrandDatasParameterInner.md)| 브랜드 데이터 | 

### Return type

**Object**

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

