/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UrgentMessageAlarm model module.
 * @module model/UrgentMessageAlarm
 * @version 0.0.1
 */
class UrgentMessageAlarm {
    /**
     * Constructs a new <code>UrgentMessageAlarm</code>.
     * @alias module:model/UrgentMessageAlarm
     */
    constructor() { 
        
        UrgentMessageAlarm.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>UrgentMessageAlarm</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UrgentMessageAlarm} obj Optional instance to populate.
     * @return {module:model/UrgentMessageAlarm} The populated <code>UrgentMessageAlarm</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UrgentMessageAlarm();

            if (data.hasOwnProperty('writerNumber')) {
                obj['writerNumber'] = ApiClient.convertToType(data['writerNumber'], 'Number');
            }
            if (data.hasOwnProperty('writerName')) {
                obj['writerName'] = ApiClient.convertToType(data['writerName'], 'String');
            }
            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
            if (data.hasOwnProperty('answer')) {
                obj['answer'] = ApiClient.convertToType(data['answer'], 'String');
            }
            if (data.hasOwnProperty('createdAt')) {
                obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
            }
            if (data.hasOwnProperty('answeredAt')) {
                obj['answeredAt'] = ApiClient.convertToType(data['answeredAt'], 'Date');
            }
        }
        return obj;
    }


}

/**
 * 작성자 번호
 * @member {Number} writerNumber
 */
UrgentMessageAlarm.prototype['writerNumber'] = undefined;

/**
 * 작성자명
 * @member {String} writerName
 */
UrgentMessageAlarm.prototype['writerName'] = undefined;

/**
 * 알람 내용
 * @member {String} message
 */
UrgentMessageAlarm.prototype['message'] = undefined;

/**
 * 답변
 * @member {String} answer
 */
UrgentMessageAlarm.prototype['answer'] = undefined;

/**
 * 알람 등록일
 * @member {Date} createdAt
 */
UrgentMessageAlarm.prototype['createdAt'] = undefined;

/**
 * 답변일
 * @member {Date} answeredAt
 */
UrgentMessageAlarm.prototype['answeredAt'] = undefined;






export default UrgentMessageAlarm;

