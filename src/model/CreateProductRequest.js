/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import CreateProductRequestCertificationDatasInnerInner from './CreateProductRequestCertificationDatasInnerInner';
import CreateProductRequestOptionsInnerInner from './CreateProductRequestOptionsInnerInner';
import CreateProductRequestProductImageDatasInnerInner from './CreateProductRequestProductImageDatasInnerInner';
import CreateProductRequestProductInformationInner from './CreateProductRequestProductInformationInner';

/**
 * The CreateProductRequest model module.
 * @module model/CreateProductRequest
 * @version 0.0.1
 */
class CreateProductRequest {
    /**
     * Constructs a new <code>CreateProductRequest</code>.
     * @alias module:model/CreateProductRequest
     * @param categoryCode {String} 카테고리 코드
     * @param brandNumber {Number} 브랜드 번호
     * @param productName {String} 상품명
     * @param productPrice {Number} 상품 판매 가격
     * @param productConsumerPrice {Number} 소비자 가격
     * @param optionTitles {Array.<String>} 상품 옵션 제목
     * @param options {Array.<Array.<module:model/CreateProductRequestOptionsInnerInner>>} 상품 옵션
     * @param productImageDatas {Array.<Array.<module:model/CreateProductRequestProductImageDatasInnerInner>>} 상품 이미지
     * @param description {String} 상품 상세 내용
     * @param productInformation {Array.<module:model/CreateProductRequestProductInformationInner>} 상품 정보 고시
     */
    constructor(categoryCode, brandNumber, productName, productPrice, productConsumerPrice, optionTitles, options, productImageDatas, description, productInformation) { 
        
        CreateProductRequest.initialize(this, categoryCode, brandNumber, productName, productPrice, productConsumerPrice, optionTitles, options, productImageDatas, description, productInformation);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, categoryCode, brandNumber, productName, productPrice, productConsumerPrice, optionTitles, options, productImageDatas, description, productInformation) { 
        obj['categoryCode'] = categoryCode;
        obj['brandNumber'] = brandNumber;
        obj['productName'] = productName;
        obj['productPrice'] = productPrice;
        obj['productConsumerPrice'] = productConsumerPrice;
        obj['optionTitles'] = optionTitles;
        obj['options'] = options;
        obj['productImageDatas'] = productImageDatas;
        obj['description'] = description;
        obj['productInformation'] = productInformation;
    }

    /**
     * Constructs a <code>CreateProductRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequest} obj Optional instance to populate.
     * @return {module:model/CreateProductRequest} The populated <code>CreateProductRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateProductRequest();

            if (data.hasOwnProperty('categoryCode')) {
                obj['categoryCode'] = ApiClient.convertToType(data['categoryCode'], 'String');
            }
            if (data.hasOwnProperty('brandNumber')) {
                obj['brandNumber'] = ApiClient.convertToType(data['brandNumber'], 'Number');
            }
            if (data.hasOwnProperty('maker')) {
                obj['maker'] = ApiClient.convertToType(data['maker'], 'String');
            }
            if (data.hasOwnProperty('origin')) {
                obj['origin'] = ApiClient.convertToType(data['origin'], 'String');
            }
            if (data.hasOwnProperty('productCode')) {
                obj['productCode'] = ApiClient.convertToType(data['productCode'], 'String');
            }
            if (data.hasOwnProperty('productName')) {
                obj['productName'] = ApiClient.convertToType(data['productName'], 'String');
            }
            if (data.hasOwnProperty('keywords')) {
                obj['keywords'] = ApiClient.convertToType(data['keywords'], ['String']);
            }
            if (data.hasOwnProperty('isUsed')) {
                obj['isUsed'] = ApiClient.convertToType(data['isUsed'], 'String');
            }
            if (data.hasOwnProperty('isSellerDeal')) {
                obj['isSellerDeal'] = ApiClient.convertToType(data['isSellerDeal'], 'String');
            }
            if (data.hasOwnProperty('isTemporary')) {
                obj['isTemporary'] = ApiClient.convertToType(data['isTemporary'], 'String');
            }
            if (data.hasOwnProperty('deliveryType')) {
                obj['deliveryType'] = ApiClient.convertToType(data['deliveryType'], 'String');
            }
            if (data.hasOwnProperty('deliveryPrice')) {
                obj['deliveryPrice'] = ApiClient.convertToType(data['deliveryPrice'], 'Number');
            }
            if (data.hasOwnProperty('isManufactured')) {
                obj['isManufactured'] = ApiClient.convertToType(data['isManufactured'], 'String');
            }
            if (data.hasOwnProperty('manufacturingPeriod')) {
                obj['manufacturingPeriod'] = ApiClient.convertToType(data['manufacturingPeriod'], 'String');
            }
            if (data.hasOwnProperty('isReservationDelivery')) {
                obj['isReservationDelivery'] = ApiClient.convertToType(data['isReservationDelivery'], 'String');
            }
            if (data.hasOwnProperty('reservationDeliveryDate')) {
                obj['reservationDeliveryDate'] = ApiClient.convertToType(data['reservationDeliveryDate'], 'String');
            }
            if (data.hasOwnProperty('isOverseasDelivery')) {
                obj['isOverseasDelivery'] = ApiClient.convertToType(data['isOverseasDelivery'], 'String');
            }
            if (data.hasOwnProperty('overseasDeliveryPeriod')) {
                obj['overseasDeliveryPeriod'] = ApiClient.convertToType(data['overseasDeliveryPeriod'], 'String');
            }
            if (data.hasOwnProperty('productPrice')) {
                obj['productPrice'] = ApiClient.convertToType(data['productPrice'], 'Number');
            }
            if (data.hasOwnProperty('productSupplyPrice')) {
                obj['productSupplyPrice'] = ApiClient.convertToType(data['productSupplyPrice'], 'Number');
            }
            if (data.hasOwnProperty('productConsumerPrice')) {
                obj['productConsumerPrice'] = ApiClient.convertToType(data['productConsumerPrice'], 'Number');
            }
            if (data.hasOwnProperty('productAbidePrice')) {
                obj['productAbidePrice'] = ApiClient.convertToType(data['productAbidePrice'], 'Number');
            }
            if (data.hasOwnProperty('productWolesalePrice')) {
                obj['productWolesalePrice'] = ApiClient.convertToType(data['productWolesalePrice'], 'Number');
            }
            if (data.hasOwnProperty('isUsingStock')) {
                obj['isUsingStock'] = ApiClient.convertToType(data['isUsingStock'], 'String');
            }
            if (data.hasOwnProperty('isTaxable')) {
                obj['isTaxable'] = ApiClient.convertToType(data['isTaxable'], 'String');
            }
            if (data.hasOwnProperty('optionTitles')) {
                obj['optionTitles'] = ApiClient.convertToType(data['optionTitles'], ['String']);
            }
            if (data.hasOwnProperty('optionType')) {
                obj['optionType'] = ApiClient.convertToType(data['optionType'], 'String');
            }
            if (data.hasOwnProperty('options')) {
                obj['options'] = ApiClient.convertToType(data['options'], [[CreateProductRequestOptionsInnerInner]]);
            }
            if (data.hasOwnProperty('isRunouted')) {
                obj['isRunouted'] = ApiClient.convertToType(data['isRunouted'], 'String');
            }
            if (data.hasOwnProperty('productImageDatas')) {
                obj['productImageDatas'] = ApiClient.convertToType(data['productImageDatas'], [[CreateProductRequestProductImageDatasInnerInner]]);
            }
            if (data.hasOwnProperty('description')) {
                obj['description'] = ApiClient.convertToType(data['description'], 'String');
            }
            if (data.hasOwnProperty('kcCertificationType')) {
                obj['kcCertificationType'] = ApiClient.convertToType(data['kcCertificationType'], 'String');
            }
            if (data.hasOwnProperty('kcCertificationInfo')) {
                obj['kcCertificationInfo'] = ApiClient.convertToType(data['kcCertificationInfo'], 'String');
            }
            if (data.hasOwnProperty('ecoCertificationType')) {
                obj['ecoCertificationType'] = ApiClient.convertToType(data['ecoCertificationType'], 'String');
            }
            if (data.hasOwnProperty('ecoCertificationAuth')) {
                obj['ecoCertificationAuth'] = ApiClient.convertToType(data['ecoCertificationAuth'], 'String');
            }
            if (data.hasOwnProperty('ecoCertificationCode')) {
                obj['ecoCertificationCode'] = ApiClient.convertToType(data['ecoCertificationCode'], 'String');
            }
            if (data.hasOwnProperty('ecoCertificationMark')) {
                obj['ecoCertificationMark'] = ApiClient.convertToType(data['ecoCertificationMark'], 'String');
            }
            if (data.hasOwnProperty('certificationDatas')) {
                obj['certificationDatas'] = ApiClient.convertToType(data['certificationDatas'], [[CreateProductRequestCertificationDatasInnerInner]]);
            }
            if (data.hasOwnProperty('productInformation')) {
                obj['productInformation'] = ApiClient.convertToType(data['productInformation'], [CreateProductRequestProductInformationInner]);
            }
            if (data.hasOwnProperty('manageMemo')) {
                obj['manageMemo'] = ApiClient.convertToType(data['manageMemo'], 'String');
            }
            if (data.hasOwnProperty('productMemo')) {
                obj['productMemo'] = ApiClient.convertToType(data['productMemo'], 'String');
            }
        }
        return obj;
    }


}

/**
 * 카테고리 코드
 * @member {String} categoryCode
 */
CreateProductRequest.prototype['categoryCode'] = undefined;

/**
 * 브랜드 번호
 * @member {Number} brandNumber
 */
CreateProductRequest.prototype['brandNumber'] = undefined;

/**
 * 메이커
 * @member {String} maker
 */
CreateProductRequest.prototype['maker'] = undefined;

/**
 * 원산지
 * @member {String} origin
 */
CreateProductRequest.prototype['origin'] = undefined;

/**
 * 브랜드 번호
 * @member {String} productCode
 */
CreateProductRequest.prototype['productCode'] = undefined;

/**
 * 상품명
 * @member {String} productName
 */
CreateProductRequest.prototype['productName'] = undefined;

/**
 * 키워드
 * @member {Array.<String>} keywords
 */
CreateProductRequest.prototype['keywords'] = undefined;

/**
 * 중고여부
 * @member {module:model/CreateProductRequest.IsUsedEnum} isUsed
 */
CreateProductRequest.prototype['isUsed'] = undefined;

/**
 * 셀러딜 상품여부
 * @member {module:model/CreateProductRequest.IsSellerDealEnum} isSellerDeal
 */
CreateProductRequest.prototype['isSellerDeal'] = undefined;

/**
 * 임시상품 여부
 * @member {module:model/CreateProductRequest.IsTemporaryEnum} isTemporary
 */
CreateProductRequest.prototype['isTemporary'] = undefined;

/**
 * 배송 유형
 * @member {module:model/CreateProductRequest.DeliveryTypeEnum} deliveryType
 */
CreateProductRequest.prototype['deliveryType'] = undefined;

/**
 * 배송비
 * @member {Number} deliveryPrice
 */
CreateProductRequest.prototype['deliveryPrice'] = undefined;

/**
 * 제조 상품 여부
 * @member {module:model/CreateProductRequest.IsManufacturedEnum} isManufactured
 */
CreateProductRequest.prototype['isManufactured'] = undefined;

/**
 * 제조기간
 * @member {String} manufacturingPeriod
 */
CreateProductRequest.prototype['manufacturingPeriod'] = undefined;

/**
 * 예약 상품 여부
 * @member {module:model/CreateProductRequest.IsReservationDeliveryEnum} isReservationDelivery
 */
CreateProductRequest.prototype['isReservationDelivery'] = undefined;

/**
 * 예약발송일자
 * @member {String} reservationDeliveryDate
 */
CreateProductRequest.prototype['reservationDeliveryDate'] = undefined;

/**
 * 해외 상품 여부
 * @member {module:model/CreateProductRequest.IsOverseasDeliveryEnum} isOverseasDelivery
 */
CreateProductRequest.prototype['isOverseasDelivery'] = undefined;

/**
 * 해외배송기간
 * @member {String} overseasDeliveryPeriod
 */
CreateProductRequest.prototype['overseasDeliveryPeriod'] = undefined;

/**
 * 상품 판매 가격
 * @member {Number} productPrice
 */
CreateProductRequest.prototype['productPrice'] = undefined;

/**
 * 상품 공급 가격
 * @member {Number} productSupplyPrice
 */
CreateProductRequest.prototype['productSupplyPrice'] = undefined;

/**
 * 소비자 가격
 * @member {Number} productConsumerPrice
 */
CreateProductRequest.prototype['productConsumerPrice'] = undefined;

/**
 * 준수 판매 가격
 * @member {Number} productAbidePrice
 */
CreateProductRequest.prototype['productAbidePrice'] = undefined;

/**
 * 도매 가격
 * @member {Number} productWolesalePrice
 */
CreateProductRequest.prototype['productWolesalePrice'] = undefined;

/**
 * 재고량 연동 여부
 * @member {module:model/CreateProductRequest.IsUsingStockEnum} isUsingStock
 */
CreateProductRequest.prototype['isUsingStock'] = undefined;

/**
 * 과세 여부
 * @member {module:model/CreateProductRequest.IsTaxableEnum} isTaxable
 */
CreateProductRequest.prototype['isTaxable'] = undefined;

/**
 * 상품 옵션 제목
 * @member {Array.<String>} optionTitles
 */
CreateProductRequest.prototype['optionTitles'] = undefined;

/**
 * 옵션 유형
 * @member {module:model/CreateProductRequest.OptionTypeEnum} optionType
 */
CreateProductRequest.prototype['optionType'] = undefined;

/**
 * 상품 옵션
 * @member {Array.<Array.<module:model/CreateProductRequestOptionsInnerInner>>} options
 */
CreateProductRequest.prototype['options'] = undefined;

/**
 * 품절 여부
 * @member {module:model/CreateProductRequest.IsRunoutedEnum} isRunouted
 */
CreateProductRequest.prototype['isRunouted'] = undefined;

/**
 * 상품 이미지
 * @member {Array.<Array.<module:model/CreateProductRequestProductImageDatasInnerInner>>} productImageDatas
 */
CreateProductRequest.prototype['productImageDatas'] = undefined;

/**
 * 상품 상세 내용
 * @member {String} description
 */
CreateProductRequest.prototype['description'] = undefined;

/**
 * KC 인증 유형
 * @member {module:model/CreateProductRequest.KcCertificationTypeEnum} kcCertificationType
 */
CreateProductRequest.prototype['kcCertificationType'] = undefined;

/**
 * KC 인증 정보
 * @member {String} kcCertificationInfo
 */
CreateProductRequest.prototype['kcCertificationInfo'] = undefined;

/**
 * 친환경 인증 유형
 * @member {module:model/CreateProductRequest.EcoCertificationTypeEnum} ecoCertificationType
 */
CreateProductRequest.prototype['ecoCertificationType'] = undefined;

/**
 * 친환경 인증 기관
 * @member {String} ecoCertificationAuth
 */
CreateProductRequest.prototype['ecoCertificationAuth'] = undefined;

/**
 * 친환경 인증 번호
 * @member {String} ecoCertificationCode
 */
CreateProductRequest.prototype['ecoCertificationCode'] = undefined;

/**
 * 친환경 인증 마크 사용여부
 * @member {module:model/CreateProductRequest.EcoCertificationMarkEnum} ecoCertificationMark
 */
CreateProductRequest.prototype['ecoCertificationMark'] = undefined;

/**
 * 상품 인증자료 첨부
 * @member {Array.<Array.<module:model/CreateProductRequestCertificationDatasInnerInner>>} certificationDatas
 */
CreateProductRequest.prototype['certificationDatas'] = undefined;

/**
 * 상품 정보 고시
 * @member {Array.<module:model/CreateProductRequestProductInformationInner>} productInformation
 */
CreateProductRequest.prototype['productInformation'] = undefined;

/**
 * 관리 메모
 * @member {String} manageMemo
 */
CreateProductRequest.prototype['manageMemo'] = undefined;

/**
 * 상품 메모
 * @member {String} productMemo
 */
CreateProductRequest.prototype['productMemo'] = undefined;





/**
 * Allowed values for the <code>isUsed</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsUsedEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isSellerDeal</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsSellerDealEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isTemporary</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsTemporaryEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>deliveryType</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['DeliveryTypeEnum'] = {

    /**
     * value: "wait"
     * @const
     */
    "wait": "wait",

    /**
     * value: "request"
     * @const
     */
    "request": "request",

    /**
     * value: "approved"
     * @const
     */
    "approved": "approved",

    /**
     * value: "hold"
     * @const
     */
    "hold": "hold",

    /**
     * value: "reject"
     * @const
     */
    "reject": "reject",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isManufactured</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsManufacturedEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isReservationDelivery</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsReservationDeliveryEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isOverseasDelivery</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsOverseasDeliveryEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isUsingStock</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsUsingStockEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isTaxable</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsTaxableEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>optionType</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['OptionTypeEnum'] = {

    /**
     * value: "single"
     * @const
     */
    "single": "single",

    /**
     * value: "double"
     * @const
     */
    "double": "double",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isRunouted</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['IsRunoutedEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>kcCertificationType</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['KcCertificationTypeEnum'] = {

    /**
     * value: "해당없음"
     * @const
     */
    "해당없음": "해당없음",

    /**
     * value: "상세설명참조"
     * @const
     */
    "상세설명참조": "상세설명참조",

    /**
     * value: "[생활용품]안전인증"
     * @const
     */
    "[생활용품]안전인증": "[생활용품]안전인증",

    /**
     * value: "[생활용품]안전확인"
     * @const
     */
    "[생활용품]안전확인": "[생활용품]안전확인",

    /**
     * value: "[생활용품]공급자적합성확인"
     * @const
     */
    "[생활용품]공급자적합성확인": "[생활용품]공급자적합성확인",

    /**
     * value: "[생활용품]어린이보호포장"
     * @const
     */
    "[생활용품]어린이보호포장": "[생활용품]어린이보호포장",

    /**
     * value: "[어린이제품]안전인증"
     * @const
     */
    "[어린이제품]안전인증": "[어린이제품]안전인증",

    /**
     * value: "[어린이제품]안전확인"
     * @const
     */
    "[어린이제품]안전확인": "[어린이제품]안전확인",

    /**
     * value: "[어린이제품]공급자적합성확인"
     * @const
     */
    "[어린이제품]공급자적합성확인": "[어린이제품]공급자적합성확인",

    /**
     * value: "[전기용품]안전인증"
     * @const
     */
    "[전기용품]안전인증": "[전기용품]안전인증",

    /**
     * value: "[전기용품]안전확인"
     * @const
     */
    "[전기용품]안전확인": "[전기용품]안전확인",

    /**
     * value: "[전기용품]공급자적합성확인"
     * @const
     */
    "[전기용품]공급자적합성확인": "[전기용품]공급자적합성확인",

    /**
     * value: "[방송통신기자재]적합성평가"
     * @const
     */
    "[방송통신기자재]적합성평가": "[방송통신기자재]적합성평가",

    /**
     * value: "[위해우려제품]자가검사번호"
     * @const
     */
    "[위해우려제품]자가검사번호": "[위해우려제품]자가검사번호",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>ecoCertificationType</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['EcoCertificationTypeEnum'] = {

    /**
     * value: "[친환경]유기축산물"
     * @const
     */
    "유기축산물": "[친환경]유기축산물",

    /**
     * value: "[친환경]유기농산물"
     * @const
     */
    "유기농산물": "[친환경]유기농산물",

    /**
     * value: "[친환경]무항생제축산물"
     * @const
     */
    "무항생제축산물": "[친환경]무항생제축산물",

    /**
     * value: "[친환경]무농약농산물"
     * @const
     */
    "무농약농산물": "[친환경]무농약농산물",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>ecoCertificationMark</code> property.
 * @enum {String}
 * @readonly
 */
CreateProductRequest['EcoCertificationMarkEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};



export default CreateProductRequest;

