/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner from './CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner';

/**
 * The CreateStaffInfoOfProviderRequestStaffMembersInner model module.
 * @module model/CreateStaffInfoOfProviderRequestStaffMembersInner
 * @version 0.0.1
 */
class CreateStaffInfoOfProviderRequestStaffMembersInner {
    /**
     * Constructs a new <code>CreateStaffInfoOfProviderRequestStaffMembersInner</code>.
     * @alias module:model/CreateStaffInfoOfProviderRequestStaffMembersInner
     */
    constructor() { 
        
        CreateStaffInfoOfProviderRequestStaffMembersInner.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>CreateStaffInfoOfProviderRequestStaffMembersInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStaffInfoOfProviderRequestStaffMembersInner} obj Optional instance to populate.
     * @return {module:model/CreateStaffInfoOfProviderRequestStaffMembersInner} The populated <code>CreateStaffInfoOfProviderRequestStaffMembersInner</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateStaffInfoOfProviderRequestStaffMembersInner();

            if (data.hasOwnProperty('sales')) {
                obj['sales'] = ApiClient.convertToType(data['sales'], [CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]);
            }
            if (data.hasOwnProperty('cs')) {
                obj['cs'] = ApiClient.convertToType(data['cs'], [CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]);
            }
            if (data.hasOwnProperty('delivery')) {
                obj['delivery'] = ApiClient.convertToType(data['delivery'], [CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]);
            }
            if (data.hasOwnProperty('settle')) {
                obj['settle'] = ApiClient.convertToType(data['settle'], [CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner]);
            }
        }
        return obj;
    }


}

/**
 * 영업 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} sales
 */
CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['sales'] = undefined;

/**
 * CS 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} cs
 */
CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['cs'] = undefined;

/**
 * 배송 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} delivery
 */
CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['delivery'] = undefined;

/**
 * 정산 담당자
 * @member {Array.<module:model/CreateStaffInfoOfProviderRequestStaffMembersInnerSalesInner>} settle
 */
CreateStaffInfoOfProviderRequestStaffMembersInner.prototype['settle'] = undefined;






export default CreateStaffInfoOfProviderRequestStaffMembersInner;

