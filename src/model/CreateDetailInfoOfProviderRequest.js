/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import CreateDetailInfoOfProviderRequestBrandDatasInnerInner from './CreateDetailInfoOfProviderRequestBrandDatasInnerInner';

/**
 * The CreateDetailInfoOfProviderRequest model module.
 * @module model/CreateDetailInfoOfProviderRequest
 * @version 0.0.1
 */
class CreateDetailInfoOfProviderRequest {
    /**
     * Constructs a new <code>CreateDetailInfoOfProviderRequest</code>.
     * @alias module:model/CreateDetailInfoOfProviderRequest
     */
    constructor() { 
        
        CreateDetailInfoOfProviderRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>CreateDetailInfoOfProviderRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateDetailInfoOfProviderRequest} obj Optional instance to populate.
     * @return {module:model/CreateDetailInfoOfProviderRequest} The populated <code>CreateDetailInfoOfProviderRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateDetailInfoOfProviderRequest();

            if (data.hasOwnProperty('bankName')) {
                obj['bankName'] = ApiClient.convertToType(data['bankName'], 'String');
            }
            if (data.hasOwnProperty('account')) {
                obj['account'] = ApiClient.convertToType(data['account'], 'String');
            }
            if (data.hasOwnProperty('accountHolder')) {
                obj['accountHolder'] = ApiClient.convertToType(data['accountHolder'], 'String');
            }
            if (data.hasOwnProperty('bankbook')) {
                obj['bankbook'] = ApiClient.convertToType(data['bankbook'], File);
            }
            if (data.hasOwnProperty('businessCondition')) {
                obj['businessCondition'] = ApiClient.convertToType(data['businessCondition'], 'String');
            }
            if (data.hasOwnProperty('businessType')) {
                obj['businessType'] = ApiClient.convertToType(data['businessType'], 'String');
            }
            if (data.hasOwnProperty('taxPayerType')) {
                obj['taxPayerType'] = ApiClient.convertToType(data['taxPayerType'], 'String');
            }
            if (data.hasOwnProperty('zipCode')) {
                obj['zipCode'] = ApiClient.convertToType(data['zipCode'], 'String');
            }
            if (data.hasOwnProperty('address')) {
                obj['address'] = ApiClient.convertToType(data['address'], 'String');
            }
            if (data.hasOwnProperty('detailAddress')) {
                obj['detailAddress'] = ApiClient.convertToType(data['detailAddress'], 'String');
            }
            if (data.hasOwnProperty('businessLicense')) {
                obj['businessLicense'] = ApiClient.convertToType(data['businessLicense'], File);
            }
            if (data.hasOwnProperty('isAutoDiscounthoppingMall')) {
                obj['isAutoDiscounthoppingMall'] = ApiClient.convertToType(data['isAutoDiscounthoppingMall'], 'String');
            }
            if (data.hasOwnProperty('isAutoExposureNaverShopping')) {
                obj['isAutoExposureNaverShopping'] = ApiClient.convertToType(data['isAutoExposureNaverShopping'], 'String');
            }
            if (data.hasOwnProperty('brandDatas')) {
                obj['brandDatas'] = ApiClient.convertToType(data['brandDatas'], [[CreateDetailInfoOfProviderRequestBrandDatasInnerInner]]);
            }
            if (data.hasOwnProperty('categoryCodes')) {
                obj['categoryCodes'] = ApiClient.convertToType(data['categoryCodes'], ['String']);
            }
            if (data.hasOwnProperty('companyLogo')) {
                obj['companyLogo'] = ApiClient.convertToType(data['companyLogo'], File);
            }
        }
        return obj;
    }


}

/**
 * 은행명
 * @member {String} bankName
 */
CreateDetailInfoOfProviderRequest.prototype['bankName'] = undefined;

/**
 * 예금주
 * @member {String} account
 */
CreateDetailInfoOfProviderRequest.prototype['account'] = undefined;

/**
 * 계좌번호
 * @member {String} accountHolder
 */
CreateDetailInfoOfProviderRequest.prototype['accountHolder'] = undefined;

/**
 * 통장사본
 * @member {File} bankbook
 */
CreateDetailInfoOfProviderRequest.prototype['bankbook'] = undefined;

/**
 * 업태
 * @member {String} businessCondition
 */
CreateDetailInfoOfProviderRequest.prototype['businessCondition'] = undefined;

/**
 * 업종
 * @member {String} businessType
 */
CreateDetailInfoOfProviderRequest.prototype['businessType'] = undefined;

/**
 * 과세 타입
 * @member {module:model/CreateDetailInfoOfProviderRequest.TaxPayerTypeEnum} taxPayerType
 */
CreateDetailInfoOfProviderRequest.prototype['taxPayerType'] = undefined;

/**
 * 우편번호
 * @member {String} zipCode
 */
CreateDetailInfoOfProviderRequest.prototype['zipCode'] = undefined;

/**
 * 주소
 * @member {String} address
 */
CreateDetailInfoOfProviderRequest.prototype['address'] = undefined;

/**
 * 상세주소
 * @member {String} detailAddress
 */
CreateDetailInfoOfProviderRequest.prototype['detailAddress'] = undefined;

/**
 * 사업자 등록증 사본
 * @member {File} businessLicense
 */
CreateDetailInfoOfProviderRequest.prototype['businessLicense'] = undefined;

/**
 * 자동 할인율 적용여부
 * @member {module:model/CreateDetailInfoOfProviderRequest.IsAutoDiscounthoppingMallEnum} isAutoDiscounthoppingMall
 */
CreateDetailInfoOfProviderRequest.prototype['isAutoDiscounthoppingMall'] = undefined;

/**
 * 자동 네이버 지식쇼핑 노출여부
 * @member {module:model/CreateDetailInfoOfProviderRequest.IsAutoExposureNaverShoppingEnum} isAutoExposureNaverShopping
 */
CreateDetailInfoOfProviderRequest.prototype['isAutoExposureNaverShopping'] = undefined;

/**
 * 브랜드 데이터
 * @member {Array.<Array.<module:model/CreateDetailInfoOfProviderRequestBrandDatasInnerInner>>} brandDatas
 */
CreateDetailInfoOfProviderRequest.prototype['brandDatas'] = undefined;

/**
 * 카테고리 코드
 * @member {Array.<String>} categoryCodes
 */
CreateDetailInfoOfProviderRequest.prototype['categoryCodes'] = undefined;

/**
 * 회사 대표 이미지/로고
 * @member {File} companyLogo
 */
CreateDetailInfoOfProviderRequest.prototype['companyLogo'] = undefined;





/**
 * Allowed values for the <code>taxPayerType</code> property.
 * @enum {String}
 * @readonly
 */
CreateDetailInfoOfProviderRequest['TaxPayerTypeEnum'] = {

    /**
     * value: "general"
     * @const
     */
    "general": "general",

    /**
     * value: "simplified"
     * @const
     */
    "simplified": "simplified",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isAutoDiscounthoppingMall</code> property.
 * @enum {String}
 * @readonly
 */
CreateDetailInfoOfProviderRequest['IsAutoDiscounthoppingMallEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};


/**
 * Allowed values for the <code>isAutoExposureNaverShopping</code> property.
 * @enum {String}
 * @readonly
 */
CreateDetailInfoOfProviderRequest['IsAutoExposureNaverShoppingEnum'] = {

    /**
     * value: "Y"
     * @const
     */
    "Y": "Y",

    /**
     * value: "N"
     * @const
     */
    "N": "N",

    /**
     * value: "unknown_default_open_api"
     * @const
     */
    "unknown_default_open_api": "unknown_default_open_api"
};



export default CreateDetailInfoOfProviderRequest;

