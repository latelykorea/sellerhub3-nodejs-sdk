/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CreateProductRequestProductInformationInner model module.
 * @module model/CreateProductRequestProductInformationInner
 * @version 0.0.1
 */
class CreateProductRequestProductInformationInner {
    /**
     * Constructs a new <code>CreateProductRequestProductInformationInner</code>.
     * @alias module:model/CreateProductRequestProductInformationInner
     * @param goodsRequired {String} 
     */
    constructor(goodsRequired) { 
        
        CreateProductRequestProductInformationInner.initialize(this, goodsRequired);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, goodsRequired) { 
        obj['goodsRequired'] = goodsRequired;
    }

    /**
     * Constructs a <code>CreateProductRequestProductInformationInner</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateProductRequestProductInformationInner} obj Optional instance to populate.
     * @return {module:model/CreateProductRequestProductInformationInner} The populated <code>CreateProductRequestProductInformationInner</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateProductRequestProductInformationInner();

            if (data.hasOwnProperty('goodsRequired')) {
                obj['goodsRequired'] = ApiClient.convertToType(data['goodsRequired'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} goodsRequired
 */
CreateProductRequestProductInformationInner.prototype['goodsRequired'] = undefined;






export default CreateProductRequestProductInformationInner;

