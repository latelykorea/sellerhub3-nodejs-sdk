/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CodeResource model module.
 * @module model/CodeResource
 * @version 0.0.1
 */
class CodeResource {
    /**
     * Constructs a new <code>CodeResource</code>.
     * @alias module:model/CodeResource
     */
    constructor() { 
        
        CodeResource.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>CodeResource</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CodeResource} obj Optional instance to populate.
     * @return {module:model/CodeResource} The populated <code>CodeResource</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CodeResource();

            if (data.hasOwnProperty('groupCode')) {
                obj['groupCode'] = ApiClient.convertToType(data['groupCode'], 'String');
            }
            if (data.hasOwnProperty('itemCode')) {
                obj['itemCode'] = ApiClient.convertToType(data['itemCode'], 'String');
            }
            if (data.hasOwnProperty('itemName')) {
                obj['itemName'] = ApiClient.convertToType(data['itemName'], 'String');
            }
        }
        return obj;
    }


}

/**
 * Group Code
 * @member {String} groupCode
 */
CodeResource.prototype['groupCode'] = undefined;

/**
 * Group Code
 * @member {String} itemCode
 */
CodeResource.prototype['itemCode'] = undefined;

/**
 * Group Code
 * @member {String} itemName
 */
CodeResource.prototype['itemName'] = undefined;






export default CodeResource;

