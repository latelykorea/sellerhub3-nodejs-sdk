/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";

/**
* Login service.
* @module api/LoginApi
* @version 0.0.1
*/
export default class LoginApi {

    /**
    * Constructs a new LoginApi. 
    * @alias module:api/LoginApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the login operation.
     * @callback module:api/LoginApi~loginCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그인
     * 사용자 로그인
     * @param {Object} opts Optional parameters
     * @param {String} opts.m_id 사용자아이디
     * @param {String} opts.password 사용자비밀번호
     * @param {module:api/LoginApi~loginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    login(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
        'm_id': opts['m_id'],
        'password': opts['password']
      };

      let authNames = ['sanctum'];
      let contentTypes = ['multipart/form-data'];
      let accepts = ['application/json'];
      let returnType = Object;
      return this.apiClient.callApi(
        '/shapi/v1/login', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the logout operation.
     * @callback module:api/LoginApi~logoutCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그아웃
     * Destroy an authenticated session.
     * @param {module:api/LoginApi~logoutCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    logout(callback) {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['sanctum'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Object;
      return this.apiClient.callApi(
        '/shapi/v1/logout', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the subLogin operation.
     * @callback module:api/LoginApi~subLoginCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 로그인
     * @param {Object} opts Optional parameters
     * @param {String} opts.sadmid 사용자아이디
     * @param {String} opts.password 사용자비밀번호
     * @param {module:api/LoginApi~subLoginCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    subLogin(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
        'sadmid': opts['sadmid'],
        'password': opts['password']
      };

      let authNames = ['sanctum'];
      let contentTypes = ['multipart/form-data'];
      let accepts = ['application/json'];
      let returnType = Object;
      return this.apiClient.callApi(
        '/shapi/v1/subLogin', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
