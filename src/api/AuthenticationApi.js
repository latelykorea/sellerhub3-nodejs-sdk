/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import AttemptMobileAuthRequest from '../model/AttemptMobileAuthRequest';
import ConfirmMobileAuthRequest from '../model/ConfirmMobileAuthRequest';

/**
* Authentication service.
* @module api/AuthenticationApi
* @version 0.0.1
*/
export default class AuthenticationApi {

    /**
    * Constructs a new AuthenticationApi. 
    * @alias module:api/AuthenticationApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the attemptMobileAuth operation.
     * @callback module:api/AuthenticationApi~attemptMobileAuthCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴대폰 인증 번호 요청
     * 휴대폰 인증 번호 요청
     * @param {module:model/AttemptMobileAuthRequest} AttemptMobileAuthRequest 
     * @param {module:api/AuthenticationApi~attemptMobileAuthCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    attemptMobileAuth(AttemptMobileAuthRequest, callback) {
      let postBody = AttemptMobileAuthRequest;
      // verify the required parameter 'AttemptMobileAuthRequest' is set
      if (AttemptMobileAuthRequest === undefined || AttemptMobileAuthRequest === null) {
        throw new Error("Missing the required parameter 'AttemptMobileAuthRequest' when calling attemptMobileAuth");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oauth2'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Object;
      return this.apiClient.callApi(
        '/shapi/v1/mobileAuth/attempt', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the confirmMobileAuth operation.
     * @callback module:api/AuthenticationApi~confirmMobileAuthCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 휴대폰 인증 번호 검증
     * 휴대폰 인증 번호 검증
     * @param {module:model/ConfirmMobileAuthRequest} ConfirmMobileAuthRequest 
     * @param {module:api/AuthenticationApi~confirmMobileAuthCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    confirmMobileAuth(ConfirmMobileAuthRequest, callback) {
      let postBody = ConfirmMobileAuthRequest;
      // verify the required parameter 'ConfirmMobileAuthRequest' is set
      if (ConfirmMobileAuthRequest === undefined || ConfirmMobileAuthRequest === null) {
        throw new Error("Missing the required parameter 'ConfirmMobileAuthRequest' when calling confirmMobileAuth");
      }

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['oauth2'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = Object;
      return this.apiClient.callApi(
        '/shapi/v1/mobileAuth/confirm', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
