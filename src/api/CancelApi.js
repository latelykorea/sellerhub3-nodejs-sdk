/**
 * Sellerhub API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";

/**
* Cancel service.
* @module api/CancelApi
* @version 0.0.1
*/
export default class CancelApi {

    /**
    * Constructs a new CancelApi. 
    * @alias module:api/CancelApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the getCancelOrder operation.
     * @callback module:api/CancelApi~getCancelOrderCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * 주문취소 리스트
     * @param {Object} opts Optional parameters
     * @param {module:model/String} opts.skey 주문검색 (옵션) (default to '')
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME 주문검색 (키워드)
     * @param {module:model/null} opts.UNKNOWN_PARAMETER_NAME2 주문일 (키워드)
     * @param {module:model/String} opts.cancelType 주문상태 (default to '')
     * @param {module:model/String} opts.settlekind 결제방법 (default to '')
     * @param {Number} opts.page 페이지 번호
     * @param {Number} opts.pageSize 페이지 개수
     * @param {module:api/CancelApi~getCancelOrderCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    getCancelOrder(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
        'skey': opts['skey'],
        'sword': opts['UNKNOWN_PARAMETER_NAME'],
        'regdt': opts['UNKNOWN_PARAMETER_NAME2'],
        'cancelType': opts['cancelType'],
        'settlekind': opts['settlekind'],
        'page': opts['page'],
        'pageSize': opts['pageSize']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearer'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Object;
      return this.apiClient.callApi(
        '/shapi/v1/order/list/cancel', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
